//
//  ExploreSavedSignalsCollectionLayout.swift
//  More
//
//  Created by Luko Gjenero on 14/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

private let itemSpace = CGFloat(100)

class ExploreSavedSignalsCollectionLayout: UICollectionViewFlowLayout {

    enum RemoveDirection {
        case left, right
    }
    
    var removeDirection: RemoveDirection = .left
    
    override var collectionViewContentSize: CGSize {
        guard let collectionView = collectionView else {
            return .zero
        }
        
        return CGSize(width: collectionView.bounds.width, height: itemSpace * CGFloat(collectionView.numberOfItems(inSection: 0)))
    }

    private func updateAttributes(_ attributes: UICollectionViewLayoutAttributes) {
        guard let collectionView = collectionView else {
            return
        }
        
        let start = collectionView.contentInset.top
        var frame = collectionView.bounds
        frame.origin.y = start + itemSpace * CGFloat(attributes.indexPath.item)
        
        attributes.frame = frame

        // rotation
        let angleOfRotation = CGFloat(-61.0)
        let rotation = CATransform3DMakeRotation(angleOfRotation.toRadians(), 1.0, 0.0, 0.0)
        
        // perspective
        let depth = CGFloat(300)
        let translateDown = CATransform3DMakeTranslation(0.0, 0.0, -depth)
        let translateUp = CATransform3DMakeTranslation(0.0, 0.0, depth)
        var scale = CATransform3DIdentity
        scale.m34 = CGFloat( -1.0 / 1500.0)
        let perspective =  CATransform3DConcat(CATransform3DConcat(translateDown, scale), translateUp)
        
        // move up
        let moveUp = CATransform3DMakeTranslation(0.0, -frame.width * 0.5 * sin(angleOfRotation) * 1.2, 0.0)
        
        // final transform
        let transform = CATransform3DConcat(CATransform3DConcat(rotation, perspective), moveUp)
        
        attributes.transform3D = transform
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        
        guard let collectionView = collectionView else {
            return attributes
        }
        
        guard collectionView.numberOfItems(inSection: 0) > 0 else {
            return attributes
        }
        
        let start = collectionView.contentInset.top
        
        var startIdx = Int((rect.minY - start) / itemSpace)
        var endIdx = Int(ceil((rect.maxY - start) / itemSpace))
        
        startIdx = min(collectionView.numberOfItems(inSection: 0) - 1, max(0, startIdx))
        endIdx = min(collectionView.numberOfItems(inSection: 0) - 1, max(0, endIdx))
        
        var updatedAttributes: [UICollectionViewLayoutAttributes] = []
        for idx in startIdx...endIdx {
            if let attrs = layoutAttributesForItem(at: IndexPath(item: idx, section: 0)) {
                updatedAttributes.append(attrs)
            }
        }
        return updatedAttributes
    }
    
    override open func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        updateAttributes(attributes)
        return attributes
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let itemAttributes = layoutAttributesForItem(at: itemIndexPath)
        if let attrs = itemAttributes {
            let offset = attrs.frame.width * (removeDirection == . left ? -2 : 2)
            let translate = CATransform3DConcat(CATransform3DMakeTranslation(offset, 0, 0), attrs.transform3D)
            itemAttributes?.transform3D = translate
        }
        return itemAttributes
    }
    
}
