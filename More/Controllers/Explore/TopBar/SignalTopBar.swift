//
//  SignalTopBar.swift
//  More
//
//  Created by Luko Gjenero on 09/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class SignalTopBar: LoadableView {

    @IBOutlet weak var avatars: AvatarStackView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var stars: StarsView!
    @IBOutlet weak var starsLabel: UILabel!
    
    override func setupNib() {
        super.setupNib()
        
        stars.padding = 6
    }
    
    func setup(for model: SignalViewModel) {
        
        avatars.setup(for: [model.creator.avatarUrl])
        
        label.text = model.mine ? "You" : model.creator.name
        
        stars.isHidden = false
        if model.creator.numOfReviews > 0 {
            stars.noStar = UIImage(named: "no_star")
            stars.rate = CGFloat(model.creator.avgReview)
        } else {
            stars.noStar = UIImage(named: "star_outline_2")
            stars.rate = 0
        }
        
        starsLabel.text = "\(model.creator.numOfReviews)"
    }
    
    func setupEmpty() {
        
        avatars.setupForEmpty()
        
        label.text = ""
        
        stars.isHidden = false
        stars.noStar = UIImage(named: "star_outline_2")
        stars.rate = 0
        
        starsLabel.text = ""
    }
    
    func setupForMore(title: String) {
        avatars.setupForMore()
        
        label.text = title
        
        stars.isHidden = true
        
        starsLabel.text = ""
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        avatars.setup(for: ["https://www.pexels.com/photo/face-facial-hair-fine-looking-guy-614810/"])
        
        label.text = "John Doe"
        
        stars.rate = 3.6
        
        starsLabel.text = "16"
    }

}
