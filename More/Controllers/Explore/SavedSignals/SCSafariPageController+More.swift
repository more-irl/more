//
//  SCSafariPageController+More.swift
//  More
//
//  Created by Luko Gjenero on 12/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SCSafariPageController
import SCScrollView



extension SCSafariZoomedOutLayouter {
    
    private static var customSet = false
    
    class func moreCustom() {
        guard !customSet else { return }
        
        // tuning the transform
        swizzle(originalSelector: #selector(SCSafariZoomedOutLayouter.sublayerTransformForPage(withFrame:totalNumberOfPages:contentOffset:)), swizzledSelector: #selector(More_sublayerTransformForPage(withFrame:totalNumberOfPages:contentOffset:)))
        
        // tuning the size
        swizzle(originalSelector: #selector(SCSafariZoomedOutLayouter.finalFrameForPage(at:pageViewController:)), swizzledSelector: #selector(More_finalFrameForPage(at:pageViewController:)))
        
        // tuning the contentInset
        swizzle(originalSelector: #selector(SCSafariZoomedOutLayouter.contentInset(for:)), swizzledSelector: #selector(More_contentInset(for:)))
        
        // tuning the nteritme spacing
        swizzle(originalSelector: #selector(SCSafariZoomedOutLayouter.interItemSpacing(for:totalNumberOfPages:)), swizzledSelector: #selector(More_interItemSpacing(for:totalNumberOfPages:)))
        
        customSet = true
    }
    
    private class func swizzle(originalSelector: Selector, swizzledSelector: Selector) {
        let forClass: AnyClass = SCSafariZoomedOutLayouter.self
        guard let originalMethod = class_getInstanceMethod(forClass, originalSelector),
            let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
            else { return }
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
    
    @objc func More_sublayerTransformForPage(withFrame frame: CGRect, totalNumberOfPages numberOfPages:CGFloat, contentOffset:CGPoint) -> CATransform3D {
        
        // rotation
        let angleOfRotation = CGFloat(-61.0)
        let rotation = CATransform3DMakeRotation(angleOfRotation.toRadians(), 1.0, 0.0, 0.0)
        
        // perspective
        let depth = CGFloat(300)
        let translateDown = CATransform3DMakeTranslation(0.0, 0.0, -depth)
        let translateUp = CATransform3DMakeTranslation(0.0, 0.0, depth)
        var scale = CATransform3DIdentity
        scale.m34 = CGFloat( -1.0 / 1500.0)
        let perspective = CATransform3DConcat(CATransform3DConcat(translateDown, scale), translateUp)
        
        // final transform
        let transform = CATransform3DConcat(rotation, perspective)
        
        // reposition
        let result = frame.applying(CATransform3DGetAffineTransform(transform))
        let moveUp = CATransform3DMakeTranslation(0, (result.height - frame.height) / 2, 0)
        
        // return transform
        return CATransform3DConcat(transform, moveUp)
    }
    
    @objc func More_finalFrameForPage(at index: UInt,  pageViewController:SCPageViewController) -> CGRect {
        var frame = pageViewController.view.bounds
        
        frame.origin.y = CGFloat(index) * 100 + 50
        frame.origin.x = 0
        
        return frame
    }
    
    @objc func More_contentInset(for pageViewController: SCPageViewController) -> UIEdgeInsets {
        return UIEdgeInsetsMake(50, 0, 0, 0)
    }
    
    @objc func More_interItemSpacing(for pageViewController:SCPageViewController, totalNumberOfPages numberOfPages:CGFloat) -> CGFloat {
        return 0
    }
    
}


extension SCPageViewController {
    
    private static var customSet = false
    
    class func moreCustom() {
        guard !customSet else { return }
        
        // tuning the update
        swizzle(originalSelector: #selector(SCPageViewController._updateBoundsAndConstraints), swizzledSelector: #selector(More__updateBoundsAndConstraints))
        
        // tuning the
        swizzle(originalSelector: #selector(SCPageViewController._center(onPageIndex:)), swizzledSelector: #selector(More__center(onPageIndex:)))
        
        customSet = true
    }
    
    private class func swizzle(originalSelector: Selector, swizzledSelector: Selector) {
        let forClass: AnyClass = SCPageViewController.self
        guard let originalMethod = class_getInstanceMethod(forClass, originalSelector),
            let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
            else { return }
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
    
    private func getScrollView() -> SCScrollView? {
        for view in view.subviews {
            for view in view.subviews {
                if let scrollView = view as? SCScrollView {
                    return scrollView
                }
            }
        }
        return nil
    }
    
    @objc func More__updateBoundsAndConstraints() {
        More__updateBoundsAndConstraints()
        
        guard let scroll = getScrollView() else { return }
        scroll.alwaysBounceVertical = true
        
        let pagesNum = numberOfPages > 0 ? numberOfPages - 1 : 0
        let frame = layouter.finalFrameForPage(at: pagesNum, pageViewController: self)
        if layouter.navigationType == .vertical {
            let contentHeight = frame.minY + frame.height * cos(CGFloat(61).toRadians())
            scroll.contentSize = CGSize(width: scroll.bounds.width, height: contentHeight)
        } else {
            let contentWidth = frame.maxX
            scroll.contentSize = CGSize(width: contentWidth, height: scroll.bounds.height)
        }
    }
    
    @objc func More__center(onPageIndex: UInt) {
        // nothing
    }
        
        
}

extension SCSafariPageWrapperViewController {
    
    private static var customSet = false
    
    class func moreCustom() {
        guard !customSet else { return }
        
        // tuning the translation anchor point
        swizzle(originalSelector: #selector(SCSafariPageWrapperViewController.viewDidLoad), swizzledSelector: #selector(More_viewDidLoad))
        
        customSet = true
    }
    
    private class func swizzle(originalSelector: Selector, swizzledSelector: Selector) {
        let forClass: AnyClass = SCSafariPageWrapperViewController.self
        guard let originalMethod = class_getInstanceMethod(forClass, originalSelector),
            let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
            else { return }
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
    
    @objc func More_viewDidLoad() {
        More_viewDidLoad()
        
        /*
        // shadow
        let shadow = UIImageView(image: UIImage.gradientImage(
            size: CGSize(width: 20, height: 20),
            start: CGPoint(x: 0, y: 0),
            end: CGPoint(x: 0, y: 20),
            startColor: UIColor.black.withAlphaComponent(0.2),
            endColor: UIColor.black.withAlphaComponent(0.0)))
        
        shadow.backgroundColor = .clear
        shadow.frame = CGRect(x: 0, y: -18, width: UIScreen.main.bounds.width, height: 20)
        /*
        shadow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(shadow)
        shadow.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        shadow.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        shadow.bottomAnchor.constraint(equalTo: view.topAnchor).isActive = true
        shadow.heightAnchor.constraint(equalToConstant: 20)
        */
 
        view.layer.addSublayer(shadow.layer)
        */
        
        /*
        view.layer.shadowPath = UIBezierPath(roundedRect: UIScreen.main.bounds.insetBy(dx: 32, dy: 32), cornerRadius: 16).cgPath
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: -10)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 10
        view.layer.masksToBounds = false
        */
    }
}
