//
//  TimesEmptyView.swift
//  More
//
//  Created by Luko Gjenero on 14/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class TimesEmptyView: LoadableView {
    
    enum EmptyType {
        case past, present
    }
    
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subtitle: UILabel!
    @IBOutlet weak var whiteBar: UIView!
    
    var emptyType: EmptyType = .past {
        didSet {
            setup(for: emptyType)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        whiteBar.enableShadow(color: .black, path: UIBezierPath(rect: CGRect(x: 0, y: 0, width: bounds.width, height: 20)).cgPath)
    }
    
    private func setup(for type: EmptyType) {
        switch type {
        case .past:
            title.text = "YOU HAVEN'T GONE ON A TIME YET!"
            subtitle.text = "Once you go on a Time, come back here to reminisce.\n\nThis is where the memory of all your unforgettable More moments will live."
        case .present:
            title.text = "KEEP TRACK OF YOUR TIMES"
            subtitle.text = "When you put up a Signal of a Time you'd like to have, come here to see who wants to join."
        }
    }

}
