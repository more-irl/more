//
//  UIViewController+Present.swift
//  More
//
//  Created by Luko Gjenero on 10/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentUser(_ userId: String) {
        
        self.showLoading()
        ProfileService.shared.loadProfile(withId: userId) { [weak self] (profile, _) in
            self?.hideLoading()
            if let profile = profile {
                
                let vc = ProfileDetailsNavigationController()
                _ = vc.view
                vc.back = { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
                }
                vc.setup(for: UserViewModel(profile: profile))
                self?.present(vc, animated: true, completion: nil)
                
                /*
                let model = UserViewModel(profile: profile)
                let vc = ProfileViewController()
                _ = vc.view
                vc.bottomPadding = 0
                vc.backTap = {
                    self?.dismiss(animated: true, completion: nil)
                }
                vc.moreTap = {
                    self?.report(model) { (reported) in
                        if reported {
                            self?.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                vc.setup(for: model)
                
                let nc = UINavigationController(rootViewController: vc)
                nc.setNavigationBarHidden(true, animated: false)
                self?.present(nc, animated: true, completion: nil)
                */
            }
        }
    }
    
    func presentSignal(_ signalId: String) {
        self.showLoading()
        SignalingService.shared.loadSignal(withId: signalId) { [weak self] (signal, _) in
            self?.hideLoading()
            if let signal = signal {
                let model = SignalViewModel(signal: signal)
                let vc = SignalDetailsViewController()
                _ = vc.view
                vc.bottomPadding = 0
                vc.backTap = {
                    self?.dismiss(animated: true, completion: nil)
                }
                vc.moreTap = {
                    self?.report(model) { (reported) in
                        if reported {
                            self?.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                vc.setup(for: model)
                
                let nc = UINavigationController(rootViewController: vc)
                nc.setNavigationBarHidden(true, animated: false)
                self?.present(nc, animated: true, completion: nil)
            }
        }
    }

}
