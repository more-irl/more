//
//  SettingsViewController.swift
//  More
//
//  Created by Luko Gjenero on 13/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet private weak var content: UIView!
    @IBOutlet private weak var version: UILabel!
    
    var closeTap: (()->())?
    var inviteTap: (()->())?
    var supportTap: (()->())?
    var termsTap: (()->())?
    var privacyTap: (()->())?
    var logoutTap: (()->())?
    var deleteTap: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(dragContent(sender:)))
        view.addGestureRecognizer(pan)
        
        let major = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "?"
        let minor = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "?"
        
        version.text = "v\(major) (\(minor))"
        
        prepareForEnter()
    }
    
    private var isFirst = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        enter()
    }

    @IBAction private func closeTouch(_ sender: Any) {
        exit { [weak self] in
            self?.closeTap?()
        }
    }
    
    @IBAction private func inviteTouch(_ sender: Any) {
        inviteTap?()
    }
    
    @IBAction private func supportTouch(_ sender: Any) {
        supportTap?()
    }
    
    @IBAction private func termsTouch(_ sender: Any) {
        termsTap?()
    }
    
    @IBAction private func privacyTouch(_ sender: Any) {
        privacyTap?()
    }
    
    @IBAction private func logoutTouch(_ sender: Any) {
        logoutTap?()
    }
    
    @IBAction private func deleteTouch(_ sender: Any) {
        deleteTap?()
    }
    
    // MARK: - drag
    
    private var dragStart: CGPoint = .zero
    
    @objc private func dragContent(sender: UIPanGestureRecognizer) {
        
        guard content.layer.animationKeys() == nil else { return }
        
        let point = sender.location(in: view)
        let offset = dragStart.x - point.x
        switch sender.state {
        case .began:
            dragStart = point
        case .changed:
            moveTop(to: offset)
        case .cancelled, .ended, .failed:
            settleTop(to: offset)
        default: ()
        }
    }
    
    private func moveTop(to offset: CGFloat) {
        let clampedOffset = max(offset, 0)
        content.layer.transform = CATransform3DMakeTranslation(-clampedOffset, 0, 0)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3 * (1 - clampedOffset / content.frame.width))
        print("Offset: \(clampedOffset)")
    }
    
    private func settleTop(to offset: CGFloat) {
        if offset > content.frame.width * 0.25 {
            exit { [weak self] in
                self?.closeTap?()
            }
        } else {
            enter()
        }
    }
    
    // MARK: - animations
    
    private func prepareForEnter() {
        view.backgroundColor = .clear
        content.layer.transform = CATransform3DMakeTranslation(-UIScreen.main.bounds.width, 0, 0)
    }
    
    private func enter() {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.content.layer.transform = CATransform3DIdentity
        }
    }
    
    private func exit(_ complete: @escaping ()->()) {
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.prepareForEnter()
            },
            completion: { _ in
                complete()
            })
    }
}
