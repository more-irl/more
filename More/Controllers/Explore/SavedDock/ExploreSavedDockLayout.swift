//
//  ExploreSavedDockLayout.swift
//  More
//
//  Created by Luko Gjenero on 14/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ExploreSavedDockLayout: UICollectionViewFlowLayout {

    static let maxItemNum = 4
    static let step: CGPoint = CGPoint(x: 16, y: 12)
    
    var insertScale: CGFloat = 1
    
    override func prepare() {
        super.prepare()
        
        // TODO
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let collectionView = collectionView else {
            return super.layoutAttributesForElements(in: rect)
        }
        
        var attributes: [UICollectionViewLayoutAttributes] = []
        for i in 0..<min(collectionView.numberOfItems(inSection: 0), ExploreSavedDockLayout.maxItemNum) {
            if let itemAttributes = layoutAttributesForItem(at: IndexPath(item: i, section: 0)) {
                attributes.append(itemAttributes)
            }
        }
        return attributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let itemAttributes = super.layoutAttributesForItem(at: indexPath)
        
        let copiedAttributes = itemAttributes?.copy() as? UICollectionViewLayoutAttributes
        copiedAttributes?.frame = itemRect(for: indexPath)
        copiedAttributes?.zIndex = ExploreSavedDockLayout.maxItemNum - indexPath.item
        copiedAttributes?.alpha = indexPath.item < 4 ? 1 : 0
        
        return copiedAttributes
    }
    
    private func itemRect(for indexPath: IndexPath) -> CGRect {
        guard let collectionView = collectionView else {
            return .zero
        }
        
        let frame = collectionView.bounds
        var origin = CGPoint(x: (frame.width - itemSize.width) * 0.5, y: frame.height - itemSize.height)
        origin.y -= ExploreSavedDockLayout.step.y * CGFloat(indexPath.item)
        origin.x += ExploreSavedDockLayout.step.x * CGFloat(indexPath.item)
        var size = itemSize
        size.width -= 2 * ExploreSavedDockLayout.step.x * CGFloat(indexPath.item)
        
        return CGRect(origin: origin, size: size)
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let itemAttributes = layoutAttributesForItem(at: itemIndexPath)
        itemAttributes?.alpha = 0
        return itemAttributes
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if itemIndexPath.item > 0 {
            let itemAttributes = layoutAttributesForItem(at: IndexPath(item: itemIndexPath.item - 1, section: 0))
            return itemAttributes
        }
        
        let itemAttributes = layoutAttributesForItem(at: itemIndexPath)
        itemAttributes?.transform3D = CATransform3DMakeScale(insertScale, 1, 1)
        return itemAttributes
    }
    
}


