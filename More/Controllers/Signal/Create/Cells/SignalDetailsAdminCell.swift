//
//  SignalDetailsAdminCell.swift
//  More
//
//  Created by Luko Gjenero on 15/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsAdminCell: SignalDetailsBaseCell {

    static let height: CGFloat = 542
    
    @IBOutlet private weak var claimable: UISwitch!
    @IBOutlet private weak var curated: UISwitch!
    @IBOutlet private weak var startDate: UITextField!
    @IBOutlet private weak var endDate: UITextField!
    @IBOutlet private weak var dailyStart: UITextField!
    @IBOutlet private weak var dailyDuration: UITextField!
    @IBOutlet private weak var location: UILabel!
    @IBOutlet private weak var locationButton: UIButton!
    @IBOutlet private weak var radius: UITextField!
    
    private let df = DateFormatter()
    private var model: CreateSignalViewModel?
    
    private let keyboardManager = KeyboardManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        keyboardManager.containerView = self
        
        let button = KeyboardManager.createToolbar()
        button.addTarget(self, action: #selector(doneTouch), for: .touchUpInside)
        
        let startDatePicker = UIDatePicker()
        startDatePicker.addTarget(self, action: #selector(startChanged(_:)), for: .valueChanged)
        startDate.inputView = startDatePicker
        startDate.inputAccessoryView = button
        
        let endDatePicker = UIDatePicker()
        endDatePicker.addTarget(self, action: #selector(endChanged(_:)), for: .valueChanged)
        endDate.inputView = endDatePicker
        endDate.inputAccessoryView = button
        
        let dailyStartPicker = UIDatePicker()
        dailyStartPicker.datePickerMode = .time
        dailyStartPicker.addTarget(self, action: #selector(dailyStartChanged(_:)), for: .valueChanged)
        dailyStart.inputView = dailyStartPicker
        dailyStart.inputAccessoryView = button
        
        let dailyDurationButton = KeyboardManager.createToolbar()
        dailyDurationButton.addTarget(self, action: #selector(dailyDurationDone), for: .touchUpInside)
        dailyDuration.inputAccessoryView = dailyDurationButton
        keyboardManager.manageTextField(dailyDuration, withMaxLength: 0, showDone: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dailyDurationChanged), name: NSNotification.Name.UITextViewTextDidEndEditing, object: dailyDuration)
        
        df.dateFormat = "YY MMM d, h:mma"
        
        let radiusButton = KeyboardManager.createToolbar()
        radiusButton.addTarget(self, action: #selector(radiusDone), for: .touchUpInside)
        radius.inputAccessoryView = radiusButton
        keyboardManager.manageTextField(radius, withMaxLength: 0, showDone: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(radiusChanged), name: NSNotification.Name.UITextViewTextDidEndEditing, object: radius)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction private func claimableChanged(_ sender: Any) {
        if claimable.isOn {
            model?.kind = .claimable
            curated.isOn = false
        } else {
            model?.kind = .standard
            model?.kindStartDate = nil
            model?.kindEndDate = nil
            model?.kindDailyStart = nil
            model?.kindDailyDuration = nil
            startDate.text = ""
            endDate.text = ""
            dailyStart.text = ""
            dailyDuration.text = ""
        }
    }
    
    @IBAction private func curatedChanged(_ sender: Any) {
        if curated.isOn {
            model?.kind = .curated
            claimable.isOn = false
        } else {
            model?.kind = .standard
            model?.kindStartDate = nil
            model?.kindEndDate = nil
            model?.kindDailyStart = nil
            model?.kindDailyDuration = nil
            startDate.text = ""
            endDate.text = ""
            dailyStart.text = ""
            dailyDuration.text = ""
        }
    }
    
    @IBAction private func locationTouch(_ sender: Any) {
        
        DebugMapView.show { [weak self] (location) in
            
            if let location = location {
                self?.model?.location = Location(coordinates: location.coordinate)
                
            }
        }
        
    }
    
    @objc private func startChanged(_ sender: UIDatePicker) {
        model?.kindStartDate = sender.date
        startDate.text = df.string(from: sender.date)
    }
    
    @objc private func endChanged(_ sender: UIDatePicker) {
        model?.kindEndDate = sender.date
        endDate.text = df.string(from: sender.date)
    }
    
    @objc private func dailyStartChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.hour, .minute, .second], from: sender.date)
        model?.kindDailyStart = Double(components.hour! * 3600 + components.minute! * 60 + components.second!)
        updateDailyStart()
    }
    
    
    
    @objc private func dailyDurationDone() {
        UIResponder.currentFirstResponder()?.resignFirstResponder()
        dailyDurationChanged()
    }
    
    @objc private func dailyDurationChanged() {
        
        if let duration = Double(self.dailyDuration.text ?? ""), duration > 0 {
            model?.kindDailyDuration = duration
        } else {
            model?.kindDailyDuration = nil
        }
        updateDailyDuration()
    }
    
    @objc private func doneTouch() {
        UIResponder.currentFirstResponder()?.resignFirstResponder()
    }
    
    @objc private func radiusDone() {
        UIResponder.currentFirstResponder()?.resignFirstResponder()
        radiusChanged()
    }
    
    @objc private func radiusChanged() {
        
        if let radius = Double(self.radius.text ?? ""), radius > 0 {
            model?.radius = radius
        } else {
            model?.radius = nil
        }
        updateRadius()
    }
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        // nothing
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: height)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return false
    }
    
    private func updateLocation() {
        
        if let location = model?.location {
            self.location.text = "\(location.latitude), \(location.longitude)\nSelected on Map"
        } else if let destination = model?.destination {
            self.location.text = "\(destination.latitude), \(destination.longitude)\nDestination: \(model?.destinationName ?? "Unknown")"
        } else if let location = LocationService.shared.currentLocation {
            self.location.text = "\(location.coordinate.latitude), \(location.coordinate.longitude)\nGPS location"
        }
    }
    
    private func updateRadius() {
        if let radius = model?.radius {
            self.radius.text = "\(Int(radius))"
        } else {
            self.radius.text = ""
        }
    }
    
    private func updateDailyStart() {
        
        var text = ""
        if let start = model?.kindDailyStart {
            let hour = Int(start / 3600)
            let minute = Int(start.truncatingRemainder(dividingBy: 3600) / 60)
            let second = Int(start.truncatingRemainder(dividingBy: 60))
            
            
            text += hour > 9 ? "\(hour):" : "0\(hour):"
            text += minute > 9 ? "\(minute):" : "0\(minute):"
            text += second > 9 ? "\(second)" : "0\(second)"
        }
        dailyStart.text = text
    }
    
    private func updateDailyDuration() {
        if let duration = model?.kindDailyDuration {
            self.dailyDuration.text = "\(Int(duration))"
        } else {
            self.dailyDuration.text = ""
        }
    }
    
    
    // MARK: - signal preview
    
    override func setup(for model: CreateSignalViewModel) {
        self.model = model
        
        if let kind = model.kind {
            switch kind {
            case .claimable:
                claimable.isOn = true
                curated.isOn = false
            case .curated:
                claimable.isOn = false
                curated.isOn = true
            case .standard:
                claimable.isOn = false
                curated.isOn = false
                break
            }
        } else {
            claimable.isOn = false
            curated.isOn = false
        }
        
        if let start = model.kindStartDate {
            startDate.text = df.string(from: start)
        } else {
            startDate.text = ""
        }
        
        if let end = model.kindEndDate {
            endDate.text = df.string(from: end)
        } else {
            endDate.text = ""
        }
        
        updateLocation()
        updateRadius()
        updateDailyStart()
        updateDailyDuration()
    }
    
    override class func size(for model: CreateSignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: height)
    }
    
    override class func isShowing(for model: CreateSignalViewModel) -> Bool {
        return ProfileService.shared.profile?.isAdmin == true
    }
    
}
