//
//  Request.swift
//  More
//
//  Created by Luko Gjenero on 02/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

struct Request: Codable, Hashable, MoreDataObject {
    
    let id: String
    let createdAt: Date
    let expiresAt: Date
    let sender: User
    let accepted: Bool?
    let messages: [Message]?
    
    init(id: String,
         createdAt: Date,
         expiresAt: Date,
         sender: User,
         accepted: Bool?,
         messages: [Message]? = nil) {
        
        self.id = id
        self.createdAt = createdAt
        self.expiresAt = expiresAt
        self.sender = sender
        self.accepted = accepted
        self.messages = messages
    }
    
    func requestWithMessages(_ messages: [Message]?) -> Request {
        return Request(
            id: id,
            createdAt: createdAt,
            expiresAt: expiresAt,
            sender: sender,
            accepted: accepted,
            messages: messages)
    }
    
    func requestWithAccepted(_ accepted: Bool?) -> Request {
        return Request(
            id: id,
            createdAt: createdAt,
            expiresAt: expiresAt,
            sender: sender,
            accepted: accepted,
            messages: messages)
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: Request, rhs: Request) -> Bool {
        return lhs.id == rhs.id
    }
    
    // JSON
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                json?.removeValue(forKey: "messages")
                
                if let messageList = Request.convertList(messages) {
                    json?["messageList"] = messageList
                }
                
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String: Any]) -> Request? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let request = try? JSONDecoder().decode(Request.self, from: jsonData) {
            
            let messages: [Message] = Request.convertListBack(json["messageList"] as? [String: Any]) ?? []
            return request.requestWithMessages(messages)
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> Request? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return Request.fromJson(json)
        }
        return nil
    }
    
    
}
