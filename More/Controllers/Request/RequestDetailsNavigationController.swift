//
//  RequestDetailsNavigationController.swift
//  More
//
//  Created by Luko Gjenero on 07/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class RequestDetailsNavigationController: UIViewController {

    private var signalId: String? = nil
    private var requestId: String? = nil
    
    var back: (()->())?
    
    var report: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNestedNavigation()
        setupMessageingOverlay()
        
        NotificationCenter.default.addObserver(self, selector: #selector(requestExpired), name: SignalTrackingService.Notifications.SignalRequestExpired, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func requestExpired(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String,
                let requestId = notice.userInfo?["requestId"] as? String,
                let cancelled = notice.userInfo?["cancelled"] as? Bool,
                self?.signalId == signalId, self?.requestId == requestId {
                
                self?.presentRequestExpired(cancelled)
            }
        }
    }

    func setup(signal: SignalViewModel, request: RequestViewModel) {
        
        signalId = signal.id
        requestId = request.id
        
        let vc = ProfileViewController()
        vc.backTap = { [weak self] in
            self?.back?()
        }
        vc.scrolling = { [weak self] in
            self?.messagingOverlay?.fullyCollapse()
        }
        _ = vc.view
        vc.bottomPadding = 267
        
        var updatedSignal = signal
        var updatedRequest = request
        if let updated = SignalTrackingService.shared.updatedSignalData(for: signal.id) {
            updatedSignal = SignalViewModel(signal: updated)
            if let updated = updated.requests?.first(where: { $0.id == request.id }) {
                updatedRequest = RequestViewModel(request: updated)
            }
        }
        
        vc.setup(for: updatedRequest.sender)
        vc.showLoading()
        ProfileService.shared.loadProfile(withId: updatedRequest.sender.id) { [weak vc] (profile, _) in
            if let profile = profile {
                let model = UserViewModel(profile: profile)
                vc?.setup(for: model)
                vc?.moreTap = { [weak self] in
                    self?.report(model) { (reported) in
                        if reported {
                            SignalingService.shared.cancelRequest(request.id, in: signal.id, complete: nil)
                        }
                    }
                }
            }
            vc?.hideLoading()
        }

        navigation.setViewControllers([vc], animated: false)
        
        messagingOverlay.setup(signal: updatedSignal, request: updatedRequest)
    }
    
    // MARK: - navigation controller
    
    private let navigation = UINavigationController()
    
    private func setupNestedNavigation() {
        navigation.view.translatesAutoresizingMaskIntoConstraints = false
        addChildViewController(navigation)
        view.addSubview(navigation.view)
        navigation.willMove(toParentViewController: self)
        navigation.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        navigation.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        navigation.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        navigation.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        navigation.setNavigationBarHidden(true, animated: false)
        
        navigation.enableSwipeToPop()
    }
    
    // MARK - messaging overlay
    
    private var messagingOverlay: ResponseMessagingOverlayViewController!
    
    private func setupMessageingOverlay() {
        messagingOverlay = ResponseMessagingOverlayViewController()
        messagingOverlay.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(messagingOverlay.view)
        addChildViewController(messagingOverlay)
        messagingOverlay.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        messagingOverlay.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        messagingOverlay.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        messagingOverlay.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        view.setNeedsLayout()
        
        messagingOverlay.accepted = { [weak self] in
            self?.back?()
        }
    }
    
    private func presentRequestExpired(_ cancelled: Bool) {
        
        let vc = RequestExpiredViewController()
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(vc.view)
        addChildViewController(vc)
        vc.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        vc.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        vc.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        view.setNeedsLayout()
        vc.setupForRequest(cancelled: cancelled)
        
        vc.doneTap = { [weak self, weak vc] in
            vc?.removeFromParentViewController()
            vc?.view.removeFromSuperview()
            self?.back?()
        }
    }
    
}
