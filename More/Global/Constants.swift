//
//  Constants.swift
//  More
//
//  Created by Anirudh Bandi on 7/10/18.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Foundation
import UIKit


enum MoreError : Error {
    case NoLocationService
    case NoNetwork
}


let places_api_key = "AIzaSyD6XeygiBH6XP-CwIt4kQ-aWBm4pA1hr3Y"
let CRON_API_TOKEN = "YNLY5X7MSSDYH5I465V46W0U2YR5P4CI"
let FIREBASE_CLOUD_FUNCTIONS_CRON_KEY = "d242d7fa4c4e63844d01b2ef717eb0f5c469a049"
let FIREBASE_PROJECT_ID = "moretest-274fe"
let FIREBASE_CRON_FUNCTION_NAME = "removeSignalFromGeoFireRef"


let MSG_TERMS_CONDITION : String    = "I have read and agree to the Terms & Conditions and the Privacy Policy of More Technologies"
let MSG_INVALID_FIRSTNAME           = "Invalid Firstname"
let MSG_INVALID_LASTNAME            = "Invalid Lastname"
let MSG_INVALID_EMAIL               = "Invalid Email"
let MSG_INVALID_USERNAME            = "Invalid Username"
let MSG_INVALID_PASSWORD            = "Invalid Password"
let MSG_INVALID_CONTACT_NUMBER      = "Invalid Contact Number"
let MSG_PASSWORD_NOT_MATCHED        = "Not matched"
let MSG_SOMETHING_WRONG             = "Ops!, Something went wrong, try again later"

let MSG_SIGN_IN                     = "Logging in..."
let MSG_SIGN_UP                     = "Signing up..."
let MSG_NO_INTERNET                 = "Make sure your device is connected to the Internet."
let MSG_NO_INTERNET_TITLE           = "No Internet connection"
let MSG_NAME_FIELD_LENGTH           = "Name length must be larger than or equal to 4 characters long!"
let MSG_PHONE_FIELD_LENGTH          = "Phone number must be larger than or equal to 10 characters long!"


public let MEDIA_FOLDER                 = "Media/"
public let MEDIA_PHOTO_FOLDER           = MEDIA_FOLDER + "Photos/"
public let MEDIA_CAMERA_FOLDER          = MEDIA_FOLDER + "Camera/"
public let MEDIA_PROFILE                = MEDIA_FOLDER + "Profile/"
let IMG_USER_PROFILE                    = "user.jpg"


struct Storyboard {
    static let main : String = "Main"
    static let login : String = "Login"
}

struct Colors {
    static let background: UIColor = UIColor(red: 230, green: 230, blue: 230)
    static let topBarBackground: UIColor = UIColor(red: 230, green: 230, blue: 230)
    static let topBarText: UIColor = UIColor(red: 67, green: 74, blue: 81)
    static let bottomBarBackground: UIColor = UIColor(red: 255, green: 255, blue: 255)
    
    // MARK: avatar
    static let avatarInnerRing: UIColor = UIColor(red: 244, green: 244, blue: 244)
    static let avatarOuterRing: UIColor = UIColor(red: 191, green: 195, blue: 202)
    
    // MARK: explore
    static let exploreText: UIColor = UIColor(red: 68, green: 74, blue: 80)
    static let exploreDockBackground: UIColor = UIColor(red: 244, green: 244, blue: 244)
    
    // MARK: profile
    static let tagBackground: UIColor = UIColor(red: 238, green: 238, blue: 238)
    static let lightBlueHighlight: UIColor = UIColor(red: 3, green: 202, blue: 255)
    static let profilekBackground: UIColor = UIColor(red: 244, green: 244, blue: 244)
    
    static let reviewItemOddBackground: UIColor = UIColor(red: 58, green: 61, blue: 67)
    
    // MARK: messaging
    static let incomingBubble: UIColor = UIColor(red: 232, green: 235, blue: 238)
    static let outgoingBubble: UIColor = UIColor(red: 97, green: 145, blue: 255)
    static let inputPlaceholder: UIColor = UIColor(red: 191, green: 195, blue: 202)
    static let inputText: UIColor = UIColor(red: 67, green: 74, blue: 81)
    
    
    // MARK: create signal
    static let selectImageButtonBorder: UIColor = UIColor(red: 245, green: 245, blue: 254)
    static let quoteInputPlaceholder: UIColor = UIColor(red: 191, green: 195, blue: 202)
    
    static let previewLightBackground: UIColor = UIColor(red: 218, green: 221, blue: 226)
    static let previewLightText: UIColor = UIColor(red: 40, green: 40, blue: 43)
    static let previewDarkBackground: UIColor = UIColor(red: 40, green: 40, blue: 43)
    static let previewDarkText: UIColor = UIColor(red: 255, green: 255, blue: 255)
    
    static let moodSelectedTitle: UIColor = UIColor(red: 124, green: 139, blue: 155)
    static let moodNotSelectedTitle: UIColor = UIColor(red: 191, green: 195, blue: 202)
}


struct Urls {
    static let support = "https://www.startwithmore.com/support"
    static let terms = "https://www.startwithmore.com/terms"
    static let privacy = "https://www.startwithmore.com/privacy"
    static let legal = "https://www.startwithmore.com/legal"
    static let welcomeVideo = "https://startwithmore.wistia.com/embed/medias/v1ib3g9vxi.m3u8"
}

struct Emails {
    static let support = "help@startwithmore.com"
}

struct MapBox {
    // static let styleUrl = "mapbox://styles/moreanastasiya/cjohmlun4008v2so3pk2o9x8f"
    static let styleUrl = "mapbox://styles/moreanastasiya/cjpr91f5h6b482smb38a820wq"
}
