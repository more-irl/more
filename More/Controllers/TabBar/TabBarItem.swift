//
//  TabBarItem.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class TabBarItem: LoadableView {

    @objc enum ButtonType: Int {
        case home, times, signal, alerts, profile
    }
    
    @IBOutlet private weak var button: UIButton!
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var notice: UIView!
    
    var isSelected: Bool {
        get {
            return button.isSelected
        }
        set {
            button.isSelected = newValue
        }
    }
    
    var isHighlighed: Bool {
        get {
            return !notice.isHidden
        }
        set {
            notice.isHidden = !newValue
        }
    }
    
    var buttonTap: (()->())?
    
    override func setupNib() {
        super.setupNib()
        
        notice.layer.cornerRadius = 2
        notice.layer.masksToBounds = true
    }
    
    @IBInspectable var type: Int = ButtonType.home.rawValue {
        didSet {
            guard let type = ButtonType(rawValue: type) else { return }
            switch type {
            case .home:
                setupHomeButton()
            case .times:
                setupTimesButton()
            case .signal:
                setupSignalButton()
            case .alerts:
                setupAlertsButton()
            case .profile:
                setupProfileButton()
            }
        }
    }
    
    @IBAction func buttonTouch(_ sender: Any) {
        buttonTap?()
    }
    
    private func setupHomeButton() {
        button.setImage(UIImage(named: "home_off"), for: .normal)
        button.setImage(UIImage(named: "home_on"), for: .selected)
        label.text = "HOME"
    }
    
    private func setupTimesButton() {
        button.setImage(UIImage(named: "times_off"), for: .normal)
        button.setImage(UIImage(named: "times_on"), for: .selected)
        label.text = "TIMES"
    }
    
    private func setupSignalButton() {
        button.setImage(UIImage(named: "signal_off"), for: .normal)
        button.setImage(UIImage(named: "signal_on"), for: .selected)
        label.text = "SIGNAL"
    }
    
    private func setupAlertsButton() {
        button.setImage(UIImage(named: "alerts_off"), for: .normal)
        button.setImage(UIImage(named: "alerts_on"), for: .selected)
        label.text = "ALERTS"
    }
    
    private func setupProfileButton() {
        button.setImage(UIImage(named: "profile_off"), for: .normal)
        button.setImage(UIImage(named: "profile_on"), for: .selected)
        label.text = "PROFILE"
    }
}
