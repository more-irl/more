//
//  SignalDetailsTransitioningDelegate.swift
//  More
//
//  Created by Luko Gjenero on 17/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}

class SignalDetailsTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {

    let interactor = Interactor()
    let transition = PullDownToDismissTransition()
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        transition.swipeToDismiss = interactor.hasStarted
        return interactor.hasStarted ? interactor : nil
    }
}


