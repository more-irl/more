//
//  ProfileService.swift
//  More
//
//  Created by Luko Gjenero on 29/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class ProfileService {
        
    enum ProfilePhase: Int {
        case phoneLogin
        case authentication
        case terms
        case requestLocation
        case locationNeeded
        case notAvailable
        case available
        case info
        case instagram
        case memories
        case invite
        case ready
    }

    struct Notifications {
        static let ProfileLogin = NSNotification.Name(rawValue: "com.more.profile.login")
        static let ProfileLogout = NSNotification.Name(rawValue: "com.more.profile.logut")
        
        static let ProfileLoaded = NSNotification.Name(rawValue: "com.more.profile.loaded")
        static let ProfileUpdated = NSNotification.Name(rawValue: "com.more.profile.updated")
        
        static let OwnSignalChanged = NSNotification.Name(rawValue: "com.more.profile.signal.changed")
        static let OwnSignalExpired = NSNotification.Name(rawValue: "com.more.profile.signal.expired")
        
        static let OwnRequestChanged = NSNotification.Name(rawValue: "com.more.profile.request.changed")
        static let OwnRequestExpired = NSNotification.Name(rawValue: "com.more.profile.request.expired")
    }
    
    static let shared = ProfileService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(referralUpdated), name: BranchService.Notifications.ReferralIDLoaded, object: nil)
        
        load()
        toForeground()
    }
    
    // MARK: - data
    
    private let profileKey = "com.more.profile.data"
    
    private(set) var profile: Profile?
    
    @objc private func toForeground() {
        guard profile != nil else { return }
        loadProfile(complete: nil)
        loadSignals()
        loadTimes()
        loadReviews()
        loadNumberOfGoings()
    }
    
    @objc private func toBackground() {
        save()
    }
    
    @objc private func locationUpdated() {
        if let location = LocationService.shared.currentLocation {
            ProfileService.shared.updateLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        }
    }
    
    @objc private func referralUpdated() {
        if let referralId = BranchService.shared.referralId, referralId.count > 0, ProfileService.shared.profile?.referral == nil {
            ProfileService.shared.updateReferral(referral: referralId)
        }
    }
    
    private func load() {
        if let data = UserDefaults.standard.value(forKey:profileKey) as? Data {
            profile = try? PropertyListDecoder().decode(Profile.self, from: data)
        }
    }
    
    private func save() {
        if let profile = profile {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(profile), forKey:profileKey)
        }
    }
    
    // MARK: - state
    
    var profilePhase: ProfilePhase {
        
        // return .phoneLogin
        
        guard let profile = profile, Auth.auth().currentUser != nil else {
            return .phoneLogin
        }
        
        /*
        if profile.authVerificationID == nil {
            return .authentication
        }
        */
        
        if !profile.terms {
            return .terms
        }
        
        if !LocationService.locationEnabled {
            if LocationService.canRequestLocation {
                return .requestLocation
            }
            return .locationNeeded
        }
        
        if profile.location == nil && WindowsService.shared.currentWindows.count == 0 {
            return .available
        }
        
        if profile.firstName == nil || profile.lastName == nil || profile.email == nil {
            return .info
        }
        
        if profile.instagram == nil {
            return .instagram
        }
        
        if profile.memories == nil {
            return .memories
        }
        
        if !profile.verified {
            return .invite
        }
        
        return .ready
    }
    
    // MARK: - login, logout, delete
    
    private func login(phoneNumber: String, formattedPhoneNumber: String, authVerificationID: String) {
        let profile = Profile(phoneNumber: phoneNumber, formattedPhoneNumber: formattedPhoneNumber, authVerificationID: authVerificationID)
        self.profile = profile
        save()
    }
    
    func logout(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        do {
            try Auth.auth().signOut()
            NotificationCenter.default.post(name: Notifications.ProfileLogout, object: self)
            stopTrackingSignals()
            stopTrackingTimes()
            stopTrackingReviews()
            stopTrackingNumberOfGoings()
            complete?(true, nil)
        } catch {
            complete?(false, error.localizedDescription)
        }
    }
    
    func deleteAccount(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        guard let id = profile?.getId() else {
            complete?(false, "Not logged in")
            return
        }
        
        guard let currentUser = Auth.auth().currentUser else {
            complete?(false, "Not logged in")
            return
        }
        
        Database.database().reference().child("users").child(id).removeValue { (error, reference) in
            if let error = error {
                complete?(false, error.localizedDescription)
            } else {
                currentUser.delete(completion: { [weak self] (error) in
                    self?.logout(complete: complete)
                })
            }
        }
    }
    
    // MARK: - data update
    
    func updateTerms(_ terms: Bool) {
        profile?.terms = terms
        save()
        saveProfile(complete: nil)
    }
    
    func updateLocation(latitude: Double, longitude: Double) {
        profile?.location = Location(latitude: latitude, longitude: longitude)
        save()
        
        guard let id = profile?.getId(),
            let locationValue = profile?.location?.json else {
                return
        }
        Database.database().reference().child("users").child(id).child("location").updateChildValues(locationValue)
    }
    
    func updateReferral(referral: String) {
        profile?.referral = referral
        save()
        saveProfile(complete: nil)
    }
    
    func updateInfo(firstName: String, lastName: String, email: String) {
        profile?.firstName = firstName.trimmingCharacters(in: .whitespacesAndNewlines)
        profile?.lastName = lastName.trimmingCharacters(in: .whitespacesAndNewlines)
        profile?.email = email.trimmingCharacters(in: .whitespacesAndNewlines)
        save()
        saveProfile(complete: nil)
    }
    
    func updateInstagram(_ instagram: String) {
        profile?.instagram = instagram.trimmingCharacters(in: .whitespacesAndNewlines)
        save()
        saveProfile(complete: nil)
    }
    
    func updateMemories(_ memories: String) {
        profile?.memories = memories.trimmingCharacters(in: .whitespacesAndNewlines)
        save()
        saveProfile(complete: nil)
    }
    
    func updateInviteLink(_ inviteLink: String) {
        profile?.inviteLink = inviteLink.trimmingCharacters(in: .whitespacesAndNewlines)
        profile?.verified = true
        save()
        saveProfile(complete: nil)
    }
    
    func updateDeviceToken(_ token: String) {
        profile?.iOSDeviceToken = token.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("iOSDeviceToken").setValue(token)
    }
    
    func updateRegistrationToken(_ token: String) {
        profile?.registrationToken = token
        
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("registrationToken").setValue(token)
    }

    func updateBirthday(_ birthday: Date) {
        profile?.birthday = birthday

        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("birthday").setValue(birthday.timeIntervalSinceReferenceDate)
    }

    func updateQuote(quote: String?, author: String?) {
        profile?.quote = quote?.trimmingCharacters(in: .whitespacesAndNewlines)
        profile?.quoteAuthor = author?.trimmingCharacters(in: .whitespacesAndNewlines)

        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("quote").setValue(quote)
        Database.database().reference().child("users").child(id).child("quoteAuthor").setValue(author)
    }

    func updateGender(_ gender: String?) {
        profile?.gender = gender?.trimmingCharacters(in: .whitespacesAndNewlines)

        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("gender").setValue(gender)
    }
    
    func updateImages(_ urls: [String]?) {
        profile?.images = urls
        
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("images").setValue(urls)
    }
    
    // MARK: - api
    
    func verifyPhoneNumber(phoneNumber: String, formattedPhoneNumber: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            ProfileService.shared.login(
                phoneNumber: phoneNumber,
                formattedPhoneNumber: formattedPhoneNumber,
                authVerificationID: verificationID ?? "")
            
            complete?(true, nil)
        }
    }
    
    func signIn(verificationCode: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        let authVerificationID = ProfileService.shared.profile?.authVerificationID ?? ""
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: authVerificationID, verificationCode: verificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            guard let uid = authResult?.user.uid else {
                complete?(false, "Unknown user")
                return
            }
            
            ProfileService.shared.profile?.verificationCode = verificationCode
            ProfileService.shared.profile?.id = uid
            if let referralId = BranchService.shared.referralId, referralId.count > 0 {
                ProfileService.shared.profile?.referral = referralId
            }
            ProfileService.shared.save()
            
            ProfileService.shared.createProfileIfNeeded(complete: { (success, errorMsg) in
                if success {
                    NotificationCenter.default.post(name: Notifications.ProfileLogin, object: ProfileService.shared)
                }
                complete?(success, errorMsg)
            })
        }
        
    }
    
    func createProfileIfNeeded(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        guard let id = profile?.getId() else {
            complete?(false, "Not logged in")
            return
        }
        
        Database.database().reference().child("users").child(id).observeSingleEvent(
            of: .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    if var value = snapshot.value as? [String: Any] {
                        value["id"] = id
                        ProfileService.shared.profile?.json = value
                    }
                    complete?(true, nil)
                } else {
                    ProfileService.shared.createProfile(complete: { (success, errorMsg) in
                        if success {
                            ProfileService.shared.checkUserLeads(complete: complete)
                        } else {
                            complete?(success, errorMsg)
                        }
                    })
                }
            },
            withCancel: { (error) in
                complete?(false, error.localizedDescription)
            })
        
    }
    
    func createProfile(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        guard let id = profile?.getId(),
            let profileValue = profile?.json else {
            complete?(false, "Not logged in")
            return
        }
        
        Database.database().reference().child("users").child(id).setValue(profileValue) { (error, reference) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            complete?(true, nil)
        }
    }
    
    func checkUserLeads(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        guard let phoneNumber = profile?.phoneNumber else {
            complete?(false, "Not logged in")
            return
        }
        
        let fixedNumber = phoneNumber
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: "(", with: "")
            .replacingOccurrences(of: ")", with: "")
            .replacingOccurrences(of: "-", with: "")
        
        Database.database().reference().child("userleads").child(fixedNumber).observeSingleEvent(
            of: .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    if var value = snapshot.value as? [String: Any] {
                        var save = false
                        if let firstName = value["firstName"] as? String {
                            ProfileService.shared.profile?.firstName = firstName
                            save = true
                        }
                        if let lastName = value["lastName"] as? String {
                            ProfileService.shared.profile?.lastName = lastName
                            save = true
                        }
                        if let email = value["email"] as? String {
                            ProfileService.shared.profile?.email = email
                            save = true
                        }
                        if let instagram = value["instagram"] as? String {
                            ProfileService.shared.profile?.instagram = instagram
                            save = true
                        }
                        if let moreMoment = value["moreMoment"] as? String {
                            ProfileService.shared.profile?.memories = moreMoment
                            save = true
                        }
                        if let referral = value["referral"] as? String {
                            ProfileService.shared.profile?.referral = referral
                            save = true
                        }
                        if save {
                            BranchService.shared.createInviteLink { (link) in
                                if let link = link {
                                    ProfileService.shared.profile?.inviteLink = link
                                    ProfileService.shared.profile?.verified = true
                                }
                                ProfileService.shared.save()
                                ProfileService.shared.saveProfile(complete: nil)
                                complete?(true, nil)
                            }
                            return
                        }
                    }
                    complete?(true, nil)
                } else {
                    complete?(true, nil)
                }
            },
            withCancel: { (error) in
                complete?(true, nil)
            })
    }
    
    func loadProfile(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        guard let id = profile?.getId() else {
            complete?(false, "Not logged in")
            return
        }
        
        Database.database().reference().child("users").child(id).observeSingleEvent(
            of: .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    if var value = snapshot.value as? [String: Any] {
                        value["id"] = id
                        ProfileService.shared.profile?.json = value
                        ProfileService.shared.save()
                    }
                    complete?(true, nil)
                    NotificationCenter.default.post(name: Notifications.ProfileLoaded, object: self)
                } else {
                    complete?(false, "Profile does not exist")
                }
            },
            withCancel: { (error) in
                complete?(false, error.localizedDescription)
            })
    }
    
    func saveProfile(complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        guard let id = profile?.getId(),
            let profileValue = profile?.json else {
                complete?(false, "Not logged in")
                return
        }
        
        Database.database().reference().child("users").child(id).updateChildValues(profileValue) { (error, reference) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            complete?(true, nil)
        }
    }
    
    // MARK: - tracking data
    
    private func loadSignals() {
        guard let id = profile?.getId() else { return }
        
        stopTrackingSignals()
        
        Database.database().reference().child("users").child(id).child("signals").child("active").observe(
            .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    for child in snapshot.children {
                        if let child = child as? DataSnapshot,
                            let shortSignal = ShortSignal.fromSnapshot(child) {
                            SignalTrackingService.shared.trackActive(signalId: shortSignal.id)
                        }
                    }
                }
            },
            withCancel: { (error) in
                // TODO
            })
        
        Database.database().reference().child("users").child(id).child("signals").child("requests").observe(
            .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    for child in snapshot.children {
                        if let child = child as? DataSnapshot,
                            let shortSignal = ShortSignal.fromSnapshot(child) {
                            SignalTrackingService.shared.trackActive(signalId: shortSignal.id)
                        }
                    }
                }
            },
            withCancel: { (error) in
                // TODO
            })
    }
    
    private func stopTrackingSignals() {
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("signals").child("active").removeAllObservers()
        Database.database().reference().child("users").child(id).child("signals").child("requests").removeAllObservers()
        
    }
    
    private func loadTimes() {
        guard let id = profile?.getId() else { return }
        
        stopTrackingTimes()
        
        Database.database().reference().child("users").child(id).child("times").child("active").observe(
            .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    for child in snapshot.children {
                        if let child = child as? DataSnapshot,
                            let time = Time.fromSnapshot(child) {
                            TimesService.shared.track(timeId: time.id)
                        }
                    }
                }
            },
            withCancel: { (error) in
                // TODO: --
            })
    }
    
    private func stopTrackingTimes() {
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("times").child("active").removeAllObservers()
    }
    
    private func loadReviews() {
        guard let id = profile?.getId() else { return }
        
        stopTrackingReviews()
        
        Database.database().reference().child("users").child(id).child("reviewList").observe(
            .childAdded,
            with: { (snapshot) in
                if snapshot.exists() {
                    if let review = Review.fromSnapshot(snapshot) {
                        if ProfileService.shared.profile?.reviews?.contains(review) == false {
                            ProfileService.shared.profile?.reviews?.append(review)
                            NotificationCenter.default.post(name: Notifications.ProfileLoaded, object: self)
                        }
                    }
                }
            },
            withCancel: { (error) in
                // TODO: --
            })
        
    }
    
    private func stopTrackingReviews() {
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("reviewList").removeAllObservers()
    }
    
    private func loadNumberOfGoings() {
        guard let id = profile?.getId() else { return }
        
        stopTrackingNumberOfGoings()
        
        Database.database().reference().child("users").child(id).child("numberOfGoings").observe(
            .value,
            with: { (snapshot) in
                if snapshot.exists() {
                    if let numberOfGoings = snapshot.value as? Int {
                        ProfileService.shared.profile?.numberOfGoings = numberOfGoings
                        NotificationCenter.default.post(name: Notifications.ProfileLoaded, object: self)
                    }
                }
            },
            withCancel: { (error) in
                // TODO: --
            })
        
    }
    
    private func stopTrackingNumberOfGoings() {
        guard let id = profile?.getId() else { return }
        Database.database().reference().child("users").child(id).child("numberOfGoings").removeAllObservers()
    }
    
    // MARK: - load profiles
    
    func loadProfile(withId userId: String, complete:((_ profile: Profile?, _ errorMsg: String?)->())?) {
        
        Database.database().reference().child("users").child(userId).observeSingleEvent(
            of: .value,
            with: { (snapshot) in
                if snapshot.exists(),
                    let profile = Profile.fromSnapshot(snapshot) {
                    complete?(profile, nil)
                } else {
                    complete?(nil, "Profile does not exist")
                }
            },
            withCancel: { (error) in
                complete?(nil, error.localizedDescription)
            })
    }
    
    // Profile view
    
    func userModel() -> UserViewModel? {
        guard let profile = profile else { return nil }
        return UserViewModel(profile: profile)
    }

}
