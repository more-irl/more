//
//  UserSignals.swift
//  More
//
//  Created by Luko Gjenero on 06/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

class UserSignals: MoreDataObject {

    let id: String = ""
    let active: [Signal]
    let past: [Signal]
    
    init(active: [Signal],
         past: [Signal]) {
        self.active = active
        self.past = past
    }
    
    func userSignals(withActive active: [Signal], andPast past: [Signal]) -> UserSignals {
        return UserSignals(active: active,
                           past: past)
    }
    
    // Hashable
    
    var hashValue: Int {
        return active.hashValue + past.hashValue
    }
    
    static func == (lhs: UserSignals, rhs: UserSignals) -> Bool {
        return lhs.active.first { !rhs.active.contains($0) } == nil &&
            rhs.active.first { !lhs.active.contains($0) } == nil &&
            lhs.past.first { !rhs.past.contains($0) } == nil &&
            rhs.past.first { !lhs.past.contains($0) } == nil
    }
    
    // JSON
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                json?.removeValue(forKey: "active")
                json?.removeValue(forKey: "past")
                
                if let activeList = UserSignals.convertList(active) {
                    json?["activeList"] = activeList
                }
                if let pastList = UserSignals.convertList(past) {
                    json?["pastList"] = pastList
                }
                
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String: Any]) -> UserSignals? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let userSignals = try? JSONDecoder().decode(UserSignals.self, from: jsonData) {
            
            let active: [Signal] = convertListBack(json["activeList"] as? [String: Any]) ?? []
            let past: [Signal] = convertListBack(json["pastList"] as? [String: Any]) ?? []
            
            return userSignals.userSignals(withActive: active, andPast: past)
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> UserSignals? {
        if let json = snapshot.value as? [String: Any] {
            return UserSignals.fromJson(json)
        }
        return nil
    }
    
}
