//
//  LocationService.swift
//  geoFireTut
//
//  Created by Anirudh Bandi on 6/25/18.
//  Copyright © 2018 More Technologies. All rights reserved.
//


import CoreLocation
import Foundation
import Firebase

class LocationService: NSObject, CLLocationManagerDelegate {

    struct Notifications {
        static let LocationUpdate = NSNotification.Name(rawValue: "com.more.location.update")
        static let LocationError = NSNotification.Name(rawValue: "com.more.location.error")
        static let LocationPermissionChange = NSNotification.Name(rawValue: "com.more.location.premission")
        static let LocationTest = NSNotification.Name(rawValue: "com.more.location.test")
    }
    
    static var locationEnabled: Bool {
        get {
            if !CLLocationManager.locationServicesEnabled() {
                return false
            }
            switch CLLocationManager.authorizationStatus() {
            case .authorizedWhenInUse, .authorizedAlways:
                return true
            default:
                return false
            }
        }
    }
    
    static var canRequestLocation: Bool {
        get {
            if !CLLocationManager.locationServicesEnabled() {
                return false
            }
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                return true
            default:
                return false
            }
        }
    }
    
    static let shared = LocationService()
    
    private let locationManager = CLLocationManager()

    private(set) var currentLocation: CLLocation?
    private(set) var currentHeading: CLHeading?
    
    private override init() {
        super.init()
        
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.activityType = .fitness
        locationManager.delegate = self
        
        if LocationService.locationEnabled {
            if UIApplication.shared.applicationState == .background {
                locationManager.startMonitoringSignificantLocationChanges()
            }
            locationManager.startUpdatingLocation()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    @objc private func toForeground() {
        if LocationService.locationEnabled {
            if !testLocation {
                locationManager.startUpdatingLocation()
            }
        }
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    @objc private func toBackground() {
        if stopUpdatesIfNotNeeded() {
            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalNever)
        } else {
            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
            if !testLocation {
                locationManager.startMonitoringSignificantLocationChanges()
            }
        }
    }
    
    @discardableResult
    private func stopUpdatesIfNotNeeded() -> Bool {
        var disable = true
        for time in TimesService.shared.getTrackedTimes() {
            if time.shouldTrackLocation() {
                disable = false
                break
            }
        }
        if let time = TimesService.shared.getActiveTime() {
            if time.shouldTrackLocation() {
                disable = false
            }
        }
        
        if disable {
            for signal in SignalTrackingService.shared.getOwnActiveSignals() {
                if signal.isMine() && signal.finalExpiration() > Date() {
                    disable = false
                    break
                }
            }
        }
        
        if disable {
            locationManager.stopUpdatingLocation()
        }
        
        return disable
    }
    
    func requestAlways() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func requestWhenInUse() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
        NotificationCenter.default.post(name: Notifications.LocationPermissionChange, object: self, userInfo: ["premission": status])
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NotificationCenter.default.post(name: Notifications.LocationError, object: self, userInfo: ["error": error])
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location else { return }
        currentLocation = location
        NotificationCenter.default.post(name: Notifications.LocationUpdate, object: self, userInfo: ["location": location])
        if UIApplication.shared.applicationState == .background {
            stopUpdatesIfNotNeeded()
        }
    }
    
    // MARK: - testing
    
    private(set) var testLocation: Bool = false
    
    func testLocation(_ location: CLLocationCoordinate2D) {
        if testLocation == false {
            testLocation = true
            NotificationCenter.default.post(name: Notifications.LocationTest, object: self)
        }
        locationManager.stopUpdatingLocation()
        currentLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        NotificationCenter.default.post(name: Notifications.LocationUpdate, object: self, userInfo: ["location": location])
    }
}
