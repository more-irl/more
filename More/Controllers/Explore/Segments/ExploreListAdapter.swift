//
//  ExploreListAdapter.swift
//  More
//
//  Created by Luko Gjenero on 31/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import IGListKit

class ExploreListAdapter: ListAdapter, ListAdapterDataSource {

    let padding: CGFloat = 32
    var cellSize: CGSize {
        if let collectionView = collectionView {
            return CGSize(width: collectionView.frame.width - 2 * padding, height: collectionView.frame.height)
        }
        return .zero
    }
    var topBarScale: CGFloat {
        if let collectionView = collectionView {
            return cellSize.width / collectionView.frame.width
        }
        return 0
    }
    
    var reload: (()->())?
    var newSignals: (()->())?
    var scrolled: (()->())?
    var selected: ((_ model: SignalViewModel)->())?
    
    private(set) var loading: Bool = true
    private(set) var panEnabled: Bool = true
    private(set) var filterTypes: [SignalType] = []
    private(set) var models: [SignalViewModel] = []
    private(set) var filteredModels: [SignalViewModel] = []
    
    private var transitionState: ExploreCollectionViewLayout.TransitionState = .add
    
    override var collectionView: UICollectionView? {
        didSet {
            collectionView?.contentInset = UIEdgeInsetsMake(0, padding, 0, padding)
            collectionView?.decelerationRate = UIScrollViewDecelerationRateFast
            collectionView?.delaysContentTouches = false
            
            if let layout = collectionView?.collectionViewLayout as? ExploreCollectionViewLayout {
                layout.exploreCollectionLayoutDelegate = self
            }
        }
    }
    
    override init(updater: ListUpdatingDelegate = ListAdapterUpdater(), viewController: UIViewController? = nil, workingRangeSize: Int = 0) {
        super.init(updater: updater, viewController: viewController, workingRangeSize: workingRangeSize)
        startTracking()
        dataSource = self
        scrollViewDelegate = self
    }
    
    deinit {
        SignalingService.shared.stopTrackingAroundMe()
    }
    
    func filter(_ types: [SignalType]) {
        filterTypes = types
        if filterTypes.count > 0 {
            self.filteredModels = models.filter { filterTypes.contains($0.type) }
        } else {
            self.filteredModels = models
        }
        reloadUI()
    }
    
    func addSignal(_ signal: Signal) {
        if signal.kind == .standard {
            SignalTrackingService.shared.trackActive(signalId: signal.id)
        }
        let model = SignalViewModel(signal: signal)
        addSignalModel(model)
        newSignals?()
    }
    
    func addSignalModel(_ model: SignalViewModel) {
        models.insert(model, at: 0)
        if filterTypes.count == 0 || filterTypes.contains(model.type) {
            filteredModels.insert(model, at: 0)
            transitionState = .add
            reloadUI()
        }
    }
    
    func removeSignal(id: String) {
        if let idx = models.firstIndex(where: { $0.id == id }) {
            models.remove(at: idx)
        }
        if let idx = filteredModels.firstIndex(where: { $0.id == id }) {
            filteredModels.remove(at: idx)
            transitionState = idx >= filteredModels.count ? .removeEnd : .remove
            reloadUI()
        }
    }
    
    func refreshSignal(_ signal: Signal) {
        if let idx = models.firstIndex(where: { $0.id == signal.id }) {
            models.remove(at: idx)
            models.insert(SignalViewModel(signal: signal), at: idx)
        }
        if let idx = filteredModels.firstIndex(where: { $0.id == signal.id }) {
            filteredModels.remove(at: idx)
            filteredModels.insert(SignalViewModel(signal: signal), at: idx)
            transitionState = .refresh
            reloadUI()
        }
    }
    
    private func reloadUI() {
        performUpdates(animated: true)
        reload?()
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        if filteredModels.count == 0 && loading {
            return ["loading" as NSString]
        }
        return filteredModels
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let object = ExploreSectionController(topBarScale: topBarScale, size: cellSize)
        object.selected = { [weak self] (model) in
            self?.selected?(model)
        }
        return object
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        // TODO: - loading
        return nil
    }
    
}


extension ExploreListAdapter {
    
    private func startTracking() {
        NotificationCenter.default.addObserver(self, selector: #selector(signalsLoaded(_:)), name: SignalingService.Notifications.SignalsLoaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newSignal(_:)), name: SignalingService.Notifications.SignalEnteredRegion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRemoved(_:)), name: SignalingService.Notifications.SignalExitedRegion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRemoved(_:)), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRequest(_:)), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalResponse(_:)), name: SignalTrackingService.Notifications.SignalResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRefresh(_:)), name: SignalTrackingService.Notifications.SignalExpirationChanged, object: nil)
        
        SignalingService.shared.startTrackingAroundMe()
        initialLoad()
    }
    
    private func initialLoad() {
        models = SignalingService.shared.claimablesInRange.map { SignalViewModel(signal: $0) }
        filteredModels = models
    }
    
    @objc private func signalsLoaded(_ notice: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            if self?.loading == true {
                self?.loading = false
                self?.reloadUI()
            }
        }
    }
    
    @objc private func newSignal(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, signal.isMine() || signal.myRequest()?.accepted == nil {
                self?.addSignal(signal)
            }
        }
    }
    
    @objc private func signalRemoved(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String {
                self?.removeSignal(id: signalId)
            }
        }
    }
    
    @objc private func signalRequest(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, signal.myRequest() != nil {
                self?.removeSignal(id: signal.id)
            }
        }
    }
    
    @objc private func signalResponse(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, signal.myRequest()?.accepted == false {
                self?.removeSignal(id: signal.id)
            }
        }
    }
    
    @objc private func signalRefresh(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, signal.isMine() || signal.myRequest()?.accepted == nil {
                self?.refreshSignal(signal)
            }
        }
    }
}

extension ExploreListAdapter: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrolled?()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        panEnabled = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        panEnabled = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            panEnabled = true
        }
    }
}


extension ExploreListAdapter: ExploreCollectionLayoutDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, transitionForItemAt indexPath: IndexPath) -> ExploreCollectionViewLayout.TransitionState {
        
        if loading {
            return .firstLoad
        }
        return transitionState
    }
}
