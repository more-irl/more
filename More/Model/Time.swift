//
//  Time.swift
//  More
//
//  Created by Luko Gjenero on 10/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

// 2 weeks in seconds
private let reviewDeadline: TimeInterval = 1.21e+6

enum TimeState: String, Codable {
    case none, queryArrived, arrived, queryMet, met, cancelled, closed
}

struct Time: MoreDataObject {

    let id: String
    let signal: ShortSignal
    let requester: ShortUser
    let createdAt: Date
    let creatorState: TimeState?
    let creatorCancelMessage: String?
    let requesterState: TimeState?
    let requesterCancelMessage: String?
    let endedAt: Date?
    
    let meeting: Location?
    
    let messages: [Message]?
    
    let reviews: [Review]?
    
    init(id: String,
         signal: ShortSignal,
         requester: ShortUser,
         createdAt: Date,
         creatorState: TimeState? = nil,
         creatorCancelMessage: String?,
         requesterState: TimeState? = nil,
         requesterCancelMessage: String? = nil,
         endedAt: Date? = nil,
         meeting: Location? = nil,
         messages: [Message]? = nil,
         reviews: [Review]? = nil) {
        
        self.id = id
        self.signal = signal
        self.requester = requester
        self.createdAt = createdAt
        self.creatorState = creatorState
        self.creatorCancelMessage = creatorCancelMessage
        self.requesterState = requesterState
        self.requesterCancelMessage = requesterCancelMessage
        self.endedAt = endedAt
        self.meeting = meeting
        self.messages = messages
        self.reviews = reviews
    }
    
    func timeWithCreatorState(_ creatorState: TimeState?) -> Time {
        return Time(
            id: id,
            signal: signal,
            requester: requester,
            createdAt: createdAt,
            creatorState: creatorState,
            creatorCancelMessage: creatorCancelMessage,
            requesterState: requesterState,
            requesterCancelMessage: requesterCancelMessage,
            endedAt: endedAt,
            meeting: meeting,
            messages: messages,
            reviews: reviews)
    }
    
    func timeWithRequesterState(_ requesterState: TimeState?) -> Time {
        return Time(
            id: id,
            signal: signal,
            requester: requester,
            createdAt: createdAt,
            creatorState: creatorState,
            creatorCancelMessage: creatorCancelMessage,
            requesterState: requesterState,
            requesterCancelMessage: requesterCancelMessage,
            endedAt: endedAt,
            meeting: meeting,
            messages: messages,
            reviews: reviews)
    }
    
    func timeWithMessages(_ messages: [Message]?) -> Time {
        return Time(
            id: id,
            signal: signal,
            requester: requester,
            createdAt: createdAt,
            creatorState: creatorState,
            creatorCancelMessage: creatorCancelMessage,
            requesterState: requesterState,
            requesterCancelMessage: requesterCancelMessage,
            endedAt: endedAt,
            meeting: meeting,
            messages: messages,
            reviews: reviews)
    }
    
    func timeWithReviews(_ reviews: [Review]) -> Time {
        return Time(
            id: id,
            signal: signal,
            requester: requester,
            createdAt: createdAt,
            creatorState: creatorState,
            creatorCancelMessage: creatorCancelMessage,
            requesterState: requesterState,
            requesterCancelMessage: requesterCancelMessage,
            endedAt: endedAt,
            meeting: meeting,
            messages: messages,
            reviews: reviews)
    }
    
    func timeWithMeeting(_ meeting: Location) -> Time {
        return Time(
            id: id,
            signal: signal,
            requester: requester,
            createdAt: createdAt,
            creatorState: creatorState,
            creatorCancelMessage: creatorCancelMessage,
            requesterState: requesterState,
            requesterCancelMessage: requesterCancelMessage,
            endedAt: endedAt,
            meeting: meeting,
            messages: messages,
            reviews: reviews)
    }
    
    func timeWithEndedAt(_ endedAt: Date) -> Time {
        return Time(
            id: id,
            signal: signal,
            requester: requester,
            createdAt: createdAt,
            creatorState: creatorState,
            creatorCancelMessage: creatorCancelMessage,
            requesterState: requesterState,
            requesterCancelMessage: requesterCancelMessage,
            endedAt: endedAt,
            meeting: meeting,
            messages: messages,
            reviews: reviews)
    }
    
    func myReview() -> Review? {
        return reviews?.first(where: { $0.creator.isMe() })
    }
    
    func otherReview() -> Review? {
        return reviews?.first(where: { $0.creator.id == otherPerson().id })
    }
    
    func haveReviewed() -> Bool {
        return myReview() != nil
    }
    
    func otherHasReviewed() -> Bool {
        return otherReview() != nil
    }
    
    func reviewsUnlocked() -> Bool {
        return (haveReviewed() && otherHasReviewed()) || finalExpiration() < Date()
    }
    
    func finalExpiration() -> Date {
        
        if !(haveReviewed() && otherHasReviewed()) {
            return createdAt.addingTimeInterval(reviewDeadline)
        }
        return createdAt
    }
    
    func otherPerson() -> ShortUser {
        if isMine() {
            return requester
        }
        return signal.creator
    }
    
    func state() -> TimeState {
        if isMine() {
            return creatorState ?? .none
        }
        return requesterState ?? .none
    }
    
    func otherState() -> TimeState {
        if isMine() {
            return requesterState ?? .none
        }
        return creatorState ?? .none
    }
    
    func isMine() -> Bool {
        return signal.creator.isMe()
    }
    
    func shouldTrackLocation() -> Bool {
        return state() != .met && otherState() != .met &&
            state() != .cancelled && otherState() != .cancelled &&
            state() != .closed && otherState() != .closed &&
            (endedAt == nil || endedAt! > Date())
    }
    
    func otherCancelMessage() -> String? {
        if isMine() {
            return requesterCancelMessage
        }
        return creatorCancelMessage
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: Time, rhs: Time) -> Bool {
        return lhs.id == rhs.id
    }
    
    // JSON
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                json?.removeValue(forKey: "messages")
                json?.removeValue(forKey: "reviews")
                
                if let messageList = Time.convertList(messages) {
                    json?["messageList"] = messageList
                }
                
                if let reviewList = Time.convertList(reviews) {
                    json?["reviewList"] = reviewList
                }
                
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String: Any]) -> Time? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            var time = try? JSONDecoder().decode(Time.self, from: jsonData) {
            
            let messages: [Message] = Time.convertListBack(json["messageList"] as? [String: Any]) ?? []
            time = time.timeWithMessages(messages)
            
            let reviews: [Review] = Time.convertListBack(json["reviewList"] as? [String: Any]) ?? []
            time = time.timeWithReviews(reviews)
            
            return time
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> Time? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return Time.fromJson(json)
        }
        return nil
    }
    
}
