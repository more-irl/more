//
//  SignalDetailsTransition.swift
//  More
//
//  Created by Luko Gjenero on 17/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

protocol SignalDetailsPresenter {
    func prepareToPresentSignalDetails() -> UIImageView?
    func prepareToDismissSignalDetails() -> UIImageView?
    func cardFrame() -> CGRect
}

protocol SignalDetailsView {
    var model: SignalViewModel? { get }
    func photosFrame() -> CGRect
    func prepareToPresentSignalDetails(size: CGSize) -> CGRect
    func prepareToDismissSignalDetails(duration: TimeInterval, complete: @escaping (_ frame: CGRect)->())
}

class SignalDetailsTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    let presentingDuration = 0.3
    let dismissingDuration = 0.5
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        if presenting(using: transitionContext) == false {
            return dismissingDuration
        }
        return presentingDuration
    }
    
    func presenting(using transitionContext: UIViewControllerContextTransitioning?) -> Bool? {
        return transitionContext?.viewController(forKey: .to) is SignalDetailsView
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let presenting = self.presenting(using: transitionContext)
        if presenting == true {
            if let presenter = transitionContext.viewController(forKey: .from) as? SignalDetailsPresenter {
                if let imageView = presenter.prepareToPresentSignalDetails() {
                    present(using: transitionContext, image: imageView)
                } else {
                    fadeIn(using: transitionContext)
                }
            } else {
                fadeIn(using: transitionContext)
            }
        } else if presenting == false {
            if let presenter = transitionContext.viewController(forKey: .to) as? SignalDetailsPresenter {
                if let imageView = presenter.prepareToDismissSignalDetails() {
                    dismiss(using: transitionContext, image: imageView)
                } else {
                    fadeOut(using: transitionContext)
                }
            } else {
                fadeOut(using: transitionContext)
            }
        } else {
            fadeIn(using: transitionContext)
        }
        
    }
    
    func fadeIn(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        let to = transitionContext.view(forKey: .to)!
        
        to.frame = containerView.bounds
        containerView.addSubview(to)
        to.alpha = 0.0
        UIView.animate(
            withDuration: presentingDuration,
            animations: {
                to.alpha = 1.0
            },
            completion: { _ in
                containerView.isUserInteractionEnabled = true
                transitionContext.completeTransition(true)
            })
    }
    
    func fadeOut(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        let to = transitionContext.view(forKey: .to)!
        let from = transitionContext.view(forKey: .from)!
        
        to.frame = containerView.bounds
        containerView.insertSubview(to, at: 0)
        UIView.animate(
            withDuration: presentingDuration,
            animations: {
                from.alpha = 0.0
            },
            completion: { _ in
                containerView.isUserInteractionEnabled = true
                transitionContext.completeTransition(true)
            })
        
    }
    
    private func present(using transitionContext: UIViewControllerContextTransitioning, image: UIImageView) {
        
        let containerView = transitionContext.containerView
        let to = transitionContext.viewController(forKey: .to)!
        let toProtocol = to as? SignalDetailsView
        
        let startFrame = image.convert(image.bounds, to: containerView)
        let endFrame = toProtocol?.prepareToPresentSignalDetails(size: containerView.bounds.size) ?? containerView.bounds
        
        // math :)
        let offset = CGPoint(x: 0, y: startFrame.midY - endFrame.midY)
        let scale = startFrame.width / endFrame.width
        var transform = CATransform3DMakeTranslation(offset.x, offset.y, 0)
        transform = CATransform3DScale(transform, scale, scale, 1)
        
        // image view
        let animationImage = UIImageView(image: image.image)
        animationImage.clipsToBounds = true
        animationImage.backgroundColor = Colors.background
        animationImage.contentMode = .scaleAspectFill
        
        // start
        animationImage.frame = startFrame
        to.view.alpha = 0
        to.view.frame = containerView.bounds
        to.view.layer.transform = transform
        
        // add views
        containerView.addSubview(animationImage)
        containerView.addSubview(to.view)
        
        // animation
        UIView.animate(
            withDuration: presentingDuration,
            animations: {
                animationImage.alpha = 0.0
                animationImage.frame = endFrame
                to.view.alpha = 1
                to.view.layer.transform = CATransform3DIdentity
            },
            completion: { _ in
                animationImage.removeFromSuperview()
                transitionContext.completeTransition(true)
            })
        
    }
    
    private func dismiss(using transitionContext: UIViewControllerContextTransitioning, image: UIImageView) {
        
        let duration = dismissingDuration - 0.2
        guard let fromProtocol = transitionContext.viewController(forKey: .from) as? SignalDetailsView else {
            fadeOut(using: transitionContext)
            return
        }
        
        fromProtocol.prepareToDismissSignalDetails(duration: 0.2) { (startFrame) in
            
            let containerView = transitionContext.containerView
            let from = transitionContext.viewController(forKey: .from)!
            let to = transitionContext.viewController(forKey: .to)!
            
            let endFrame = image.convert(image.bounds, to: containerView)
            
            // math :)
            let offset = CGPoint(x: 0, y: endFrame.midY - startFrame.midY)
            let scale = endFrame.width / startFrame.width
            var transform = CATransform3DMakeTranslation(offset.x, offset.y, 0)
            transform = CATransform3DScale(transform, scale, scale, 1)
            
            // image view
            let animationImage = UIImageView(image: image.image)
            animationImage.clipsToBounds = true
            animationImage.backgroundColor = Colors.background
            animationImage.contentMode = .scaleAspectFill
            
            // start
            animationImage.frame = startFrame
            from.view.alpha = 1
            from.view.frame = containerView.bounds
            
            // add views
            containerView.insertSubview(to.view, at: 0)
            containerView.addSubview(animationImage)
            
            // animation
            UIView.animate(
                withDuration: duration,
                animations: {
                    animationImage.alpha = 0.0
                    animationImage.frame = endFrame
                    from.view.alpha = 0
                    from.view.layer.transform = transform
                },
                completion: { _ in
                    animationImage.removeFromSuperview()
                    transitionContext.completeTransition(true)
                })
            
        }
    }
    

}
