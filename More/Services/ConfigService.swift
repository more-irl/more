//
//  ConfigService.swift
//  More
//
//  Created by Luko Gjenero on 14/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig


class ConfigService: NSObject {

    static let shared = ConfigService()
    
    private var remoteConfig = RemoteConfig.remoteConfig()
    
    struct Notifications {
        static let ConfigLoaded = NSNotification.Name(rawValue: "com.more.config.loaded")
    }
    
    // MARK: - properties
    
    @objc dynamic var signalExpiration: TimeInterval = 1800
    @objc dynamic var signalRequestExpiration: TimeInterval = 300
    @objc dynamic var signalSearchRadius: Double = 10000
    
    // MARK: - init
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        toForeground()
    }
    
    @objc private func toForeground() {
        fetch(nil)
    }
    
    @objc private func toBackground() {
        // nothing
    }
    
    // MARK: - setup variables
    
    private func setup() {
        
        // configs
        let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: false)
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        applyRemoteConfig()
    }
    
    // MARK: - remote config
    
    private var isFetching = false
    
    func fetch(_ completeBlock: ((_ success: Bool) -> Void)?) {
        
        var proceed = true
        synced(self) {
            if self.isFetching {
                proceed = false
            } else {
                self.isFetching = true
            }
        }
        
        if proceed {
            
            // cache expiration
            let cacheExpiration: Double = 21600 // 6 hours
            remoteConfig.fetch(withExpirationDuration: cacheExpiration) {
                [weak self] (status, error) in
                if status == .success {
                    self?.remoteConfig.activateFetched()
                    self?.applyRemoteConfig()
                    
                    if let completeBlock = completeBlock {
                        completeBlock(true)
                    }
                    
                    NotificationCenter.default.post(name: Notifications.ConfigLoaded, object: self)
                } else {
                    if let completeBlock = completeBlock {
                        completeBlock(false)
                    }
                }
                
                if let sself = self {
                    synced(sself) {
                        sself.isFetching = false
                    }
                }
            }
        }

    }
    
    private func applyRemoteConfig() {
        for case let (label?, value) in Mirror(reflecting: self).children {
            if !nonConfigProperties.contains(label) {
                switch String(describing: type(of: value)) {
                case "__NSCFBoolean", "Swift.Bool", "Bool":
                    self.setValue(remoteConfig[label].boolValue, forKey: label)
                    break
                case "__NSCFNumber", "Swift.Int", "Int":
                    self.setValue(remoteConfig[label].numberValue, forKey: label)
                    break
                default: // String
                    self.setValue(remoteConfig[label].stringValue!, forKey: label)
                    break
                }
            }
        }
    }
    
    // MARK: - helper
    
    private let nonConfigProperties: [String] =
        ["shared",
         "remoteConfig",
         "isFetching",
         "nonConfigProperties"]
}
