//
//  TimesViewController.swift
//  More
//
//  Created by Luko Gjenero on 04/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

private let signalCell = String(describing: TimesOwnSignalCell.self)
private let requestCell = String(describing: TimesOwnRequestCell.self)
private let pastCell = String(describing: TimesPastTimeCell.self)
private let claimableCell = String(describing: TimesClaimableCell.self)

class TimesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private struct PastRow: Hashable {
        let time: Time?
        let review: Review?
        
        // Hashable
        
        var hashValue: Int {
            return time?.id.hashValue ?? review?.id.hashValue ?? 0
        }
        
        var timestamp: Date {
            if let time = time {
                return time.endedAt ?? time.createdAt
            }
            if let review = review {
                return review.time?.endedAt ?? review.time?.createdAt ?? review.createdAt
            }
            return Date(timeIntervalSince1970: 0)
        }
        
        static func == (lhs: PastRow, rhs: PastRow) -> Bool {
            if let lt = lhs.time, let rt = rhs.time {
                return lt.id == rt.id
            }
            if let lr = lhs.review, let rr = rhs.review {
                return lr.id == rr.id
            }
            return false
        }
    }
    
    @IBOutlet private weak var tabBar: UnderlineTabBar!
    @IBOutlet private weak var tableContainer: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyView: TimesEmptyView!
    
    private var presentRows: [SignalViewModel] = []
    private var pastRows: [PastRow] = []
    private var presentHeights: [SignalViewModel: CGFloat] = [:]
    private var pastHeights: [PastRow: CGFloat] = [:]
    
    var insets: UIEdgeInsets {
        get {
            return tableView.contentInset
        }
        set {
            tableView.contentInset = insets
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.font = UIFont(name: "Gotham-Medium", size: 16) ?? UIFont.systemFont(ofSize: 16)
        tabBar.items = [
            UnderlineTabBar.Item(id: 1, title: "Past"),
            UnderlineTabBar.Item(id: 2, title: "Present")
        ]
        tabBar.selectedIndex = 1
        tabBar.itemSelected = { [weak self] (item) in
            switch item.id {
            case 1:
                self?.switchToPast(animated: true)
            case 2:
                self?.switchToPresent(animated: true)
            default:
                ()
            }
        }
        
        emptyView.emptyType = .present
        
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.insetsContentViewsToSafeArea = false
        
        let header = TimesListHeader()
        header.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60)
        tableView.tableHeaderView = header
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Dummy")
        tableView.register(UINib(nibName: signalCell, bundle: nil), forCellReuseIdentifier: signalCell)
        tableView.register(UINib(nibName: requestCell, bundle: nil), forCellReuseIdentifier: requestCell)
        tableView.register(UINib(nibName: pastCell, bundle: nil), forCellReuseIdentifier: pastCell)
        tableView.register(UINib(nibName: claimableCell, bundle: nil), forCellReuseIdentifier: claimableCell)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setup(for rows: [SignalViewModel]) {
        self.presentRows = rows
        tableView.reloadData()
        emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
    }
    
    private var isFirst: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        startTracking()
    }
    
    // MARK: - Data tracking
    
    private func startTracking() {
        
        initialDataLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(newSignal(_:)), name: SignalTrackingService.Notifications.SignalLoaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newRequest(_:)), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newResponse(_:)), name: SignalTrackingService.Notifications.SignalResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRemoved(_:)), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(claimedChanged(_:)), name: SignalingService.Notifications.ClaimedSignalsChanged, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(timeStateChanged(_:)), name: TimesService.Notifications.TimeStateChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(timeReview(_:)), name: TimesService.Notifications.TimeReview, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(profileLoaded(_:)), name: ProfileService.Notifications.ProfileLoaded, object: nil)
    }
    
    private func initialDataLoad() {
        initialPastDataLoad()
        initialPresentDataLoad()
        tableView.reloadData()
        emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
    }
    
    @objc private func newSignal(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, signal.isMine() || (signal.haveRequested() && !signal.wasRejected()) {
                self?.addSignal(signal)
            }
        }
    }
    
    @objc private func newRequest(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal {
                self?.addRequest(signal)
            }
        }
    }
    
    @objc private func newResponse(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, let response = notice.userInfo?["request"] as? Request {
                if response.accepted == false {
                    self?.removeSignal(id: signal.id)
                }
            }
        }
    }
    
    @objc private func signalRemoved(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String {
                self?.removeSignal(id: signalId)
            }
        }
    }
    
    @objc private func claimedChanged(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            self?.initialPresentDataLoad()
            if self?.tabBar.selectedIndex == 1 {
                self?.tableView.reloadData()
                self?.emptyView.isHidden = (self?.tableView.numberOfRows(inSection: 0) ?? 0) > 0
            }
        }
    }

    @objc private func timeStateChanged(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let time = notice.userInfo?["time"] as? Time,
                (time.state() == .met || time.state() == .closed) && (time.otherState() == .met || time.otherState() == .closed) {
                self?.addTime(time)
            }
        }
    }
    
    @objc private func timeReview(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let time = notice.userInfo?["time"] as? Time {
                self?.addTime(time)
            }
        }
    }
    
    @objc private func profileLoaded(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            self?.reloadReviews()
        }
    }
    
    // MARK: - past tab
    private func initialPastDataLoad() {
        
        let times = TimesService.shared.getTrackedTimes().filter { ($0.state() == .met || $0.state() == .closed) && ($0.otherState() == .met || $0.otherState() == .closed) }
        let reviews = ProfileService.shared.profile?.reviews ?? []
        
        pastRows = times.map { PastRow(time: $0, review: nil) } + reviews.map { PastRow(time: nil, review: $0) }
        pastRows.sort { (lhs, rhs) -> Bool in
            return lhs.timestamp > rhs.timestamp
        }
        
        /*
        times.sort { (lhs, rhs) -> Bool in
            return lhs.createdAt < rhs.createdAt
        }
        
        var reviews = ProfileService.shared.profile?.reviews ?? []
        reviews.sort { (lhs, rhs) -> Bool in
            return lhs.createdAt < rhs.createdAt
        }
        
        pastRows = times.map { PastRow(time: $0, review: nil) } + reviews.map { PastRow(time: nil, review: $0) }
        */
    }
    
    private func addTime(_ time: Time) {
        if time.haveReviewed() && time.otherHasReviewed() {
            pastRows.removeAll(where: { $0.time?.id == time.id })
            if tabBar.selectedIndex == 0 {
                tableView.reloadData()
                emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
            }
            return
        }
        if let idx = pastRows.firstIndex(where: { $0.time?.id == time.id }) {
            pastRows.remove(at: idx)
            pastRows.insert(PastRow(time: time, review: nil), at: idx)
        } else {
            pastRows.insert(PastRow(time: time, review: nil), at: 0)
        }
        if tabBar.selectedIndex == 0 {
            tableView.reloadData()
            emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
        }
    }
    
    private func reloadReviews() {
        var reviews = ProfileService.shared.profile?.reviews ?? []
        reviews.sort { (lhs, rhs) -> Bool in
            return lhs.createdAt < rhs.createdAt
        }
        pastRows.removeAll(where: { $0.review != nil })
        pastRows += reviews.map { PastRow(time: nil, review: $0) }
        if tabBar.selectedIndex == 0 {
            tableView.reloadData()
            emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
        }
    }
    
    // MARK: - present tab
    
    private func initialPresentDataLoad() {
        var signals = SignalTrackingService.shared.getOwnActiveSignals().filter { $0.kind == .standard } + SignalingService.shared.getOwnClaimables()
        signals.sort { (lhs, rhs) -> Bool in
            return lhs.finalExpiration() < rhs.finalExpiration()
        }
        
        presentRows = signals.map { SignalViewModel(signal: $0) }
    }
    
    private func addSignal(_ signal: Signal) {
        guard presentRows.first(where: { $0.id == signal.id }) == nil else { return }
        
        var insertIdx = presentRows.count
        for (idx,row) in presentRows.enumerated() {
            if row.expiresAt > signal.finalExpiration() {
                insertIdx = idx
                break
            }
        }
        
        presentRows.insert(SignalViewModel(signal: signal), at: insertIdx)
        
        if tabBar.selectedIndex == 1 {
            tableView.beginUpdates()
            tableView.insertRows(at: [IndexPath(row: insertIdx, section: 0)], with: .right)
            tableView.endUpdates()
            emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
        }
    }
    
    private func addRequest(_ signal: Signal) {
        if let idx = presentRows.firstIndex(where: { $0.id == signal.id }) {
            presentRows.remove(at: idx)
            presentRows.insert(SignalViewModel(signal: signal), at: idx)
            
            if tabBar.selectedIndex == 1 {
                tableView.beginUpdates()
                tableView.reloadRows(at: [IndexPath(row: idx, section: 0)], with: .fade)
                tableView.endUpdates()
                emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
            }
        }
    }
    
    private func removeSignal(id: String) {
        if let idx = presentRows.firstIndex(where: { $0.id == id }) {
            presentRows.remove(at: idx)
            
            if tabBar.selectedIndex == 1 {
                tableView.beginUpdates()
                tableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .left)
                tableView.endUpdates()
                emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
            }
        }
    }
    
    // MARK: - tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tabBar.selectedIndex == 1 {
            return presentRows.count
        }
        return pastRows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tabBar.selectedIndex == 1 && indexPath.row < presentRows.count {
            let rowModel = presentRows[indexPath.row]
            let identifier = rowModel.kind == .claimable ? claimableCell : rowModel.mine ? signalCell : requestCell
            if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? TimesBaseCell {
                cell.setup(for: rowModel)
                
                if let cell = cell as? TimesOwnSignalCell {
                    cell.requestTap = { [weak self] (request) in
                        self?.openRequest(request, in: rowModel)
                    }
                    cell.removeTap = { [weak self] in
                        self?.deleteSignal(rowModel.id)
                    }
                } else if let cell = cell as? TimesOwnRequestCell {
                    cell.removeTap = { [weak self] in
                        self?.removeRequest(in: rowModel)
                    }
                } else if let cell = cell as? TimesClaimableCell {
                    cell.removeTap = { [weak self] in
                        self?.deleteClaimableSignal(rowModel.id)
                    }
                    cell.userTap = { [weak self] user in
                        self?.presentUser(user.id)
                    }
                }
                
                return cell
            }
        } else if indexPath.row < pastRows.count {
            let rowModel = pastRows[indexPath.row]
            if let cell = tableView.dequeueReusableCell(withIdentifier: pastCell, for: indexPath) as? TimesPastTimeCell {
                
                if let time = rowModel.time {
                    cell.setup(for: time)
                } else if let review = rowModel.review {
                    cell.setup(for: review)
                }
                
                cell.writeReview = { [weak self] in
                    if let time = rowModel.time, let tc = self?.tabBarController as? MoreTabBarController {
                        tc.presentReview(for: time)
                    }
                }
                cell.viewTime = { [weak self] in
                    if let time = rowModel.time {
                        self?.presentSignal(time.signal.id)
                    } else if let time = rowModel.review?.time {
                        self?.presentSignal(time.signal.id)
                    }
                }
                cell.viewUser = { [weak self] in
                    if let time = rowModel.time {
                        self?.presentUser(time.otherPerson().id)
                    } else if let user = rowModel.review?.creator {
                        self?.presentUser(user.id)
                    }
                }
                
                return cell
            }
            
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "Dummy", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tabBar.selectedIndex == 1 && indexPath.row < presentRows.count {
            return presentHeight(for: presentRows[indexPath.row])
        } else if indexPath.row < pastRows.count {
            return pastHeight(for: pastRows[indexPath.row])
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tabBar.selectedIndex == 1 && indexPath.row < presentRows.count {
            let rowModel = presentRows[indexPath.row]
            openSignal(rowModel)
        } else if indexPath.row < pastRows.count {
            // let rowModel = pastRows[indexPath.row]
            // nothing for now
        }
    }
    
    // MARK: - height cache
    
    private func clearHeightCache() {
        presentHeights = [:]
        pastHeights = [:]
    }
    
    private func presentHeight(for model: SignalViewModel) -> CGFloat {
        
        if let height = presentHeights[model] {
            return height
        }
        
        let height = calculatePresentHeight(for: model)
        presentHeights[model] = height
        return height
    }
    
    private func calculatePresentHeight(for model: SignalViewModel) -> CGFloat {
        
        let identifier = model.mine ? signalCell : requestCell
        switch identifier {
        case requestCell:
            return TimesOwnRequestCell.size(for: model, in: tableView.frame.size).height
        case signalCell:
            return TimesOwnSignalCell.size(for: model, in: tableView.frame.size).height
        default:
            return 0
        }
    }
    
    private func pastHeight(for model: PastRow) -> CGFloat {
        
        if let height = pastHeights[model] {
            return height
        }
        
        let height = calculatePastHeight(for: model)
        pastHeights[model] = height
        return height
    }
    
    private func calculatePastHeight(for model: PastRow) -> CGFloat {
        if let time = model.time {
            return TimesPastTimeCell.size(for: time, in: tableView.frame.size).height
        } else if let review = model.review {
            return TimesPastTimeCell.size(for: review, in: tableView.frame.size).height
        }
        return 0
    }
    
    // MARK: - tabs
    
    private func switchToPast(animated: Bool) {
        var transition: CATransition!
        if animated {
            transition = CATransition()
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
        }
        
        tableView.reloadData()
        emptyView.emptyType = .past
        emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
    
        if animated {
            tableContainer.layer.add(transition, forKey: "push")
        }
    }
    
    private func switchToPresent(animated: Bool) {
        var transition: CATransition!
        if animated {
            transition = CATransition()
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
        }
        
        
        tableView.reloadData()
        emptyView.emptyType = .present
        emptyView.isHidden = tableView.numberOfRows(inSection: 0) > 0
        
        if animated {
            tableContainer.layer.add(transition, forKey: "push")
        }
    }
    
    // MARK: - api actions
    
    private func openSignal(_ signal: SignalViewModel) {
        let vc = SignalDetailsNavigationController()
        vc.back = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        vc.requested = { [weak self] in
            self?.dismiss(animated: true, completion: {
                self?.requested(for: signal)
            })
        }
        vc.removed = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        _ = vc.view
        vc.setup(for: signal)
        let nc = UINavigationController(rootViewController: vc)
        nc.setNavigationBarHidden(true, animated: false)
        nc.enableSwipeToPop()
        present(nc, animated: true, completion: nil)
    }
    
    private func openRequest(_ request: RequestViewModel, in signal: SignalViewModel) {
        let vc = RequestDetailsNavigationController()
        vc.back = {
            self.dismiss(animated: true, completion: nil)
        }
        _ = vc.view
        vc.setup(signal: signal, request: request)
        present(vc, animated: true, completion: nil)
    }
    
    private func deleteSignal(_ signalId: String) {
        let alert = UIAlertController(title: nil, message: "This Signal and related messages will be deleted. This cannot be undone.", preferredStyle: .actionSheet)
        
        let delete = UIAlertAction(title: "Delete Signal", style: .destructive, handler: { [weak self] _ in
            self?.deleteSignalProcess(signalId)
        })
        
        alert.addAction(delete)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func deleteSignalProcess(_ signalId: String) {
        showLoading()
        SignalingService.shared.deleteSignal(signalId: signalId, complete: { [weak self] (success, errorMsg) in
            self?.hideLoading()
            if !success {
                self?.errorAlert(text: errorMsg ?? "Unknown error")
            }
        })
    }
    
    private func deleteClaimableSignal(_ signalId: String) {
        let alert = UIAlertController(title: nil, message: "This Claimable Signal will be deleted. This cannot be undone.", preferredStyle: .actionSheet)
        
        let delete = UIAlertAction(title: "Delete Signal", style: .destructive, handler: { [weak self] _ in
            self?.deleteClaimableSignalProcess(signalId)
        })
        
        alert.addAction(delete)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func deleteClaimableSignalProcess(_ signalId: String) {
        showLoading()
        SignalingService.shared.deleteClaimableSignal(signalId: signalId, complete: { [weak self] (success, errorMsg) in
            self?.hideLoading()
            if success {
                self?.removeSignal(id: signalId)
            } else {
                self?.errorAlert(text: errorMsg ?? "Unknown error")
            }
        })
    }
    
    private func removeRequest(in signal: SignalViewModel) {
        let alert = UIAlertController(title: nil, message: "Your Request will be withdrawn, and you won’t be able to request this Time again.", preferredStyle: .actionSheet)
        
        let delete = UIAlertAction(title: "Withdraw Request", style: .destructive, handler: { [weak self] _ in
            self?.removeRequestProcess(in: signal)
        })
        
        alert.addAction(delete)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    private func removeRequestProcess(in signal: SignalViewModel) {
        guard let request = signal.requests.first(where: { $0.sender.id == ProfileService.shared.profile?.id }) else {
            return
        }
        
        showLoading()
        SignalingService.shared.cancelRequest(request.id, in: signal.id, complete: { [weak self] (success, errorMsg) in
            self?.hideLoading()
            if !success {
                self?.errorAlert(text: errorMsg ?? "Unknown error")
            }
        })
    }
    
}
