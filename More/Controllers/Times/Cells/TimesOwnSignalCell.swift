//
//  TimesOwnSignalCell.swift
//  More
//
//  Created by Luko Gjenero on 04/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

class TimesOwnSignalCell: TimesBaseCell {

    static let height: CGFloat = 365
    
    @IBOutlet private weak var content: UIView!
    @IBOutlet private weak var header: TimeHeader!
    @IBOutlet private weak var quote: UILabel!
    @IBOutlet private weak var requestCount: UILabel!
    @IBOutlet private weak var chooseLabel: UILabel!
    @IBOutlet private weak var requestsView: TimeRequestsView!
    @IBOutlet private weak var removeButton: UIButton!
    
    private var signalId: String = ""
    
    var requestTap: ((_ request: RequestViewModel)->())?
    var removeTap: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        requestsView.requestTap = { [weak self] (request) in
            self?.requestTap?(request)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.enableShadow(color: .black, path: UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height - 25)).cgPath)
    }
    
    @IBAction func removeTouch(_ sender: Any) {
        removeTap?()
    }
    
    // MARK: - base
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: TimesOwnSignalCell.height)
    }
    
    override func setup(for model: SignalViewModel) {
        signalId = model.id
        
        header.setup(for: model)
        quote.text = model.quote
        
        requestsView.setup(for: model)
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: SignalTrackingService.Notifications.SignalRequestExpired, object: nil)
        reload()
    }
    
    @objc private func reload() {
        let signal = SignalTrackingService.shared.updatedSignalData(for: signalId)
        let now = Date()
        let requests = signal?.requests?
            .filter { $0.expiresAt > now && $0.accepted == nil }
            .map { RequestViewModel(request: $0) } ?? []
        if requests.count > 0 {
            if requests.count > 1 {
                requestCount.text = "\(requests.count) REQUESTS"
            } else {
                requestCount.text = "1 REQUEST"
            }
            chooseLabel.isHidden = false
        } else {
            requestCount.text = "NO REQUEST YET"
            chooseLabel.isHidden = true
        }
    }
}
