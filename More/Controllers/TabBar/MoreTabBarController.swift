//
//  MoreTabBarController.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import Firebase
import SwiftMessages

class MoreTabBarController: UITabBarController {
    
    struct Notifications {
        static let MuteTimeAlerts = NSNotification.Name(rawValue: "com.more.tabbar.muteTimeAlerts")
        static let MuteSignalAlerts = NSNotification.Name(rawValue: "com.more.tabbar.muteTimeAlerts")
        static let MuteRequestAlerts = NSNotification.Name(rawValue: "com.more.tabbar.muteRequestAlerts")
    }
    
    static let height: CGFloat = 90
    
    private var moreTabBar: TabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tabBar.isHidden = true
        
        addTabBar()
        addControllers()
        
        trackTimes()
        
        trackPNs()
        
        trackAlerts()
        
        trackConnectionState()
        
        trackWindows()
        
        trackMuting()
    }
    
    private var isFirst = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // DebugOverlay.show()
        
        guard isFirst else { return }
        isFirst = false
 
        /*
        ProfileService.shared.loadProfile(withId: "1HDa0YsbH6TxBmqWpWH5knK92Dq2") { [weak self] (user, _) in
            guard let user = user else { return }
            let vc = ProfileViewController()
            vc.backTap = {
                self?.dismiss(animated: true, completion: nil)
            }
            _ = vc.view
            vc.setup(for: UserViewModel(profile: user))
            vc.bottomPadding = 0
            
            self?.present(vc, animated: true, completion: nil)
            
        }
        */
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func addControllers() {
        var controllers: [UIViewController] = []
        controllers.append(addHome())
        controllers.append(addTimes())
        controllers.append(addAlerts())
        controllers.append(addProfile())
        
        setViewControllers(controllers, animated: false)
        selectedIndex = 0
        moreTabBar.home.isSelected = true
    }
    
    private func addTabBar() {
        let tabBar = TabBar()
        tabBar.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tabBar)
        tabBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tabBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tabBar.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tabBar.heightAnchor.constraint(equalToConstant: MoreTabBarController.height).isActive = true
        view.layoutIfNeeded()
        
        tabBar.tabSelected = { [weak self] (item) in
            self?.processTabItem(item)
        }
        
        moreTabBar = tabBar
    }
    
    private func addHome() -> UIViewController {
        let nc = MoreTabBarNestedNavigationController()
        let vc = ExploreViewController()
        nc.viewControllers = [vc]
        return nc
    }
    
    private func addTimes() -> UIViewController {
        let nc = MoreTabBarNestedNavigationController()
        _ = nc.view
        nc.hideNavigation(hide: true, animated: false)
        let vc = TimesViewController()
        nc.viewControllers = [vc]
        return nc
    }
    
    private func addAlerts() -> UIViewController {
        let nc = MoreTabBarNestedNavigationController()
        let vc = AlertsViewController()
        nc.viewControllers = [vc]
        return nc
    }
    
    private func addProfile() -> UIViewController {
        let nc = MoreTabBarNestedNavigationController()
        _ = nc.view
        nc.hideNavigation(hide: true, animated: false)
        let vc = OwnProfileViewController()
        nc.viewControllers = [vc]
        return nc
    }

    private func processTabItem(_ item: TabBarItem.ButtonType) {
        switch item {
        case .home:
            selectedIndex = 0
        case .times:
            selectedIndex = 1
        case .signal:
            LocationService.shared.requestAlways()
            let vc = CreateSignalViewController()
            vc.closeTap = { [weak self] (signal) in
                
                /*
                let transition: CATransition = CATransition()
                transition.duration = 0.3
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFade
                transition.subtype = kCATransitionFromBottom
                self?.view.window!.layer.add(transition, forKey: nil)
                self?.dismiss(animated: false, completion: nil)
                if let signal = signal {
                    self?.requested(for: SignalViewModel(signal: signal))
                }
                */
                
                
                self?.dismiss(animated: true, completion: {
                    if let signal = signal {
                        self?.requested(for: SignalViewModel(signal: signal))
                    }
                })
                
            }
            present(vc, animated: true, completion: nil)
        case .alerts:
            selectedIndex = 2
            AlertService.shared.allRead()
            moreTabBar.alerts.isHighlighed = false
        case .profile:
            selectedIndex = 3
        default:
            ()
        }
    }
    
    
    // MARK: - tracking alerts
    
    private func trackAlerts() {
        NotificationCenter.default.addObserver(self, selector: #selector(newAlert(_:)), name: AlertService.Notifications.NewAlert, object: nil)
    }
    
    @objc private func newAlert(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let alert = notice.userInfo?["alert"] as? AlertService.AlertItem {
                
                self?.presentAlert(alert)
                self?.highlightAlertTabIfNeeded()
            }
        }
    }
    
    private func presentAlert(_ alert: AlertService.AlertItem) {
        // guard !(presentedViewController is EnRouteViewController) else { return }
        
        let view: AlertMessageView = try! SwiftMessages.viewFromNib()
        view.configureTheme(backgroundColor: UIColor(red: 244, green: 244, blue: 244), foregroundColor: .clear)
        view.button?.backgroundColor = .clear
    
        switch alert.type {
        case .request, .response:
            view.content.setupForRequest(alert.request!, in: alert.signal!)
        case .review:
            view.content.setupForReview(in: alert.time!)
        case .signalMessage:
            if mutedSignals.contains(alert.signal!.id) { return }
            let combined = MoreTabBarController.requestCombinedId(alert.signal!.id, alert.request?.id ?? "")
            if mutedRequests.contains(combined) { return }
            view.content.setupForMessgae(alert.message!, in: alert.signal!)
        case .timeMessage:
            if mutedTimes.contains(alert.signal!.id) { return }
            view.content.setupForMessgae(alert.message!, in: alert.time!)
        case .welcome:
            view.content.setupForWelcomeVideo()
        }
        
        view.tapHandler = { [weak self] _ in
            self?.processAlert(alert)
        }
    
        SwiftMessages.show(view: view)
    }
    
    private func highlightAlertTabIfNeeded() {
        if moreTabBar.alerts.isSelected {
            AlertService.shared.allRead()
        } else {
            moreTabBar.alerts.isHighlighed = AlertService.shared.hasNewAlerts
        }
    }
    
    func processAlert(_ alert: AlertService.AlertItem) {
        switch alert.type {
        case .request, .response:
            showSignal(signalId: alert.signal!.id, requestId: alert.request!.id)
        case .review:
            presentReview(for: alert.time!)
        case .signalMessage:
            showSignal(signalId: alert.signal!.id, requestId: alert.request!.id)
        case .timeMessage:
            if let vc = presentedViewController as? EnRouteViewController {
                if vc.isPresentingTime(withId: alert.time!.id) {
                    vc.openMessages()
                }
            }
        case .welcome:
            playVideo(url: alert.url ?? "")
        }
    }
    
    // MARK: - tracking times
    
    private func trackTimes() {
        NotificationCenter.default.addObserver(self, selector: #selector(newTime(_:)), name: TimesService.Notifications.TimeLoaded, object: nil)
        if let time = TimesService.shared.getActiveTime() {
            presentTime(time)
        }
    }
    
    @objc private func newTime(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let time = notice.userInfo?["time"] as? Time {
                if time.state() != .met && time.state() != .cancelled && time.state() != .closed {
                    self?.presentTime(time)
                }
            }
        }
    }
    
    private func presentTime(_ time: Time) {
        guard !(presentedViewController is EnRouteViewController) else { return }
        
        if presentedViewController != nil {
            dismiss(animated: true) { [weak self] in
                self?.internalPresentTime(time)
            }
        } else {
            internalPresentTime(time)
        }
    }
    
    private func internalPresentTime(_ time: Time) {
        let vc = EnRouteViewController()
        _ = vc.view
        vc.setup(for: time)
        present(vc, animated: true, completion: nil)
    }
    
    func presentReview(for time: Time) {
        guard !time.haveReviewed() else { return }
        
        let vc = ReviewViewController()
        _ = vc.view
        vc.setup(for: time)
        vc.closeTap = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        present(vc, animated: true, completion: nil)
    }
    
    // MARK: - push notifications

    private func trackPNs() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        toForeground()
    }
    
    @objc private func toForeground() {
        
        if let info = PushNotificationService.shared.popLastMessage() {
            process(info: info)
        }
        processWindows()
    }
    
    @objc private func toBackground() {
        // nothing
    }
    
    private func process(info: [AnyHashable: Any]) {
        
        guard let type = info["type"] as? String else { return }
        
        switch type {
        case "newMessage":
            newMessage(info)
        case "newRequest":
            newRequest(info)
        case "newResponse":
            newResponse(info)
        default:
            ()
        }
    }
    
    private func newMessage(_ info: [AnyHashable: Any]) {
        if let timeId = info["timeId"] as? String {
            TimesService.shared.track(timeId: timeId)
            if let vc = presentedViewController as? EnRouteViewController {
                if vc.isPresentingTime(withId: timeId) {
                    vc.openMessages()
                }
            }
        } else if let signalId = info["signalId"] as? String, let requestId = info["requestId"] as? String {
            guard !(presentedViewController is EnRouteViewController) else { return }
            showSignal(signalId: signalId, requestId: requestId)
        }
    }
    
    private func newRequest(_ info: [AnyHashable: Any]) {
        if let signalId = info["signalId"] as? String, let requestId = info["requestId"] as? String {
            showSignal(signalId: signalId, requestId: requestId)
        }
    }
    
    private func newResponse(_ info: [AnyHashable: Any]) {
        // it should open en route automatically
        if let timeId = info["timeId"] as? String {
            TimesService.shared.track(timeId: timeId)
        }
    }
    
    private func showSignal(signalId: String, requestId: String) {
        if let signal = SignalTrackingService.shared.updatedSignalData(for: signalId) {
            presentSignal(signal, requestId: requestId)
        } else {
            loadSignalAndPresent(signalId, requestId: requestId)
        }
    }
            
    private func presentSignal(_ signal: Signal, requestId: String) {
        
        // dismiss whatever was being shown
        if presentedViewController != nil {
            dismiss(animated: true, completion: nil)
        }
        
        if signal.isMine() {
            if let request = signal.requests?.first(where: { $0.id == requestId }) {
                let vc = RequestDetailsNavigationController()
                vc.back = {
                    self.dismiss(animated: true, completion: nil)
                }
                _ = vc.view
                vc.setup(signal: SignalViewModel(signal: signal), request: RequestViewModel(request: request))
                present(vc, animated: true, completion: nil)
            }
        } else {
            if let myRequest = signal.myRequest(), myRequest.id == requestId {
                let model = SignalViewModel(signal: signal)
                let vc = SignalDetailsNavigationController()
                vc.back = { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
                }
                vc.report = { [weak self] in
                    self?.report(model)
                }
                _ = vc.view
                vc.setup(for: model)
                let nc = UINavigationController(rootViewController: vc)
                nc.setNavigationBarHidden(true, animated: false)
                nc.enableSwipeToPop()
                present(nc, animated: true, completion: nil)
            }
        }
    }
    
    private func loadSignalAndPresent(_ signalId: String, requestId: String) {
        Database.database().reference().child("signals").child("active").child(signalId).observeSingleEvent(
            of: .value,
            with: { [weak self] (snapshot) in
                if snapshot.exists(), let signal = Signal.fromSnapshot(snapshot), !signal.allExpired() {
                    self?.presentSignal(signal, requestId: requestId)
                }
            },
            withCancel: { _ in /* nothing */ })
    }
    
    // MARK: - network states
    
    private var speedIsLow = false
    
    private func trackConnectionState() {
        // Network speed
        NotificationCenter.default.addObserver(self, selector: #selector(networkConnection(_:)), name: NetworkSpeedProvider.Notifications.Connection, object: nil)
        
        // Network speed
        NotificationCenter.default.addObserver(self, selector: #selector(networkSpeed(_:)), name: NetworkSpeedProvider.Notifications.Speed, object: nil)
    }
    
    private func stopTrackingConnectionState() {
        NotificationCenter.default.removeObserver(self, name: NetworkSpeedProvider.Notifications.Connection, object: nil)
        NotificationCenter.default.removeObserver(self, name: NetworkSpeedProvider.Notifications.Speed, object: nil)
    }
    
    @objc private func networkConnection(_ notice: Notification) {
        errorAlert(text: "We're having trouble reaching our networks. Please check your connection.")
    }
    
    @objc private func networkSpeed(_ notice: Notification) {
        if let speed = notice.userInfo?["speed"] as? CGFloat {
            if speed < 0.01 {
                if !speedIsLow {
                    speedIsLow = true
                    DispatchQueue.main.async { [weak self] in
                        self?.warningAlert(text: "This is taking a bit longer than expected. Thank you for your patience.")
                    }
                }
            } else {
                speedIsLow = false
            }
        }
    }
    
    // MARK: - windows
    
    private weak var windowUI: WindowsOverlay?
    
    private func trackWindows() {
        NotificationCenter.default.addObserver(self, selector: #selector(windowsLoaded(_:)), name: WindowsService.Notifications.WindowsLoaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(windowEntered(_:)), name: WindowsService.Notifications.WindowsEnteredRegion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(windowExited(_:)), name: WindowsService.Notifications.WindowsExitedRegion, object: nil)
        processWindows()
    }
    
    private func stopTrackingkWindows() {
        NotificationCenter.default.removeObserver(self, name: WindowsService.Notifications.WindowsLoaded, object: nil)
        NotificationCenter.default.removeObserver(self, name: WindowsService.Notifications.WindowsEnteredRegion, object: nil)
        NotificationCenter.default.removeObserver(self, name: WindowsService.Notifications.WindowsExitedRegion, object: nil)
    }

    @objc private func windowsLoaded(_ notice: Notification) {
        processWindows()
    }
    
    @objc private func windowEntered(_ notice: Notification) {
        processWindows()
    }
    
    @objc private func windowExited(_ notice: Notification) {
        processWindows()
    }
    
    private func processWindows() {
        
        return
        
        let windows = WindowsService.shared.currentWindows
        let insideWindows = WindowsService.shared.insideWindows
        
        // BETA only
        
        if windows.count == 0 {
            presentBetaOverlay()
            return
        }
        
        betaOverlay?.view.removeFromSuperview()
        betaOverlay = nil
        
        // ---
        
        if insideWindows.count == 0, let closest = WindowsService.shared.closestWindow {
            presentWindowUI(for: closest)
        } else {
            windowUI?.removeFromSuperview()
        }
    }
    
    private func presentWindowUI(for window: Window) {
        
        betaOverlay?.view.removeFromSuperview()
        betaOverlay = nil
        
        if let current = self.windowUI {
            if current.data?.id == window.id {
                return
            }
            current.removeFromSuperview()
        }
        
        let windowUI = WindowsOverlay()
        windowUI.translatesAutoresizingMaskIntoConstraints = false
        if let splash = splash {
            view.insertSubview(windowUI, belowSubview: splash)
        } else {
            view.addSubview(windowUI)
        }
        windowUI.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        windowUI.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        windowUI.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        windowUI.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        view.layoutIfNeeded()
        windowUI.setup(for: window, viewController: self)
        self.windowUI = windowUI
    }
    
    // MARK: - muting alerts
    
    private var mutedTimes: [String] = []
    private var mutedSignals: [String] = []
    private var mutedRequests: [String] = []
    
    private func trackMuting() {
        NotificationCenter.default.addObserver(self, selector: #selector(muteTimeAlerts(_:)), name: Notifications.MuteTimeAlerts, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(muteSignalAlerts(_:)), name: Notifications.MuteSignalAlerts, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(muteRequestAlerts(_:)), name: Notifications.MuteRequestAlerts, object: nil)
    }
    
    @objc private func muteTimeAlerts(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let timeId = notice.userInfo?["timeId"] as? String,
                let mute = notice.userInfo?["mute"] as? Bool {
                
                if mute {
                    if self?.mutedTimes.contains(timeId) == false {
                        self?.mutedTimes.append(timeId)
                    }
                } else {
                    self?.mutedTimes.removeAll(where: { $0 == timeId })
                }
            }
        }
    }
    
    @objc private func muteSignalAlerts(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String,
                let mute = notice.userInfo?["mute"] as? Bool {
                
                if mute {
                    if self?.mutedSignals.contains(signalId) == false {
                        self?.mutedSignals.append(signalId)
                    }
                } else {
                    self?.mutedSignals.removeAll(where: { $0 == signalId })
                }
            }
        }
    }
    
    @objc private func muteRequestAlerts(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String,
                let requestId = notice.userInfo?["requestId"] as? String,
                let mute = notice.userInfo?["mute"] as? Bool {
                
                let combined = MoreTabBarController.requestCombinedId(signalId, requestId)
                
                if mute {
                    if self?.mutedRequests.contains(combined) == false {
                        self?.mutedRequests.append(combined)
                    }
                } else {
                    self?.mutedRequests.removeAll(where: { $0 == combined })
                }
            }
        }
    }
    
    private class func requestCombinedId(_ signalId: String, _ requestId: String) -> String {
        return "\(signalId)\(requestId)"
    }
    
    // MARK: - splash loading
    
    private var splash: SplashView? {
        return view.viewWithTag(12345678) as? SplashView
    }
    
    func waitWithSplash() {
        
        guard WindowsService.shared.currentWindows.count == 0 else { return }
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeSplash(_:)), name: WindowsService.Notifications.WindowsLoaded, object: nil)
        
        let image = SplashView()
        image.tag = 12345678
        image.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(image)
        image.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        image.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        image.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        image.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        view.layoutIfNeeded()
        
        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 2) { [weak self] in
            self?.removeSplash()
        }
    }
    
    @objc private func removeSplash(_ notice: Notification) {
        if WindowsService.shared.currentWindows.count > 0 {
            removeSplash()
        }
    }
    
    private func removeSplash() {
        UIView.animate(
            withDuration: 0.2,
            animations: { [weak self] in
                self?.splash?.alpha = 0
            },
            completion: { [weak self] _ in
                self?.splash?.removeFromSuperview()
            })
    }
    
    
    // MARK: - beta overlay
    
    private var betaOverlay: LoginInviteToGetInViewController?
    
    private func presentBetaOverlay() {
        guard self.betaOverlay == nil else { return }
        
        windowUI?.removeFromSuperview()
        if ProfileService.shared.profile?.isAdmin == true { return }
        
        let betaOverlay = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginInviteToGetInViewController") as! LoginInviteToGetInViewController
        betaOverlay.view.translatesAutoresizingMaskIntoConstraints = false
        _ = betaOverlay.view
        betaOverlay.setupForBeta()
        if let splash = splash {
            view.insertSubview(betaOverlay.view, belowSubview: splash)
        } else {
            view.addSubview(betaOverlay.view)
        }
        betaOverlay.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        betaOverlay.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        betaOverlay.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        betaOverlay.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        view.layoutIfNeeded()
        
        self.betaOverlay = betaOverlay
    }
    

}

