//
//  CreateSignalViewController.swift
//  More
//
//  Created by Luko Gjenero on 26/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

private let signalDraft = "com.more.simpleStore.signal.draft"

private let quoteCell = String(describing: CreateSignalQuoteCell.self)
private let destinationCell = String(describing: CreateSignalDestinationCell.self)
private let photosCell = String(describing: CreateSignalPhotosCell.self)

class CreateSignalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    private class CreateSignalStoreItem: SimpleStoreService.StoreItem {
        
        required init(from decoder: Decoder) throws {
            try super.init(from: decoder)
            let container = try decoder.container(keyedBy: CodingKeys.self)
            value = try container.decode(CreateSignalViewModel.self, forKey: .key)
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(value as! CreateSignalViewModel, forKey: .value)
        }
        
        override init(key: String, value: Codable?) {
            super.init(key: key, value: value)
        }
        
    }
    
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var locationButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    private weak var textView: UITextView?
    
    var closeTap: ((_ signal: Signal?)->())?
    // var finished: ((_ model: CreateSignalViewModel)->())?
    private var rows: [String] = []
    private var model: CreateSignalViewModel!
    private var keyboardVisible: Bool = false
    private var isFirst: Bool = true
    
    private var pauseKeyboardEvents: Bool {
        if let firstResponder = UIResponder.currentFirstResponder(), firstResponder == textView {
            return false
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let signalDraft = SimpleStoreService.shared.get(forKey: signalDraft) as? CreateSignalViewModel {
            model = signalDraft
        } else {
            model = CreateSignalViewModel()
        }
        
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.insetsContentViewsToSafeArea = false
        tableView.contentInset = .zero
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Dummy")
        tableView.register(UINib(nibName: quoteCell, bundle: nil), forCellReuseIdentifier: quoteCell)
        tableView.register(UINib(nibName: destinationCell, bundle: nil), forCellReuseIdentifier: destinationCell)
        tableView.register(UINib(nibName: photosCell, bundle: nil), forCellReuseIdentifier: photosCell)
        tableView.delegate = self
        tableView.dataSource = self
        
        setupRows()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardDidShow),
            name: Notification.Name.UIKeyboardWillShow,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardDidHide),
            name: Notification.Name.UIKeyboardWillHide,
            object: nil)
        
        for constraint in view.constraints {
            if constraint.firstAnchor == tableView.bottomAnchor ||
                constraint.secondAnchor == tableView.bottomAnchor {
                bottomContraint = constraint
                break
            }
        }
        trackKeyboardAndPushUp()
        trackKeyboard(onlyFor: [])
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        checkForInput()
    }
    
    private func checkForInput() {
        if model.quote.count == 0 {
            if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CreateSignalQuoteCell {
                cell.showKeyboard()
            }
        } else if photoBottomView == nil && moodBottomView == nil {
            showPhotoBottomView()
        }
        
        photoBottomView?.setDark(model.images.count > 0 && model.images.filter { $0 == nil }.count == 0)
    }
    
    @objc private func keyboardDidShow() {
        guard !pauseKeyboardEvents else { return }
        
        keyboardVisible = true
        
        var deletedRows: [IndexPath] = []
        if rows.count > 1 {
            for idx in 1..<rows.count {
                deletedRows.append(IndexPath(row: idx, section: 0))
            }
        }
        setupRows()
        
        tableView.beginUpdates()
        if let quoteCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CreateSignalQuoteCell {
            quoteCell.setup(for: model, keyboardVisible: keyboardVisible)
        }
        if deletedRows.count > 0 {
            tableView.deleteRows(at: deletedRows, with: .fade)
        }
        tableView.endUpdates()
    }
    
    @objc private func keyboardDidHide() {
        guard !pauseKeyboardEvents else { return }
        
        keyboardVisible = false
        
        setupRows()
        var insertedRows: [IndexPath] = []
        if rows.count > 1 {
            for idx in 1..<rows.count {
                insertedRows.append(IndexPath(row: idx, section: 0))
            }
        }
        
        tableView.beginUpdates()
        if let quoteCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CreateSignalQuoteCell {
            quoteCell.setup(for: model, keyboardVisible: keyboardVisible)
        }
        if insertedRows.count > 0 {
            tableView.insertRows(at: insertedRows, with: .fade)
        }
        tableView.endUpdates()

    }
    
    @IBAction private func closeTouch(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Do you want to save your draft or discard your progress?", preferredStyle: .actionSheet)
        
        let save = UIAlertAction(title: "Save draft", style: .default, handler: { [weak self] _ in
            SimpleStoreService.shared.store(item: CreateSignalStoreItem(key: signalDraft, value: self?.model))
            self?.closeTap?(nil)
        })
        
        let discard = UIAlertAction(title: "Discard", style: .destructive, handler: { [weak self] _ in
            SimpleStoreService.shared.remove(forKey: signalDraft)
            self?.closeTap?(nil)
        })
        
        alert.addAction(save)
        alert.addAction(discard)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func locationTouch(_ sender: Any) {
        showLocationSelector()
    }
    
    private func setupRows() {
        rows.removeAll()
        rows.append(quoteCell)
        if !keyboardVisible {
            if CreateSignalDestinationCell.isShowing(for: model) {
                rows.append(destinationCell)
            }
            if CreateSignalPhotosCell.isShowing(for: model) {
                rows.append(photosCell)
            }
        }
    }
    
    
    // MARK: - tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = rows[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CreateSignalBaseCell {
            cell.setup(for: model, keyboardVisible: keyboardVisible)
            connectCellActions(cell)
            return cell
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "Dummy", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let identifier = rows[indexPath.row]
        switch identifier {
        case quoteCell:
            return CreateSignalQuoteCell.size(for: model, in: tableView.frame.size, keyboardVisible: keyboardVisible).height
        case destinationCell:
            return CreateSignalDestinationCell.size(for: model, in: tableView.frame.size, keyboardVisible: keyboardVisible).height
        case photosCell:
            return CreateSignalPhotosCell.size(for: model, in: tableView.frame.size, keyboardVisible: keyboardVisible).height
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // TODO : - nothig for now
    }
    
    private func connectCellActions(_ cell: CreateSignalBaseCell) {
        
        if let quote = cell as? CreateSignalQuoteCell {
            quote.textUpdated = { [weak self] (text) in
                self?.model.quote = text ?? ""
            }
            quote.doneTap = { [weak self, weak quote] (text) in
                let quoteText = text ?? ""
                self?.model.quote = quoteText
                if quoteText.count > 0 {
                    quote?.hideKeyboard()
                    self?.checkForInput()
                }
            }
            trackKeyboard(onlyFor: [quote.textView])
            textView = quote.textView
            quote.textView.returnKeyType = .done
        } else if let photos = cell as? CreateSignalPhotosCell {
            photos.addTap = { [weak self] in
                self?.showPhotoBottomView()
            }
            photos.photoTap = { [weak self] (photo, webUrl) in
                self?.showDeletePhoto(photo, webUrl)
            }
            photos.rearranged = { [weak self] (photos, urls) in
                self?.model.images = photos
                self?.model.imagesWebUrls = urls
            }
        }
    }
    
    // MARK: - locations
    
    private weak var locationSelector: SearchDestinationViewController?
    
    private func showLocationSelector() {
        guard locationSelector == nil else { return }
        
        UIResponder.resignAnyFirstResponder()
        
        let vc = SearchDestinationViewController()
        _ = vc.view
        vc.setup(for: model)
        vc.closeTap = { [weak self] in
            self?.hideLocationSelector()
        }
    
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(vc.view)
        addChildViewController(vc)
        vc.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        vc.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        vc.view.setNeedsLayout()

        locationSelector = vc
    }
    
    
    private func hideLocationSelector() {
        guard locationSelector != nil else { return }
        
        locationSelector?.view.removeFromSuperview()
        locationSelector?.removeFromParentViewController()
        
        setupRows()
        tableView.reloadData()
        locationButton.isSelected = model.destination != nil /* || model.somewhere */
        
        checkForInput()
    }
    
    
    // MARK: - photos
    
    private weak var photoBottomView: CreateSignalImageBottomView?
    
    private func createPhotoBottomView() -> CreateSignalImageBottomView {
        let view = CreateSignalImageBottomView()
        view.searchTap = { [weak self] in
            self?.showPhotoSearch()
        }
        view.photosTap = { [weak self] in
            self?.showAlbum()
        }
        view.cameraTap = { [weak self] in
            self?.showCamera()
        }
        view.nextTap = { [weak self] in
            self?.showMoodBottomView()
        }
        view.setDark(model.images.count > 0 && model.images.filter { $0 == nil }.count == 0)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        view.heightAnchor.constraint(equalToConstant: 274).isActive = true
        view.setNeedsLayout()
        
        return view
    }
    
    private func showPhotoBottomView() {
        guard photoBottomView == nil else { return }
     
        photoBottomView = createPhotoBottomView()
        
        // slide from the botom
        var photoTransform = CATransform3DMakeTranslation(0, 274 + view.safeAreaInsets.bottom, 0)
        if moodBottomView != nil {
            // slide in from the left
            photoTransform = CATransform3DMakeTranslation(-view.frame.width, 0, 0)
        }
        let moodTransform = CATransform3DMakeTranslation(view.frame.width, 0, 0)
        
        photoBottomView?.layer.transform = photoTransform
        self.view.isUserInteractionEnabled = false
        UIView.animate(
            withDuration: 0.25,
            animations: { [weak self] in
                self?.photoBottomView?.layer.transform = CATransform3DIdentity
                self?.moodBottomView?.layer.transform = moodTransform
            },
            completion: { [weak self] (_) in
                self?.moodBottomView?.removeFromSuperview()
                self?.view.isUserInteractionEnabled = true
            })
    }
    
    private func showDeletePhoto(_ photo: UIImage?, _ url: String?) {
        let alert = UIAlertController(title: "Remove Image?", message: "Would you like to remove this image from the signal?", preferredStyle: .actionSheet)
        
        let yes = UIAlertAction(title: "Remove", style: .destructive, handler: { [weak self] _ in
            var index: Int? = nil
            if let photo = photo, let idx = self?.model.images.firstIndex(of: photo) {
                index = idx
            }
            if let url = url, let idx = self?.model.imagesWebUrls.firstIndex(of: url) {
                index = idx
            }
            if let idx = index {
                self?.model.images.remove(at: idx)
                self?.model.imagesWebUrls.remove(at: idx)
                self?.setupRows()
                self?.tableView.reloadData()
                self?.checkForInput()
            }
        })
        
        alert.addAction(yes)
        alert.addAction(UIAlertAction(title: "Leave it", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    // MARK: image search
    
    private weak var imageSearch: SearchImagesViewController?
    
    private func showPhotoSearch() {
        guard imageSearch == nil else { return }
        
        UIResponder.resignAnyFirstResponder()
        
        let vc = SearchImagesViewController()
        _ = vc.view
        vc.selectedPhotos = { [weak self] (photoUrls) in
            self?.hidePhotoSearch(photoUrls: photoUrls)
        }
        vc.setup(for: model)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(vc.view)
        addChildViewController(vc)
        vc.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        vc.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        vc.view.setNeedsLayout()
        
        imageSearch = vc
    }
    
    private func hidePhotoSearch(photoUrls: [ImageSearchService.ImageData]) {
        guard imageSearch != nil else { return }
        
        imageSearch?.view.removeFromSuperview()
        imageSearch?.removeFromParentViewController()
        
        loadImages(photoUrls)
        checkForInput()
    }
    
    private func loadImages(_ images: [ImageSearchService.ImageData]) {
        for image in images {
            model.images.append(nil)
            model.imagesWebUrls.append(image.url)
            SDWebImageDownloader.shared().downloadImage(
                with: URL(string: image.url),
                options: .highPriority,
                progress: nil,
                completed: { [weak self] (photo, _, _, _) in
                    if let photo = photo {
                        if let idx = self?.model.imagesWebUrls.firstIndex(of: image.url) {
                            self?.model.images.remove(at: idx)
                            self?.model.images.insert(photo, at: idx)
                            self?.checkForInput()
                            self?.setupRows()
                            self?.tableView.reloadData()
                        }
                    }
                })
        }
        checkForInput()
        setupRows()
        tableView.reloadData()
    }
    
    // MARK: image album & camera
    
    private func showAlbum() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func showCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            model.images.append(image)
            model.imagesWebUrls.append(nil)
            setupRows()
            tableView.reloadData()
            checkForInput()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
    
    // MARK: - mood
    
    private weak var moodBottomView: CreateSignalMoodBottomView?
    
    private func createMoodBottomView() -> CreateSignalMoodBottomView {

        let view = CreateSignalMoodBottomView()
        view.setup(for: model)
        view.backTap = { [weak self] in
            self?.showPhotoBottomView()
        }
        view.previewTap = { [weak self] in
            self?.showPreview()
        }
        
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        view.heightAnchor.constraint(equalToConstant: 327).isActive = true
        view.layoutSubviews()
        
        return view
    }

    private func showMoodBottomView() {
        guard moodBottomView == nil else { return }

        moodBottomView = createMoodBottomView()

        // slide from the botom
        var moodTransform = CATransform3DMakeTranslation(0, 327 + view.safeAreaInsets.bottom, 0)
        if photoBottomView != nil {
            // slide in from the left
            moodTransform = CATransform3DMakeTranslation(view.frame.width, 0, 0)
        }
        let photoTransform  = CATransform3DMakeTranslation(-view.frame.width, 0, 0)
        
        moodBottomView?.layer.transform = moodTransform
        self.view.isUserInteractionEnabled = false
        UIView.animate(
            withDuration: 0.25,
            animations: { [weak self] in
                self?.moodBottomView?.layer.transform = CATransform3DIdentity
                self?.photoBottomView?.layer.transform = photoTransform
            },
            completion: { [weak self] (_) in
                self?.view.isUserInteractionEnabled = true
                self?.photoBottomView?.removeFromSuperview()
        })
    }
    
    // MARK: - preview
    
    private func showPreview() {
        let vc = CreateSignalPreviewViewController()
        vc.backTap = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        vc.shareTap = { [weak self] in
            self?.createSignal()
        }
        _ = vc.view
        vc.setup(for: model)
        present(vc, animated: true, completion: nil)
    }
    
    private func createSignal() {
        if PushNotificationService.shared.permissionsRequested {
            creatSignalProcess()
        } else {
            PushNotificationService.shared.requestPersmissions { [weak self] in
                self?.dismiss(animated: true, completion: {
                    self?.createSignal()
                })
            }
        }
    }
    
    private func creatSignalProcess() {
        
        showUploading()
        
        if LocationService.shared.currentLocation != nil {
            internalCreatSignalProcess()
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
        }
    }
    
    private var uploadProgress: SignalUploadViewController? = nil
    
    private func showUploading() {
        let root = presentedViewController ?? self
        
        let vc = SignalUploadViewController()
        _ = vc.view
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        root.view.addSubview(vc.view)
        vc.view.leadingAnchor.constraint(equalTo: root.view.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: root.view.trailingAnchor).isActive = true
        vc.view.topAnchor.constraint(equalTo: root.view.topAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: root.view.bottomAnchor).isActive = true
        vc.viewDidAppear(false)
        
        uploadProgress = vc
    }
    
    @objc private func locationUpdated() {
        NotificationCenter.default.removeObserver(self, name: LocationService.Notifications.LocationUpdate, object: nil)
        internalCreatSignalProcess()
    }
    
    private func internalCreatSignalProcess() {

        uploadProgress?.stepCount = model.images.count + 2
        uploadProgress?.step = 1
        
        let images = model.images.map { $0 ?? UIImage() }
        
        uploadImages(images, complete: { [weak self] (success, urls, paths) in
            
            self?.model.imageUrls = urls
            self?.model.imagePaths = paths
            guard let signal = self?.model.signal(id: "") else { return }
            
            SignalingService.shared.createSignal(from: signal) { (signalId, errorMsg) in
                
                self?.uploadProgress?.step += 1
                self?.uploadProgress?.exitFromBelow({
                    self?.uploadProgress?.view.removeFromSuperview()
                    if let signalId = signalId {
                        SimpleStoreService.shared.remove(forKey: signalDraft)
                        self?.closeTap?(self?.model.signal(id: signalId))
                    } else {
                        self?.errorAlert(text: errorMsg ?? "Unknown error")
                    }
                })
            }
        })
    }
    
    private func uploadImages(_ images:  [UIImage], urlBuffer: [String] = [], pathBuffer: [String] = [], success: Bool = true, complete: ((_ success: Bool, _ urls: [String], _ paths: [String])->())?) {
        
        guard let first = images.first else {
            complete?(success, urlBuffer, pathBuffer)
            return
        }
        
        let name = MediaService.newSignalImageFilename()
        let restOfImages = Array(images.suffix(from: 1))
        
        MediaService.shared.uploadImage(to: MediaService.Buckets.Signals, name: name, image: first) { [weak self] (imageSuccess, url, errorMsg) in
            
            var nextSuccess = success
            var urls = urlBuffer
            var paths = pathBuffer
            if let url = url {
                urls.append(url)
                paths.append(MediaService.path(bucket: MediaService.Buckets.Signals, filename: name))
            } else {
                nextSuccess = false
            }
            
            self?.uploadProgress?.step += 1
            
            self?.uploadImages(restOfImages, urlBuffer: urls, pathBuffer: paths, success: nextSuccess, complete: complete)
        }
    }
    
}
