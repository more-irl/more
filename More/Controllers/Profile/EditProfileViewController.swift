//
//  EditProfileViewController.swift
//  More
//
//  Created by Luko Gjenero on 15/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var content: EditProfileContentView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        content.addPhoto = { [weak self] in
            self?.addPhoto()
        }
        
        content.editPhoto = { [weak self] (url) in
            self?.editPhoto(url)
        }
        
        content.photosRearranged = { [weak self] (urls) in
            self?.photosRearranged(urls)
        }
        
        content.nameTap = { [weak self] in
            self?.editNameAndEmail()
        }
        
        content.emailTap = { [weak self] in
            self?.editNameAndEmail()
        }
        
        content.quoteTap = { [weak self] in
            self?.editQuote()
        }
        
        content.birthdayTap = { [weak self] in
            self?.editBirthday()
        }
        
        content.genderTap = { [weak self] in
            self?.editGender()
        }
        
        content.momentTap = { [weak self] in
            self?.editMemories()
        }
    }
    
    private func update( ){
        if let profile = ProfileService.shared.profile {
            content.setup(for: profile)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        update()
    }

    @IBAction func doneTouch(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - photo actions
    
    private func addPhoto() {
        replacingPhoto = nil
        showAlbum()
    }
    
    private func editPhoto(_ url: String) {
        let alert = UIAlertController(title: "", message: "Do you want to replace or remove this photo?", preferredStyle: .actionSheet)
        
        let replace = UIAlertAction(title: "Replace", style: .default, handler: { [weak self] _ in
            self?.replacePhoto(url)
        })
        
        let remove = UIAlertAction(title: "Remove", style: .destructive, handler: { [weak self] _ in
            self?.removePhoto(url)
        })
        
        alert.addAction(replace)
        alert.addAction(remove)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func photosRearranged(_ urls: [String]) {
        ProfileService.shared.updateImages(urls)
        update()
    }
    
    private var replacingPhoto: String? = nil
    
    private func replacePhoto(_ url: String) {
        replacingPhoto = url
        showAlbum()
    }
    
    private func removePhoto(_ url: String) {
        var images = ProfileService.shared.profile?.images ?? []
        images.removeAll(where: { $0 == url })
        ProfileService.shared.updateImages(images)
        update()
    }
    
    private func showAlbum() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func showCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            showLoading()
            
            let name = "\(ProfileService.shared.profile?.id ?? "---").\(Date().timeIntervalSince1970)"
            MediaService.shared.uploadImage(to: "users", name: name, image: image) { [weak self] (success, url, errorMsg) in
                
                self?.hideLoading()
                
                if let url = url {
                    var images = ProfileService.shared.profile?.images ?? []
                    if let replacingPhoto = self?.replacingPhoto {
                        if let idx = images.firstIndex(of: replacingPhoto) {
                            images.remove(at: idx)
                            images.insert(url, at: idx)
                        } else {
                            images.append(url)
                        }
                    } else {
                        images.append(url)
                    }
                    ProfileService.shared.updateImages(images)
                    self?.update()
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
    
    // MARK: - editing
    
    private func editNameAndEmail() {
        let vc = EditProfileInfoViewController()
        _ = vc.view
        if let profile = ProfileService.shared.profile {
            vc.setup(for: profile)
        }
        vc.closeTap = { [weak self] (first, last, email) in
            if let profile = ProfileService.shared.profile {
                ProfileService.shared.updateInfo(
                    firstName: first ?? profile.firstName ?? "",
                    lastName: last ?? profile.lastName ?? "",
                    email: email ?? profile.email ?? "")
            }
            self?.navigationController?.popViewController(animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func editQuote() {
        let vc = EditProfileQuoteViewController()
        _ = vc.view
        if let profile = ProfileService.shared.profile {
            vc.setup(for: profile)
        }
        vc.closeTap = { [weak self] (quote, author) in
            if let quote = quote {
                ProfileService.shared.updateQuote(quote: quote, author: author ?? "")
            }
            self?.navigationController?.popViewController(animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func editBirthday() {
        let vc = EditProfileBirthdayViewController()
        _ = vc.view
        if let profile = ProfileService.shared.profile {
            vc.setup(for: profile)
        }
        vc.closeTap = { [weak self] (birthday) in
            ProfileService.shared.updateBirthday(birthday)
            self?.navigationController?.popViewController(animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func editGender() {
        let vc = EditProfileGenderViewController()
        _ = vc.view
        if let profile = ProfileService.shared.profile {
            vc.setup(for: profile)
        }
        vc.closeTap = { [weak self] (gender) in
            ProfileService.shared.updateGender(gender)
            self?.navigationController?.popViewController(animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func editMemories() {
        let vc = EditProfileMomentViewController()
        _ = vc.view
        if let profile = ProfileService.shared.profile {
            vc.setup(for: profile)
        }
        vc.closeTap = { [weak self] (moment) in
            if let profile = ProfileService.shared.profile {
                ProfileService.shared.updateMemories(moment ?? profile.memories ?? "")
            }
            self?.navigationController?.popViewController(animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }

}
