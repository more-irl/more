//
//  MGLMapView+CustomAdditions.h
//  More
//
//  Created by Luko Gjenero on 21/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

#ifndef MGLMapView_CustomAdditions_h
#define MGLMapView_CustomAdditions_h

#import <Mapbox/Mapbox.h>

@interface MGLMapView (CustomAdditions) <CLLocationManagerDelegate>

// FIXME: This will be removed once https://github.com/mapbox/mapbox-gl-native/issues/6867 is implemented
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations;

@property (nonatomic, readonly) CLLocationManager *locationManager;

@end


#endif /* MGLMapView_CustomAdditions_h */
