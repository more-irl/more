//
//  ShortTime.swift
//  More
//
//  Created by Luko Gjenero on 27/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

struct ShortTime: MoreDataObject {
    
    let id: String
    let signal: ShortSignal
    let createdAt: Date
    let endedAt: Date?
    
    init(id: String,
         signal: ShortSignal,
         createdAt: Date,
         endedAt: Date?) {
        
        self.id = id
        self.signal = signal
        self.createdAt = createdAt
        self.endedAt = endedAt
    }
    
    init() {
        self.init(id: "", signal: ShortSignal(), createdAt: Date(), endedAt: nil)
    }
    
    func isMine() -> Bool {
        return signal.creator.isMe()
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: ShortTime, rhs: ShortTime) -> Bool {
        return lhs.id == rhs.id
    }
    
    // JSON
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                let json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String: Any]) -> ShortTime? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let time = try? JSONDecoder().decode(ShortTime.self, from: jsonData) {
            return time
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> ShortTime? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return ShortTime.fromJson(json)
        }
        return nil
    }
    
}
