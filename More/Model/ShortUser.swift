//
//  ShortUser.swift
//  More
//
//  Created by Luko Gjenero on 01/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

struct ShortUser : MoreDataObject {
    
    let id: String
    let name: String
    let avatar: String
    let age: Int?
    
    let location: Location?
    
    init(id: String,
         name: String,
         avatar:String,
         age: Int? = nil,
         location: Location? = nil) {
        self.id = id
        self.name = name
        self.avatar = avatar
        self.age = age
        self.location = location
    }
    
    init() {
        self.init(id: "", name: "", avatar: "")
    }
    
    func isMe() -> Bool {
        return id == ProfileService.shared.profile?.id
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: ShortUser, rhs: ShortUser) -> Bool {
        return lhs.id == rhs.id
    }
    
    // Data protocol
    
    var json: [String : Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String : Any]) -> ShortUser? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let user = try? JSONDecoder().decode(ShortUser.self, from: jsonData) {
            return user
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> ShortUser? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return ShortUser.fromJson(json)
        }
        return nil
    }
    
    

}
