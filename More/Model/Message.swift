//
//  Message.swift
//  More
//
//  Created by Luko Gjenero on 02/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

struct Message: MoreDataObject {
    
    let id: String
    let createdAt: Date
    let sender: User
    let text: String
    var deliveredAt: Date?
    var readAt: Date?
    
    init(id: String,
        createdAt: Date,
        sender: User,
        text: String,
        deliveredAt: Date?,
        readAt: Date?) {
        
        self.id = id
        self.createdAt = createdAt
        self.sender = sender
        self.text = text
        self.deliveredAt = deliveredAt
        self.readAt = readAt
    }
    
    func isMine() -> Bool {
        return sender.isMe()
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: Message, rhs: Message) -> Bool {
        return lhs.id == rhs.id
    }
    
    // JSON
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String: Any]) -> Message? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let message = try? JSONDecoder().decode(Message.self, from: jsonData) {
            return message
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> Message? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return Message.fromJson(json)
        }
        return nil
    }
    
    

}
