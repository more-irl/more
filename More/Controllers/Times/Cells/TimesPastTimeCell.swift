//
//  TimesPastTimeCell.swift
//  More
//
//  Created by Luko Gjenero on 28/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

class TimesPastTimeCell: TimesBaseCell {
    
    @IBOutlet weak var content: UIView!
    @IBOutlet private weak var myAvatar: UIImageView!
    @IBOutlet private weak var otherAvatar: UIImageView!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var reviewButton: UIButton!
    @IBOutlet private weak var rate: StarsView!
    @IBOutlet private weak var timeText: SpecialLabel!
    @IBOutlet private weak var date: UILabel!
    @IBOutlet private weak var viewButton: UIButton!

    static let height: CGFloat = 250
    
    var writeReview: (()->())?
    
    var viewTime: (()->())?
    
    var viewUser: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let buttonColor = UIColor(red: 149, green: 182, blue: 255)
        reviewButton.setTitleColor(buttonColor, for: .normal)
        reviewButton.layer.borderColor = buttonColor.cgColor
        reviewButton.layer.cornerRadius = 15
        reviewButton.layer.borderWidth = 1
        reviewButton.setTitle("Write Review", for: .normal)
        
        otherAvatar.isUserInteractionEnabled = true
        otherAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTouch)))
        timeText.isUserInteractionEnabled = true
        timeText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(titleTouch)))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.enableShadow(color: .black, path: UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height - 25)).cgPath)
    }
    
    @IBAction func reviewTouch(_ sender: Any) {
        writeReview?()
    }
    
    
    @IBAction func viewTouch(_ sender: Any) {
        viewTime?()
    }
    
    @objc private func profileTouch() {
        viewUser?()
    }
    
    @objc private func titleTouch() {
        viewTime?()
    }
    
    // MARK: - base
    
    override class func size(for time: Time, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: TimesPastTimeCell.height)
    }
    
    override class func size(for review: Review, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: TimesPastTimeCell.height)
    }
    
    override func setup(for time: Time) {
        if let me = ProfileService.shared.profile {
            myAvatar.sd_progressive_setImage(with: URL(string: me.images?.first ?? ""), placeholderImage: UIImage.profileThumbPlaceholder())
        }
        otherAvatar.sd_progressive_setImage(with: URL(string: time.otherPerson().avatar), placeholderImage: UIImage.profileThumbPlaceholder())
        
        setupTitle(for: time.otherPerson().name)
        
        if let myReview = time.myReview() {
            rate.rate = CGFloat(myReview.timeRate ?? 0)
            rate.isHidden = false
            reviewButton.isHidden = true
        } else {
            rate.isHidden = true
            reviewButton.isHidden = false
        }
        
        timeText.text = time.signal.text
        
        setupDate(for: time.endedAt ?? time.createdAt)
    }
    
    override func setup(for review: Review) {
        if let me = ProfileService.shared.profile {
            myAvatar.sd_progressive_setImage(with: URL(string: me.images?.first ?? ""), placeholderImage: UIImage.profileThumbPlaceholder())
        }
        otherAvatar.sd_progressive_setImage(with: URL(string: review.creator.avatar), placeholderImage: UIImage.profileThumbPlaceholder())
        
        setupTitle(for: review.creator.name)
        
        rate.rate = CGFloat(review.timeRate ?? 0)
        rate.isHidden = false
        reviewButton.isHidden = true
        
        timeText.text = review.time?.signal.text
        
        setupDate(for: review.time?.endedAt ?? review.time?.createdAt ?? review.createdAt)
    }
    
    private func setupTitle(for name: String) {
        let text = NSMutableAttributedString()
        
        var font = UIFont(name: "Avenir-Medium", size: 13) ?? UIFont.systemFont(ofSize: 13)
        var part = NSAttributedString(
            string: "Time with ",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Heavy", size: 13) ?? UIFont.systemFont(ofSize: 13)
        part = NSAttributedString(
            string: name,
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 67, green: 74, blue: 81),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        title.attributedText = text
    }
    
    private func setupDate(for time: Date) {
        let df = DateFormatter()
        df.dateFormat = "MMMM d, h:mma"
        date.text = df.string(from: time).uppercased()
    }
    
}
