//
//  ExploreSavedDockCollectionViewCell.swift
//  More
//
//  Created by Luko Gjenero on 14/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ExploreSavedDockCollectionViewCell: UICollectionViewCell {

    @IBOutlet  weak var signalBar: SignalTopBar!

    private var background: UIColor!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        background = signalBar.backgroundColor ?? .clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        signalBar.layoutIfNeeded()
        signalBar.roundCorners(corners: [.topLeft, .topRight], radius: 8, background: background, shadow: .black)
    }
    
    func setup(for model: SignalViewModel) {
        if model.kind == .claimable {
            signalBar.setupForMore(title: "Claim This Signal")
        } else if model.kind == .curated {
            signalBar.setupForMore(title: "Claim This Signal")
        } else {
            signalBar.setup(for: model)
        }
    }

}
