//
//  WindowsService.swift
//  More
//
//  Created by Luko Gjenero on 17/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

class WindowsService /* : BufferedRunInterface */ {
    
    /*
    // buffered calls
    typealias BufferedData = Location
    var buffer: Location?
    var bufferDelay: TimeInterval {
        return 10
    }
    var bufferTimer: Timer?
    */
    
    private weak var windowTimer: Timer?
    
    struct Notifications {
        static let WindowsLoaded = NSNotification.Name(rawValue: "com.more.windows.listLoaded")
        static let WindowsEnteredRegion = NSNotification.Name(rawValue: "com.more.windows.entered")
        static let WindowsExitedRegion = NSNotification.Name(rawValue: "com.more.windows.exited")
    }
    
    static let shared = WindowsService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
        
        trackWindows()
        
        windowTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [weak self] (_) in
            self?.checkWindowsTime()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        windowTimer?.invalidate()
    }
    
    // MARK: - data
    
    private(set) var currentWindows: [Window] = []
    private(set) var insideWindows: [Window] = []
    private(set) var closestWindow: Window?
    
    @objc private func toForeground() {
        // nothing
    }
    
    @objc private func toBackground() {
        // nothing
    }
    
    @objc private func locationUpdated() {
        if let location = LocationService.shared.currentLocation {
            checkWindows(Location(coordinates: location.coordinate))
        }
    }

    private func checkWindows(_ location: Location) {
        var entered: [Window] = []
        var exited: [Window] = []
        
        for windows in currentWindows {
            if insideWindows.contains(windows) {
                if !windows.isInsideTime() || !(windows.isInsideRegionBuffer(location) || windows.isInsideRegion(location)) {
                    exited.append(windows)
                }
            } else {
                if windows.isInsideTime() && windows.isInsideRegion(location) {
                    entered.append(windows)
                }
            }
        }
        
        insideWindows.removeAll(where: { exited.contains($0) })
        insideWindows.append(contentsOf: entered)
        
        if insideWindows.count == 0 {
            var minTimeDist = TimeInterval.greatestFiniteMagnitude
            for window in currentWindows {
                let dist = window.timeDistanceTo()
                if dist < minTimeDist {
                    minTimeDist = dist
                }
            }
            let timeWindows = currentWindows.filter { $0.timeDistanceTo() <= minTimeDist }
            
            var minDist = Double.greatestFiniteMagnitude
            var closest = timeWindows.first
            for window in timeWindows {
                let dist = window.distanceTo()
                if dist < minDist {
                    minDist = dist
                    closest = window
                }
            }
            closestWindow = closest
        } else {
            closestWindow = nil
        }

        NotificationCenter.default.post(name: Notifications.WindowsLoaded, object: self)
        
        /*
        for window in exited {
            NotificationCenter.default.post(name: Notifications.WindowsExitedRegion, object: self, userInfo: ["window": window])
        }
        
        for window in entered {
            NotificationCenter.default.post(name: Notifications.WindowsEnteredRegion, object: self, userInfo: ["window": window])
        }
        */
    }
    
    func checkWindowsTime() {
        updateWindows(to: currentWindows)
    }
    
    private func trackWindows() { 
        Database.database().reference().child("windows").observe(
            .value,
            with: { [weak self] (snapshot) in
                if snapshot.exists() {
                    var windows: [Window] = []
                    for child in snapshot.children {
                        if let child = child as? DataSnapshot,
                            let window = Window.fromSnapshot(child) {
                            windows.append(window)
                        }
                    }
                    self?.updateWindows(to: windows)
                }
            },
            withCancel: { (error) in
                // TODO: --
            })
    }
    
    private func updateWindows(to windows: [Window]) {
        currentWindows = windows.filter { !$0.isExpired() }
        if let location = LocationService.shared.currentLocation {
            checkWindows(Location(coordinates: location.coordinate))
        } else {
            insideWindows.removeAll(where: { !currentWindows.contains($0) })
            NotificationCenter.default.post(name: Notifications.WindowsLoaded, object: self)
        }
    }
    
    // MARK: - notifications
    
    func setupNotifications(for window: Window) {
        setupTimeNotifications(for: window)
    }
    
    private func setupTimeNotifications(for window: Window) {
        
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-1")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-2")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-3")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-4")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-5")
        
        let now = Date()
        
        if let start = window.start {
            
            // 1.) 60 mins before star
            let min60BeforeStart = start.addingTimeInterval(-3600)
            if min60BeforeStart > now {
                PushNotificationService.shared.scheduleTimerNotification(
                    identifier: "windows-time-1",
                    date: min60BeforeStart,
                    title: "Special Announcement",
                    text: "More goes live in 60 minutes. See where →")
            }
            
            // 2.) 10 mins before start
            let min10BeforeStart = start.addingTimeInterval(-600)
            if min10BeforeStart > now {
                PushNotificationService.shared.scheduleTimerNotification(
                    identifier: "windows-time-2",
                    date: min10BeforeStart,
                    title: "Your Next Adventure Begins in 10 minutes",
                    text: "Get ready by opening the app now.")
            }
            
            // 3.) start
            if start > now {
                PushNotificationService.shared.scheduleTimerNotification(
                    identifier: "windows-time-3",
                    date: start,
                    title: "More is Live!",
                    text: "Let the adventures begin!")
            }
        }
        
        if let end = window.end {
            
            // 4.) 30 mins before end
            let min30BeforeEnd = end.addingTimeInterval(-1800)
            if min30BeforeEnd > now {
                PushNotificationService.shared.scheduleTimerNotification(
                    identifier: "windows-time-4",
                    date: min30BeforeEnd,
                    title: "30 minutes left to use More today",
                    text: "Cross one last item off your NYC bucket list before time runs out.")
            }
            
            // 5.) end
            if end > now {
                PushNotificationService.shared.scheduleTimerNotification(
                    identifier: "windows-time-5",
                    date: end,
                    title: "Time's up!",
                    text: "But don't worry – More goes live again in X days.")
            }
        }
    }
    
    /*
 
    private func setupGPSNotifications(for window: Window) {
        
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-gps-1")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-gps-2")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-gps-3")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-4")
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-time-5")
        
        
        
        PushNotificationService.shared.removeLocalNotification(identifier: "windows-start")
        if let start = data.start {
            let title = "More is Live!"
            let text = "You can now use More!"
            PushNotificationService.shared.scheduleTimerNotification(identifier: "windows-start", date: start, title: title, text: text)
        }
        
        
    }
    
    func scheduleLocationNotification(identifier: String, location: Location, title:String, text: String) {
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = text
        
        let region = CLCircularRegion(center: location.coordinates(), radius: 50.0, identifier: UUID().uuidString)
        region.notifyOnEntry = true
        region.notifyOnExit = false
        let trigger = UNLocationNotificationTrigger(region: region, repeats: false)
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [identifier])
        center.add(request, withCompletionHandler: nil)
    }
     */
}
