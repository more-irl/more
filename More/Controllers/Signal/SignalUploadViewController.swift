//
//  SignalUploadViewController.swift
//  More
//
//  Created by Luko Gjenero on 14/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalUploadViewController: UIViewController {

    @IBOutlet private weak var progress: UIProgressView!
    
    var stepCount = 0
    var step: Int = 0 {
        didSet {
            progress.progress = Float(step) / Float(stepCount)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progress.progress = 0
        setupForEnterFromBelow()
    }
    
    private var isFirst = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        enterFromBelow()
    }
    
    func setProgress(_ progress: Float) {
        self.progress.progress = progress
    }
}
