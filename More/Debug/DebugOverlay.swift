//
//  DebugOverlay.swift
//  More
//
//  Created by Luko Gjenero on 21/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import MapKit

class DebugOverlay: UIView, UIGestureRecognizerDelegate {

    static var overlay = DebugOverlay(frame: .zero)
    
    class func show( _ locationSelected: ((_ location: CLLocation?)->())? = nil) {
    
        if let window = AppDelegate.appDelegate()?.window {
            let debug = overlay
            
            /*
            debug.translatesAutoresizingMaskIntoConstraints = false
            window.addSubview(debug)
            */
            
            debug.translatesAutoresizingMaskIntoConstraints = false
            window.addSubview(debug)
            debug.leadingAnchor.constraint(equalTo: window.leadingAnchor).isActive = true
            debug.trailingAnchor.constraint(equalTo: window.trailingAnchor).isActive = true
            debug.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
            debug.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
            debug.layoutIfNeeded()
            
            debug.locationSelected = locationSelected
        }
    }
    
    
    private let menuButton: UIButton = {
        let menu = UIButton(type: .custom)
        menu.translatesAutoresizingMaskIntoConstraints = false
        menu.layer.cornerRadius = 20
        menu.backgroundColor = UIColor(red: 140, green: 182, blue: 255)
        menu.setImage(UIImage(named: "pin"), for: .normal)
        return menu
    }()
    
    private var locationSelected : ((_ location: CLLocation?)->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        
        
        addSubview(menuButton)
        menuButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        menuButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        menuButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        layoutIfNeeded()
        
        
        menuButton.addTarget(self, action: #selector(menuTouch), for: .touchUpInside)

        let pan = UIPanGestureRecognizer(target: self, action: #selector(pan(sender:)))
        pan.delegate = self
        menuButton.addGestureRecognizer(pan)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func menuTouch() {
        // TODO
        
        let map = DebugMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        addSubview(map)
        map.translatesAutoresizingMaskIntoConstraints = false
        map.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32).isActive = true
        map.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        map.topAnchor.constraint(equalTo: topAnchor, constant: 60).isActive = true
        map.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        layoutIfNeeded()
        
        map.setup(location: LocationService.shared.currentLocation)
        map.cancelTap = { [weak map] in
            map?.removeFromSuperview()
        }
        map.okTap = { [weak map, weak self] (location) in
            
            if let locationSelected = self?.locationSelected {
                locationSelected(location)
            } else {
                if let location = location {
                    LocationService.shared.testLocation(location.coordinate)
                }
            }
            
            map?.removeFromSuperview()
        }
    }
    
    private var panStart: CGPoint = .zero
    
    @objc private func pan(sender: UIGestureRecognizer) {
        
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let point = sender.location(in: self)
        switch sender.state {
        case .began: ()
        case .changed:
            menuButton.layer.transform = CATransform3DMakeTranslation(point.x - center.x, point.y - center.y, 0)
        case .cancelled, .ended, .failed:
            menuButton.layer.transform = CATransform3DMakeTranslation(point.x - center.x, point.y - center.y, 0)
        default: ()
        }
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    /*
     func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
     return otherGestureRecognizer == collectionView.panGestureRecognizer
     }
     */
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer.view == menuButton
    }
    
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hit = super.hitTest(point, with: event)
        if hit == self {
            return nil
        }
        return hit
    }

}
