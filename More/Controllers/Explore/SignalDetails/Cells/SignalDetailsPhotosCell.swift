//
//  SignalDetailsPhotosCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

class SignalDetailsPhotosCell: SignalDetailsBaseCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    private static let cellIdentifier = "ImageCell"
    
    private static let gradientHeight: CGFloat = 100
    
    @IBOutlet private weak var photos: UICollectionView!
    @IBOutlet private weak var type: UILabel!
    @IBOutlet private weak var page: UIPageControl!
    @IBOutlet private weak var gradientView: UIView!
    
    private let gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [Colors.profilekBackground.cgColor,
                        Colors.profilekBackground.withAlphaComponent(0.85).cgColor,
                        Colors.profilekBackground.withAlphaComponent(0).cgColor]
        layer.locations = [0, 0.2, 1.0]
        layer.startPoint = CGPoint(x: 0.5, y: 1)
        layer.endPoint = CGPoint(x: 0.5, y: 0)
        return layer
    }()
    
    private var photoURLs: [String] = []
    private var photoImages: [UIImage?] = []
    
    private var usePhotoImages: Bool {
        return photoImages.count > photoURLs.count
    }
    
    var scrolling: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        /*
        layer.zPosition = 1000
        gradient.backgroundColor = UIColor.red.cgColor
        */
        
        gradientView.layer.addSublayer(gradient)

        photos.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        photos.register(Cell.self, forCellWithReuseIdentifier: SignalDetailsPhotosCell.cellIdentifier)
        photos.dataSource = self
        photos.delegate = self
        
        page.addTarget(self, action: #selector(pageChanged), for: .valueChanged)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradient.frame = CGRect(x: 0, y: bounds.height - SignalDetailsPhotosCell.gradientHeight, width: bounds.width, height: SignalDetailsPhotosCell.gradientHeight)
        
        if let layout = photos.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = bounds.size
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                photos.collectionViewLayout.invalidateLayout()
                photos.reloadData()
            }
        }
    }
    
    @objc private func pageChanged() {
        photos.scrollToItem(at: IndexPath(item: page.currentPage, section: 0), at: .left, animated: true)
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if usePhotoImages {
            return photoImages.count
        }
        return photoURLs.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SignalDetailsPhotosCell.cellIdentifier, for: indexPath) as? Cell {
            if usePhotoImages {
                cell.setup(for: photoImages[indexPath.item])
            } else {
                cell.setup(for: photoURLs[indexPath.item])
            }
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var currentPage = max(0,min(Int(round(scrollView.contentOffset.x / scrollView.frame.width)), photoURLs.count))
        if usePhotoImages {
            currentPage = max(0,min(Int(round(scrollView.contentOffset.x / scrollView.frame.width)), photoImages.count))
        }
        page.currentPage = currentPage
        scrolling?()
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let image: UIImageView = {
           let image = UIImageView(frame: .zero)
            image.translatesAutoresizingMaskIntoConstraints = false
            image.backgroundColor = .clear
            image.contentMode = .scaleAspectFill
            image.clipsToBounds = true
            return image
        }()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.addSubview(image)
            image.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
            image.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            image.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            contentView.layoutIfNeeded()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(for url: String) {
            guard let url = URL(string: url) else {
                image.sd_cancelCurrentImageLoad_progressive()
                image.image = nil
                return
            }
            
            image.sd_progressive_setImage(with: url, placeholderImage: UIImage.expandedSignalPlaceholder(), options: .highPriority)
        }
        
        func setup(for image: UIImage?) {
            self.image.sd_cancelCurrentImageLoad_progressive()
            self.image.image = image
        }
        
    }
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        photoURLs = model.imageUrls
        photoImages = []
        photos.collectionViewLayout.invalidateLayout()
        photos.reloadData()
        
        page.numberOfPages = photoURLs.count
        
        type.textColor = model.type.color
        type.text = model.type.rawValue.uppercased()
        
        page.isHidden = photoURLs.count <= 1
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: size.height - SignalDetailsQuoteCell.height - 70)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return true
    }
    
    // MARK: - signal preview
    
    override func setup(for model: CreateSignalViewModel) {
        photoURLs = []
        photoImages = model.images
        photos.collectionViewLayout.invalidateLayout()
        photos.reloadData()
        
        page.numberOfPages = photoImages.count
        page.isHidden = photoImages.count <= 1
        
        if let modelType = model.type {
            type.textColor = modelType.color
            type.text = modelType.rawValue.uppercased()
        }
    }
    
    override class func size(for model: CreateSignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: size.height - SignalDetailsQuoteCell.height - 60)
    }
    
    override class func isShowing(for model: CreateSignalViewModel) -> Bool {
        return true
    }
    
    
}


