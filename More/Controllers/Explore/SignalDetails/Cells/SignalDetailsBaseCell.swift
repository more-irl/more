//
//  SignalDetailsBaseCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsBaseCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    // MARK: - signal details view
    
    class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return .zero
    }
    
    class func isShowing(for model: SignalViewModel) -> Bool {
        return false
    }
    
    func setup(for model: SignalViewModel) { }
    
    // MARK: - signal preview
    
    class func size(for model: CreateSignalViewModel, in size: CGSize) -> CGSize {
        return .zero
    }
    
    class func isShowing(for model: CreateSignalViewModel) -> Bool {
        return false
    }
    
    func setup(for model: CreateSignalViewModel) { }
}


