//
//  SearchDestinationViewController.swift
//  More
//
//  Created by Luko Gjenero on 28/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import CoreLocation

private let itemCell = String(describing: SearchDestinationItemCell.self)
private let searchCell = String(describing: SearchDestinationSearchCell.self)
private let doneCell = String(describing: SearchDestinationDoneCell.self)

class SearchDestinationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, BufferedRunInterface {

    typealias BufferedData = String
    var buffer: String?
    var bufferDelay: TimeInterval {
        return 0.5
    }
    var bufferTimer: Timer?
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchBackground: UIView!
    @IBOutlet weak var searchBackgroundBottom: NSLayoutConstraint!
    @IBOutlet private weak var safeAreaBackground: UIView!
    @IBOutlet weak var safeAreaBackgroundHeight: NSLayoutConstraint!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    
    private var rows: [String] = []
    private var model: CreateSignalViewModel?
    private var searchMode: Bool = false
    private var searchRows: [PlacesSearchService.PlaceData] = []
    
    private var somewhere: Bool = false
    private var destination: Location?
    private var destinationName: String?
    private var destinationAddress: String?
    
    private var isFirst: Bool = true
    
    var closeTap: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupForEnter()
        
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.insetsContentViewsToSafeArea = false
        tableView.contentInset = .zero
        tableView.tableFooterView = UIView()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Dummy")
        tableView.register(UINib(nibName: itemCell, bundle: nil), forCellReuseIdentifier: itemCell)
        tableView.register(UINib(nibName: searchCell, bundle: nil), forCellReuseIdentifier: searchCell)
        tableView.register(UINib(nibName: doneCell, bundle: nil), forCellReuseIdentifier: doneCell)
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBackground.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        
        setupRows()
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        safeAreaBackgroundHeight.constant = view.safeAreaInsets.top
        safeAreaBackground.setNeedsLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        enter()
    }

    @IBAction func closeTouch(_ sender: Any) {
        model?.somewhere = somewhere
        model?.destination = destination
        model?.destinationName = destinationName
        model?.destinationAddress = destinationAddress
        
        exit()
    }
    
    private func setupRows() {
        rows.removeAll()
        
        rows.append(itemCell)
        
        if searchMode {
            rows.append(searchCell)
            for _ in searchRows {
                rows.append(itemCell)
            }
        } else {
            rows.append(itemCell)
            rows.append(doneCell)
        }
    }
    
    func setup(for model: CreateSignalViewModel) {
        self.model = model
        
        // initialize
        if !model.somewhere && model.destination == nil {
            model.somewhere = true
        }
        
        somewhere = model.somewhere
        destination = model.destination
        destinationName = model.destinationName
        destinationAddress = model.destinationAddress
        
        setupRows()
        tableView.reloadData()
    }
    
    // MARK: - tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = rows[indexPath.row]
        guard let model = model else {
            return tableView.dequeueReusableCell(withIdentifier: "Dummy", for: indexPath)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if let itemCell = cell as? SearchDestinationItemCell {
            switch indexPath.row {
            case 0:
                itemCell.setupSomewhere(for: model)
            case 1:
                if model.destination != nil {
                    itemCell.setupDestination(for: model)
                } else {
                    itemCell.setupAdd(for: model)
                }
            default:
                itemCell.setupPlace(for: searchRows[indexPath.row - 2])
            }
        } else if let searchCell = cell as? SearchDestinationSearchCell {
            searchCell.searchUpdated = { [weak self] (searchString) in
                self?.searchUpdated(to: searchString ?? "")
            }
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let identifier = rows[indexPath.row]
        switch identifier {
        case itemCell:
            return 82
        case doneCell, searchCell:
            return 60
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            model?.somewhere = true
            model?.destination = nil
            model?.destinationName = nil
            model?.destinationAddress = nil
            
            if searchMode {
                searchMode = false
                exitSearchMode()
            } else {
                tableView.reloadData()
            }
        case 1:
            if !searchMode {
                searchMode = true
                enterSearchMode()
            }
        default:
            if searchMode {
                searchMode = false
                let place = searchRows[indexPath.row - 2]
                model?.somewhere = false
                model?.destination = Location(coordinates: place.location)
                model?.destinationName = place.name
                model?.destinationAddress = place.address
                
                exitSearchMode()
            } else if indexPath.row == 2 {
                exit()
            }
        }
        
    }
    
    // MARK: - search
    
    private func searchUpdated(to searchText: String) {
        bufferCall(with: searchText) { [weak self] (searchText) in
            self?.initiateSearch(searchText)
        }
    }
    
    private func initiateSearch(_ searchString: String) {
        
        guard searchString.count > 0 else {
            updateSearchResults(to: [])
            return
        }
        
        PlacesSearchService.shared.search(for: searchString) { [weak self] (results) in
            self?.updateSearchResults(to: results)
        }
    }
    
    private func updateSearchResults(to results: [PlacesSearchService.PlaceData]) {
        
        var deletedIndexes: [IndexPath] = []
        for idx in 0..<searchRows.count {
            deletedIndexes.append(IndexPath(row: 2 + idx, section: 0))
        }
        
        var insertedIndexes: [IndexPath] = []
        for idx in 0..<results.count {
            insertedIndexes.append(IndexPath(row: 2 + idx, section: 0))
        }
        
        searchRows = results
        setupRows()
        tableView.beginUpdates()
        if deletedIndexes.count > 0 {
            tableView.deleteRows(at: deletedIndexes, with: .fade)
        }
        if insertedIndexes.count > 0 {
            tableView.insertRows(at: insertedIndexes, with: .fade)
        }
        tableView.endUpdates()
    }
    
    private func enterSearchMode() {
        setupRows()
        let deletedIndexes = [IndexPath(row: 1, section: 0), IndexPath(row: 2, section: 0)]
        let insertedIndexes = [IndexPath(row: 1, section: 0)]
        let reloadRows = [IndexPath(row: 0, section: 0)]
        tableView.beginUpdates()
        tableView.deleteRows(at: deletedIndexes, with: .fade)
        tableView.insertRows(at: insertedIndexes, with: .fade)
        tableView.reloadRows(at: reloadRows, with: .none)
        tableView.endUpdates()
        
        if let searchCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? SearchDestinationSearchCell {
            searchCell.showKeyboard()
        }
        
        UIView.animate(withDuration: 0.3) {
            self.setupSearchBackground()
        }
    }
    
    private func exitSearchMode() {
        var deletedIndexes: [IndexPath] = []
        deletedIndexes.append(IndexPath(row: 1, section: 0))
        for idx in 0..<searchRows.count {
            deletedIndexes.append(IndexPath(row: 2 + idx, section: 0))
        }
        let insertedIndexes = [IndexPath(row: 1, section: 0), IndexPath(row: 2, section: 0)]
        let reloadRows = [IndexPath(row: 0, section: 0)]
        searchRows = []
        setupRows()
        tableView.beginUpdates()
        tableView.deleteRows(at: deletedIndexes, with: .fade)
        tableView.insertRows(at: insertedIndexes, with: .fade)
        tableView.reloadRows(at: reloadRows, with: .none)
        tableView.endUpdates()
        
        UIView.animate(withDuration: 0.3) {
            self.setupSearchBackground()
        }
    }
    
    private func setupSearchBackground() {
        var height: CGFloat = view.bounds.height - view.safeAreaInsets.bottom - view.safeAreaInsets.top
        if !searchMode {
            height = 144
        }
        searchBackground.layer.transform = CATransform3DMakeScale(1, height / 144 , 1)
    }
    
    // MARK - enter / exit animations
    
    private func setupForEnter() {
        backgroundView.alpha = 0
        let transform = CATransform3DMakeTranslation(0, -view.frame.height, 0)
        for view in view.subviews {
            if view != backgroundView {
                view.layer.transform = transform
            }
        }
    }
    
    private func enter() {
        UIView.animate(
        withDuration: 0.3) {
            for view in self.view.subviews {
                if view != self.backgroundView {
                    view.layer.transform = CATransform3DIdentity
                }
            }
            self.backgroundView.alpha = 1
        }
    }
    
    private func exit() {
        UIResponder.resignAnyFirstResponder()
        let transform = CATransform3DMakeTranslation(0, -view.frame.height, 0)
        UIView.animate(
            withDuration: 0.3,
            animations: {
                for view in self.view.subviews {
                    if view != self.backgroundView {
                        view.layer.transform = transform
                    }
                }
                self.backgroundView.alpha = 0
            },
            completion: { (_) in
                self.closeTap?()
            })
    }

}
