//
//  SignalRequestTopBar.swift
//  More
//
//  Created by Luko Gjenero on 19/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class SignalRequestTopBar: LoadableView {
    
    @IBOutlet weak var background: BackgroundWithOval!
    @IBOutlet private weak var bottomCountdown: CountdownLabel!
    @IBOutlet private weak var down: UIButton!
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var safeAreaBackground: UIView!
    @IBOutlet private weak var safeAreaBackgroundHeight: NSLayoutConstraint!
    @IBOutlet weak var topCountdown: CountdownLabel!
    
    var safeAreaHeight: CGFloat {
        get {
            return safeAreaBackgroundHeight.constant
        }
        set {
            safeAreaBackgroundHeight.constant = newValue
            setNeedsLayout()
        }
    }
    
    var downTap: (()->())?
    
    var downHidden: Bool {
        get {
            return down.isHidden
        }
        set {
            down.isHidden = newValue
        }
    }
    
    var counterOnTop: Bool = false {
        didSet {
            topCountdown.isHidden = !counterOnTop
            bottomCountdown.isHidden = counterOnTop
            background.ovalPosition = counterOnTop ? BackgroundWithOval.OvalPosition.top.rawValue : BackgroundWithOval.OvalPosition.bottom.rawValue
        }
    }
    
    override func setupNib() {
        super.setupNib()
        
        bottomCountdown.setDateFormat("m:ss")
        bottomCountdown.refreshIterval = 0.5
        
        topCountdown.setDateFormat("m:ss")
        topCountdown.refreshIterval = 0.5
    }
    
    func setup(for model: SignalViewModel) {
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(expirationChanged(_:)), name: SignalTrackingService.Notifications.SignalExpirationChanged, object: nil)
        
        bottomCountdown.countdown(to: model.expiresAt)
        topCountdown.countdown(to: model.expiresAt)
        label.text = model.quote
    }
    
    func setup(signal: SignalViewModel, request: RequestViewModel) {
        
        bottomCountdown.countdown(to: request.expiresAt)
        topCountdown.countdown(to: request.expiresAt)
        label.text = signal.quote
    }
    
    @IBAction private func downTouch(_ sender: Any) {
        downTap?()
    }
    
    @objc private func expirationChanged(_ notice: Notification) {
        if let signal = notice.userInfo?["signal"] as? Signal {
            bottomCountdown.countdown(to: signal.finalExpiration())
            topCountdown.countdown(to: signal.finalExpiration())
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hit = super.hitTest(point, with: event)
        var selfView: UIView = self
        if self.subviews.count == 1 {
            selfView = self.subviews.first!
        }
        if hit == selfView {
            return nil
        }
        return hit
    }
    
}
