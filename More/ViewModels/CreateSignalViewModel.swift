//
//  CreateSignalViewModel.swift
//  More
//
//  Created by Luko Gjenero on 27/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import CoreLocation

class CreateSignalViewModel: Codable {
    
    let creator: UserViewModel
    
    var imageUrls: [String] = []
    var imagePaths: [String] = []
    var images: [UIImage?] = []
    var imagesWebUrls: [String?] = []
    
    var type: SignalType?
    var quote: String = ""

    var somewhere: Bool = false
    var location: Location?
    var destination: Location?
    var destinationName: String?
    var destinationAddress: String?
    
    var kind: SignalKind?
    var kindStartDate: Date?
    var kindEndDate: Date?
    var kindDailyStart: TimeInterval?
    var kindDailyDuration: TimeInterval?
    var radius: Double?

    init() {
        creator = UserViewModel(user: ProfileService.shared.profile?.user ?? User())
    }
    
    func signal(id: String) -> Signal {
        var location = Location(latitude: 0, longitude: 0)
        if let coordinates = LocationService.shared.currentLocation?.coordinate {
            location = Location(coordinates: coordinates)
        }
        
        var createdAt = Date()
        var expiresAt = Date(timeIntervalSinceNow: ConfigService.shared.signalExpiration)
        if kind != .standard {
            createdAt = kindStartDate ?? createdAt
            expiresAt = kindEndDate ?? expiresAt
            location = self.location ?? destination ?? location
        }
        
        return Signal(
            id: id,
            text: quote,
            imageUrls: imageUrls,
            imagePaths: imagePaths,
            kind: kind ?? .standard,
            type: type ?? .magical,
            radius: radius,
            kindDailyStart: kindDailyStart,
            kindDailyDuration: kindDailyDuration,
            createdAt: createdAt,
            expiresAt: expiresAt,
            location: location,
            destination: destination,
            destinationName: destinationName,
            destinationAddress: destinationAddress,
            creator: ProfileService.shared.profile?.user ?? User(id: "", name: "", avatar: ""))
    }
    
    enum CodingKeys: String, CodingKey {
        case creator
        case imageUrls
        case images
        case type
        case quote
        case somewhere
        case destination
        case destinationName
        case destinationAddress
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        creator = UserViewModel(user: ProfileService.shared.profile?.user ?? User())
        imageUrls = []
        let imageData: [Data] = try container.decode([Data].self, forKey: .images)
        images = imageData.map { UIImage(data: $0) ?? UIImage() }
        type = try container.decodeIfPresent(SignalType.self, forKey: .type)
        quote = try container.decode(String.self, forKey: .quote)
        somewhere = try container.decode(Bool.self, forKey: .somewhere)
        destination = try container.decodeIfPresent(Location.self, forKey: .destination)
        destinationName = try container.decodeIfPresent(String.self, forKey: .destinationName)
        destinationAddress = try container.decodeIfPresent(String.self, forKey: .destinationAddress)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        let imageData: [Data] = images.map { UIImageProgressiveJPEGRepresentation($0 ?? UIImage(), 1) ?? Data() }
        try container.encodeIfPresent(imageData, forKey: .images)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encode(quote, forKey: .quote)
        try container.encode(somewhere, forKey: .somewhere)
        try container.encodeIfPresent(destination, forKey: .destination)
        try container.encodeIfPresent(destinationName, forKey: .destinationName)
        try container.encodeIfPresent(destinationAddress, forKey: .destinationAddress)
        
    }
}

