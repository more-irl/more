//
//  SignalClaimedView.swift
//  More
//
//  Created by Luko Gjenero on 18/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit
import Firebase

class SignalClaimedView: UICollectionView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    private static let cellIdentifier = "UserCell"
    
    private var users: [User] = []
    
    var showBlanks: Bool = true
    
    var userTap: ((_ user: User)->())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    private func setup() {
        register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        register(Cell.self, forCellWithReuseIdentifier: SignalClaimedView.cellIdentifier)
        dataSource = self
        delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = self.itemSize
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                collectionViewLayout.invalidateLayout()
                reloadData()
            }
        }
    }
    
    private var itemSize: CGSize {
        return CGSize(width: 93, // floor((frame.width - contentInset.left - contentInset.right) / 3),
            height: frame.height - contentInset.top - contentInset.bottom)
    }
    
    func setup(for users: [User]) {
        self.users = users
        isScrollEnabled = users.count > 0
        reloadData()
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if showBlanks && users.count == 0 {
            return 5
        }
        return users.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SignalClaimedView.cellIdentifier, for: indexPath) as? Cell {
            if indexPath.item < users.count {
                cell.setup(for: users[indexPath.item])
            } else {
                cell.setupForEmpty()
            }
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item < users.count {
            userTap?(users[indexPath.item])
        }
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let avatar: TimeAvatarView = {
            let avatar = TimeAvatarView()
            avatar.translatesAutoresizingMaskIntoConstraints = false
            avatar.clipsToBounds = true
            avatar.backgroundColor = .clear
            return avatar
        }()
        
        private let name: UILabel = {
            let label = SpecialLabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: "Avenir-Medium", size: 11)
            label.kern = 0.46
            label.textColor = UIColor(red: 67, green: 74, blue: 81)
            label.backgroundColor = .clear
            return label
        }()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.addSubview(avatar)
            contentView.addSubview(name)
            
            avatar.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            avatar.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -8).isActive = true
            avatar.widthAnchor.constraint(equalTo: avatar.heightAnchor).isActive = true
            avatar.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 24).isActive = true
            avatar.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 10).isActive = true
            let widthConstraint = NSLayoutConstraint(item: avatar, attribute: .width, relatedBy: .equal, toItem: contentView, attribute: .width, multiplier: 1, constant: 0)
            widthConstraint.priority = .defaultLow
            contentView.addConstraint(widthConstraint)
            
            name.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            name.topAnchor.constraint(equalTo: avatar.bottomAnchor, constant: 5).isActive = true
            name.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 4).isActive = true

            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(for user: User) {
            avatar.imageUrl = user.avatar
            avatar.type = .boozy
            avatar.progress = 1
            name.text = user.name
        }
        
        func setupForEmpty() {
            avatar.imageUrl = ""
            avatar.setupRunningProgress(start: Date(timeIntervalSince1970: 0), end: Date(timeIntervalSince1970: 1))
            name.text = ""
        }
    }
    
}
