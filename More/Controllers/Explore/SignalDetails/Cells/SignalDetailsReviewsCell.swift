//
//  SignalDetailsReviewsCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsReviewsCell: SignalDetailsBaseCell {

    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var stars: StarsView!
    @IBOutlet private weak var numOfReviews: UILabel!
    @IBOutlet private weak var header: SignalReviewHeader!
    @IBOutlet private weak var bubble: SignalReviewBubble!
    
    var width: CGFloat = 0
    
    var readMore: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bubble.enableReadMore = true
        
        bubble.readMoreTap = { [weak self] in
            self?.readMore?()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        
        title.text = "\(model.creator.name.uppercased())'S TIMES"
        
        if model.creator.numOfReviews > 0 {
            stars.noStar = UIImage(named: "no_star")
            stars.rate = CGFloat(model.creator.avgReview)
        } else {
            stars.noStar = UIImage(named: "star_outline_2")
            stars.rate = 0
        }
        numOfReviews.text = "\(model.creator.numOfReviews)"
        
        if let review = model.lastReview {
            header.setup(for: review)
            
            let font = UIFont(name: "Avenir-Medium", size: 13) ?? UIFont.systemFont(ofSize: 13)
            var truncatedComment = review.comment.truncatedText(lineWidths: [width - 72, width - 152], withFont: font)
            if truncatedComment.count < review.comment.count {
                truncatedComment += " ..."
            }
            
            bubble.setup(for: review, comment: truncatedComment)
        }
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        
        var headerHeight: CGFloat = 0
        // var bubbleHeight: CGFloat = 0
        
        if let review = model.lastReview {
            headerHeight = SignalReviewHeader.size(for: review, in: CGSize(width: size.width - 50, height: size.height)).height
            // bubbleHeight = SignalReviewBubble.size(for: review, in: CGSize(width: size.width - 50, height: size.height)).height
        }
        return CGSize(width: size.width, height: 103 + headerHeight + 12 + 128 + 25)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        guard model.kind == .standard else { return false }
        return model.creator.numOfReviews > 0
    }
    
}
