//
//  UIViewController+Video.swift
//  More
//
//  Created by Luko Gjenero on 10/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit
import AVKit

extension UIViewController {
    
    func playVideo(url urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true, completion: nil)
    }

}
