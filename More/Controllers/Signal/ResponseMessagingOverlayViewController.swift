//
//  ResponseMessagingOverlayViewController.swift
//  More
//
//  Created by Luko Gjenero on 04/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ResponseMessagingOverlayViewController: UIViewController {

    @IBOutlet private weak var requestBar: SignalRequestBar!
    @IBOutlet private weak var topBar: SignalRequestTopBar!
    @IBOutlet private weak var messagingView: MessagingView!
    @IBOutlet weak var messageViewHeight: NSLayoutConstraint!
    
    private var signalModel: SignalViewModel? = nil
    private var requestModel: RequestViewModel? = nil
    private var profileModel: UserViewModel? = nil
    
    var accepted: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        requestBar.requestTap = { [weak self] in
            self?.acceptTimeIfNeeded()
        }
        requestBar.countdownHidden = true
        
        topBar.downTap = { [weak self] in
            self?.collapseMessageView()
        }
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(dragTop(sender:)))
        topBar.addGestureRecognizer(pan)
        
        messagingView.send = { [weak self] (text) in
            self?.sendMessage(text)
        }
        
        messagingView.isEditable = false
        
        for constraint in view.constraints {
            if constraint.firstAnchor == messagingView.bottomAnchor ||
                constraint.secondAnchor == messagingView.bottomAnchor {
                bottomContraint = constraint
                break
            }
        }
        trackKeyboard(onlyFor: [messagingView.input.textView])
        trackKeyboardAndPushUp()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: SignalTrackingService.Notifications.SignalResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: SignalTrackingService.Notifications.SignalMessage, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardUp(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDown(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if let signal = signalModel,
            let request = requestModel {
            SignalTrackingService.shared.setCreatorIsTyping(signalId: signal.id, requestId: request.id, isTyping: false)
        }
        stopTrackingTyping()
        
        NotificationCenter.default.post(
            name: MoreTabBarController.Notifications.MuteRequestAlerts,
            object: nil,
            userInfo: ["signalId": signalModel?.id ?? "",
                       "requestId": requestModel?.id ?? "",
                       "mute": false ])
    }
    
    private var collapsedMessagingViewMaxHeight: CGFloat {
        let viewHeight = view.bounds.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom
        let maxHeight = viewHeight - requestBar.frame.height + messagingView.insets.top - topBar.frame.height - 120
        return maxHeight
    }
    
    private var expandedMessagingViewMaxHeight: CGFloat {
        return view.bounds.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom - topBar.frame.height + 37
    }
    
    private var isFirst: Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        if messageViewHeight.constant > 73 {
            if let signal = signalModel,
                let request = requestModel {
                
                let unreadList = request.messages.filter({ !$0.isMine() && $0.read == nil })
                guard unreadList.count > 0 else { return }
                
                DispatchQueue.global(qos: .default).async {
                    for message in unreadList {
                        SignalTrackingService.shared.setMesageRead(signalId: signal.id, requestId: request.id, messageId: message.id)
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard isFirst else { return }
        
        messagingView.layoutIfNeeded()
        messageViewHeight.constant = min(collapsedMessagingViewMaxHeight, messagingView.height - messagingView.insets.top)
        topBar.safeAreaHeight = 0
        topBar.downHidden = true
        topBar.counterOnTop = true
        requestBar.countdownHidden = true
        messagingView.insets = .zero
        view.layoutIfNeeded()
        
        messagingView.scrollToBottom()
    }
    
    private func collapseMessageView(fully: Bool = false, duration: Double = 0.3, curve: UIView.AnimationOptions = .curveEaseInOut, force: Bool = false) {
        
        NotificationCenter.default.post(
            name: MoreTabBarController.Notifications.MuteRequestAlerts,
            object: nil,
            userInfo: ["signalId": signalModel?.id ?? "",
                       "requestId": requestModel?.id ?? "",
                       "mute": false ])
        
        messagingView.tableView.showTyping = false
        let height = fully ? 73 : min(collapsedMessagingViewMaxHeight, messagingView.height - messagingView.insets.top)
        
        guard messageViewHeight.constant > height || force else { return }
        
        messagingView.hideKeyboard()
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: [.beginFromCurrentState, curve],
            animations: {
                self.messageViewHeight.constant = height
                self.topBar.safeAreaHeight = 0
                self.topBar.downHidden = true
                self.topBar.counterOnTop = true
                self.requestBar.countdownHidden = true
                self.messagingView.insets = .zero
                self.view.backgroundColor = fully ? .clear :  UIColor.black.withAlphaComponent(0.3)
                
                self.view.layoutIfNeeded()
            },
            completion: { (_) in
                self.messagingView.scrollToBottom()
            })
    }
    
    private func expandMessageView(duration: Double = 0.3, curve: UIView.AnimationOptions = .curveEaseInOut, force: Bool = false) {
       
        NotificationCenter.default.post(
            name: MoreTabBarController.Notifications.MuteRequestAlerts,
            object: nil,
            userInfo: ["signalId": signalModel?.id ?? "",
                       "requestId": requestModel?.id ?? "",
                       "mute": true ])
        
        messagingView.tableView.showTyping = otherIsTyping
        let height = expandedMessagingViewMaxHeight
        
        guard messageViewHeight.constant < height || force else { return }
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: [.beginFromCurrentState, curve],
            animations: {
                self.messageViewHeight.constant = height
                self.messagingView.insets = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
                self.topBar.safeAreaHeight = self.view.safeAreaInsets.top
                self.topBar.downHidden = false
                self.topBar.counterOnTop = false
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                
                self.view.layoutIfNeeded()
            },
            completion: { (_) in
                self.messagingView.showKeyboard()
                self.messagingView.scrollToBottom()
            })
        
        if let signal = signalModel,
            let request = requestModel {
            
            let unreadList = request.messages.filter({ !$0.isMine() && $0.read == nil })
            guard unreadList.count > 0 else { return }
            
            DispatchQueue.global(qos: .default).async {
                for message in unreadList {
                    SignalTrackingService.shared.setMesageRead(signalId: signal.id, requestId: request.id, messageId: message.id)
                }
            }
        }
    }
    
    @objc private func keyboardUp(_ notification: Notification) {

        if let userInfo = notification.userInfo,
            let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber,
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {

            expandMessageView(duration: animationDuration.doubleValue, curve: UIView.AnimationOptions(rawValue: animationCurve.uintValue))
            return
        }
    
        expandMessageView()
    }
    
    @objc private func keyboardDown(_ notification: Notification) {

        if let userInfo = notification.userInfo,
            let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber,
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {

            collapseMessageView(duration: animationDuration.doubleValue, curve: UIView.AnimationOptions(rawValue: animationCurve.uintValue))
            return
        }

        collapseMessageView()
    }
    
    func setup(signal: SignalViewModel, request: RequestViewModel) {
        
        signalModel = signal
        requestModel = request
        
        topBar.setup(signal: signal, request: request)
        messagingView.setup(for: request.messages)
        if let user = ProfileService.shared.profile?.user {
            messagingView.setup(for: UserViewModel(user: user))
        }
        requestBar.setup(signal: signal, request: request)
        
        stopTrackingTyping()
        trackTyping()
    }
    
    func fullyCollapse() {
        collapseMessageView(fully: true)
    }
    
    // MARK: - drag
    
    private var dragStart: CGPoint = .zero
    private var dragStartHeight: CGFloat = 0
    
    @objc private func dragTop(sender: UIPanGestureRecognizer) {
        
        guard messagingView.layer.animationKeys() == nil else { return }
        
        let point = sender.location(in: view)
        let offset = dragStart.y - point.y
        switch sender.state {
        case .began:
            dragStart = point
            dragStartHeight = messageViewHeight.constant
        case .changed:
            moveTop(to: offset + dragStartHeight)
        case .cancelled, .ended, .failed:
            settleTop(to: offset + dragStartHeight)
        default: ()
        }
    }
    
    private func moveTop(to height: CGFloat) {
        var keyboardHeight = abs(bottomContraint?.constant ?? 0)
        keyboardHeight = keyboardHeight > 100 ? keyboardHeight : 0
        self.messageViewHeight.constant = height - keyboardHeight
        self.view.layoutIfNeeded()
    }
    
    private func settleTop(to height: CGFloat) {
        var keyboardHeight = abs(bottomContraint?.constant ?? 0)
        keyboardHeight = keyboardHeight > 100 ? keyboardHeight : 0
        let correctedHeight = height - keyboardHeight
        
        let maxHeight = expandedMessagingViewMaxHeight
        let midHeight = min(collapsedMessagingViewMaxHeight, messagingView.height - messagingView.insets.top)
        let minHeight = CGFloat(73)
        
        let minDist = abs(minHeight - correctedHeight)
        let midDist = abs(midHeight - correctedHeight)
        let maxDist = abs(maxHeight - height)
        
        if minDist < midDist {
            collapseMessageView(fully: true, force: true)
        } else if midDist < maxDist {
            collapseMessageView(fully: false, force: true)
        } else {
            expandMessageView(force: true)
        }
    }
    
    // MARK: - interaction
    
    @objc private func updateData(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            guard let signal = notice.userInfo?["signal"] as? Signal, signal.id == self?.signalModel?.id else { return }
            guard let request = signal.requests?.first(where: { $0.id == self?.requestModel?.id }) else { return }
            
            let signalModel = SignalViewModel(signal: signal)
            let requestModel = RequestViewModel(request: request)
            self?.signalModel = signalModel
            self?.requestModel = requestModel
            self?.setup(signal: signalModel, request: requestModel)
            
            guard let msg = notice.userInfo?["message"] as? Message, !msg.isMine() else { return }
            
            if let height = self?.messageViewHeight.constant, height > 73 {
                if msg.readAt == nil {
                    DispatchQueue.global(qos: .default).async {
                        SignalTrackingService.shared.setMesageRead(signalId: signal.id, requestId: request.id, messageId: msg.id)
                    }
                }
            }
        }
    }
    
    private func acceptTimeIfNeeded() {
        
        // guard let profile = ProfileService.shared.profile else { return }
        guard let signalId = signalModel?.id else { return }
        guard let requestId = requestModel?.id else { return }
        
        guard signalModel?.accepted == false else { return }
        
        showLoading()
        SignalingService.shared.acceptRequest(for: signalId, request: requestId) { [weak self] (success, errorMsg) in
            
            self?.hideLoading()
            
            if success {
                self?.accepted?()

            } else {
                self?.errorAlert(text: errorMsg ?? "Unknown error")
            }
        }
        
    }
    
    private func sendMessage(_ text: String) {
        
        guard let profile = ProfileService.shared.profile,
            let signalId = signalModel?.id,
            let requestId = requestModel?.id
            else { return }
        
        let messageId = "\(profile.id.hashValue)-\(Date().hashValue)"
        let message = Message(id: messageId, createdAt: Date(), sender: profile.user, text: text, deliveredAt: nil, readAt: nil)
        
        SignalingService.shared.sendMessage(message, for: requestId, in: signalId) { (success, errorMsg) in
            if !success {
                // mark message as not sent
            }
        }
        
        messagingView.newMessage(MessageViewModel(message: message))
    }
    
    // MARK: - typing
    
    private var otherIsTyping = false
    
    private func trackTyping() {
        
        // me typing
        messagingView.input.isTypingChanged = { [weak self] in
            guard let isTyping = self?.messagingView.input.isTyping else { return }
            guard let signal = self?.signalModel else { return }
            guard let request = self?.requestModel else { return }
            
            SignalTrackingService.shared.setCreatorIsTyping(signalId: signal.id, requestId: request.id, isTyping: isTyping)
        }
        
        // other side typing
        guard let signal = signalModel else { return }
        guard let request = requestModel else { return }
        
        SignalTrackingService.shared.getRequesterIsTypingReference(signalId: signal.id, requestId: request.id)
            .observe(.value) { [weak self] (snapshot) in
                if snapshot.exists(), let isTyping = snapshot.value as? Bool {
                    if let height = self?.messageViewHeight.constant,
                        let max = self?.expandedMessagingViewMaxHeight,
                        height >= max {
                        self?.messagingView.tableView.showTyping = isTyping
                    }
                    self?.otherIsTyping = isTyping
                }
        }
    }
    
    private func stopTrackingTyping() {
        messagingView.input.isTypingChanged = nil
        if let signal = signalModel,
            let request = requestModel {
            SignalTrackingService.shared.removeTypingObservers(signalId: signal.id, requestId: request.id)
        }
    }
}

