const functions = require('firebase-functions');
const admin = require('firebase-admin');
const messaging = require('./messaging');

admin.initializeApp(functions.config().firebase);

//////////////////////////////////////////////////
//
// utils
//

function now() {
  return Math.floor(Date.now() / 1000) - 978307200;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371000; // Radius of the earth in m
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in m
  return d;
}

//////////////////////////////////////////////////
//
// Data utils
//

function countNumberOfGoings(time) {
  const creatorId = time.child('signal/creator/id').val();
  const requesterId = time.child('requester/id').val();

  const creatorState = time.child('creatorState').val();
  const requesterState = time.child('requesterState').val();

  if ((creatorState === "met" || creatorState === "closed") && 
      (requesterState === "met" || requesterState === "closed")) {

    return admin.database().ref("/users/" + creatorId)
    .once('value')
    .then((user) => {
      var count = user.child('numberOfGoings').val();
      if (count === null || count === undefined) {
        count = 0;
      }
      count = count + 1;
      return admin.database().ref("/users/" + creatorId + "/numberOfGoings").set(count);
    })
    .then((unused) => {
      return admin.database().ref("/users/" + requesterId)
      .once('value')
      .then((user) => {
        var count = user.child('numberOfGoings').val();
        if (count === null || count === undefined) {
          count = 0;
        }
        count = count + 1;
        return admin.database().ref("/users/" + requesterId + "/numberOfGoings").set(count);
      });
    });
  }

  return null;
}

//////////////////////////////////////////////////
//
// CRON
//

exports.fivemin_job = functions.pubsub
  .topic('fivemin-tick')
  .onPublish((message) => {

    let thirtyMinsAgo = now() - 1800;
    return admin.database().ref("signals/active").orderByChild("expiresAt").endAt(thirtyMinsAgo)
    .once('value')
    .then((signals) => {

      if (!signals.exists()) return null;

      return signals.forEach((child) => {
        const signalId = child.key;
        admin.database().ref("signals/active/" + signalId).remove();
      });
    });
  });

/*
exports.fivemin2_job = functions.pubsub
  .topic('fivemin2-tick')
  .onPublish((message) => {

    let nowTime = now();
    return admin.database().ref("signals/claimable").orderByChild("expiresAt").endAt(now)
    .once('value')
    .then((signals) => {

      if (!signals.exists()) return null;

      return signals.forEach((child) => {
        const signalId = child.key;
        admin.database().ref("signals/claimable/" + signalId).remove();
      });
    });
  });
*/

exports.tenmin_job = functions.pubsub
  .topic('tenmin-tick')
  .onPublish((message) => {

    let nowTime = now();
    let tenMinsAgo = nowTime - 600;
    return admin.database().ref("times/active").orderByChild("endedAt").startAt(tenMinsAgo).endAt(nowTime)
    .once('value')
    .then((times) => {

      if (!times.exists()) return null;

      return times.forEach((time) => {
        const timeId = time.key;

        const requesterState = time.child('requesterState').val();
        const creatorState = time.child('creatorState').val();

        if (creatorState !== "met" && creatorState !== "cancelled" && creatorState !== "closed") {
          admin.database().ref("times/active/" + timeId + "/creatorState").set("closed");
        }
        if (requesterState !== "met" && requesterState !== "cancelled" && requesterState !== "closed") {
          admin.database().ref("times/active/" + timeId + "/requesterState").set("closed");
        }

      });
    });
  });

exports.hourly_job = functions.pubsub
  .topic('hourly-tick')
  .onPublish((message) => {
    
    let nowTime = now();
    let threeHoursAgo = nowTime - 10800;
    let fourHoursAgo = nowTime - 14400;
    return admin.database().ref("times/active").orderByChild("endedAt").startAt(fourHoursAgo).endAt(threeHoursAgo)
    .once('value')
    .then((times) => {

      if (!times.exists()) return null;

      return times.forEach((time) => {
        
        const timeId = time.key;
        const creatorId = time.child('signal/creator/id').val();
        const timeText = time.child('signal/text').val();
        const requesterId = time.child('requester/id').val();

        const creatorState = time.child('creatorState').val();
        const requesterState = time.child('requesterState').val();

        if (creatorState === "cancelled" || requesterState === "cancelled") {

          admin.database().ref("times/cancelled/" + timeId).set(time.val());
          admin.database().ref("times/active/" + timeId).remove();
          return;
        }

        // PN to remind about reviews
        var creatorReviewed = false;
        var requesterReviewed = false;

        time.child('reviewList').forEach((child) => {
          const writterId = child.child('creator/id').val();
          if (writterId === creatorId) {
            creatorReviewed = true;
          } else if (writterId === requesterId) {
            requesterReviewed = true;
          }

          var title = "";
          var text = "";
          var params = {};
          params['type'] = 'timeReviewReminder';
          params['timeId'] = timeId;

          if (!creatorReviewed) {
            const requesterName = child.child("requester/name").val();
            title = "Review your Time with " + requesterName;
            text = "How was " + timeText;
            messaging.sendMessage(creatorId, title, text, params, '10');
          }

          if (!requesterReviewed) {
            const creatorName = child.child("signal/creator/name").val();
            title = "Review your Time with " + creatorName;
            text = "How was " + timeText;
            messaging.sendMessage(requesterId, title, text, params, '10');
          }        
        });
      });
    });
  });

exports.hourly1_4_job = functions.pubsub
  .topic('hourly1-4-tick')
  .onPublish((message) => {
    
    let nowTime = now();
    let dayAgo = nowTime - 86400;
    let dayAndHourAgo = nowTime - 90000;
    return admin.database().ref("times/active").orderByChild("endedAt").startAt(dayAndHourAgo).endAt(dayAgo)
    .once('value')
    .then((times) => {

      if (!times.exists()) return null;

      return times.forEach((time) => {
        
        const timeId = time.key;
        const creatorId = time.child('signal/creator/id').val();
        const timeText = time.child('signal/text').val();
        const requesterId = time.child('requester/id').val();

        // PN to remind about reviews
        var creatorReviewed = false;
        var requesterReviewed = false;

        time.child('reviewList').forEach((child) => {
          const writterId = child.child('creator/id').val();
          if (writterId === creatorId) {
            creatorReviewed = true;
          } else if (writterId === requesterId) {
            requesterReviewed = true;
          }

          var title = "Reminder: Write a Review";
          var text = "";
          var params = {};
          params['type'] = 'timeReviewReminder';
          params['timeId'] = timeId;

          if (!creatorReviewed) {
            const requesterName = child.child("requester/name").val();
            text = "Loved your Time with " + requesterName + "? Review the experience while the memory is still fresh!";
            messaging.sendMessage(creatorId, title, text, params, '10');
          }

          if (!requesterReviewed) {
            const creatorName = child.child("signal/creator/name").val();
            text = "Loved your Time with " + creatorName + "? Review the experience while the memory is still fresh!";
            messaging.sendMessage(requesterId, title, text, params, '10');
          }        
        });
      });
    });
  });

exports.hourly1_2_job = functions.pubsub
  .topic('hourly1_2-tick')
  .onPublish((message) => {
    
    let nowTime = now();
    let sixDaysAgo = nowTime - 518400;
    let sixDaysAndHourAgo = nowTime - 522000;
    return admin.database().ref("times/active").orderByChild("endedAt").startAt(sixDaysAndHourAgo).endAt(sixDaysAgo)
    .once('value')
    .then((times) => {

      if (!times.exists()) return null;

      return times.forEach((time) => {
        
        const timeId = time.key;
        const creatorId = time.child('signal/creator/id').val();
        const timeText = time.child('signal/text').val();
        const requesterId = time.child('requester/id').val();
        
        // PN to remind about reviews
        var creatorReviewed = false;
        var requesterReviewed = false;

        time.child('reviewList').forEach((child) => {
          const writterId = child.child('creator/id').val();
          if (writterId === creatorId) {
            creatorReviewed = true;
          } else if (writterId === requesterId) {
            requesterReviewed = true;
          }

          var title = "Last Chance to Review";
          var text = "";
          var params = {};
          params['type'] = 'timeReviewReminder';
          params['timeId'] = timeId;

          if (!creatorReviewed) {
            const requesterName = child.child("requester/name").val();
            text = "Review your Time with " + requesterName;
            messaging.sendMessage(creatorId, title, text, params, '10');
          }

          if (!requesterReviewed) {
            const creatorName = child.child("signal/creator/name").val();
            text = "Review your Time with " + creatorName;
            messaging.sendMessage(requesterId, title, text, params, '10');
          }        
        });
      });
    });
  });

exports.hourly3_4_job = functions.pubsub
  .topic('hourly3_4-tick')
  .onPublish((message) => {
    
    let nowTime = now();
    let sevenDaysAgo = nowTime - 604800;
    return admin.database().ref("times/active").orderByChild("endedAt").endAt(sevenDaysAgo)
    .once('value')
    .then((times) => {

      if (!times.exists()) return null;

      return times.forEach((time) => {

        // ids
        const creatorId = time.child('signal/creator/id').val();
        const requesterId = time.child('requester/id').val();
        
        // store review in user data
        var shortTime = {
          id: time.key,
          signal: time.child('signal').val(),
          createdAt: time.child('createdAt').val(),
        };

        // reviews
        var creatorReview = {};
        var requesterReview = {};

        // notice
        var title = "";
        var text = "";
        var review = "";

        // remove time
        admin.database().ref("/times/active/" + time.key).remove();

        return time.child('reviewList').forEach((child) => {
          const writterId = child.child('creator/id').val();
          if (writterId === creatorId) {

            creatorReview = {
              id: child.child('id').val(),
              creator: child.child('creator').val(),
              createdAt: child.child('createdAt').val(),
              time: shortTime,
              timeRate: child.child('timeRate').val(),
              userRate: child.child('userRate').val(),
              comment: child.child('comment').val(),
              userTags: child.child('userTags').val(),
              duration: child.child('duration').val(),
            };

            // store review
            admin.database().ref("/users/" + requesterId + "/reviewList/" + context.params.reviewId).set(creatorReview);

            // notify
            title = "Read " + creatorReview.creator.name + "'s Review";
            review = creatorReview.comment;
            if (review.length > 100) {
              review = review.substr(0, 97) + "...";
            }
            text = creatorReview.creator.name + "said:\n" + review;
            params['reviewId'] = creatorReview.id;

            messaging.sendMessage(requesterId, title, text, params, '10');

          } else if (writterId === requesterId) {

            requesterReview = {
              id: child.child('id').val(),
              creator: child.child('creator').val(),
              createdAt: child.child('createdAt').val(),
              time: shortTime,
              timeRate: child.child('timeRate').val(),
              userRate: child.child('userRate').val(),
              comment: child.child('comment').val(),
              userTags: child.child('userTags').val(),
              duration: child.child('duration').val(),
            };

            // store review
            admin.database().ref("/users/" + creatorId + "/reviewList/" + context.params.reviewId).set(requesterReview);

            // notify
            title = "Read " + requesterReview.creator.name + "'s Review";
            review = requesterReview.comment;
            if (review.length > 100) {
              review = review.substr(0, 97) + "...";
            }
            text = requesterReview.creator.name + "said:\n" + review;
            params['reviewId'] = requesterReview.id;

            messaging.sendMessage(creatorId, title, text, params, '10');
          }    
        });
      });
    });
  });

exports.daily_job = functions.pubsub
  .topic('daily-tick')
  .onPublish((message) => {
    console.log("This job is run every day!");
    return true;
  });

//////////////////////////////////////////////////
//
// users patch
//

exports.signalCreate = functions.database.ref('/users/{userId}')
  .onCreate((change, context) => {
    return admin.database().ref("/users/" + context.params.userId + "/verified").set(true);
  });

//////////////////////////////////////////////////
//
// tracking active signals
//

exports.signalCreate = functions.database.ref('/signals/active/{signalId}')
  .onCreate((change, context) => {

    // Create active signal in user list

    const creator = change.child('creator').val();
    const text = change.child('text').val();
    const type = change.child('type').val();
    const expiresAt = change.child('expiresAt').val();
    
    const signal = {
      text: text,
      type: type,
      expiresAt: now() + 1800,
      creator: creator,
    };

    return admin.database().ref("/users/" + creator.id + "/signals/active/" + context.params.signalId).set(signal);
  });

exports.signalRequest = functions.database.ref('/signals/active/{signalId}/requestList/{requestId}')
  .onCreate((change, context) => {

    const senderId = change.child('sender/id').val();
    const senderName = change.child('sender/name').val();
    const requestId = change.key;

    return admin.database().ref("/signals/active/" + context.params.signalId)
    .once('value')
    .then((signal) => {

      // Create active signal in user list

      const creator = signal.child('creator').val();
      const text = signal.child('text').val();
      const type = signal.child('type').val();
      const expiresAt = signal.child('expiresAt').val();
      
      const signalValue = {
        text: text,
        type: type,
        expiresAt: now() + 180,
        creator: creator,
        requestId: requestId,
      };

      admin.database().ref("/users/" + senderId + "/signals/requests/" + context.params.signalId).set(signalValue);

      // Send message

      const userId = signal.child('creator/id').val();

      var params = {};
      params['type'] = 'newRequest';
      params['requestId'] = context.params.requestId;
      params['signalId'] = context.params.signalId;

      const title = 'New Time Request';
      const msgText = senderName + ' is requesting a Time with you!';

      return messaging.sendMessage(userId, title, msgText, params, '10')
    })
    .catch((error) => {
      console.log("Signal request read failed: " + error);
    });

  });

exports.signalMessage = functions.database.ref('/signals/active/{signalId}/requestList/{requestId}/messageList/{messageId}')
  .onCreate((change, context) => {

    const senderId = change.child('sender/id').val();
    const senderName = change.child('sender/name').val();

    var params = {};
    params['type'] = 'newMessage';
    params['requestId'] = context.params.requestId;
    params['signalId'] = context.params.signalId;
    params['messageId'] = context.params.messageId;

    const title = senderName;
    const text = change.child('text').val();

    return admin.database().ref("/signals/active/" + context.params.signalId)  // + "/creator/id")
    .once('value')
    .then((signal) => {

      const creatorId = signal.child('creator/id').val();
      const requesterId = signal.child('requestList/' + context.params.requestId + '/sender/id').val();

      if (creatorId !== senderId) {
        return messaging.sendMessage(creatorId, title, text, params, '10');
      }
      return messaging.sendMessage(requesterId, title, text, params, '10');
    })
    .catch((error) => {
      console.log("Signal message read failed: " + error);
    });
  });

exports.signalResponse = functions.database.ref('/signals/active/{signalId}/requestList/{requestId}/accepted')
  .onCreate((change, context) => {

    if (change.val() !== true) {
      return null;
    }

    var params = {};
    params['type'] = 'newResponse';
    params['requestId'] = context.params.requestId;
    params['signalId'] = context.params.signalId;

    const title = 'Request accepted';

    return admin.database().ref("/signals/active/" + context.params.signalId)
    .once('value')
    .then((signal) => {

      const creator = signal.child('creator').val();
      const requester = signal.child('requestList/' + context.params.requestId + '/sender').val();
      const messages = signal.child('requestList/' + context.params.requestId + '/messageList').val();
      const text = signal.child('text').val();
      const type = signal.child('type').val();
      const imageUrls = signal.child('imageUrls').val();
      const imagePaths = signal.child('imagePaths').val();
      
      // copy signal to connected
      admin.database().ref("/signals/connected/" + context.params.signalId).set(signal.val());

      // Create active time
      var time = {
        signal: {
          id: context.params.signalId,
          text: text,
          type: type,
          expiresAt: now(),
          creator: creator,
          imageUrls: imageUrls,
          imagePaths: imagePaths
        },
        requester: requester,
        createdAt: now(),
        messageList: messages,
      };

      return time;
    })
    .then((time) => { 

      // Store active time

      admin.database().ref("/times/active/" + time.signal.id).set(time);

      // put time info in creator and requester lists

      admin.database().ref("/users/" + time.signal.creator.id + "/times/active/" + time.signal.id).set(time);
      admin.database().ref("/users/" + time.requester.id + "/times/active/" + time.signal.id).set(time);

      // remove image paths from signal (time will handle deletion from now on)
      admin.database().ref("/signals/active/" + time.signal.id + "/signal/imagePaths").remove();

      return time;
    })
    .then((time) => {

      // delete signal
      return admin.database().ref("/signals/active/" + time.signal.id).remove()
      .then(() => {
        return time;
      })
    })
    .then((time) => { 

      // close all time requests posted by creator

      return admin.database().ref("/users/" + time.signal.creator.id + "/signals/requests")
      .once('value')
      .then((requests) => {

        if (requests.exists()) {
          requests.forEach((child) => {
            const signalId = child.key;
            const requestId = child.child("requestId").val();

            if (signalId !== context.params.signalId) {
              admin.database().ref("signals/active/" + signalId + "/requestList/" + requestId + "/accepted").set(false);
            }
          });
        }
        return time;
      });
    })
    .then((time) => { 

      // close all time requests posted by requester

      return admin.database().ref("/users/" + time.requester.id + "/signals/requests")
      .once('value')
      .then((requests) => {

        if (requests.exists()) {
          requests.forEach((child) => {
            const signalId = child.key;
            const requestId = child.child("requestId").val();

            if (signalId !== context.params.signalId) {
              admin.database().ref("signals/active/" + signalId + "/requestList/" + requestId + "/accepted").set(false);
            }
          });
        }
        return time;
      });
    })
    .then((time) => {

      // remove all signals posted by creator

      return admin.database().ref("signals/active").orderByChild("creator/id").equalTo(time.signal.creator.id)
      .once('value')
      .then((signals) => {

        if (signals.exists()) {
          signals.forEach((child) => {
            const signalId = child.key;
            if (signalId !== context.params.signalId) {
              admin.database().ref("signals/active/" + signalId).remove();
            }
          });
        }
        return time;
      });
    })
    .then((time) => { 

      // remove all signals posted by requester

      return admin.database().ref("signals/active").orderByChild("creator/id").equalTo(time.requester.id)
      .once('value')
      .then((signals) => {

        if (signals.exists()) {
          signals.forEach((child) => {
            const signalId = child.key;
            if (signalId !== context.params.signalId) {
              admin.database().ref("signals/active/" + signalId).remove();
            }
          });
        }
        return time;
      });
    })
    .then((time) => {

      // Notification
      const userId = time.requester.id;
      const text = time.signal.creator.name + ' accepted your Time request!';

      return messaging.sendMessage(userId, title, text, params, '10');
    })
    .catch((error) => {
      console.log("Signal response read failed: " + error);
    });

  });

exports.signalRemove = functions.database.ref('/signals/active/{signalId}')
  .onDelete((change, context) => {

    // delete location
    return admin.database().ref("signalLocations/" + context.params.signalId).remove()
    .then(() => {

      // delete from creator list
      const creatorId = change.child('creator').child('id').val();
      return admin.database().ref("users/" + creatorId + "/signals/active/" + context.params.signalId).remove();
    })
    .then(() => {

      // delete from requestors lists
      return change.child('requestList').forEach((child) => {
        const requesterId = child.child('sender').child('id').val();
        admin.database().ref("users/" + requesterId + "/signals/requests/" + context.params.signalId).remove();      
      });
    })
    .then(() => {

      console.log("Paths: " + JSON.stringify(change.child('imagePaths').val()));

      // delete image resources
      return change.child('imagePaths').forEach((child) => {
        const path = child.val();

        console.log("Removing image path: " + path);

        admin.storage().bucket().file(path).delete();
      });
    });
  });

//////////////////////////////////////////////////
//
// tracking active times
//

exports.timeMessage = functions.database.ref('/times/active/{timeId}/messageList/{messageId}')
  .onCreate((change, context) => {

    const senderId = change.child('sender/id').val();
    const senderName = change.child('sender/name').val();

    var params = {};
    params['type'] = 'newMessage';
    params['timeId'] = context.params.timeId;
    params['messageId'] = context.params.messageId;

    const title = senderName;
    const text = change.child('text').val();

    return admin.database().ref("/times/active/" + context.params.timeId)
    .once('value')
    .then((time) => {
      
      const creatorId = time.child('signal/creator/id').val();
      const requesterId = time.child('requester/id').val();

      if (creatorId !== senderId) {
        return messaging.sendMessage(creatorId, title, text, params, '10');
      }
      return messaging.sendMessage(requesterId, title, text, params, '10');

    })
    .catch((error) => {
      console.log("Time message read failed: " + error);
    });

  });

exports.timeReview = functions.database.ref('/times/active/{timeId}/reviewList/{reviewId}/')
  .onCreate((change, context) => {

    const reviewerId = change.child('creator/id').val();
    const reviewerName = change.child('creator/name').val();

    var params = {};
    params['type'] = 'newReview';
    params['timeId'] = context.params.timeId;
    
    return admin.database().ref("/times/active/" + context.params.timeId)
    .once('value')
    .then((time) => {

      // notification
      const creatorId = time.child('signal/creator/id').val();
      const requesterId = time.child('requester/id').val();
      
      // store review in user data
      var shortTime = {
        id: context.params.timeId,
        signal: time.child('signal').val(),
        createdAt: time.child('createdAt').val(),
        endedAt: time.child('endedAt').val(),
      };

      // check if both reviews are there
      var creatorReviewed = false;
      var requesterReviewed = false;
      var creatorReview = {};
      var requesterReview = {};

      return time.child('reviewList').forEach((child) => {
        const writterId = child.child('creator/id').val();
        if (writterId === creatorId) {
          creatorReviewed = true;

          creatorReview = {
            id: child.child('id').val(),
            creator: child.child('creator').val(),
            createdAt: child.child('createdAt').val(),
            time: shortTime,
            timeRate: child.child('timeRate').val(),
            userRate: child.child('userRate').val(),
            comment: child.child('comment').val(),
            userTags: child.child('userTags').val(),
            duration: child.child('duration').val(),
          };

        } else if (writterId === requesterId) {
          requesterReviewed = true;

          requesterReview = {
            id: child.child('id').val(),
            creator: child.child('creator').val(),
            createdAt: child.child('createdAt').val(),
            time: shortTime,
            timeRate: child.child('timeRate').val(),
            userRate: child.child('userRate').val(),
            comment: child.child('comment').val(),
            userTags: child.child('userTags').val(),
            duration: child.child('duration').val(),
          };
        }

        var title = "";
        var text = "";
        var review = "";

        if (creatorReviewed && requesterReviewed) {

          // add reviews
          admin.database().ref("/users/" + creatorId + "/reviewList/" + context.params.reviewId).set(requesterReview);
          admin.database().ref("/users/" + requesterId + "/reviewList/" + context.params.reviewId).set(creatorReview);

          // remove time
          admin.database().ref("/times/active/" + context.params.timeId).remove();

          // notify requester
          title = "Read " + creatorReview.creator.name + "'s Review";
          review = creatorReview.comment;
          if (review.length > 100) {
            review = review.substr(0, 97) + "...";
          }
          text = creatorReview.creator.name + "said:\n" + review;
          params['reviewId'] = creatorReview.id;

          messaging.sendMessage(requesterId, title, text, params, '10');

          // notify crator
          title = "Read " + requesterReview.creator.name + "'s Review";
          review = requesterReview.comment;
          if (review.length > 100) {
            review = review.substr(0, 97) + "...";
          }
          text = requesterReview.creator.name + "said:\n" + review;
          params['reviewId'] = requesterReview.id;

          messaging.sendMessage(creatorId, title, text, params, '10');

        } else {

          title = reviewerName + " Reviewed Your Time";
          text = "Leave your review to see what they‌ wrote!";
          params['reviewId'] = context.params.reviewId;

          var receiverId = creatorId;
          if (creatorId === reviewerId) {
            receiverId = requesterId;
          }
          messaging.sendMessage(receiverId, title, text, params, '10');
        }

        return null;
      });

    })
    .catch((error) => {
      console.log("Time review read failed: " + error);
    });

  });

exports.timeCreatorLocation = functions.database.ref('/times/active/{timeId}/signal/creator/location')
  .onWrite((change, context) => {

    if (!change.after.exists()) {
      return null;
    }

    const creatorLocation = change.after.val();

    var params = {};
    params['timeId'] = context.params.timeId;

    return admin.database().ref("/times/active/" + context.params.timeId)
    .once('value')
    .then((time) => {

      const creatorState = time.child('creatorState').val();
      const requesterState = time.child('requesterState').val();

      if (creatorState === "cancelled" || requesterState === "cancelled") {
        return null;
      }

      const creatorId = time.child('signal/creator/id').val();
      const creatorName = time.child('signal/creator/name').val();
      const requesterId = time.child('requester/id').val();
      const requesterName = time.child('requester/nam').val();
      const meet = time.child('meeting').val();
      const requesterLocation = time.child('requester/location').val();

      var title = "You've met!";
      var text = "You have met with ";

      if (creatorState === "queryMet" || creatorState === "met" || creatorState === "closed") {
        return null;
      }

      if (requesterLocation !== undefined && requesterLocation !== null &&
        getDistanceFromLatLonInKm(creatorLocation.latitude, creatorLocation.longitude, 
        requesterLocation.latitude, requesterLocation.longitude) < 15) {

        admin.database().ref("/times/active/" + context.params.timeId + "/creatorState").set("queryMet");
        admin.database().ref("/times/active/" + context.params.timeId + "/requesterState").set("queryMet");

        title = "Did You Find Each Other?";
        params['type'] = 'timeMet';
        text = "Please confirm you and " + requesterName + " have met up.";

        messaging.sendMessage(creatorId, title, text, params, '10');

        text = "Please confirm you and " + creatorName + " have met up.";        
        return messaging.sendMessage(requesterId, title, text, params, '10');
      }

      if (meet === undefined || meet === null) {
        return null;
      }

      if (creatorState === "queryArrived" || creatorState === "arrived") {
        return null;
      }

      if (getDistanceFromLatLonInKm(creatorLocation.latitude, creatorLocation.longitude, 
        meet.latitude, meet.longitude) < 15) {

        admin.database().ref("/times/active/" + context.params.timeId + "/creatorState").set("queryArrived");

        if (requesterState === "queryArrived" || requesterState === "arrived") {
          admin.database().ref("/times/active/" + context.params.timeId + "/arrivedExpiresAt").set(now() + 1800);
        }

        title = "Did you arrive?";
        params['type'] = 'timeArrived';
        params['userId'] = creatorId;
        text = "Let " + requestName + " know you're at the meeting point.";

        return messaging.sendMessage(creatorId, title, text, params, '10');
      }

      return null;
    })
    .catch((error) => {
      console.log("Time Creator Location read failed: " + error);
    });

  });

exports.timeRequesterLocation = functions.database.ref('/times/active/{timeId}/requester/location')
  .onWrite((change, context) => {

    if (!change.after.exists()) {
      return null;
    }

    const requesterLocation = change.after.val();

    var params = {};
    params['timeId'] = context.params.timeId;

    return admin.database().ref("/times/active/" + context.params.timeId)
    .once('value')
    .then((time) => {

      const creatorState = time.child('creatorState').val();
      const requesterState = time.child('requesterState').val();

      if (creatorState === "cancelled" || requesterState === "cancelled") {
        return null;
      }

      const creatorId = time.child('signal/creator/id').val();
      const creatorName = time.child('signal/creator/name').val();
      const requesterId = time.child('requester/id').val();
      const requesterName = time.child('requester/nam').val();
      const meet = time.child('meeting').val();
      const creatorLocation = time.child('signal/creator/location').val();

      var title = "You've met!";
      var text = "You have met with ";

      if (requesterState === "queryMet" || requesterState === "met" || requesterState === "closed") {
        return null;
      }

      if (creatorLocation !== undefined && creatorLocation !== null &&
        getDistanceFromLatLonInKm(creatorLocation.latitude, creatorLocation.longitude, 
        requesterLocation.latitude, requesterLocation.longitude) < 15) {

        admin.database().ref("/times/active/" + context.params.timeId + "/creatorState").set("queryMet");
        admin.database().ref("/times/active/" + context.params.timeId + "/requesterState").set("queryMet");

        title = "Did You Find Each Other?"
        params['type'] = 'timeMet';
        text = "Please confirm you and " + requesterName + " have met up.";

        messaging.sendMessage(creatorId, title, text, params, '10');

        text = "Please confirm you and " + creatorName + " have met up.";        
        return messaging.sendMessage(requesterId, title, text, params, '10');
      }

      if (meet === undefined || meet === null) {
        return null;
      }

      if (requesterState === "queryArrived" || requesterState === "arrived") {
        return null;
      }

      if (getDistanceFromLatLonInKm(requesterLocation.latitude, requesterLocation.longitude, 
        meet.latitude, meet.longitude) < 15) {

        admin.database().ref("/times/active/" + context.params.timeId + "/requesterState").set("queryArrived");

        if (creatorState === "queryArrived" || creatorState === "arrived") {
          admin.database().ref("/times/active/" + context.params.timeId + "/arrivedExpiresAt").set(now() + 1800);
        }

        title = "Did you arrive?";
        params['type'] = 'timeArrived';
        params['userId'] = requesterId;
        text = "Let " + creatorName + " know you're at the meeting point.";

        return messaging.sendMessage(requesterId, title, text, params, '10');
      }

      return null;
    })
    .catch((error) => {
      console.log("Time Creator Location read failed: " + error);
    });

  });

exports.timeCreatorCompleted = functions.database.ref('/times/active/{timeId}/creatorState')
  .onWrite((change, context) => {

    if (!change.after.exists()) {
      return null;
    }

    const state = change.after.val();
    return admin.database().ref("/times/active/" + context.params.timeId)
    .once('value')
    .then((time) => {

      const creatorId = time.child('signal/creator/id').val();
      const creatorName = time.child('signal/creator/name').val();
      const creatorMessage = time.child('creatorCancelMessage').val();
      const requesterId = time.child('requester/id').val();
      const requesterName = time.child('requester/name').val();
      const creatorState = time.child('creatorState').val();
      const requesterState = time.child('requesterState').val();
      const endedAt = time.child('endedAt').val();

      var params = {};
      params['timeId'] = context.params.timeId;
      params['userId'] = creatorId;
      var title = "";
      var text = "";

      if (state === "met" || state === "closed") {
        countNumberOfGoings(time);
      }

      if (state === "arrived") {

        admin.database().ref("/times/active/" + context.params.timeId + "/endedAt").set(now() + 900);

        params['type'] = 'timeArrived';
        title = creatorName + " has arrived!";
        text = "Great news! " + creatorName + " just arrived at the meeting point.";
        if (requesterState === "arrived") {
          text = creatorName + " is nearby! Confirm in the app when you find each other.";
        }
        return messaging.sendMessage(requesterId, title, text, params, '10');
      }

      if (state === "met") {
        
        var retVal = null;

        var endTime = now();
        if (requesterState !== "met" && requesterState !== "cancelled" && requesterState !== "closed") {
          endTime = now() + 900;
        }
        if (endedAt === undefined || endedAt === null || endedAt > endTime) {
          retVal = admin.database().ref("/times/active/" + context.params.timeId + "/endedAt").set(endTime);
        }

        if (requesterState !== "met" && requesterState !== "cancelled" && requesterState !== "closed") {
          params['type'] = 'timeMet';
          title = creatorName + " says you've met!";
          text = creatorName + " says you've found each other. Please confirm to end navigation.";
          retVal = messaging.sendMessage(requesterId, title, text, params, '10');
        }
        return retVal;
      }

      if (state === "cancelled") {
      
        admin.database().ref("/times/active/" + context.params.timeId + "/endedAt").set(now());

        params['type'] = 'timeCancelled';

        title = creatorName + " has cancelled!";
        text = creatorName + " has cancelled the Time.";
        if (creatorMessage !== null && creatorMessage !== undefined) {
          text = creatorMessage;
        }
        return messaging.sendMessage(requesterId, title, text, params, '10');
      }

      return null;
    });
  });

exports.timeRequesterCompleted = functions.database.ref('/times/active/{timeId}/requesterState')
  .onWrite((change, context) => {

    if (!change.after.exists()) {
      return null;
    }

    const state = change.after.val();
    return admin.database().ref("/times/active/" + context.params.timeId)
    .once('value')
    .then((time) => {

      const creatorId = time.child('signal/creator/id').val();
      const creatorName = time.child('signal/creator/name').val();
      const requesterId = time.child('requester/id').val();
      const requesterName = time.child('requester/name').val();
      const creatorState = time.child('creatorState').val();
      const requesterState = time.child('requesterState').val();
      const requesterMessage = time.child('requesterCancelMessage').val();
      const endedAt = time.child('endedAt').val();

      var params = {};
      params['timeId'] = context.params.timeId;
      params['userId'] = requesterId;
      var title = "";
      var text = "";

      if (state === "met" || state === "closed") {
        countNumberOfGoings(time);
      }

      if (state === "arrived") {

        admin.database().ref("/times/active/" + context.params.timeId + "/endedAt").set(now() + 900);

        params['type'] = 'timeArrived';

        title = requesterName + " has arrived!";
        text = "Great news! " + requesterName + " just arrived at the meeting point.";
        if (creatorState === "arrived") {
          text = requesterName + " is nearby! Confirm in the app when you find each other.";
        }
        return messaging.sendMessage(creatorId, title, text, params, '10');
      }

      if (state === "met") {

        var retVal = null;

        var endTime = now();
        if (creatorState !== "met" && creatorState !== "cancelled" && creatorState !== "closed") {
          endTime = now() + 900;
        }
        if (endedAt === undefined || endedAt === null || endedAt > endTime) {
          retVal = admin.database().ref("/times/active/" + context.params.timeId + "/endedAt").set(endTime);
        }

        if (creatorState !== "met" && creatorState !== "cancelled" && creatorState !== "closed") {
          params['type'] = 'timeMet';
          title = requesterName + " says you've met!";
          text = requesterName + " says you've found each other. Please confirm to end navigation.";
          retVal = messaging.sendMessage(creatorId, title, text, params, '10');
        }
        return retVal;
      }

      if (state === "cancelled") {
      
        admin.database().ref("/times/active/" + context.params.timeId + "/endedAt").set(now());

        params['type'] = 'timeCancelled';

        title = requesterName + " has cancelled!";
        text = requesterName + " has cancelled the Time.";
        if (requesterMessage !== null && requesterMessage !== undefined) {
          text = requesterMessage;
        }
        return messaging.sendMessage(creatorId, title, text, params, '10');
      }

      return null;
    });
  });

exports.timeRemove = functions.database.ref('/times/active/{timeId}')
  .onDelete((change, context) => {

    const creatorId = change.child('signal/creator/id').val();
    const requesterId = change.child('requester/id').val();

    // delete from creator list      
    return admin.database().ref("users/" + creatorId + "/times/active/" + context.params.timeId).remove()
    .then(() => {

      // delete from requestors lists
      return admin.database().ref("users/" + requesterId + "/times/active/" + context.params.timeId).remove();
    })
    .then(() => {

      // delete image resources
      return change.child('signal/imagePaths').forEach((child) => {
        const path = child.val();
        admin.storage().bucket().file(path).delete();
      });
    });
  });
