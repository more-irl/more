//
//  SCPageViewController+More.h
//  More
//
//  Created by Luko Gjenero on 12/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

#ifndef SCPageViewController_More_h
#define SCPageViewController_More_h

@import SCPageViewController;

@interface SCPageViewController (More)

- (void)_updateBoundsAndConstraints;
- (void)_centerOnPageIndex:(NSUInteger)pageIndex;

@end


#endif /* SCPageViewController_More_h */
