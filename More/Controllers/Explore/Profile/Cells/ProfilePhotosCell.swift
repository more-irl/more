//
//  ProfilePhotosCell.swift
//  More
//
//  Created by Luko Gjenero on 18/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ProfilePhotosCell: ProfileBaseCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    private static let cellIdentifier = "ImageCell"
    
    @IBOutlet private weak var maskingView: UIView!
    @IBOutlet private weak var photos: UICollectionView!
    @IBOutlet private weak var page: UIPageControl!
    @IBOutlet private weak var placeholderImage: UIImageView!
    
    private var photoURLs: [String] = []
    
    var placeholder: UIImage? {
        get {
            return placeholderImage.image
        }
        set {
            placeholderImage.image = newValue
            placeholderImage.isHidden = newValue == nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        photos.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        photos.register(Cell.self, forCellWithReuseIdentifier: ProfilePhotosCell.cellIdentifier)
        photos.dataSource = self
        photos.delegate = self
        
        page.addTarget(self, action: #selector(pageChanged), for: .valueChanged)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // maskingView.layoutIfNeeded()
        maskingView.layer.cornerRadius = maskingView.frame.width * 0.5
        maskingView.layer.masksToBounds = true
        
        if let layout = photos.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = bounds.size
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                photos.collectionViewLayout.invalidateLayout()
                photos.reloadData()
            }
        }
    }
    
    @objc private func pageChanged() {
        photos.scrollToItem(at: IndexPath(item: page.currentPage, section: 0), at: .left, animated: true)
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoURLs.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfilePhotosCell.cellIdentifier, for: indexPath) as? Cell {
            cell.setup(for: photoURLs[indexPath.item])
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = max(0,min(Int(round(scrollView.contentOffset.x / scrollView.frame.width)), photoURLs.count))
        page.currentPage = currentPage
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let image: UIImageView = {
            let image = UIImageView(frame: .zero)
            image.translatesAutoresizingMaskIntoConstraints = false
            image.backgroundColor = .clear
            image.contentMode = .scaleAspectFill
            image.clipsToBounds = true
            return image
        }()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.addSubview(image)
            image.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
            image.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            image.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(for url: String) {
            guard let url = URL(string: url) else {
                image.sd_cancelCurrentImageLoad_progressive()
                image.image = nil
                return
            }
            
            image.sd_progressive_setImage(with: url, placeholderImage: UIImage.profilePlaceholder(), options: .highPriority)
        }
        
    }
    
    
    
    // MARK: - base
    
    override func setup(for model: UserViewModel) {
        photoURLs = model.imageUrls
        photos.collectionViewLayout.invalidateLayout()
        photos.reloadData()
        
        page.numberOfPages = photoURLs.count
        page.isHidden = photoURLs.count <= 1
    }
    
    override class func size(for model: UserViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: size.width * 0.75)
    }
    
    override class func isShowing(for model: UserViewModel) -> Bool {
        return true
    }
}
