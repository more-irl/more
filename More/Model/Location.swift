//
//  Location.swift
//  More
//
//  Created by Luko Gjenero on 31/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//


import Firebase
import CoreLocation

struct Location: MoreDataObject {

    let id: String = ""
    var latitude: Double? = nil
    var longitude: Double? = nil
    var name: String? = nil
    var address: String? = nil
    
    init(latitude: Double?,
         longitude: Double?,
         name: String? = nil,
         address: String? = nil) {
        self.latitude = latitude
        self.longitude = longitude
        self.name = name
        self.address = address
    }
    
    init(coordinates: CLLocationCoordinate2D,
         name: String? = nil,
         address: String? = nil) {
        self.latitude = coordinates.latitude
        self.longitude = coordinates.longitude
        self.name = name
        self.address = address
    }
    
    // Hashable
    
    public var hashValue: Int {
        return (latitude.hashValue &* 397) &+ longitude.hashValue
    }
    
    static func == (lhs: Location, rhs: Location) -> Bool {
        return Double.equal(lhs.latitude ?? 0, rhs.latitude ?? 0, precise: 5) &&
            Double.equal(lhs.longitude ?? 0, rhs.longitude ?? 0, precise: 5)
    }
    
    // Data protocol
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                return json
            }
            return nil
        }
        set {
            guard let newValue = newValue else { return }
            if let jsonData = try? JSONSerialization.data(withJSONObject: newValue, options: []),
                let location = try? JSONDecoder().decode(Location.self, from: jsonData) {
                
                self.latitude = location.latitude
                self.longitude = location.longitude
            }
        }
    }
    
    static func fromJson(_ json: [String : Any]) -> Location? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let location = try? JSONDecoder().decode(Location.self, from: jsonData) {
            return location
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> Location? {
        if let json = snapshot.value as? [String: Any] {
            return Location.fromJson(json)
        }
        return nil
    }
    
    func coordinates() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude ?? 0, longitude: longitude ?? 0)
    }
    
    func location() -> CLLocation {
        return CLLocation(latitude: latitude ?? 0, longitude: longitude ?? 0)
    }
    
    func center(between otherLocation: Location) -> Location {
        let lat = ((latitude ?? 0) + (otherLocation.latitude ?? 0)) * 0.5
        let lon = ((longitude ?? 0) + (otherLocation.longitude ?? 0)) * 0.5
        return Location(latitude: lat, longitude: lon)
    }
}

