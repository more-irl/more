//
//  ExploreSavedDockView.swift
//  More
//
//  Created by Luko Gjenero on 10/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class ExploreSavedDockView: LoadableView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    private static let cellIdentifier = "ExploreSavedDockCollectionViewCell"
    
    @IBOutlet private weak var saveLabelContainer: UIView!
    @IBOutlet private weak var saveLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var models: [SignalViewModel] = []
    
    var insertScale: CGFloat {
        get {
            if let layout = collectionView.collectionViewLayout as? ExploreSavedDockLayout {
                return layout.insertScale
            }
            return 1
        }
        set {
            if let layout = collectionView.collectionViewLayout as? ExploreSavedDockLayout {
                layout.insertScale = newValue
            }
        }
    }
    
    override func setupNib() {
        super.setupNib()
        setup()
    }
    
    private func setup() {
        saveLabelContainer.isHidden = false
        saveLabelContainer.layer.borderColor = UIColor(red: 223, green: 226, blue: 231).cgColor
        saveLabelContainer.layer.borderWidth = 1
        saveLabelContainer.layer.cornerRadius = 30
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        collectionView.register(UINib(nibName: "ExploreSavedDockCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ExploreSavedDockView.cellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isUserInteractionEnabled = false
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        }
    }
    
    private var cellSize: CGSize {
        return CGSize(width: collectionView.frame.width , height: 60)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionView.layoutIfNeeded()
            let itemSize = cellSize
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                collectionView.collectionViewLayout.invalidateLayout()
                collectionView.reloadData()
            }
        }
    }
    
    func setup(for models: [SignalViewModel]) {
        saveLabelContainer.isHidden = !models.isEmpty
        updateData(models)
    }
    
    private func updateData(_ models: [SignalViewModel]) {
        
        if models.count > self.models.count {
            self.models = models
            insertCell(at: 0)
            return
        }
        /*
        else if models.count < self.models.count {
            var deletions: [Int] = []
            for (idx, model) in self.models.enumerated() {
                if !models.contains(model) {
                    deletions.append(idx)
                }
            }
            if self.models.count == models.count + deletions.count {
                self.models = models
                deleteCells(at: deletions)
                return
            }
        }
        */
        
        self.models = models
        collectionView.reloadData()
    }
    
    private func insertCell(at idx: Int) {
        collectionView.insertItems(at: [IndexPath(item: idx, section: 0)])
    }
    
    private func deleteCells(at indicies: [Int]) {
        collectionView.deleteItems(at: indicies.map { IndexPath(item: $0, section: 0) })
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExploreSavedDockView.cellIdentifier, for: indexPath) as? ExploreSavedDockCollectionViewCell {
            cell.setup(for: models[indexPath.item])
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt: IndexPath) {
        // TODO
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // TODO
    }
}


