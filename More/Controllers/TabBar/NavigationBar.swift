//
//  NavigationBar.swift
//  More
//
//  Created by Luko Gjenero on 01/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class NavigationBar: LoadableView {

    @IBOutlet private weak var leftButton: UIButton!
    @IBOutlet private weak var rightButton: UIButton!
    @IBOutlet weak var titleLabel: SpecialLabel!
    @IBOutlet weak var topBackground: UIView!
    
    var leftTap: (()->())?
    var rightTap: (()->())?
    
    func setLeftIcon(_ icon: UIImage, selectedIcon: UIImage? = nil) {
        leftButton.setImage(icon, for: .normal)
        leftButton.setImage(selectedIcon, for: .selected)
    }
    
    func setRightIcon(_ icon: UIImage, selectedIcon: UIImage? = nil) {
        rightButton.setImage(icon, for: .normal)
        rightButton.setImage(selectedIcon, for: .selected)
    }
    
    func setTitle(_ title: String) {
        titleLabel.text = title
    }
    
    var leftSelected: Bool {
        get {
            return leftButton.isSelected
        }
        set {
            leftButton.isSelected = newValue
        }
    }
    
    var rightSelected: Bool {
        get {
            return rightButton.isSelected
        }
        set {
            rightButton.isSelected = newValue
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            topBackground.backgroundColor = backgroundColor
        }
    }
    
    @IBAction private func leftTouch(_ sender: Any) {
        leftTap?()
    }
    
    @IBAction private func rightTouch(_ sender: Any) {
        rightTap?()
    }
}
