//
//  SignalDetailsReportCell.swift
//  More
//
//  Created by Luko Gjenero on 16/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsReportCell: SignalDetailsBaseCell {

    static let height: CGFloat = 116
    
    @IBOutlet weak var label: UILabel!
        
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        
        let text = NSMutableAttributedString()
        
        var part = NSAttributedString(
            string: "\(model.creator.name) has gone on",
            attributes: [NSAttributedStringKey.foregroundColor : label.textColor,
                         NSAttributedStringKey.font : label.font])
        text.append(part)
        
        var font = UIFont(name: "DIN-Bold", size: 24) ?? UIFont.systemFont(ofSize: 24)
        part = NSAttributedString(
            string: " \(model.creator.numOfGoings) ",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        let timesText = model.creator.numOfGoings == 1 ? "Time." : "Times."
        font = UIFont(name: "Avenir-Heavy", size: 14) ?? UIFont.systemFont(ofSize: 14)
        part = NSAttributedString(
            string: timesText,
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        label.attributedText = text
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: SignalDetailsReportCell.height)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return model.kind == .standard
    }
}
