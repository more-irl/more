//
//  TimeHeader.swift
//  More
//
//  Created by Luko Gjenero on 04/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

@IBDesignable
class TimeHeader: LoadableView {

    @IBOutlet private weak var avatar: AvatarImage!
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var countdownLabel: UILabel!
    @IBOutlet private weak var countdown: CountdownLabel!
    
    override func setupNib() {
        super.setupNib()
        countdown.setDateFormat("m:ss")
    }
    
    func setup(for model: SignalViewModel) {
        
        if model.kind == .claimable {
            avatar.image = UIImage(named: "more-icon")
            name.text = "Claimable"
            countdownLabel.text = "Ends:"
            countdown.reset()
            let df = DateFormatter()
            df.dateFormat = "MMM d, h:mma"
            countdown.text = df.string(from: model.expiresAt)
        } else if model.kind == .curated {
            avatar.image = UIImage(named: "more-icon")
            name.text = "Curated"
            countdownLabel.text = "Ends:"
            countdown.reset()
            let df = DateFormatter()
            df.dateFormat = "MMM d, h:mma"
            countdown.text = df.string(from: model.expiresAt)
        } else {
            if model.mine {
                name.text = "You"
            } else {
                name.text = model.creator.name
            }
            avatar.sd_progressive_setImage(with: URL(string: model.creator.avatarUrl), placeholderImage: UIImage.profileThumbPlaceholder())
            countdownLabel.text = "Time expires in"
            countdown.countdown(to: model.expiresAt)
        }
    }

}
