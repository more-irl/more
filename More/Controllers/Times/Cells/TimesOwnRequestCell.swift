//
//  TimesOwnRequestCell.swift
//  More
//
//  Created by Luko Gjenero on 05/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class TimesOwnRequestCell: TimesBaseCell {

    static let height: CGFloat = 196
    
    @IBOutlet weak var content: UIView!
    @IBOutlet private weak var header: TimeHeader!
    @IBOutlet private weak var quote: UILabel!
    @IBOutlet private weak var removeButton: UIButton!
    
    var removeTap: (()->())?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.enableShadow(color: .black, path: UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height - 25)).cgPath)
    }
    
    @IBAction func removeTouch(_ sender: Any) {
        removeTap?()
    }
    
    // MARK: - base
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: TimesOwnRequestCell.height)
    }
    
    override func setup(for model: SignalViewModel) {
        header.setup(for: model)
        quote.text = model.quote
    }
    
}
