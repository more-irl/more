//
//  SignalDetailsMapCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import MapKit
import Mapbox

class SignalDetailsMapCell: SignalDetailsBaseCell, MGLMapViewDelegate {

    private static let fullSize: CGFloat = 488
    private static let fullSizeMapPadding: CGFloat = 228
    
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var nearby: SignalJourneyItemView!
    @IBOutlet private weak var separator: UIView!
    @IBOutlet private weak var destination: SignalJourneyItemView!
    @IBOutlet private weak var mapBox: MGLMapView!
    @IBOutlet private weak var shadow: FadeView!
    @IBOutlet private weak var mapPadding: NSLayoutConstraint!
    
    private var annotationTitle: String? = nil
    private var annotationText: String? = nil
    private var location: Location? = nil
    private var animating: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shadow.orientation = .up
        shadow.color = UIColor.black.withAlphaComponent(0.1)
        
        mapBox.delegate = self
        mapBox.styleURL = URL(string: MapBox.styleUrl)
    }
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        
        title.text = "Your Journey"
        
        nearby.setup(for: model)
        destination.setup(for: model)
        
        if model.destination != nil {
            self.destination.isHidden = false
            separator.isHidden = false
            mapPadding.constant = SignalDetailsMapCell.fullSizeMapPadding
            nearby.bottomConnectionHidden = false
            destination.topConnectionHidden = false
        } else {
            destination.isHidden = true
            separator.isHidden = true
            mapPadding.constant = SignalDetailsMapCell.fullSizeMapPadding - 82
            nearby.bottomConnectionHidden = true
            destination.topConnectionHidden = true
        }
        
        // map
        if model.kind == .claimable {
            if let location = model.location {
                setupMidpoint(at: location.coordinates())
            }
        } else if model.kind == .curated {
            // nothign for now
        } else if let location = model.creator.location {
            setupMidpoint(at: location.coordinates())
        }
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        if model.destination != nil {
            return CGSize(width: size.width, height: SignalDetailsMapCell.fullSize)
        }
        return CGSize(width: size.width, height: SignalDetailsMapCell.fullSize - 82)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return true
    }
    
    // MARK: - signal preview
    
    override func setup(for model: CreateSignalViewModel) {
        title.text = "Your Journey"
        
        nearby.type = SignalJourneyItemView.ItemType.nearby.rawValue
        nearby.setup(for: model)
        
        destination.setup(for: model)
        
        if model.destination != nil {
            self.destination.isHidden = false
            mapPadding.constant = SignalDetailsMapCell.fullSizeMapPadding
            nearby.bottomConnectionHidden = false
            destination.topConnectionHidden = false
        } else {
            destination.isHidden = true
            mapPadding.constant = SignalDetailsMapCell.fullSizeMapPadding - 82
            nearby.bottomConnectionHidden = true
            destination.topConnectionHidden = true
        }
        
        // map
        if let location = LocationService.shared.currentLocation {
            setupMap(at: location.coordinate)
        }
    }
    
    override class func size(for model: CreateSignalViewModel, in size: CGSize) -> CGSize {
        if model.destination != nil {
            return CGSize(width: size.width, height: SignalDetailsMapCell.fullSize)
        }
        return CGSize(width: size.width, height: SignalDetailsMapCell.fullSize - 82)
    }
    
    override class func isShowing(for model: CreateSignalViewModel) -> Bool {
        return true
    }
    
    // MARK: - map
    
    private func setupMidpoint(at location: CLLocationCoordinate2D) {
        
        let camera = MGLMapCamera(lookingAtCenter: location, altitude: Math.mileInMeters * 4, pitch: 50, heading: 0)
        mapBox.setCamera(camera, animated: false)
        
        if let me = LocationService.shared.currentLocation {
            GeoService.shared.getMidpoint(from: me.coordinate, to: location) { [weak self] (midpoint, _) in
                DispatchQueue.main.async {
                    if let midpoint = midpoint {
                        self?.setupMap(at: midpoint.coordinates())
                    } else {
                        self?.randomLocation(Location(coordinates: location))
                    }
                }
            }
        } else {
            randomLocation(Location(coordinates: location))
        }
    }
    
    private func randomLocation(_ location: Location) {
        
        // we randomize the location for safety
        let region = MKCoordinateRegionMakeWithDistance(location.coordinates(), Math.mileInMeters * 0.2, Math.mileInMeters * 0.2)
        var coordinates = location.coordinates()
        var random = Double(arc4random()) / Double(UInt32.max) - 0.5
        coordinates.latitude += region.span.latitudeDelta * random
        random = Double(arc4random()) / Double(UInt32.max)
        coordinates.longitude += region.span.longitudeDelta * random
        
        setupMap(at: coordinates)
    }
    
    private func setupMap(at location: CLLocationCoordinate2D) {
    
        if let old = mapBox.annotations, old.count > 0 {
            mapBox.removeAnnotations(old)
        }
        
        animating = mapBox.style != nil
        let camera = MGLMapCamera(lookingAtCenter: location, altitude: Math.mileInMeters * 2, pitch: 50, heading: 0)
        if animating {
            mapBox.fly(to: camera) { [weak self] in
                self?.animating = false
                self?.addAnnotation()
            }
        } else {
            mapBox.setCamera(camera, animated: false)
        }
        
        let location = Location(coordinates: location)
        addArea(at: location)
        self.location = location
        
        GeoService.shared.getPlace(for: location.location()) { [weak self] (place, _) in
            DispatchQueue.main.async {
                if let place = place {
                    self?.annotationTitle = place.neighbourhood
                    self?.annotationText = "Exact location revealed after connecting"
                } else {
                    self?.annotationTitle = "Somewhere"
                    self?.annotationText = "Exact location revealed after connecting"
                }
                self?.addAnnotation()
            }
        }
    }
    
    private var areaAdded = false
    
    private func addArea(at location: Location) {
        guard !areaAdded else { return }
        guard let style = mapBox.style else { return }
        
        areaAdded = true
        
        let areaRegion = MKCoordinateRegionMakeWithDistance(location.coordinates(), Math.mileInMeters * 0.5, Math.mileInMeters * 0.5)
        
        var area: MGLImageSource!
        if let existingArea = style.source(withIdentifier: "more-area") as? MGLImageSource {
            existingArea.coordinates = areaRegion.mapBoxQuad
            area = existingArea
        } else {
            area = MGLImageSource(identifier: "more-area", coordinateQuad: areaRegion.mapBoxQuad, image: UIImage(named: "location_area")!)
            style.addSource(area)
        }
        
        // Create a raster layer from the MGLImageSource.
        if let areaLayer = style.layer(withIdentifier: "more-area-layer") {
            style.removeLayer(areaLayer)
        }
        let areaLayer = MGLRasterStyleLayer(identifier: "more-area-layer", source: area)
        
        // Insert the image below the map's symbol layers.
        for layer in style.layers.reversed() {
            if !layer.isKind(of: MGLSymbolStyleLayer.self) {
                style.insertLayer(areaLayer, above: layer)
                break
            }
        }
    }
    
    // private var annotationAdded = false
    
    private func addAnnotation() {
        guard !animating else { return }
        // guard !annotationAdded else { return }
        guard let location = location, let title = annotationTitle, let text = annotationText else { return }
        guard mapBox.style != nil else { return }
        
        // annotationAdded = true
        
        mapBox.removeAnnotations(mapBox.annotations ?? [])
        
        // Annotation
        let coordinates = location.coordinates()
        let areaRegion = MKCoordinateRegionMakeWithDistance(location.coordinates(), Math.mileInMeters * 0.5, Math.mileInMeters * 0.5)
        let point = MorePointAnnotation()
        point.coordinate = coordinates.move(latitudeDelta: areaRegion.span.latitudeDelta * 0.33, longitudeDelta: 0)
        point.title = title
        point.subtitle = text
        if let old = mapBox.annotations, old.count > 0 {
            mapBox.removeAnnotations(old)
        }
        mapBox.addAnnotations([point])
        mapBox.selectAnnotation(point, animated: true)
    }
    
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        
        if let location = location {
            addArea(at: location)
            addAnnotation()
        }
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        
        guard let _ = annotation as? MorePointAnnotation else { return nil }
        
        let reuseIdentifier = "reusableDotView"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MGLAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView?.frame = CGRect(x: 0, y: 0, width: 1, height: 1)
            annotationView?.layer.borderColor = UIColor.clear.cgColor
            annotationView?.backgroundColor = .clear
        }
    
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, calloutViewFor annotation: MGLAnnotation) -> MGLCalloutView? {
        let callout = MoreCalloutView()
        callout.representedObject = annotation
        return callout
    }
    
}
