//
//  PullDownToDismissTransition.swift
//  More
//
//  Created by Luko Gjenero on 31/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class PullDownToDismissTransition: SignalDetailsTransition {
    
    
    var swipeToDismiss: Bool = false
    
    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let presenting = self.presenting(using: transitionContext)
        if presenting == true || !swipeToDismiss {
            super.animateTransition(using: transitionContext)
        } else {
            if let presenter = transitionContext.viewController(forKey: .to) as? SignalDetailsPresenter {
                if let imageView = presenter.prepareToDismissSignalDetails() {
                    swipeToDismiss(using: transitionContext, image: imageView, cardFrame: presenter.cardFrame())
                } else {
                    fadeOut(using: transitionContext)
                }
            }
        }
    }
    
    private func swipeToDismiss(using transitionContext: UIViewControllerContextTransitioning, image: UIImageView, cardFrame: CGRect) {
        
        guard let fromProtocol = transitionContext.viewController(forKey: .from) as? SignalDetailsView else {
            fadeOut(using: transitionContext)
            return
        }
        
        let startFrame = fromProtocol.photosFrame()
    
        let containerView = transitionContext.containerView
        let from = transitionContext.viewController(forKey: .from)!
        let to = transitionContext.viewController(forKey: .to)!
        
        let endFrame = image.convert(image.bounds, to: containerView)
        
        // math :)
        let offset = CGPoint(x: 0, y: endFrame.midY - startFrame.midY)
        let scale = endFrame.width / startFrame.width
        var transform = CATransform3DMakeTranslation(offset.x, offset.y, 0)
        transform = CATransform3DScale(transform, scale, scale, 1)
        
        // end position
        let cardFrameStep4 = cardFrame
        
        // mid position
        let cardFrameStep3 = CGRect(
            origin: cardFrame.origin,
            size: CGSize(width: containerView.bounds.width * scale, height: containerView.bounds.height * scale))
        
        // start position
        let cardFrameStep1 = CGRect(
            x: 0,
            y: -60,
            width: containerView.bounds.width,
            height: containerView.bounds.height + 60)
        
        // reveal position
        let cardFrameStep2 = CGRect(
            x: (cardFrameStep3.minX + cardFrameStep1.minX) * 0.5,
            y: (cardFrameStep3.minY + cardFrameStep1.minY) * 0.5,
            width: (cardFrameStep3.width + cardFrameStep1.width) * 0.5,
            height: (cardFrameStep3.height + cardFrameStep1.height) * 0.5)
        
        // screenshot
        let screenshot = UIImageView(image: UIImage.screenshot(from: from.view, afterScreenUpdates: true))
        
        // card view
        let cardView = ExploreCollectionViewCellContent()
        cardView.translatesAutoresizingMaskIntoConstraints = false
        if let model = fromProtocol.model {
            cardView.setup(for: model, scale: 1)
        }
        
        // start
        screenshot.frame = containerView.bounds
        screenshot.alpha = 1.0
        cardView.alpha = 0
        from.view.frame = containerView.bounds
        from.view.alpha = 0
        
        // add views
        containerView.insertSubview(to.view, at: 0)
        containerView.addSubview(screenshot)
        containerView.addSubview(cardView)
        
        // constraints
        let top = cardView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: -60)
        top.isActive = true
        let bottom = cardView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        bottom.isActive = true
        let left = cardView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0)
        left.isActive = true
        let right = cardView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0)
        right.isActive = true
        containerView.layoutIfNeeded()
        
        func constrainCard(_ card: ExploreCollectionViewCellContent, to frame: CGRect) {
            top.constant = frame.minY
            bottom.constant = -(containerView.bounds.height - frame.maxY)
            left.constant = frame.minX
            right.constant = -(containerView.bounds.width - frame.maxX)
        }

        let duration = dismissingDuration
        
        // animation
        UIView.animate(
            withDuration: duration - 0.2,
            animations: {
                constrainCard(cardView, to: cardFrameStep3)
                containerView.layoutIfNeeded()
                cardView.alpha = 3
                screenshot.alpha = -1.5
                screenshot.layer.transform = transform
            },
            completion: { _ in
                
                if transitionContext.transitionWasCancelled {
                    screenshot.removeFromSuperview()
                    cardView.removeFromSuperview()
                    from.view.alpha = 1
                    transitionContext.completeTransition(false)
                    return
                }
                
                UIView.animate(
                    withDuration: 0.2,
                    animations: {
                        constrainCard(cardView, to: cardFrameStep4)
                        containerView.layoutIfNeeded()
                },
                    completion: { _ in
                        screenshot.removeFromSuperview()
                        cardView.removeFromSuperview()
                        from.view.alpha = transitionContext.transitionWasCancelled ? 1 : 0
                        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                })
            })
    }

}
