//
//  ExploreSavedSignalsViewController.swift
//  More
//
//  Created by Luko Gjenero on 11/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SCSafariPageController

class ExploreSavedSignalsViewController: UIViewController, SCSafariPageControllerDataSource, SCSafariPageControllerDelegate {
    
    @IBOutlet private weak var close: UIButton!
    @IBOutlet private weak var separator: UIView!
    @IBOutlet private weak var header: UIView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private let safariPageController:SCSafariPageController = SCSafariPageController()
    
    private var models: [SignalViewModel] = []
    private var topBarScale: CGFloat = 1
    private var isFirst = true
    private var needsReload = false
    
    var closeTap: ((_ models: [SignalViewModel])->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.isHidden = true
        
        safariPageController.dataSource = self
        safariPageController.delegate = self
        safariPageController.view.clipsToBounds = true
        
        addChildViewController(safariPageController)
        safariPageController.view.translatesAutoresizingMaskIntoConstraints = false
        safariPageController.view.backgroundColor = .clear
        view.insertSubview(safariPageController.view, at: 0)
        safariPageController.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        safariPageController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        safariPageController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        safariPageController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        safariPageController.didMove(toParentViewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        safariPageController.zoomOut(animated: false, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        isFirst = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if isFirst {
            safariPageController.reloadData()
        }
    }
    
    override func viewSafeAreaInsetsDidChange() {
        for page in safariPageController.visibleViewControllers {
            if let page = page as? SignalDetailsNavigationController {
                page.additionalSafeAreaInsets = view.safeAreaInsets
            }
        }
    }
    
    func setup(for models: [SignalViewModel], scale: CGFloat) {
        self.models = models
        self.topBarScale = scale
        
        if safariPageController.isZoomedOut {
            safariPageController.reloadData()
        } else {
            needsReload = true
        }
    }
    
    @IBAction func closeTouch(_ sender: Any) {
        closeTap?(models)
    }
    
    // MARK: SCSafariPageController
    
    func numberOfPages(in pageController: SCSafariPageController!) -> UInt {
        return UInt(models.count)
    }
    
    func pageController(_ pageController: SCSafariPageController!, viewControllerForPageAt index: UInt) -> UIViewController! {
        
        let model = models[Int(index)]
        
        let vc = SignalDetailsNavigationController()
        let tap = { [weak self, weak vc] in
            vc?.tapped = nil
            self?.zoomIn(page: vc, index: index)
        }
        vc.back = { [weak self, weak vc] in
            vc?.tapped = tap
            self?.zoomOut(page: vc)
        }
        vc.report = { [weak self] in
            self?.report(model)
        }
        vc.requested = { [weak self, weak vc] in
            vc?.tapped = tap
            self?.zoomOutAndRemove(page: vc, index: index, signalId: model.id)
        }
        vc.removed = { [weak self] in
            self?.zoomOutAndRemove(page: vc, index: index, signalId: model.id)
        }
        vc.claimed = { [weak self] in
            self?.zoomOutAndRemove(page: vc, index: index, signalId: model.id)
        }
        vc.tapped = tap
        vc.view.layer.masksToBounds = true
        vc.view.layer.cornerRadius = 16
        
        vc.setup(for: model)
        
        return vc
    }
    
    func pageController(_ pageController: SCSafariPageController!, willDeletePageAt pageIndex: UInt) {
        self.models.remove(at: Int(pageIndex))
    }
    
    // MARK: - animations
    
    private func zoomOut(page: SignalDetailsNavigationController?) {
        UIView.animate(withDuration: 0.3, animations: {
            page?.view.layer.cornerRadius = 16
            self.header.alpha = 1
        })
        page?.scrollToTop(animated: true)
        safariPageController.zoomOut(animated: true, completion: {
            if self.needsReload == true {
                self.needsReload = false
                self.safariPageController.reloadData()
            }
        })
        UIResponder.currentFirstResponder()?.resignFirstResponder()
    }
    
    private func zoomIn(page: SignalDetailsNavigationController?, index: UInt) {
        UIView.animate(withDuration: 0.3, animations: {
            page?.view.layer.cornerRadius = 0
            self.header.alpha = 0
        })
        safariPageController.zoomIntoPage(at: index, animated: true, completion: nil)
    }
    
    private func zoomOutAndRemove(page: SignalDetailsNavigationController?, index: UInt, signalId: String) {
        
        UIView.animate(withDuration: 0.3, animations: {
            page?.view.layer.cornerRadius = 16
            self.header.alpha = 1
        })
        page?.scrollToTop(animated: true)
        safariPageController.zoomOut(animated: true, completion: {
            if self.needsReload == true {
                self.needsReload = false
                self.models.removeAll(where: { $0.id == signalId })
                self.safariPageController.reloadData()
            } else {
                self.models.remove(at: Int(index))
                self.safariPageController.deletePages(at: IndexSet(integer: Int(index)), animated: true, completion: nil)
            }
        })
        UIResponder.currentFirstResponder()?.resignFirstResponder()
    }
}

    
/*
class ExploreSavedSignalsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    private static let cellIdentifier = "ExploreSavedSignalsCollectionViewCell"
    
    @IBOutlet private weak var close: UIButton!
    @IBOutlet private weak var separator: UIView!
    @IBOutlet private weak var header: UIView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var models: [SignalViewModel] = []
    
    var selectedSignal: ((_ model: SignalViewModel)->())?
    var closeTap: ((_ models: [SignalViewModel])->())?
    
    private var topBarScale: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        collectionView.register(UINib(nibName: "ExploreSavedSignalsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ExploreSavedSignalsViewController.cellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(pan(sender:)))
        pan.delegate = self
        collectionView.addGestureRecognizer(pan)
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(signalExpired(_:)), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                collectionView.collectionViewLayout.invalidateLayout()
                collectionView.reloadData()
            }
        }
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        collectionView.contentInset = UIEdgeInsets(top: view.safeAreaInsets.top + 50, left: 0, bottom: view.safeAreaInsets.bottom, right: 0)
    }
    
    private var itemSize: CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func setup(for models: [SignalViewModel], scale: CGFloat) {
        self.models = models
        self.topBarScale = scale
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
    }
    
    @objc private func signalExpired(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String {
                if let idx = self?.models.firstIndex(where: { $0.id == signalId }) {
                    self?.models.remove(at: idx)
                    self?.collectionView.reloadData()
                }
            }
        }
    }

    @IBAction func closeTouch(_ sender: Any) {
        closeTap?(models)
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExploreSavedSignalsViewController.cellIdentifier, for: indexPath) as? ExploreSavedSignalsCollectionViewCell {
            let model = models[indexPath.item]
            cell.setup(for: model, parent: self)
            cell.tapped = { [weak self] in
                self?.presentCell(at: indexPath.item) //   selectedSignal?(model)
            }
            cell.setCornerRadius(16)
            
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedSignal?(models[indexPath.item])
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        panEnabled = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        panEnabled = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            panEnabled = true
        }
    }
    
    // MARK: - save in dock
    
    private var panEnabled = true
    private var panStart: CGPoint = .zero
    private var panIdx: Int = 0
    private var panTransform: CATransform3D = CATransform3DIdentity
    private weak var panCell: ExploreSavedSignalsCollectionViewCell? = nil
    
    @objc private func pan(sender: UIGestureRecognizer) {
        
        guard panEnabled else { return }
        guard collectionView.layer.animationKeys() == nil else { return }
        guard !collectionView.isDecelerating && !collectionView.isDragging else { return }
        
        let point = sender.location(in: collectionView)
        switch sender.state {
        case .began:
            guard let idx = collectionView.indexPathForItem(at: sender.location(in: collectionView))?.item else { return }
            guard let cell = collectionView.cellForItem(at: IndexPath(item: idx, section: 0)) as? ExploreSavedSignalsCollectionViewCell else { return }
            panStart = point
            panIdx = idx
            panCell = cell
            panTransform = cell.layer.transform
        case .changed:
            let offset = point.x - panStart.x
            move(cell: panCell, to: offset)
        case .cancelled, .ended, .failed:
            let offset = point.x - panStart.x
            if abs(offset) > 120 {
                remove(cell: panCell, at: panIdx, toLeft: offset < 0)
            } else {
                returnToCenter(cell: panCell)
            }
        default: ()
        }
    }
    
    private func move(cell: ExploreSavedSignalsCollectionViewCell?, to offset: CGFloat) {
        guard let cell = cell else { return }
        
        guard offset != 0 else {
            cell.layer.transform = panTransform
            return
        }
        
        let translate = CATransform3DConcat(CATransform3DMakeTranslation(offset, 0, 0), panTransform)
        cell.layer.transform = translate
    }
    
    private func returnToCenter(cell: ExploreSavedSignalsCollectionViewCell?) {
        view.isUserInteractionEnabled = false
        UIView.animate(
            withDuration: 0.2,
            animations: {
                self.move(cell: cell, to: 0)
            },
            completion: { (_) in
                self.view.isUserInteractionEnabled = true
            })
    }
    
    private func remove(cell: ExploreSavedSignalsCollectionViewCell?, at idx: Int, toLeft: Bool) {
        view.isUserInteractionEnabled = false
        UIView.animate(
            withDuration: 0.3,
            animations: {
                let offset = self.collectionView.frame.width + 60
                self.move(cell: cell, to: toLeft ? -offset : offset)
            },
            completion: { (_) in
                self.removeFromList(idx, cell: cell, toLeft: toLeft)
            })
    }
    
    private func removeFromList(_ idx: Int, cell: ExploreSavedSignalsCollectionViewCell?, toLeft: Bool) {
        
        if let layout = collectionView.collectionViewLayout as? ExploreSavedSignalsCollectionLayout {
            layout.removeDirection = toLeft ? .left : .right
        }
        
        models.remove(at: idx)
        collectionView.deleteItems(at: [IndexPath(item: idx, section: 0)])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: { [weak self] in
            self?.view.isUserInteractionEnabled = true
        })
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = pan.translation(in: collectionView)
            return abs(translation.x) > abs(translation.y)
        }
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    /*
     func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
     return otherGestureRecognizer == collectionView.panGestureRecognizer
     }
     */
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer == collectionView.panGestureRecognizer
    }
    
    // MARK: - present
    
    private func presentCell(at index: Int) {
        
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? ExploreSavedSignalsCollectionViewCell {
            
            cell.back = { [weak self] in
                self?.dismissCell(cell, at: index)
            }
            
            let frame = view.convert(cell.frame, from: collectionView)
            let offset = frame.origin.y + collectionView.contentInset.top
            UIView.animate(
                withDuration: 0.5,
                animations: {
                    cell.layer.transform = CATransform3DMakeTranslation(0, -offset, 1000)
                    // cell.frame = frame
                    cell.setCornerRadius(0)
                    self.header.alpha = 0
                },
                completion: { _ in
                    cell.tapped = nil
                })
        }
    }
    
    private func dismissCell(_ cell: ExploreSavedSignalsCollectionViewCell, at index: Int) {
        
        let indexPath = IndexPath(item: index, section: 0)
        if let layout = collectionView.collectionViewLayout as? ExploreSavedSignalsCollectionLayout,
            let attrs = layout.layoutAttributesForItem(at: indexPath) {
            
            UIView.animate(
                withDuration: 0.5,
                animations: {
                    cell.layer.transform = attrs.transform3D
                    cell.setCornerRadius(16)
                    self.header.alpha = 1
                },
                completion: { _ in
                    self.collectionView.reloadData()
                })
        }
    }

}
*/
