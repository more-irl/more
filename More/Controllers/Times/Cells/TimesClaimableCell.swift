//
//  TimesClaimableCell.swift
//  More
//
//  Created by Luko Gjenero on 18/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import UIKit
import Firebase

class TimesClaimableCell: TimesBaseCell {

    static let height: CGFloat = 250
    
    @IBOutlet private weak var content: UIView!
    @IBOutlet private weak var header: TimeHeader!
    @IBOutlet private weak var quote: UILabel!
    @IBOutlet private weak var requestCount: UILabel!
    @IBOutlet private weak var requestsView: SignalClaimedView!
    @IBOutlet private weak var removeButton: UIButton!
    
    private var signalId: String = ""
    
    var userTap: ((_ uesr: User)->())?
    var removeTap: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        requestsView.userTap = { [weak self] (user) in
            self?.userTap?(user)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.enableShadow(color: .black, path: UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height - 25)).cgPath)
    }
    
    @IBAction func removeTouch(_ sender: Any) {
        removeTap?()
    }
    
    // MARK: - base
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: TimesOwnSignalCell.height)
    }
    
    override func setup(for model: SignalViewModel) {
        signalId = model.id
        
        header.setup(for: model)
        quote.text = model.quote
        
        load()
    }
    
    private func load() {
        
        requestCount.text = "NOT CLAIMED YET"
        requestsView.setup(for: [])
        
        Database.database().reference().child("signals").child("claimable").child(signalId).child("claimedBy").observeSingleEvent(
            of: .value,
            with: { [weak self] (snapshot) in
                var users: [User] = []
                if snapshot.exists() {
                    for child in snapshot.children {
                        if let child = child as? DataSnapshot,
                            let user = User.fromSnapshot(child) {
                            users.append(user)
                        }
                    }
                }
                self?.updateUsers(to: users)
            },
            withCancel: nil)
    }
    
    private func updateUsers(to users: [User]) {
        
        if users.count > 0 {
            if users.count > 1 {
                requestCount.text = "CLAIMED \(users.count) TIMES"
            } else {
                requestCount.text = "CLAIMED 1 TIME"
            }
        } else {
            requestCount.text = "NOT CLAIMED YET"
        }
        
        requestsView.setup(for: users)
        
    }
}
