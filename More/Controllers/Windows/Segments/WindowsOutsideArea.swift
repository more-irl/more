//
//  WindowsOutsideArea.swift
//  More
//
//  Created by Luko Gjenero on 18/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import Mapbox
import MapKit

@IBDesignable
class WindowsOutsideArea: LoadableView, MGLMapViewDelegate {

    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subtitle: SpecialLabel!
    @IBOutlet private weak var mapBox: MGLMapView!
    @IBOutlet private weak var button: UIButton!
    @IBOutlet private weak var spinner: UIActivityIndicatorView!
    @IBOutlet private weak var callout: WindowsCallout!
    @IBOutlet private weak var countdown: WindowsCountdownBar!
    
    private var data: Window?
    
    override func setupNib() {
        super.setupNib()
        mapBox.styleURL = URL(string: MapBox.styleUrl)
        
        mapBox.isUserInteractionEnabled = false
        mapBox.delegate = self
        mapBox.showsUserLocation = true
        mapBox.compassView.isHidden = true
        
        spinner.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        timer?.invalidate()
    }
    
    @objc private func locationUpdated() {
        if let location = LocationService.shared.currentLocation {
            updateLocation(location)
        }
    }
    
    @IBAction func buttonTouch(_ sender: Any) {
        guard let data = data else { return }
        if !data.isInsideTime() {
            startCountdown()
            
            PushNotificationService.shared.requestPersmissions { [weak self] in
                WindowsService.shared.setupNotifications(for: data)
            }
        } else {
            button.isEnabled = false
            spinner.isHidden = false
            spinner.startAnimating()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
                self?.button.isEnabled = true
                self?.spinner?.isHidden = true
            }
        }
    }
    
    func setup(for window: Window) {
        self.data = window
     
        setupUI()
        
        setupMap()
        
        areaAdded = false
        addArea()
    }
    
    private func setupUI() {
        guard let window = data else { return }
        
        let calloutText = calloutSubtitle(for: window)
        callout.setup(title: window.shortName ?? window.name, subtitle: calloutText)
        
        if window.isInsideTime() {
            title.text = "Out of Range"
            let subtitle = """
            Oops! Looks like you're out of range. Please move into range to use the app.
            
            More is now live in \(window.name).
            """
            self.subtitle.attributedText = nil
            self.subtitle.text = subtitle
            button.setTitle("Try Again", for: .normal)
        } else {
            title.text = "More is not Live Yet"
            subtitle.text = nil
            subtitle.attributedText = timeSubtitle(for: window)
            button.setTitle("REMIND ME", for: .normal)
        }
        
        if !window.isInsideTime() && showCountdown(for: window.id) {
            startCountdown()
        } else {
            countdown.isHidden = true
        }
    }
    
    func updateLocation(_ location: CLLocation) {
        guard data?.isInsideTime() == true else { return }
        
        if data?.isInsideRegionBuffer(Location(coordinates: location.coordinate)) ?? true {
           title.text = "You're Just Out of Range"
        } else {
           title.text = "Out of Range"
        }
    }
    
    func calloutSubtitle(for window: Window) -> String {
        var calloutsubtitle = ""
        let df = DateFormatter()
        df.dateFormat = "MMMM d"
        if let start = window.start {
            if let end = window.end {
                let isSameDay = Calendar.current.isDate(start, inSameDayAs: end)
                df.dateFormat = "MMMM d"
                calloutsubtitle = df.string(from: start)
                
                if isSameDay {
                    calloutsubtitle += ", "
                } else {
                    calloutsubtitle += " "
                }
                df.dateFormat = "ha"
                calloutsubtitle += df.string(from: start) + " - "
                if isSameDay {
                    calloutsubtitle += df.string(from: end)
                } else {
                    df.dateFormat = "MMMM d ha"
                    calloutsubtitle += df.string(from: end)
                }
            } else {
                df.dateFormat = "MMMM d ha"
                calloutsubtitle = "Starts \(df.string(from: start))"
            }
        } else if let end = window.end {
            df.dateFormat = "MMMM d ha"
            calloutsubtitle = "Ends \(df.string(from: end))"
        }
        return calloutsubtitle
    }
    
    func timeDateSubtitle(for window: Window) -> String {
        var timeDateSubtitle = ""
        let df = DateFormatter()
        df.dateFormat = "MMMM d"
        if let start = window.start {
            if let end = window.end {
                let isSameDay = Calendar.current.isDate(start, inSameDayAs: end)
                df.dateFormat = "MMMM d"
                timeDateSubtitle = "on \(df.string(from: start))"
                timeDateSubtitle += getOrdinalEnding(for: timeDateSubtitle)
                if isSameDay {
                    timeDateSubtitle += " from "
                } else {
                    timeDateSubtitle += " "
                }
                df.dateFormat = "ha"
                timeDateSubtitle += df.string(from: start) + " - "
                if isSameDay {
                    timeDateSubtitle += df.string(from: end)
                } else {
                    df.dateFormat = "MMMM d"
                    timeDateSubtitle += df.string(from: end)
                    timeDateSubtitle += getOrdinalEnding(for: timeDateSubtitle)
                    timeDateSubtitle += " "
                    df.dateFormat = "ha"
                    timeDateSubtitle += df.string(from: end)
                }
            } else {
                timeDateSubtitle = "starting \(df.string(from: start))"
                timeDateSubtitle += getOrdinalEnding(for: timeDateSubtitle)
                timeDateSubtitle += " "
                df.dateFormat = "ha"
                timeDateSubtitle += df.string(from: start)
            }
        } else if let end = window.end {
            timeDateSubtitle = "ending \(df.string(from: end))"
            timeDateSubtitle += getOrdinalEnding(for: timeDateSubtitle)
            timeDateSubtitle += " "
            df.dateFormat = "ha"
            timeDateSubtitle += df.string(from: end)
        }
        return timeDateSubtitle
    }
    
    private func getOrdinalEnding(for text: String) -> String {
        if text.hasSuffix(" 1") || text.hasSuffix("21") || text.hasSuffix("31") {
            return "st"
        } else if text.hasSuffix(" 2") || text.hasSuffix("22") {
            return "nd"
        } else if text.hasSuffix(" 3") || text.hasSuffix(" 3") {
            return "rd"
        }
        return "th"
    }
    
    
    func timeSubtitle(for window: Window) -> NSAttributedString {
        let subtitle = NSMutableAttributedString()
        
        var font = UIFont(name: "Avenir-Medium", size: 14) ?? UIFont.systemFont(ofSize: 14)
        var part = NSAttributedString(
            string: "More goes live ",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        subtitle.append(part)
        
        var second = "in \(window.name) "
        second += timeDateSubtitle(for: window)
        
        font = UIFont(name: "Avenir-Black", size: 14) ?? UIFont.systemFont(ofSize: 14)
        part = NSAttributedString(
            string: second,
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 67, green: 74, blue: 81),
                         NSAttributedStringKey.font : font])
        subtitle.append(part)
        
        return subtitle
    }
    
    // MARK: - countdown
    
    private static let countdownKey = "com.startwithmore.windows.countdown"
    private func showCountdown(for windowId: String) -> Bool {
        return UserDefaults.standard.string(forKey: WindowsOutsideArea.countdownKey) == windowId
    }
    
    private func startCountdown() {
        guard let data = data else { return }
        guard let start = data.start, start > Date() else { return }
        
        UserDefaults.standard.set(data.id, forKey: WindowsOutsideArea.countdownKey)
        countdown.isHidden = false
        setupCountdown()
    }
    
    
    private var timer: Timer?
    
    private func setupCountdown() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { [weak self] (_) in
            self?.updateCountdown()
        })
        updateCountdown()
    }
    
    private func updateCountdown() {
        guard let start = data?.start else { return }
        let seconds = max(0, start.timeIntervalSinceNow)
        countdown.setup(for: seconds)
        
        if seconds <= 0 {
            timer?.invalidate()
            WindowsService.shared.checkWindowsTime()
            DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.5) { [weak self] in
                self?.setupUI()
            }
        }
    }
    
    // MARK: - map
    
    private func setupMap() {
        guard let data = data else { return }
        
        let animated = mapBox.style != nil
        
        if let mapCenter = data.mapCenter,
            let mapRadius = data.mapRadius {
            let cameraRegion = MKCoordinateRegionMakeWithDistance(mapCenter.coordinates(), mapRadius * 2, mapRadius * 2)
            mapBox.setVisibleCoordinateBounds(cameraRegion.mapBoxBounds, animated: animated)
        } else if data.type == .circle {
            if let center = data.points?.first,
                let radius = data.radius {
                let camera = MGLMapCamera(lookingAtCenter: center.coordinates(), altitude: radius * 5, pitch: 0, heading: 0)
                mapBox.setCamera(camera, animated: animated)
            }
        } else if data.type == .polygon {
            if let points = data.points, points.count > 0 {
                var coordinates = points.map { $0.coordinates() }
                mapBox.setVisibleCoordinates(&coordinates, count: UInt(coordinates.count), edgePadding: UIEdgeInsets(top: 40, left: 40, bottom: 100, right: 40), animated: animated)
            }
        }
    }
    
    private var areaAdded = false
    
    private func addArea() {
        guard let data = data else { return }
        guard mapBox.style != nil else { return }
        
        areaAdded = true
    
        if data.type == .circle {
            if let center = data.points?.first,
                let radius = data.radius {
                addCircleArea(center: center, radius: radius)
            }
        } else if data.type == .polygon {
            if let points = data.points, points.count > 2 {
                addPolygonArea(points: points)
            }
        }
    }
    
    private func addCircleArea(center: Location, radius: Double) {
        guard let style = mapBox.style else { return }
        
        var area: MGLSource!
        let existingArea = style.source(withIdentifier: "more-area")
        let areaRegion = MKCoordinateRegionMakeWithDistance(center.coordinates(), radius * 2, radius * 2)
        
        if let circleArea = existingArea as? MGLImageSource {
            circleArea.coordinates = areaRegion.mapBoxQuad
            area = circleArea
        } else {
            if let existingArea = existingArea {
                style.sources.remove(existingArea)
            }
            area = MGLImageSource(identifier: "more-area", coordinateQuad: areaRegion.mapBoxQuad, image: UIImage(named: "location_area")!)
            style.addSource(area)
        }
        
        // Create a raster layer from the MGLImageSource.
        if let areaLayer = style.layer(withIdentifier: "more-area-layer") {
            style.removeLayer(areaLayer)
        }
        if let areaLayer = style.layer(withIdentifier: "more-area-fill-layer") {
            style.removeLayer(areaLayer)
        }
        let areaLayer = MGLRasterStyleLayer(identifier: "more-area-layer", source: area)
        
        // Insert the image below the map's symbol layers.
        for layer in style.layers.reversed() {
            if !layer.isKind(of: MGLSymbolStyleLayer.self) {
                style.insertLayer(areaLayer, above: layer)
                break
            }
        }
    }
    
    private func addPolygonArea(points: [Location]) {
        guard let style = mapBox.style else { return }
        guard points.count > 2 else { return }
        
        let strokeColors = [0.00: UIColor(red: 190, green: 255, blue: 255),
                            0.20: UIColor(red: 185, green: 219, blue: 255),
                            0.42: UIColor(red: 143, green: 161, blue: 255),
                            0.50: UIColor(red: 156, green: 91, blue: 255),
                            0.58: UIColor(red: 143, green: 161, blue: 255),
                            0.80: UIColor(red: 185, green: 219, blue: 255),
                            1.00: UIColor(red: 190, green: 255, blue: 255)]
        
        var coordinates = points.map { $0.coordinates() }
        let polygon = MGLPolygon(coordinates: &coordinates, count: UInt(coordinates.count))
        
        var area: MGLSource!
        let existingArea = style.source(withIdentifier: "more-area")
        if let polygonArea = existingArea as? MGLShapeSource {
            polygonArea.shape = polygon
        } else {
            if let existingArea = existingArea {
                style.sources.remove(existingArea)
            }
            area = MGLShapeSource(identifier: "more-area", shape: polygon, options: [MGLShapeSourceOption.lineDistanceMetrics : true])
            style.addSource(area)
        }
        
        // Create a stroke layer.
        let strokeLayer = MGLLineStyleLayer(identifier: "more-area-layer", source: area)
        strokeLayer.lineJoin = NSExpression(forConstantValue: "round")
        strokeLayer.lineCap = NSExpression(forConstantValue: "round")
        // layer.lineColor = NSExpression(forConstantValue: UIColor(red: 143, green: 161, blue: 255))
        strokeLayer.lineGradient = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($lineProgress, 'linear', nil, %@)", strokeColors)
        strokeLayer.lineWidth = NSExpression(forConstantValue: 4)
        
        // Create a fill layer.
        let fillLayer = MGLFillStyleLayer(identifier: "more-area-fill-layer", source: area)
        fillLayer.fillColor = NSExpression(forConstantValue: UIColor(red: 143, green: 161, blue: 255))
        fillLayer.fillOpacity = NSExpression(forConstantValue: 0.3)
        
        if let areaLayer = style.layer(withIdentifier: "more-area-layer") {
            style.removeLayer(areaLayer)
        }
        if let areaLayer = style.layer(withIdentifier: "more-area-fill-layer") {
            style.removeLayer(areaLayer)
        }
        
        // Insert the image below the map's symbol layers.
        for layer in style.layers.reversed() {
            if !layer.isKind(of: MGLSymbolStyleLayer.self) {
                style.insertLayer(fillLayer, above: layer)
                style.insertLayer(strokeLayer, above: fillLayer)
                break
            }
        }
    }

    
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        if data != nil {
            if !areaAdded {
                addArea()
            }
        }
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        if let point = annotation as? MGLUserLocation {
            let reuseIdentifier = "me"
            
            var annotationView: MoreUserAnnotationView?
            if let view = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? MoreUserAnnotationView {
                annotationView = view
            } else {
                annotationView = MoreUserAnnotationView(annotation: point, reuseIdentifier: reuseIdentifier)
            }
            annotationView?.setup(for: UIImage(named: "me_pin")!)
            return annotationView
        }
        return nil
    }
    
}
