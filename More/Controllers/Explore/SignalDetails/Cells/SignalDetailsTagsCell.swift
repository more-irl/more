//
//  SignalDetailsTagsCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsTagsCell: SignalDetailsBaseCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private static let cellIdentifier = "TagCell"
    
    static let height: CGFloat = 160
    
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var tags: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tags.contentInset = UIEdgeInsetsMake(0, 25, 0, 25)
        tags.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        tags.register(Cell.self, forCellWithReuseIdentifier: SignalDetailsTagsCell.cellIdentifier)
        tags.dataSource = self
        tags.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private var tagList: [Review.Tag: Int] = [:]
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        
        title.text = "PEOPLE SAY \(model.creator.name.uppercased()) IS"
        tagList = model.creator.tags
        
        tags.collectionViewLayout.invalidateLayout()
        tags.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        tags.layoutIfNeeded()
        tags.collectionViewLayout.invalidateLayout()
        tags.reloadData()
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: SignalDetailsTagsCell.height)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        guard model.kind == .standard else { return false }
        return model.creator.tags.count > 0
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagList.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SignalDetailsTagsCell.cellIdentifier, for: indexPath) as? Cell {
            let label = Array(tagList.keys)[indexPath.item]
            let count = tagList[label] ?? 0
            cell.setup(label: label.rawValue.capitalized, count: count)
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = Array(tagList.keys)[indexPath.item]
        let count = tagList[label] ?? 0
        let size = TagView.size(label: label.rawValue.capitalized, count: count, in: collectionView.bounds.size)
        
        return CGSize(width: size.width, height: collectionView.bounds.size.height)
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let tagView: TagView = {
            let tagView = TagView(frame: .zero)
            tagView.translatesAutoresizingMaskIntoConstraints = false
            tagView.backgroundColor = Colors.tagBackground
            return tagView
        }()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.addSubview(tagView)
            tagView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
            tagView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
            tagView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            tagView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 7).isActive = true
            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(label: String, count: Int) {
            tagView.setup(label: label, count: count)
        }
    }
    
}
