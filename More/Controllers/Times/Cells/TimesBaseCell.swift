//
//  TimesBaseCell.swift
//  More
//
//  Created by Luko Gjenero on 05/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class TimesBaseCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return .zero
    }
    
    class func size(for time: Time, in size: CGSize) -> CGSize {
        return .zero
    }
    
    class func size(for review: Review, in size: CGSize) -> CGSize {
        return .zero
    }
    
    func setup(for model: SignalViewModel) { }
    
    func setup(for time: Time) { }
    
    func setup(for review: Review) { }

}
