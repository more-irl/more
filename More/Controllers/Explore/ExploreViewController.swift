//
//  ExploreViewController.swift
//  More
//
//  Created by Luko Gjenero on 09/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import FirebaseAuth
import IGListKit

class ExploreViewController: MoreBaseViewController, UIGestureRecognizerDelegate {
    
    private static let cellIdentifier = "ExploreCollectionViewCell"
    
    @IBOutlet private weak var topBar: ExploreTopBar!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var savedDock: ExploreSavedDockView!
    @IBOutlet private weak var newSignals: UIButton!
    @IBOutlet private weak var emptyView: ExploreEmptyView!
    
    private lazy var adapter = ExploreListAdapter(updater: ListAdapterUpdater(), viewController: self)
    private var savedModels: [SignalViewModel] = []
    private let fullSignalTransition = SignalDetailsTransitioningDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adapter.collectionView = collectionView
        
        adapter.selected = { [weak self] (model) in
            self?.itemSelected(model: model)
        }
        
        adapter.reload = { [weak self] in
            self?.topBar.setup(for: self?.adapter.filteredModels ?? [])
            self?.updateEmptyView()
        }
        
        adapter.scrolled = { [weak self] in
            self?.refreshTopBar(scrollOffset: self?.collectionView.contentOffset.x ?? 0)
            if self?.newSignals.isHidden == false && self?.newSignals.layer.animationKeys() == nil {
                self?.hideNewSignals(in: 5)
            }
        }
        
        adapter.newSignals = { [weak self] in
            self?.showNewSignalsIfNecessary()
        }
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        }
        
        // hide new signals button
        newSignals.backgroundColor = .clear
        newSignals.isHidden = true
        newSignals.roundCorners(corners: .allCorners, radius: 20, background: .white, shadow: .black)
        
        //  save in dock
        let pan = UIPanGestureRecognizer(target: self, action: #selector(pan(sender:)))
        pan.delegate = self
        collectionView.addGestureRecognizer(pan)
        
        // dock tap
        savedDock.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openDock)))
        
        // mock
        // mockNewSignals()
    }
    
    deinit {
        SignalingService.shared.stopTrackingAroundMe()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = adapter.cellSize
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                collectionView.collectionViewLayout.invalidateLayout()
                // collectionView.reloadData()
                adapter.reloadData(completion: nil)
                updateEmptyView()
                savedDock.insertScale = adapter.topBarScale
            }
        }
    }
    
    
    private var isFirst: Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        startTracking()
    }
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard isFirst else { return }
        
        setupNavigationBar()
    }
    
    private var settingsShowing: Bool = false
    
    override var prefersStatusBarHidden: Bool {
        return settingsShowing
    }
    
    // MARK: - Data tracking
    
    private func startTracking() {
        NotificationCenter.default.addObserver(self, selector: #selector(signalRemoved(_:)), name: SignalingService.Notifications.SignalExitedRegion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRemoved(_:)), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalRequest(_:)), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        SignalingService.shared.startTrackingAroundMe()
    }
    
    @objc private func signalRemoved(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String {
                self?.removeSignal(id: signalId)
            }
        }
    }
    
    @objc private func signalRequest(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signal = notice.userInfo?["signal"] as? Signal, signal.myRequest() != nil {
                self?.removeSignal(id: signal.id)
            }
        }
    }
    
    private func removeSignal(id: String) {
        if let idx = savedModels.firstIndex(where: { $0.id == id }) {
            savedModels.remove(at: idx)
            savedDock.setup(for: savedModels)
        }
    }
    
    // MARK: - Selection
    
    private func itemSelected(model: SignalViewModel) {
        let vc = SignalDetailsNavigationController()
        vc.back = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        vc.report = { [weak vc] in
            vc?.report(model)
        }
        vc.requested = { [weak self] in
            self?.dismiss(animated: true, completion: {
                self?.requested(for: model)
            })
        }
        vc.removed = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        vc.claimed = { [weak self] in
            SignalTrackingService.shared.removeTracking(model.id)
            self?.adapter.removeSignal(id: model.id)
            self?.dismiss(animated: true, completion: nil)
        }
        _ = vc.view
        vc.setup(for: model)
        let nc = UINavigationController(rootViewController: vc)
        nc.setNavigationBarHidden(true, animated: false)
        nc.transitioningDelegate = fullSignalTransition
        present(nc, animated: true, completion: nil)
        nc.enableSwipeToPop()
    }
    
    private func refreshTopBar(scrollOffset: CGFloat) {
        let offset = (scrollOffset + adapter.padding) / adapter.cellSize.width * topBar.frame.width
        topBar.contentOffset = CGPoint(x: offset, y: 0)
    }
    
    // MARK: - open dock
    
    @objc private func openDock() {
        if savedModels.count > 0 {
            let vc = ExploreSavedSignalsViewController()
            vc.closeTap = { [weak self] (newSavedModel) in
                self?.closeDock(newSavedModel)
                self?.dismiss(animated: true, completion: nil)
            }

            _ = vc.view
            vc.setup(for: self.savedModels, scale: adapter.topBarScale)
            let nc = UINavigationController(rootViewController: vc)
            nc.setNavigationBarHidden(true, animated: false)
            present(nc, animated: true, completion: nil)
        }
    }
    
    private func closeDock(_ newDockItems: [SignalViewModel]) {
        let deleted = savedModels.filter { !newDockItems.contains($0) }
        for model in deleted {
            adapter.addSignalModel(model)
        }
        
        savedModels = newDockItems
        savedDock.setup(for: newDockItems)
    }
    
    // MARK: - save in dock
    
    private var panStart: CGPoint = .zero
    
    @objc private func pan(sender: UIGestureRecognizer) {

        guard adapter.panEnabled else { return }
        guard collectionView.layer.animationKeys() == nil else { return }
        guard !collectionView.isDecelerating && !collectionView.isDragging else { return }
        
        let idx = Int(round((collectionView.contentOffset.x  + collectionView.contentInset.left) / adapter.cellSize.width))
        guard idx >= 0 && idx < adapter.filteredModels.count else { return }
        guard let card = collectionView.cellForItem(at: IndexPath(item: 0, section: idx)) else { return }
        guard let topBar = topBar.cellForItem(at: IndexPath(item: idx, section: 0)) else { return }
        
        let point = sender.location(in: collectionView)
        switch sender.state {
        case .began:
            panStart = point
        case .changed:
            let offset = point.y - panStart.y
            move(card: card, topBar: topBar, to: offset)
        case .cancelled, .ended, .failed:
            if point.y - panStart.y > 120 {
                saveToDock(idx, card: card, topBar: topBar)
            } else {
                returnToCenter(card: card, topBar: topBar)
            }
        default: ()
        }
    }
    
    private func move(card: UICollectionViewCell, topBar: UICollectionViewCell, to offset: CGFloat) {
        
        guard offset > 0 else {
            card.layer.transform = CATransform3DIdentity
            topBar.layer.transform = CATransform3DIdentity
            return
        }
        
        let translate = CATransform3DMakeTranslation(0, offset, 0)
        card.layer.transform = translate
        let progress = max(0, 1 - offset / (topBar.frame.height * 0.6))
        let scale = (1 - adapter.topBarScale) * progress + adapter.topBarScale
        let translateAndScale = CATransform3DScale(translate, scale, scale, 0)
        topBar.layer.transform = translateAndScale
    }
    
    
    
    private func returnToCenter(card: UICollectionViewCell, topBar: UICollectionViewCell) {
        self.view.isUserInteractionEnabled = false
        UIView.animate(
            withDuration: 0.2,
            animations: {
                self.move(card: card, topBar: topBar, to: 0)
            },
            completion: { (_) in
                self.view.isUserInteractionEnabled = true
            })
    }
    
    private func saveToDock(_ idx: Int, card: UICollectionViewCell, topBar: UICollectionViewCell) {
        if savedModels.contains(adapter.filteredModels[idx]) {
            returnToCenter(card: card, topBar: topBar)
            return
        }
        
        view.isUserInteractionEnabled = false
        
        UIView.animate(
            withDuration: 0.3,
            animations: {
                let offset = self.collectionView.frame.height + self.savedDock.frame.height - 60
                self.move(card: card, topBar: topBar, to: offset)
            },
            completion: { (_) in
                self.removeFromListAndSaveToDock(idx, card: card, topBar: topBar)
            })
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = pan.translation(in: collectionView)
            return translation.y > abs(translation.x)
        }
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer == collectionView.panGestureRecognizer
    }
    */
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer == collectionView.panGestureRecognizer
    }
    
    // MARK: - save to dock animation
    
    private func removeFromListAndSaveToDock(_ idx: Int, card: UICollectionViewCell, topBar: UICollectionViewCell) {
        
        card.alpha = 0
        topBar.alpha = 0
        
        let model = adapter.filteredModels[idx]
        
        // 1.) add model to dock
        savedModels.insert(model, at: 0)
        savedDock.setup(for: self.savedModels)
        
        // 2.) remove model from list
        adapter.removeSignal(id: model.id)
        
        // 3.) enable the stack again
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: { [weak self] in
            self?.view.isUserInteractionEnabled = true
        })
    }

    // MARK: - new signals

    private var newSignalsTimer: Timer?

    @IBAction func newSignalsTouch(_ sender: Any) {
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        hideNewSignals()
    }
    
    func showNewSignals() {
        
        newSignalsTimer?.invalidate()
        newSignals.alpha = 0
        newSignals.isHidden = false
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.newSignals.alpha = 1
            },
            completion: { (_) in
                // nop
            })
        
        hideNewSignals(in: 15)
    }
    
    func hideNewSignals(in duration: TimeInterval) {
        
        let newFireDate = Date(timeIntervalSinceNow: duration)
        let fireDate = newSignalsTimer?.fireDate ?? newFireDate
        
        guard newFireDate >= fireDate else { return }
        
        newSignalsTimer?.invalidate()
        newSignalsTimer = Timer.scheduledTimer(withTimeInterval: duration, repeats: false, block: { [weak self] (_) in
            self?.hideNewSignals()
        })
    }
    
    func hideNewSignals() {

        newSignalsTimer?.invalidate()
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.newSignals.alpha = 0
            },
            completion: { (_) in
                self.newSignals.isHidden = true
            })
    }
    
    func showNewSignalsIfNecessary() {
        guard newSignals.isHidden && newSignals.layer.animationKeys() == nil else { return }
        
        if collectionView.contentOffset.x > adapter.cellSize.width * 3 {
            showNewSignals()
        }
    }
    
    
    /*
    // mock
    
    func mockNewSignals() {
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] (timer) in
            
            guard let sself = self else { timer.invalidate(); return }
            guard sself.newSignals.isHidden && sself.newSignals.layer.animationKeys() == nil else { return }
            
            if sself.collectionView.contentOffset.x > sself.cellSize.width * 3 {
                sself.showNewSignals()
            }
        }
    }
    */

    // MARK: - empty view
    
    private func updateEmptyView() {
        let hide = adapter.loading || adapter.filteredModels.count > 0 || savedModels.count > 0
        UIView.animate(
            withDuration: 0.2,
            animations: { [weak self] in
                self?.emptyView.alpha = hide ? 0 : 1
            },
            completion: { (finished) in })
    }
    
}

extension ExploreViewController: SFSafariViewControllerDelegate  {
    
    
    
    private func setupNavigationBar() {
        if let nc = navigationController as? MoreTabBarNestedNavigationController {
            
            nc.moreNavigationBar.backgroundColor = UIColor(red: 230, green: 230, blue: 230)
            
            nc.moreNavigationBar.setLeftIcon(UIImage(named: "settings")!)
            nc.moreNavigationBar.setRightIcon(UIImage(named: "filter_off")!, selectedIcon: UIImage(named: "filter_on"))
            
            nc.moreNavigationBar.leftTap = { [weak self] in
                self?.presentSettings()
            }
            
            nc.moreNavigationBar.rightTap = { [weak self] in
                self?.presentFilters()
            }
        }
    }
    
    private func checkFiltersButton() {
        if let nc = navigationController as? MoreTabBarNestedNavigationController {
            nc.moreNavigationBar.rightSelected = adapter.filterTypes.count > 0
        }
    }
    
    private func presentSettings() {
        guard let root = tabBarController else { return }
        
        let vc = SettingsViewController()
        
        vc.closeTap = { [weak vc, weak self] in
            vc?.view.removeFromSuperview()
            vc?.removeFromParentViewController()
            /*
            self?.settingsShowing = false
            self?.setNeedsStatusBarAppearanceUpdate()
            */
        }
        
        vc.inviteTap = { [weak self] in
            guard let linkString = ProfileService.shared.profile?.inviteLink,
                let link = URL(string: linkString)
                else { return }
            
            let subject = "Join Me On More"
            let body = "Have you tried More? Meet new people, have adventures. Get the app now! - "
            
            let activityVC = UIActivityViewController(activityItems: [body, link], applicationActivities: nil)
            activityVC.setValue(subject, forKey: "Subject")
            let excludeActivities: [UIActivity.ActivityType] = [.print, .assignToContact, .saveToCameraRoll, .addToReadingList, .postToFlickr, .postToVimeo]
            activityVC.excludedActivityTypes = excludeActivities
            activityVC.completionWithItemsHandler = { (_ activityType: UIActivity.ActivityType?, _ completed: Bool, _ returnedItems: [Any]?, _ activityError: Error?) -> Void in
                // nothing
            }
            self?.present(activityVC, animated: true, completion: { })
        }
        
        vc.supportTap = { [weak self] in
            guard MFMailComposeViewController.canSendMail() else { return }
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("Support")
            mail.setToRecipients([Emails.support])
            self?.present(mail, animated: true, completion: { })
        }
        
        vc.termsTap = { [weak self] in
            guard let url = URL(string: Urls.terms) else { return }
            
            let vc = SFSafariViewController(url: url)
            vc.delegate = self
            self?.present(vc, animated: true)
        }
        
        vc.privacyTap = { [weak self] in
            guard let url = URL(string: Urls.privacy) else { return }
            
            let vc = SFSafariViewController(url: url)
            vc.delegate = self
            self?.present(vc, animated: true)
        }
        
        vc.logoutTap = { [weak self] in
            ProfileService.shared.logout(complete: { (success, errorMsg) in
                if success {
                    let login = LoginNavigationController()
                    AppDelegate.appDelegate()?.transitionRootViewController(viewController: login, animated: true, completion: nil)
                } else {
                    self?.errorAlert(text: errorMsg ?? "Unknown error")
                }
            })
        }
        
        vc.deleteTap = { [weak self] in
            
            let alert = UIAlertController(
                title: "Delete Account",
                message: "Are you sure you want to delete your account? All your data will be lost forever.",
                preferredStyle: .actionSheet)

            let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
                ProfileService.shared.deleteAccount(complete: { (success, errorMsg) in
                    if success {
                        let login = LoginNavigationController()
                        AppDelegate.appDelegate()?.transitionRootViewController(viewController: login, animated: true, completion: nil)
                    } else {
                        self?.errorAlert(text: errorMsg ?? "Unknown error")
                    }
                })
            })
            
            alert.addAction(delete)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        root.addChildViewController(vc)
        root.view.addSubview(vc.view)
        vc.view.leftAnchor.constraint(equalTo: root.view.leftAnchor).isActive = true
        vc.view.rightAnchor.constraint(equalTo: root.view.rightAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: root.view.bottomAnchor).isActive = true
        vc.view.topAnchor.constraint(equalTo: root.view.topAnchor).isActive = true
        root.view.setNeedsLayout()
        vc.viewDidAppear(false)
        
        /*
        settingsShowing = true
        setNeedsStatusBarAppearanceUpdate()
        */
    }
    
    private func presentFilters() {
        
        guard let root = tabBarController else { return }
        
        let vc = MoodFilterViewController()
        _ = vc.view
        vc.closeTap = { [weak vc] in
            vc?.view.removeFromSuperview()
            vc?.removeFromParentViewController()
        }
        vc.doneTap = { [weak self, weak vc] (selectedTypes) in
            self?.adapter.filter(selectedTypes)
            vc?.view.removeFromSuperview()
            vc?.removeFromParentViewController()
            self?.checkFiltersButton()
        }
        vc.setSelection(adapter.filterTypes)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        root.addChildViewController(vc)
        root.view.addSubview(vc.view)
        vc.view.topAnchor.constraint(equalTo: root.view.safeAreaLayoutGuide.topAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: root.view.bottomAnchor).isActive = true
        vc.view.leadingAnchor.constraint(equalTo: root.view.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: root.view.trailingAnchor).isActive = true
        vc.view.setNeedsLayout()
        vc.viewDidAppear(false)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    override func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}




extension ExploreViewController: SignalDetailsPresenter {
    
    fileprivate var topCard: ExploreCollectionViewCell? {
        var card: ExploreCollectionViewCell? = nil
        let idx = Int(round((collectionView.contentOffset.x  + collectionView.contentInset.left) / adapter.cellSize.width))
        if idx >= 0 && idx < adapter.filteredModels.count {
            card = collectionView.cellForItem(at: IndexPath(item: 0, section: idx)) as? ExploreCollectionViewCell
        }
        return card
    }
    
    func prepareToPresentSignalDetails() -> UIImageView? {
        return topCard?.image
    }
    
    func prepareToDismissSignalDetails() -> UIImageView? {
        return topCard?.image
    }
    
    func cardFrame() -> CGRect {
        if let top = topCard {
            return self.view.convert(top.frame, from: collectionView)
        }
        return .zero
    }
}

extension MoreTabBarController: SignalDetailsPresenter {
    
    func prepareToPresentSignalDetails() -> UIImageView? {
        let nc = selectedViewController as? UINavigationController
        let explore = nc?.topViewController as? ExploreViewController
        return explore?.prepareToPresentSignalDetails()
    }
    
    func prepareToDismissSignalDetails() -> UIImageView? {
        let nc = selectedViewController as? UINavigationController
        let explore = nc?.topViewController as? ExploreViewController
        return explore?.prepareToDismissSignalDetails()
    }
    
    func cardFrame() -> CGRect {
        let nc = selectedViewController as? UINavigationController
        let explore = nc?.topViewController as? ExploreViewController
        return explore?.cardFrame() ?? .zero
    }
    
}


