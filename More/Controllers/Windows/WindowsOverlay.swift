//
//  WindowsOverlay.swift
//  More
//
//  Created by Luko Gjenero on 18/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class WindowsOverlay: LoadableView {

    @IBOutlet private weak var content: WindowsOutsideArea!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var profileButton: UIButton!
    
    private var viewController: UIViewController?
    
    private(set) var data: Window?
    
    override func setupNib() {
        super.setupNib()
        
        profileButton.layer.cornerRadius = 35
        closeButton.layer.cornerRadius = 15
        
        if ProfileService.shared.profile?.isAdmin == true {
            closeButton.isHidden = false
        }
    }
    
    func setup(for window: Window, viewController: UIViewController) {
        data = window
        content.setup(for: window)
        self.viewController = viewController
    }
    
    @IBAction private func closeTouch(_ sender: Any) {
        removeFromSuperview()
    }
    
    @IBAction private func profileTouch(_ sender: Any) {
        let nc = MoreTabBarNestedNavigationController()
        _ = nc.view
        nc.hideNavigation(hide: true, animated: false)
        nc.additionalSafeAreaInsets = .zero
        let vc = OwnProfileViewController()
        _ = vc.view
        vc.backTap = { [weak self] in
            self?.viewController?.dismiss(animated: true, completion: nil)
        }
        nc.viewControllers = [vc]
        viewController?.present(nc, animated: true, completion: nil)
    }
    
}
