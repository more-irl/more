//
//  ExploreCollectionViewCell.swift
//  More
//
//  Created by Luko Gjenero on 10/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ExploreCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var content: ExploreCollectionViewCellContent!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        enableCardShadow()
    }
    
    func setup(for model: SignalViewModel, scale: CGFloat) {
        content.setup(for: model, scale: scale)
    }
    
    var image: UIImageView! {
        return content.image
    }
    
    func setupEmpty() {
        content.setupEmpty()
    }
    
    /*
    override func prepareForReuse() {
        super.prepareForReuse()
        layer.transform = CATransform3DIdentity
        setNeedsLayout()
    }
    */
    
    private func enableCardShadow() {
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 16).cgPath
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.1
        layer.shadowRadius = 2
    }
    
}
