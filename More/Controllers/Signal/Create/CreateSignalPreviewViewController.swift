//
//  CreateSignalPreviewViewController.swift
//  More
//
//  Created by Luko Gjenero on 31/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

private let photosCell = String(describing: SignalDetailsPhotosCell.self)
private let quoteCell = String(describing: SignalDetailsQuoteCell.self)
private let userCell = String(describing: SignalDetailsUserCell.self)
private let tagsCell = String(describing: SignalDetailsTagsCell.self)
private let moreCell = String(describing: SignalDetailsMoreCell.self)
private let reviewsCell = String(describing: SignalDetailsReviewsCell.self)
private let reportCell = String(describing: SignalDetailsReportCell.self)
private let mapCell = String(describing: SignalDetailsMapCell.self)
private let adminCell = String(describing: SignalDetailsAdminCell.self)


class CreateSignalPreviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet private weak var fade: FadeView!
    
    private var model: CreateSignalViewModel?
    private var rows: [String] = []
    private var heights: [String: CGFloat] = [:]
    
    var backTap: (()->())?
    var reportTap: (()->())?
    var shareTap: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fade.orientation = .up
        fade.color = UIColor.black.withAlphaComponent(0.5)
        
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.insetsContentViewsToSafeArea = false
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Dummy")
        tableView.register(UINib(nibName: photosCell, bundle: nil), forCellReuseIdentifier: photosCell)
        tableView.register(UINib(nibName: quoteCell, bundle: nil), forCellReuseIdentifier: quoteCell)
        tableView.register(UINib(nibName: userCell, bundle: nil), forCellReuseIdentifier: userCell)
        tableView.register(UINib(nibName: moreCell, bundle: nil), forCellReuseIdentifier: moreCell)
        tableView.register(UINib(nibName: tagsCell, bundle: nil), forCellReuseIdentifier: tagsCell)
        tableView.register(UINib(nibName: reportCell, bundle: nil), forCellReuseIdentifier: reportCell)
        tableView.register(UINib(nibName: reviewsCell, bundle: nil), forCellReuseIdentifier: reviewsCell)
        tableView.register(UINib(nibName: mapCell, bundle: nil), forCellReuseIdentifier: mapCell)
        tableView.register(UINib(nibName: adminCell, bundle: nil), forCellReuseIdentifier: adminCell)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        tableView.contentInset = UIEdgeInsetsMake(0, 0, view.safeAreaInsets.bottom + 60, 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        clearHeightCache()
        tableView.reloadData()
    }
    
    @IBAction func backTouch(_ sender: Any) {
        backTap?()
    }
    
    @IBAction func shareTouch(_ sender: Any) {
        let signals = SignalTrackingService.shared.getOwnActiveSignals().filter { $0.kind == .standard }
        guard !(model?.kind == .standard) || signals.count < 3 else {
            cannotRequest()
            return
        }
        
        shareTap?()
    }
    
    func setup(for model: CreateSignalViewModel) {
        self.model = model
        
        rows = []
        
        if SignalDetailsPhotosCell.isShowing(for: model) {
            rows.append(photosCell)
        }
        if SignalDetailsQuoteCell.isShowing(for: model) {
            rows.append(quoteCell)
        }
        if SignalDetailsUserCell.isShowing(for: model) {
            rows.append(userCell)
        }
        if SignalDetailsTagsCell.isShowing(for: model) {
            rows.append(tagsCell)
        }
        if SignalDetailsMoreCell.isShowing(for: model) {
            rows.append(moreCell)
        }
        if SignalDetailsReviewsCell.isShowing(for: model) {
            rows.append(reviewsCell)
        }
        if SignalDetailsMoreCell.isShowing(for: model) {
            rows.append(moreCell)
        }
        if SignalDetailsReportCell.isShowing(for: model) {
            rows.append(reportCell)
        }
        if SignalDetailsMapCell.isShowing(for: model) {
            rows.append(mapCell)
        }
        if SignalDetailsAdminCell.isShowing(for: model) {
            rows.append(adminCell)
        }
        
        tableView.reloadData()
    }
    
    // MARK: - tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = rows[indexPath.row]
        if let model = model,
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SignalDetailsBaseCell {
            
            if let cell = cell as? SignalDetailsMoreCell, identifier == moreCell {
                if indexPath.row == rows.firstIndex(of: moreCell) {
                    cell.type = SignalDetailsMoreCell.MoreType.presence.rawValue
                } else {
                    cell.type = SignalDetailsMoreCell.MoreType.reviews.rawValue
                }
            }
            
            cell.setup(for: model)
            return cell
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "Dummy", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return height(for: rows[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let model = model else { return }
        
        let identifier = rows[indexPath.row]
        if identifier == moreCell {
            if indexPath.row == rows.firstIndex(of: moreCell) {
                let vc = ProfileViewController()
                vc.backTap = { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
                vc.moreTap = { [weak self] in
                    self?.reportTap?()
                }
                _ = vc.view
                vc.setup(for: model.creator)
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = ReviewsViewController()
                vc.backTap = { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
                _ = vc.view
                vc.setup(for: model.creator)
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    // MARK: - height cache
    
    private func clearHeightCache() {
        heights = [:]
    }
    
    private func height(for identifier: String) -> CGFloat {
        
        guard model != nil else { return 0 }
        
        if let height = heights[identifier] {
            return height
        }
        
        let height = calculateHeight(for: identifier)
        heights[identifier] = height
        return height
    }
    
    private func calculateHeight(for identifier: String) -> CGFloat {
        
        guard let model = model else { return 0 }
        
        switch identifier {
        case photosCell:
            return SignalDetailsPhotosCell.size(for: model, in: tableView.frame.size).height
        case quoteCell:
            return SignalDetailsQuoteCell.size(for: model, in: tableView.frame.size).height
        case userCell:
            return SignalDetailsUserCell.size(for: model, in: tableView.frame.size).height
        case tagsCell:
            return SignalDetailsTagsCell.size(for: model, in: tableView.frame.size).height
        case moreCell:
            return SignalDetailsMoreCell.size(for: model, in: tableView.frame.size).height
        case reviewsCell:
            return SignalDetailsReviewsCell.size(for: model, in: tableView.frame.size).height
        case reportCell:
            return SignalDetailsReportCell.size(for: model, in: tableView.frame.size).height
        case mapCell:
            return SignalDetailsMapCell.size(for: model, in: tableView.frame.size).height
        case adminCell:
            return SignalDetailsAdminCell.size(for: model, in: tableView.frame.size).height
        default:
            return 0
        }
    }

}
