//
//  MessageViewModel.swift
//  More
//
//  Created by Luko Gjenero on 24/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class MessageViewModel: Codable, Hashable {
    
    let id: String
    
    let sender: UserViewModel
    
    let createdAt: Date
    let text: String
    let delivered: Date?
    let read: Date?
    
    let isTyping: Bool?
    
    init(id: String, text: String, signal: Signal, creator: User) {
        
        self.id = id
        
        self.sender = UserViewModel(user: creator)
        
        createdAt = Date(timeIntervalSinceNow: -30)
        self.text = text
        delivered = Date(timeIntervalSinceNow: -10)
        read = Date(timeIntervalSinceNow: -10)
        
        isTyping = nil
    }
    
    init(message: Message) {
        id = message.id
        
        sender = UserViewModel(user: message.sender)
        
        createdAt = message.createdAt
        text = message.text
        delivered = message.deliveredAt
        read = message.readAt
        
        isTyping = nil
    }
    
    init(isTyping: Bool, user: User) {
        id = "typing"
        
        sender = UserViewModel(user: user)
        
        createdAt = Date(timeIntervalSinceNow: 86400)
        text = ""
        delivered = nil
        read = nil
        
        self.isTyping = isTyping
    }
    
    func isMine() -> Bool {
        return sender.id == ProfileService.shared.profile?.id
    }
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: MessageViewModel, rhs: MessageViewModel) -> Bool {
        return lhs.id == rhs.id
    }

}
