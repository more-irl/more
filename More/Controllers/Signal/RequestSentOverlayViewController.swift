//
//  RequestSentOverlayViewController.swift
//  More
//
//  Created by Luko Gjenero on 12/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import Lottie

class RequestSentOverlayViewController: UIViewController {

    @IBOutlet weak var background: BackgroundWithOval!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var clock: UIView!
    
    var doneTap: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupClock()
        background.backgroundColor = UIColor(red: 254, green: 254, blue: 254)
        setupForEnterFromBelow()
    }
    
    private var isFirst = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        enterFromBelow()
    }

    @IBAction func doneTouch(_ sender: Any) {
        exitFromBelow { [weak self] in
            self?.doneTap?()
        }
    }
    
    func setupSignal(for signal: SignalViewModel) {
        let text = NSMutableAttributedString()
        
        var font = UIFont(name: "Avenir-Roman", size: 15) ?? UIFont.systemFont(ofSize: 15)
        var part = NSAttributedString(
            string: "You've successfully shared a signal.\nIt will be live for ",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        let minutes = Int(round(ConfigService.shared.signalExpiration / 60))
        font = UIFont(name: "Arial-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: "\(minutes) ",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Heavy", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: "minutes",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Roman", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: ".",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        titleLabel.attributedText = text
        
        setupSubTitle(for: signal)
    }
    
    func setupRequest(for signal: SignalViewModel) {
        if signal.mine {
            setupForSharing(signal)
        } else {
            setupForRequesting(signal)
        }
    }
    
    private func setupForSharing(_ signal: SignalViewModel) {
        
        let text = NSMutableAttributedString()
        
        var font = UIFont(name: "Avenir-Roman", size: 15) ?? UIFont.systemFont(ofSize: 15)
        var part = NSAttributedString(
            string: "You've successfully shared a Signal.\nIt will be live for ",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        let minutes = Int(round(ConfigService.shared.signalExpiration / 60))
        font = UIFont(name: "DIN-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: "\(minutes) ",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Heavy", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: "minutes",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Roman", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: ".",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        titleLabel.attributedText = text
        
        setupSubTitle(for: signal)
    }
    
    private func setupForRequesting(_ signal: SignalViewModel) {
    
        let text = NSMutableAttributedString()
    
        var font = UIFont(name: "Avenir-Roman", size: 15) ?? UIFont.systemFont(ofSize: 15)
        var part = NSAttributedString(
            string: "You've successfully picked up \(signal.creator.name)'s Signal.\n\(signal.creator.name) now has ",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        let minutes = Int(round(ConfigService.shared.signalRequestExpiration / 60))
        font = UIFont(name: "DIN-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: "\(minutes) ",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Heavy", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: "minutes",
            attributes: [NSAttributedStringKey.foregroundColor : Colors.lightBlueHighlight,
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        font = UIFont(name: "Avenir-Roman", size: 15) ?? UIFont.systemFont(ofSize: 15)
        part = NSAttributedString(
            string: " to accept.",
            attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 124, green: 139, blue: 155),
                         NSAttributedStringKey.font : font])
        text.append(part)
        
        titleLabel.attributedText = text
        
        setupSubTitle(for: signal)
    }
    
    private func setupSubTitle(for signal: SignalViewModel) {
        
        let ownSignals = SignalTrackingService.shared.getOwnActiveSignals().filter { $0.kind == .standard }
        var count = ownSignals.count
        if signal.kind != .standard && !ownSignals.contains(where: { $0.id == signal.id }) {
            count += 1
        }
        
        var text = ""
        switch count {
        case 1: text = "You may choose up to 2 more Times."
        case 2: text = "You may choose 1 more Time."
        case 3: text = "You've used 3 of your active Times."
        default:
            ()
        }
        
        subtitleLabel.text = text
        
        if count < 3 {
            doneButton.setTitle("KEEP EXPLORING", for: .normal)
        } else {
            doneButton.setTitle("GOT IT", for: .normal)
        }
    }
    
    private func setupClock() {
        if let path = Bundle.main.path(forResource: "light-timer", ofType: "json", inDirectory: "Animations/light-timer") {
            let lottieView = LOTAnimationView(filePath: path)
            lottieView.contentMode = .scaleAspectFit
            lottieView.loopAnimation = true
            clock.addSubview(lottieView)
            lottieView.translatesAutoresizingMaskIntoConstraints = false
            lottieView.topAnchor.constraint(equalTo: clock.topAnchor).isActive = true
            lottieView.bottomAnchor.constraint(equalTo: clock.bottomAnchor).isActive = true
            lottieView.leadingAnchor.constraint(equalTo: clock.leadingAnchor).isActive = true
            lottieView.trailingAnchor.constraint(equalTo: clock.trailingAnchor).isActive = true
            lottieView.play()
        }
    }
}
