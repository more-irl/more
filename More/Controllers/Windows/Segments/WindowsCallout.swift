//
//  WindowsCallout.swift
//  More
//
//  Created by Luko Gjenero on 18/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit


private let corner: CGFloat = 0
private let tipSize: CGFloat = 12

@IBDesignable
class WindowsCallout: LoadableView {

    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subTitle: UILabel!
    
    private let background: CAShapeLayer = {
        let layer = CAShapeLayer()
        return layer
    }()
    
    override func setupNib() {
        super.setupNib()
        
        backgroundColor = .white
        layer.insertSublayer(background, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let path = generatePath().cgPath
        background.path = path
        enableShadow(color: .black, path: path)
    }
    
    func setup(title: String, subtitle: String) {
        self.title.text = title
        self.subTitle.text = subtitle
    }
    
    override var backgroundColor: UIColor? {
        get {
            if let bg = background.fillColor {
                return UIColor(cgColor: bg)
            }
            return nil
        }
        set {
            background.fillColor = newValue?.cgColor
        }
    }
    
    private func generatePath() -> UIBezierPath {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: corner, y: tipSize))
        
        path.addLine(to: CGPoint(x: frame.size.width/2 - tipSize, y: tipSize))
        path.addLine(to: CGPoint(x: frame.size.width/2, y: 0))
        path.addLine(to: CGPoint(x: frame.size.width/2 + tipSize, y: tipSize))
        path.addLine(to: CGPoint(x: frame.size.width - corner, y: tipSize))
        
        if corner > 0 {
            path.addArc(withCenter: CGPoint(x: frame.size.width - corner, y: tipSize + corner), radius: corner, startAngle: CGFloat(-90).toRadians(), endAngle: CGFloat(0).toRadians(), clockwise: true)
        }
        
        path.addLine(to: CGPoint(x: frame.size.width, y: frame.size.height - corner))
        
        if corner > 0 {
            path.addArc(withCenter: CGPoint(x: frame.size.width - corner, y: frame.size.height - corner), radius: corner, startAngle: CGFloat(0).toRadians(), endAngle: CGFloat(90).toRadians(), clockwise: true)
        }
        
        path.addLine(to: CGPoint(x: corner, y: frame.size.height))
        
        if corner > 0 {
            path.addArc(withCenter: CGPoint(x: corner, y: frame.size.height - corner), radius: corner, startAngle: CGFloat(90).toRadians(), endAngle: CGFloat(180).toRadians(), clockwise: true)
        }
        
        path.addLine(to: CGPoint(x: 0, y: corner + tipSize))
        
        if corner > 0 {
            path.addArc(withCenter: CGPoint(x: corner, y: corner + tipSize), radius: corner, startAngle: CGFloat(180).toRadians(), endAngle: CGFloat(270).toRadians(), clockwise: true)
        }
        path.close()
        
        return path
    }

}
