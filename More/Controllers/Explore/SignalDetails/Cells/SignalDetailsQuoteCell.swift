//
//  SignalDetailsQuoteCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsQuoteCell: SignalDetailsBaseCell {

    static let height: CGFloat = 170
    
    @IBOutlet private weak var firstLetter: UILabel!
    // @IBOutlet private weak var content: UILabel!
    @IBOutlet private weak var content: UITextViewNoPadding!
    
    class private func size(for quote: String, in size: CGSize) -> CGSize {
        if quote.count <= 1 {
            return CGSize(width: size.width, height: 170)
        }
        
        var height = CGFloat(50)
        
        var font = UIFont(name: "Stereopticon", size: 90) ?? UIFont.systemFont(ofSize: 90)
        let firstLetter = String(quote.first!).uppercased()
        let firstLetterWidth = firstLetter.width(withConstrainedHeight: CGFloat.greatestFiniteMagnitude, font: font)
        
        // first 4 lines are narrower
        var width = size.width - 25 - firstLetterWidth - 40
        font = UIFont(name: "Avenir-Black", size: 16) ?? UIFont.systemFont(ofSize: 16)
        var text = quote.substring(from: 1)!
        let firstPart = text.truncatedText(lineWidths: [width, width, width, width], withFont: font)
        let firstPartHeight = firstPart.height(withConstrainedWidth: width, font: font)
        
        height += firstPartHeight
        
        // next lines are wider
        if text.count > firstPart.count {
            text = text.substring(from: firstPart.count)!
            width = size.width - 50
            let secondPartHeight = text.height(withConstrainedWidth: width, font: font)
            height += secondPartHeight
        }
        
        return CGSize(width: size.width, height: max(height, 170))
    }
    
    private func setup(for quote: String) {
        firstLetter.text = String(quote.first ?? " ").uppercased()
        var size = firstLetter.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
        size.width += 15
        size.height -= 9
        
        content.textContainer.exclusionPaths = [UIBezierPath(rect: CGRect(origin: .zero, size: size))]
        if quote.count > 1 {
            content.text = quote.substring(from: 1)
        } else {
            content.text = ""
        }
    }
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        setup(for: model.quote)
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return self.size(for: model.quote, in: size)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return true
    }
    
    // MARK: - signal preview
    
    override func setup(for model: CreateSignalViewModel) {
        setup(for: model.quote)
    }
    
    override class func size(for model: CreateSignalViewModel, in size: CGSize) -> CGSize {
        return self.size(for: model.quote, in: size)
    }
    
    override class func isShowing(for model: CreateSignalViewModel) -> Bool {
        return true
    }
    
}
