//
//  SearchImagesViewController.swift
//  More
//
//  Created by Luko Gjenero on 26/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SearchImagesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, BufferedRunInterface {
    
    typealias BufferedData = (String, Int)
    var buffer: (String, Int)?
    var bufferDelay: TimeInterval {
        return 0.5
    }
    var bufferTimer: Timer?
    
    private static let cellIdentifier = "ImageCell"
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var searchBar: SearchBar!
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var background: UIView!
    
    private var quote: String = ""
    private var searchString: String = ""
    private var photos: [ImageSearchService.ImageData] = []
    private var hasMorePhotos: Bool = true
    private var photosSelected: [ImageSearchService.ImageData] = []
    
    private var isFirst: Bool = true
    
    var selectedPhotos: ((_ photos: [ImageSearchService.ImageData])->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupForEnter()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(backgroundTouch))
        tap.cancelsTouchesInView = false
        background.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(raiseHeight), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        /*
        searchBar.textUpdated = { [weak self] (text) in
            self?.searchUpdated(to: text ?? "", offset: 0)
        }
        */
        searchBar.searchTap = { [weak self] (text) in
            self?.searchBar.hideKeyboard()
            self?.searchUpdated(to: text ?? "", offset: 0)
        }
        
        
        collectionViewHeight.constant = UIScreen.main.bounds.height * 0.5
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        collectionView.register(Cell.self, forCellWithReuseIdentifier: SearchImagesViewController.cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        updateDoneButton(dark: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = self.itemSize
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                collectionView.collectionViewLayout.invalidateLayout()
                collectionView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        enter()
    }
    
    func setup(for model: CreateSignalViewModel) {
        quote = model.quote
        if quote.count > 0 {
            searchUpdated(to: quote, offset: 0)
        }
    }
    
    private var itemSize: CGSize {
        let width = floor((collectionView.frame.width - 6) / 2)
        let height = floor((UIScreen.main.bounds.height * 0.5 - 8) / 3)
        // let height = floor((collectionView.frame.height - 8) / 3)
        return CGSize(width: width, height: height)
    }
    
    @objc private func backgroundTouch() {
        searchBar.hideKeyboard()
    }

    @IBAction private func doneTouch(_ sender: Any) {
        exit()
    }
    
    private func updateDoneButton(dark: Bool) {
        if dark {
            doneButton.backgroundColor = Colors.previewDarkBackground
            doneButton.setTitleColor(Colors.previewDarkText, for: .normal)
        } else {
            doneButton.backgroundColor = Colors.previewLightBackground
            doneButton.setTitleColor(Colors.previewLightText, for: .normal)
        }
    }
    
    @objc private func raiseHeight() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.collectionViewHeight.constant = UIScreen.main.bounds.height * 0.75
            self?.view.layoutIfNeeded()
        }
    }
    
    // MARK: - photo data
    
    private func searchUpdated(to searchText: String, offset: Int) {
        bufferCall(with: (searchText, offset)) { [weak self] (serchText, offset) in
            self?.initiateSearch(searchText, offset: offset)
        }
    }
    
    private func initiateSearch(_ searchText: String, offset: Int) {
        
        let searchString = searchText.count > 0 ? searchText : quote
        guard searchString.count > 0 else {
            photos = []
            photosSelected = []
            collectionView.reloadData()
            return
        }
        
        ImageSearchService.shared.searchImages(for: searchString, offset: offset) { [weak self] (results) in
            DispatchQueue.main.async {
                self?.searchString = searchString
                self?.hasMorePhotos = results.count > 0
                
                if offset > 0 {
                    let inserted = results.map { IndexPath(item: (self?.photos.count ?? 0) + results.firstIndex(of: $0)!, section: 0) }
                    self?.photos.append(contentsOf: results)
                    self?.collectionView.insertItems(at: inserted)
                } else {
                    self?.photosSelected = []
                    self?.photos = results
                    self?.collectionView.reloadData()
                }
            }
        }
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let image: UIImageView = {
            let image = UIImageView(frame: .zero)
            image.translatesAutoresizingMaskIntoConstraints = false
            image.isUserInteractionEnabled = false
            image.backgroundColor = .clear
            image.contentMode = .scaleAspectFill
            image.clipsToBounds = true
            return image
        }()
        
        private let button: UIButton = {
            let button = UIButton(type: .custom)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.isUserInteractionEnabled = false
            button.backgroundColor = .clear
            button.setBackgroundImage(UIImage(named: "image_not_selected"), for: .normal)
            button.setBackgroundImage(UIImage(named: "image_selected"), for: .selected)
            button.setTitleColor(.white, for: .normal)
            button.setTitleColor(.white, for: .selected)
            let font = UIFont(name: "DIN-Bold", size: 13) ?? UIFont.systemFont(ofSize: 13)
            button.titleLabel?.font = font
            button.clipsToBounds = true
            return button
        }()
        
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.addSubview(image)
            contentView.addSubview(button)
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            image.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            image.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            button.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12).isActive = true
            button.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12).isActive = true
            button.heightAnchor.constraint(equalToConstant: 22).isActive = true
            button.widthAnchor.constraint(equalToConstant: 22).isActive = true
            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(for url: String, selectIndex: Int?) {
            guard let url = URL(string: url) else {
                image.sd_cancelCurrentImageLoad_progressive()
                image.image = nil
                button.isSelected = false
                button.setTitle(nil, for: .normal)
                button.setTitle(nil, for: .selected)
                return
            }
            
            image.sd_progressive_setImage(with: url, placeholderImage: UIImage.imageSearchPlaceholder(), options: .highPriority)
            setSelected(selectIndex: selectIndex)
        }
        
        func setSelected(selectIndex: Int?) {
            if let selectIndex = selectIndex {
                button.isSelected = true
                button.setTitle("\(selectIndex + 1)", for: .normal)
                button.setTitle("\(selectIndex + 1)", for: .selected)
            } else {
                button.isSelected = false
                button.setTitle(nil, for: .normal)
                button.setTitle(nil, for: .selected)
            }
        }
        
        func setupForEmpty() {
            image.sd_cancelCurrentImageLoad_progressive()
            image.image = UIImage.imageSearchPlaceholder()
            button.isSelected = false
            button.setTitle(nil, for: .normal)
            button.setTitle(nil, for: .selected)
        }
    }
    
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if photos.count == 0 {
            return 10
        }
        return photos.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchImagesViewController.cellIdentifier, for: indexPath) as? Cell {
            if photos.count == 0 {
                cell.setupForEmpty()
            } else {
                let cellPhoto = photos[indexPath.item]
                cell.setup(for: cellPhoto.url, selectIndex: photosSelected.firstIndex(of: cellPhoto))
            }
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard photos.count > 0 else { return }
        
        if indexPath.item >= photos.count - 1 && hasMorePhotos {
            searchUpdated(to: searchString, offset: photos.count)
        }
    }
    
    // MARK: - Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard photos.count > 0 else { return }
        
        let cellPhoto = photos[indexPath.item]
        
        var indexesToReload: [IndexPath] = photosSelected.map { IndexPath(item: photos.firstIndex(of: $0)!, section: 0) }
        if let idx = photosSelected.firstIndex(of: cellPhoto) {
            photosSelected.remove(at: idx)
        } else {
            photosSelected.append(cellPhoto)
            indexesToReload.append(indexPath)
        }
        
        for index in indexesToReload {
            if let cell = collectionView.cellForItem(at: index) as? Cell {
                let cellPhoto = photos[index.item]
                cell.setSelected(selectIndex: photosSelected.firstIndex(of: cellPhoto))
            }
        }
        
        updateDoneButton(dark: photosSelected.count > 0)
        searchBar.hideKeyboard()
    }
    
    // MARK - enter / exit animations
    
    private func setupForEnter() {
        background.alpha = 0
        let transform = CATransform3DMakeTranslation(0, view.frame.height, 0)
        for view in view.subviews {
            if view != background {
                view.layer.transform = transform
            }
        }
    }
    
    private func enter() {
        UIView.animate(
        withDuration: 0.3) {
            for view in self.view.subviews {
                if view != self.background {
                    view.layer.transform = CATransform3DIdentity
                }
            }
            self.background.alpha = 1
        }
    }
    
    private func exit() {
        UIResponder.resignAnyFirstResponder()
        let transform = CATransform3DMakeTranslation(0, view.frame.height, 0)
        UIView.animate(
            withDuration: 0.3,
            animations: {
                for view in self.view.subviews {
                    if view != self.background {
                        view.layer.transform = transform
                    }
                }
                self.background.alpha = 0
            },
            completion: { (_) in
                self.selectedPhotos?(self.photosSelected)
        })
    }
    
}
