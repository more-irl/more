//
//  SignalRequestBar.swift
//  More
//
//  Created by Luko Gjenero on 19/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalRequestBar: LoadableView {
    
    @IBOutlet private weak var background: BackgroundWithOval!
    @IBOutlet private weak var countdown: CountdownLabel!
    @IBOutlet private weak var request: UIButton!
    @IBOutlet private weak var eta: UILabel!
    @IBOutlet private weak var etaLabel: UILabel!
    @IBOutlet private weak var rate: StarsView!
    
    var requestTap: (()->())?
    
    var countdownHidden: Bool = true {
        didSet {
            countdown.isHidden = countdownHidden
            background.ovalSize = countdownHidden ? 0 : 75
        }
    }
    
    override func setupNib() {
        super.setupNib()
        countdown.setDateFormat("m:ss")
    }

    func setup(for model: SignalViewModel, confirm: Bool) {
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(expirationChanged(_:)), name: SignalTrackingService.Notifications.SignalExpirationChanged, object: nil)
        
        countdown.countdown(to: model.expiresAt)
        request.isHidden = false
        if model.mine {
            if model.accepted {
                request.setTitle("CHAT", for: .normal)
            } else {
                request.setTitle("ACCEPT REQUEST", for: .normal)
            }
        } else {
            if model.requested {
                request.setTitle("REMOVE REQUEST", for: .normal)
            } else {
                if confirm {
                    request.setTitle("CONFIRM REQUEST", for: .normal)
                } else {
                    request.setTitle("REQUEST TIME", for: .normal)
                }
            }
        }
        
        if model.creator.numOfReviews > 0 {
            rate.noStar = UIImage(named: "no_star")
            rate.rate = CGFloat(model.creator.avgReview)
        } else {
            rate.noStar = UIImage(named: "star_outline_2")
            rate.rate = 0
        }
        
        setEta(for: model)
    }
    
    private func setEta(for model: SignalViewModel, request: RequestViewModel? = nil) {
        eta.isHidden = true
        etaLabel.isHidden = true
        eta.text = ""
        
        var signalLocation = model.creator.location
        if model.mine {
            if let request = request {
                signalLocation = request.sender.location
            } else {
                etaLabel.text = ""
                return
            }
        }
        
        etaLabel.text = "ETA"
        if let myLocation = LocationService.shared.currentLocation,
            let signalLocation = signalLocation {
            GeoService.shared.getDistanceAndETA(from: myLocation.coordinate, to: signalLocation.coordinates()) { [weak self] (_, time, _) in
                if let time = time {
                    let df = DateFormatter()
                    df.dateFormat = "h:mm a"
                    self?.eta.text = df.string(from: Date(timeIntervalSinceNow: time))
                    self?.etaLabel.text = "ETA"
                    self?.eta.isHidden = false
                    self?.etaLabel.isHidden = false
                }
            }
        }
    }
    
    func setup(signal: SignalViewModel, request: RequestViewModel) {
        setup(for: signal, confirm: false)
        setEta(for: signal, request: request)
        countdown.countdown(to: request.expiresAt)
    }
    
    @IBAction private func requestTouch(_ sender: Any) {
        requestTap?()
    }
    
    @objc private func expirationChanged(_ notice: Notification) {
        if let signal = notice.userInfo?["signal"] as? Signal {
            countdown.countdown(to: signal.finalExpiration())
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hit = super.hitTest(point, with: event)
        var selfView: UIView = self
        if self.subviews.count == 1 {
            selfView = self.subviews.first!
        }
        if hit == selfView {
            return nil
        }
        return hit
    }
}
