//
//  RequestListOverlayViewController.swift
//  More
//
//  Created by Luko Gjenero on 11/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class RequestListOverlayViewController: UIViewController {

    @IBOutlet private weak var countdown: CountdownLabel!
    @IBOutlet private weak var requestsView: TimeRequestsView!
    @IBOutlet private weak var requestNum: SpecialLabel!
    @IBOutlet private weak var chooseLabel: SpecialLabel!
    @IBOutlet private weak var removeButton: UIButton!
    @IBOutlet weak var requestsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var expandButton: UIButton!
    
    private var model: SignalViewModel? = nil
    
    var deleted: (()->())?
    
    var selected: ((_ signal: SignalViewModel, _ request: RequestViewModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countdown.setDateFormat("m:ss")
        
        requestsView.requestTap = { [weak self] (request) in
            self?.presentRequest(request)
        }
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(dragTop(sender:)))
        expandButton.addGestureRecognizer(pan)
    }

    private var isFirst = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        startTracking()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction private func removeTouch(_ sender: Any) {
        
        guard let signalId = model?.id else { return }
        
        let alert = UIAlertController(title: nil, message: "This Signal and related messages will be deleted. This cannot be undone.", preferredStyle: .actionSheet)
        
        let delete = UIAlertAction(title: "Delete Signal", style: .destructive, handler: { [weak self] _ in
            self?.deleteSignalProcess(signalId)
        })
        
        alert.addAction(delete)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func expandTouch(_ sender: Any) {
        expand()
    }
    
    private func deleteSignalProcess(_ signalId: String) {
        showLoading()
        SignalingService.shared.deleteSignal(signalId: signalId, complete: { [weak self] (success, errorMsg) in
            self?.hideLoading()
            if success {
                self?.deleted?()
            } else {
                self?.errorAlert(text: errorMsg ?? "Unknown error")
            }
        })
        
    }
    
    // MARK: - Data tracking
    
    private func startTracking() {
        NotificationCenter.default.addObserver(self, selector: #selector(newRequest(_:)), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
    }
    
    func setup(for model: SignalViewModel) {
        
        self.model = model
        
        countdown.countdown(to: model.expiresAt)
    
        requestsView.setup(for: model)
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: SignalTrackingService.Notifications.SignalMessage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: SignalTrackingService.Notifications.SignalRequestExpired, object: nil)
        reload()
    }
    
    @objc private func reload() {
        guard let signalId = model?.id else {
            requestNum.text = "NO REQUEST YET"
            chooseLabel.isHidden = true
            return
        }
        
        let signal = SignalTrackingService.shared.updatedSignalData(for: signalId)
        let now = Date()
        let requests = signal?.requests?
            .filter { $0.expiresAt > now && $0.accepted == nil }
            .map { RequestViewModel(request: $0) } ?? []
        if requests.count > 0 {
            if requests.count > 1 {
                requestNum.text = "\(requests.count) REQUESTS"
            } else {
                requestNum.text = "1 REQUEST"
            }
            chooseLabel.isHidden = false
        } else {
            requestNum.text = "NO REQUEST YET"
            chooseLabel.isHidden = true
        }
    }
    
    
    
    // MARK: - collapse expand
    
    func fullyCollapse() {
        collapse()
    }
    
    private func collapse(duration: Double = 0.3, curve: UIView.AnimationOptions = .curveEaseInOut, force: Bool = false) {
        
        guard requestsViewHeight.constant > 0 || force  else { return }
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: [.beginFromCurrentState, curve],
            animations: {
                self.requestsViewHeight.constant = 0
                self.view.backgroundColor = .clear
                self.view.layoutIfNeeded()
            },
            completion: { (_) in /* nothing */ })
    }
    
    private func expand(duration: Double = 0.3, curve: UIView.AnimationOptions = .curveEaseInOut, force: Bool = false) {
        
        guard requestsViewHeight.constant < 123 || force else { return }
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: [.beginFromCurrentState, curve],
            animations: {
                self.requestsViewHeight.constant = 123
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                self.view.layoutIfNeeded()
            },
            completion: { (_) in /* nothing */})
    }
    
    // MARK: - drag
    
    private var dragStart: CGPoint = .zero
    private var dragStartHeight: CGFloat = 0
    
    @objc private func dragTop(sender: UIPanGestureRecognizer) {
        
        guard requestsView.layer.animationKeys() == nil else { return }
        
        let point = sender.location(in: view)
        let offset = dragStart.y - point.y
        switch sender.state {
        case .began:
            dragStart = point
            dragStartHeight = requestsViewHeight.constant
        case .changed:
            moveTop(to: offset + dragStartHeight)
        case .cancelled, .ended, .failed:
            settleTop(to: offset + dragStartHeight)
        default: ()
        }
    }
    
    private func moveTop(to height: CGFloat) {
        self.requestsViewHeight.constant = min(123, height)
        self.view.layoutIfNeeded()
    }
    
    private func settleTop(to height: CGFloat) {
        let maxHeight = CGFloat(123)
        let minHeight = CGFloat(0)
        
        let minDist = abs(minHeight - height)
        let maxDist = abs(maxHeight - height)
        
        if minDist < maxDist {
            collapse(force: true)
        } else  {
            expand(force: true)
        }
    }
    
    
    // MARK: - interaction
    
    @objc private func newRequest(_ notice: Notification) {
        if let signal = notice.userInfo?["signal"] as? Signal, signal.id == model?.id {
            setup(for: SignalViewModel(signal: signal))
        }
    }
    
    private func presentRequest(_ request: RequestViewModel) {
        guard let signal = model else { return }
        
        selected?(signal, request)
    }
    

}
