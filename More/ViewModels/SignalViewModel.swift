//
//  SignalViewModel.swift
//  More
//
//  Created by Luko Gjenero on 09/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import CoreLocation
import IGListKit

class SignalViewModel: Codable, Hashable, NSCopying {
    
    let id: String
    
    let creator: UserViewModel
    
    let createdAt: Date
    let expiresAt: Date
    
    let imageUrls: [String]
    
    let kind: SignalKind
    let type: SignalType
    let quote: String
    
    let lastReview: ReviewViewModel?
    
    let location: Location?
    let destination: Location?
    let destinationName: String?
    let destinationAddress: String?
    
    let mine: Bool
    let requested: Bool
    let accepted: Bool
    
    let requests: [RequestViewModel]
    var messages: [MessageViewModel]
    
    init(signal: Signal) {
        
        id = signal.id
        
        creator = UserViewModel(user: signal.creator)
        
        createdAt = signal.createdAt
        
        imageUrls = signal.imageUrls
        
        kind = signal.kind ?? .standard
        type = signal.type
        
        quote = signal.text
        
        if let last = signal.creator.lastReview {
            lastReview = ReviewViewModel(review: last)
        } else {
            lastReview = nil
        }
        
        location = signal.location
        if let destination = signal.destination {
            self.destination = destination
            destinationName = signal.destinationName
            destinationAddress = signal.destinationAddress
        } else {
            destination = nil
            destinationName = nil
            destinationAddress = nil
        }
                
        mine = signal.isMine()
        requested = signal.haveRequested()
        accepted = signal.wasAccepted()
        
        let messageData = signal.messages(for: signal.myRequest()?.id ?? "") ?? []
        messages = messageData.map { MessageViewModel(message: $0) }
        
        expiresAt = signal.finalExpiration()
        
        if mine {
            requests = signal.requests?.map { RequestViewModel(request: $0) } ?? []
        } else {
            if let myRequest = signal.myRequest() {
                requests = [RequestViewModel(request: myRequest)]
            } else {
                requests = []
            }
        }
    }
    
    init(signal: ShortSignal) {
        id = signal.id
        
        creator = UserViewModel(user: signal.creator)
        
        createdAt = Date()
        
        imageUrls = signal.imageUrls ?? []
        
        kind = .standard
        type = signal.type
        
        quote = signal.text ?? ""
        
        lastReview = nil
        
        location = signal.creator.location
        destination = nil
        destinationName = nil
        destinationAddress = nil
        
        mine = signal.isMine()
        requested = true
        accepted = true
        
        messages = []
        
        expiresAt = Date()
        
        requests = []
    }
    
    private init(signal: SignalViewModel) {
        id = signal.id
        creator = signal.creator
        createdAt = signal.createdAt
        imageUrls = signal.imageUrls
        kind = signal.kind
        type = signal.type
        quote = signal.quote
        lastReview = signal.lastReview
        location = signal.location
        destination = signal.destination
        destinationName = signal.destinationName
        destinationAddress = signal.destinationAddress
        mine = signal.mine
        requested = signal.requested
        accepted = signal.accepted
        messages = signal.messages
        expiresAt = signal.expiresAt
        requests = signal.requests
    }
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: SignalViewModel, rhs: SignalViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        return SignalViewModel(signal: self)
    }
    
}

extension SignalViewModel: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSString
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let other = object as? SignalViewModel {
            return self == other
        }
        return false
    }
}
