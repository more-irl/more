//
//  ExploreTopBarCollectionViewCell.swift
//  More
//
//  Created by Luko Gjenero on 14/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ExploreTopBarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var signalBar: SignalTopBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        layer.transform = CATransform3DIdentity
    }
    
    func setup(for model: SignalViewModel) {
        if model.kind == .claimable {
            signalBar.setupForMore(title: "Claim This Signal")
        } else if model.kind == .curated {
            signalBar.setupForMore(title: "Claim This Signal")
        } else {
            signalBar.setup(for: model)
        }
    }
    
    func setupEmpty() {
        signalBar.setupEmpty()
    }

}
