//
//  SignalDetailsNavigationController.swift
//  More
//
//  Created by Luko Gjenero on 19/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsNavigationController : UIViewController {

    private var signalId: String? = nil
    private var requestId: String? = nil
    private var bottomPadding: CGFloat = 0
    
    private weak var tapButton: UIButton? = nil
    
    var back: (()->())?
    var report: (()->())?
    var requested: (()->())?
    var removed: (()->())?
    var claimed: (()->())?
    
    var tapped: (()->())? {
        didSet {
            if tapped != nil {
                let button = UIButton(type: .custom)
                guard tapButton == nil else { return }
                button.backgroundColor = .clear
                button.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(button)
                button.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
                button.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
                button.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
                button.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
                button.addTarget(self, action: #selector(tapTouch), for: .touchUpInside)
                tapButton = button
            } else {
                tapButton?.removeFromSuperview()
            }
        }
    }
    
    @objc private func tapTouch() {
        tapped?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNestedNavigation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(signalExpired), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc private func signalExpired(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            if let signalId = notice.userInfo?["signalId"] as? String,
                let cancelled = notice.userInfo?["cancelled"] as? Bool,
                self?.signalId == signalId {
                
                self?.presentSignalExpired(cancelled)
            }
        }
    }
    
    func setup(for model: SignalViewModel) {
        signalId = model.id
        
        let vc = SignalDetailsViewController()
        vc.backTap = { [weak self] in
            self?.back?()
        }
        vc.moreTap = { [weak self] in
            self?.report?()
        }
        vc.scrolling = { [weak self] in
            self?.messagingOverlay?.fullyCollapse()
            self?.requestsOverlay?.fullyCollapse()
        }
        _ = vc.view
        
        var updatedModel = model
        if let updated = SignalTrackingService.shared.updatedSignalData(for: model.id) {
            updatedModel = SignalViewModel(signal: updated)
        }
        
        vc.setup(for: updatedModel)
        navigation.setViewControllers([vc], animated: false)
        navigation.delegate = self
        
        if updatedModel.kind == .claimable {
            setupClaimOverlay(for: model)
            vc.bottomPadding = 73
            bottomPadding = 73
        } else if updatedModel.kind == .curated {
            // TODO: --
            vc.bottomPadding = 73
            bottomPadding = 73
        } else {
            if updatedModel.mine {
                setupRequestsOverlay()
                requestsOverlay?.setup(for: model)
                vc.bottomPadding = 100
                bottomPadding = 100
            } else {
                requestId = model.requests.first?.id
                setupMessageingOverlay()
                messagingOverlay?.setup(for: model)
                if model.requested {
                    vc.bottomPadding = 222
                    bottomPadding = 222
                } else {
                    vc.bottomPadding = 73
                    bottomPadding = 73
                }
            }
        }
        
        requestsOverlay?.setup(for: model)
    }
    
    func scrollToTop(animated: Bool) {
        if let vc = navigation.topViewController as? SignalDetailsViewController {
            vc.scrollToTop(animated: animated)
        }
    }
    
    // MARK: - navigation controller
    
    private let navigation = UINavigationController()
    
    private func setupNestedNavigation() {
        navigation.view.translatesAutoresizingMaskIntoConstraints = false
        addChildViewController(navigation)
        if let tap = tapButton {
            view.insertSubview(navigation.view, belowSubview: tap)
        } else {
            view.addSubview(navigation.view)
        }
        navigation.willMove(toParentViewController: self)
        navigation.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        navigation.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        navigation.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        navigation.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        navigation.setNavigationBarHidden(true, animated: false)
        
        navigation.enableSwipeToPop()
    }
    
    // MARK: - messaging overlay
    
    private var messagingOverlay: RequestMessagingOverlayViewController?
    
    private func setupMessageingOverlay() {
        
        claimOverlay?.removeFromSuperview()
        
        requestsOverlay?.willMove(toParentViewController: nil)
        requestsOverlay?.removeFromParentViewController()
        requestsOverlay?.view.removeFromSuperview()
        requestsOverlay = nil
        
        guard self.messagingOverlay == nil else { return }
        
        let messagingOverlay = RequestMessagingOverlayViewController()
        messagingOverlay.view.translatesAutoresizingMaskIntoConstraints = false
        addChildViewController(messagingOverlay)
        if let tap = tapButton {
            view.insertSubview(messagingOverlay.view, belowSubview: tap)
        } else {
            view.addSubview(messagingOverlay.view)
        }
        messagingOverlay.didMove(toParentViewController: self)
        messagingOverlay.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        messagingOverlay.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        messagingOverlay.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        messagingOverlay.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        view.setNeedsLayout()
        
        messagingOverlay.requested = { [weak self] in
            self?.requested?()
        }
        
        messagingOverlay.removed = { [weak self] in
            self?.removed?()
        }
        
        self.messagingOverlay = messagingOverlay
    }
    
    private var requestsOverlay: RequestListOverlayViewController?
    
    private func setupRequestsOverlay() {
        
        claimOverlay?.removeFromSuperview()
        
        messagingOverlay?.willMove(toParentViewController: nil)
        messagingOverlay?.removeFromParentViewController()
        messagingOverlay?.view.removeFromSuperview()
        messagingOverlay = nil
        
        guard self.requestsOverlay == nil else { return }
        
        let requestsOverlay = RequestListOverlayViewController()
        requestsOverlay.view.translatesAutoresizingMaskIntoConstraints = false
        addChildViewController(requestsOverlay)
        if let tap = tapButton {
            view.insertSubview(requestsOverlay.view, belowSubview: tap)
        } else {
            view.addSubview(requestsOverlay.view)
        }
        requestsOverlay.didMove(toParentViewController: self)
        requestsOverlay.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        requestsOverlay.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        requestsOverlay.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        requestsOverlay.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        view.setNeedsLayout()
        
        requestsOverlay.deleted = { [weak self] in
            self?.removed?()
        }
        
        requestsOverlay.selected = { [weak self] (signal, request) in
            let vc = RequestDetailsNavigationController()
            vc.back = {
                self?.navigationController?.popViewController(animated: true)
            }
            _ = vc.view
            vc.setup(signal: signal, request: request)
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.requestsOverlay = requestsOverlay
    }
    
    private func presentSignalExpired(_ cancelled: Bool) {
        
        UIResponder.currentFirstResponder()?.resignFirstResponder()
        
        let vc = RequestExpiredViewController()
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(vc.view)
        addChildViewController(vc)
        vc.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        vc.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        vc.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        view.setNeedsLayout()
        vc.setupForSignal(cancelled: cancelled)
        
        vc.doneTap = { [weak self, weak vc] in
            vc?.removeFromParentViewController()
            vc?.view.removeFromSuperview()
            self?.back?()
        }
    }
    
    // MARK: - claim overlay
    
    private var claimOverlay: SignalClaimBar?
    
    private func setupClaimOverlay(for model: SignalViewModel) {
        
        messagingOverlay?.willMove(toParentViewController: nil)
        messagingOverlay?.removeFromParentViewController()
        messagingOverlay?.view.removeFromSuperview()
        messagingOverlay = nil
        
        requestsOverlay?.willMove(toParentViewController: nil)
        requestsOverlay?.removeFromParentViewController()
        requestsOverlay?.view.removeFromSuperview()
        requestsOverlay = nil
        
        guard self.claimOverlay == nil else { return }
        
        let claimOverlay = SignalClaimBar()
        claimOverlay.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(claimOverlay)
        claimOverlay.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        claimOverlay.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        claimOverlay.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        claimOverlay.heightAnchor.constraint(equalToConstant: 110).isActive = true
        view.setNeedsLayout()
        
        claimOverlay.shareTap = { [weak self] in
            
            
            
            guard let signal = SignalingService.shared.claimableSignalData(for: model.id) else { return }
            
            self?.showLoading()
            SignalingService.shared.claimSignal(from: signal, completion: { (signalId, errorMsg) in
                self?.hideLoading()
                if signalId != nil {
                    self?.claimed?()
                } else {
                    self?.errorAlert(text: errorMsg ?? "Unknown error")
                }
            })
        }
        
        self.claimOverlay = claimOverlay
    }
    
}

extension SignalDetailsNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let vc = viewController as? ProfileViewController {
            vc.bottomPadding = bottomPadding
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if let vc = viewController as? ProfileViewController {
            vc.scrolling = { [weak self] in
                self?.messagingOverlay?.fullyCollapse()
                self?.requestsOverlay?.fullyCollapse()
            }
        }
    }
}

extension SignalDetailsNavigationController: SignalDetailsView {
    
    var model: SignalViewModel? {
        guard let vc = navigation.viewControllers.first(where: { $0 is SignalDetailsViewController }) as? SignalDetailsViewController else {
            return nil
        }
        return vc.model
    }
    
    func photosFrame() -> CGRect {
        guard let vc = navigation.viewControllers.first(where: { $0 is SignalDetailsViewController }) as? SignalDetailsViewController else {
            return .zero
        }
        return vc.photosFrame()
    }
    
    func prepareToPresentSignalDetails(size: CGSize) -> CGRect {
        guard let vc = navigation.viewControllers.first(where: { $0 is SignalDetailsViewController }) as? SignalDetailsViewController else {
             return CGRect(origin: .zero, size: size)
        }
        return vc.prepareToPresentSignalDetails(size: size)
    }
    
    func prepareToDismissSignalDetails(duration: TimeInterval, complete: @escaping (_ frame: CGRect)->()) {
        guard let vc = navigation.viewControllers.first(where: { $0 is SignalDetailsViewController }) as? SignalDetailsViewController else {
            complete(view.bounds)
            return
        }
        vc.prepareToDismissSignalDetails(duration: duration, complete: complete)
    }
}

extension UINavigationController: SignalDetailsView {
    
    var model: SignalViewModel? {
        guard let details = viewControllers.first as? SignalDetailsNavigationController else {
            return nil
        }
        return details.model
    }
    
    func photosFrame() -> CGRect {
        guard let details = viewControllers.first as? SignalDetailsNavigationController else {
            return .zero
        }
        return details.photosFrame()
    }
    
    func prepareToPresentSignalDetails(size: CGSize) -> CGRect {
        guard let details = viewControllers.first as? SignalDetailsNavigationController else {
            return CGRect(origin: .zero, size: size)
        }
        return details.prepareToPresentSignalDetails(size: size)
    }
    
    func prepareToDismissSignalDetails(duration: TimeInterval, complete: @escaping (_ frame: CGRect)->()) {
        guard let details = viewControllers.first as? SignalDetailsNavigationController else {
            complete(view.bounds)
            return
        }
        details.prepareToDismissSignalDetails(duration: duration, complete: complete)
    }
    
}

/*
extension SignalDetailsNavigationController: UIViewControllerTransitioningDelegate {
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let vc = navigation.viewControllers.first(where: { $0 is SignalDetailsViewController }) as? SignalDetailsViewController else {
            return nil
        }
        return vc.animationController(forDismissed: dismissed)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let vc = navigation.viewControllers.first(where: { $0 is SignalDetailsViewController }) as? SignalDetailsViewController else {
            return nil
        }
        return vc.interactionControllerForDismissal(using: animator)
    }
}
*/
