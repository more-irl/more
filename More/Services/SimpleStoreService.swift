//
//  SimpleStoreService.swift
//  More
//
//  Created by Luko Gjenero on 07/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SimpleStoreService {
    
    typealias CodableClass = AnyObject & Codable
    
    static let shared = SimpleStoreService()
    
    class StoreItem : Codable {
        let key: String
        var value: Codable?
        
        enum CodingKeys: String, CodingKey {
            case key = "key"
            case value = "value"
        }
        
        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            key = try container.decode(String.self, forKey: .key)
        }
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(key, forKey: .key)
        }
        
        init(key: String, value: Codable?) {
            self.key = key
            self.value = value
        }
    }
    
    private var store: [String: StoreItem] = [:]
    
    private let storeKey = "com.more.simpleStore.data"
    
    init() {
        load()
    }
    
    private func load() {
        if let data = UserDefaults.standard.value(forKey:storeKey) as? Data,
            let store = try? PropertyListDecoder().decode([String: StoreItem].self, from: data) {
            self.store = store
        }
    }
    
    private func save() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(store), forKey:storeKey)
    }
    
    func store(item: StoreItem) {
        store[item.key] = item
        save()
    }
    
    func remove(forKey key: String) {
        store.removeValue(forKey: key)
        save()
    }
    
    func get(forKey key: String) -> Codable? {
        return store[key]?.value
    }

}
