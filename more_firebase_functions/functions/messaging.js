const functions = require('firebase-functions');
const admin = require('firebase-admin');
// admin.initializeApp(functions.config().firebase);

exports.sendMessage = function(userId, title, text, params, priority = '5') {

	return admin.database().ref("/users/" + userId + "/registrationToken")
	.once('value')
  .then((token) => {
	  
    if (token.exists()) {

      const deviceToken = token.val();    

      var message = {
        notification: {
          title: title,
          body: text,
        },
        apns: {
          headers: {
            'apns-priority': priority,
          },
          payload: {
            aps: {
              alert: {
                title: title,
                body: text,
              },
              sound: 'default',
              badge: 1, 
            },
          },
        },
        token: deviceToken, 
      };

      for (var key in params) {
      	message.apns.payload[key] = params[key]
      }

      return admin.messaging().send(message);
    }

    return null; 
  })
  .then((response) => {
    if (response === null) {
      console.log('Message not sent. UserID ' + userId + ' has not registrationToken');
    } else {
      console.log('Successfully sent message:', response);
    }
    return null;
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });
};