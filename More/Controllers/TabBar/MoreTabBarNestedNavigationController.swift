//
//  MoreTabBarNestedNavigationController.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class MoreTabBarNestedNavigationController: UINavigationController {

    let moreNavigationBar = NavigationBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.isTranslucent = false
        setNavigationBarHidden(true, animated: false)
        
        setupNavBar()
        
        /*
        
        // background
        let bg = Colors.topBarBackground
        navigationBar.setBackgroundImage(
            UIImage.onePixelImage(color: bg),
            for: .default)
        
        // title
        let textColor = Colors.topBarText
        let font = UIFont(name: "Gotham-Black", size: 26) ?? UIFont.systemFont(ofSize: 26)
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.kern: 1.53]
        
        // shadow
        navigationBar.shadowImage = UIImage.onePixelImage(color: .clear)
        
        // title
        navigationItem.title = "MORE"
        */
        
        enableSwipeToPop()
    }
    
    // MARK: - navigation bar
    
    private func setupNavBar() {
        moreNavigationBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(moreNavigationBar)
        moreNavigationBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        moreNavigationBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        moreNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -60).isActive = true
        moreNavigationBar.heightAnchor.constraint(equalToConstant: 60).isActive = true
        additionalSafeAreaInsets = UIEdgeInsets(top: 60, left: 0, bottom: 70, right: 0)
    }
    
    private var navigationHidden: Bool = false
    
    func hideNavigation(hide: Bool, animated: Bool) {
        if hide {
            guard !navigationHidden else { return }
            if animated {
                UIView.animate(withDuration: 0.3) {
                    self.hideNavigation()
                }
            } else {
                hideNavigation()
            }
        } else {
            guard navigationHidden else { return }
            if animated {
                UIView.animate(withDuration: 0.3) {
                    self.showNavigation()
                }
            } else {
                showNavigation()
            }
        }
    }
    
    private func hideNavigation() {
        moreNavigationBar.layer.transform = CATransform3DMakeTranslation(0, -60, 0)
        additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        navigationHidden = true
    }
    
    private func showNavigation() {
        moreNavigationBar.layer.transform = CATransform3DIdentity
        additionalSafeAreaInsets = UIEdgeInsets(top: 60, left: 0, bottom: 70, right: 0)
        navigationHidden = false
    }
    
    

}
