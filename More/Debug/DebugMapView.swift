//
//  DebugMapView.swift
//  More
//
//  Created by Luko Gjenero on 21/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import MapKit

class DebugMapView: UIView, MKMapViewDelegate {

    private let mapView: MKMapView = {
        let map =  MKMapView(frame: .zero)
        map.translatesAutoresizingMaskIntoConstraints = false
        return map
    }()
    private let cancel: UIButton = {
        let cancel = UIButton(type: .custom)
        cancel.translatesAutoresizingMaskIntoConstraints = false
        cancel.backgroundColor = UIColor(red: 238, green: 238, blue: 238)
        cancel.setTitleColor(UIColor(red: 44, green: 44, blue: 44), for: .normal)
        cancel.setTitle("Cancel", for: .normal)
        return cancel
    }()
    private let ok: UIButton = {
        let ok = UIButton(type: .custom)
        ok.translatesAutoresizingMaskIntoConstraints = false
        ok.backgroundColor = UIColor(red: 140, green: 182, blue: 255)
        ok.setTitleColor(UIColor(red: 255, green: 255, blue: 255), for: .normal)
        ok.setTitle("OK", for: .normal)
        return ok
    }()
    
    private var pin: MKPointAnnotation?
    private var location: CLLocation?
    
    var cancelTap: (()->())?
    var okTap: ((_ location: CLLocation?)->())?
    
    init() {
        super.init(frame: .zero)
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 4
        
        addSubview(mapView)
        addSubview(ok)
        addSubview(cancel)
        
        mapView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mapView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        mapView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: cancel.topAnchor).isActive = true
        cancel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        cancel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        cancel.rightAnchor.constraint(equalTo: ok.leftAnchor).isActive = true
        cancel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        ok.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        ok.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        ok.heightAnchor.constraint(equalTo: cancel.heightAnchor).isActive = true
        ok.widthAnchor.constraint(equalTo: cancel.widthAnchor).isActive = true
        layoutIfNeeded()
        
        mapView.delegate = self
        
        cancel.addTarget(self, action: #selector(cancelTouch), for: .touchUpInside)
        ok.addTarget(self, action: #selector(okTouch), for: .touchUpInside)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureReconizer:)))
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func cancelTouch() {
        cancelTap?()
    }
    
    @objc private func okTouch() {
        okTap?(location)
    }
    
    func setup(location: CLLocation?) {
        self.location = location
        
        let coordinates = location?.coordinate ?? CLLocationCoordinate2D(latitude: 40.758896, longitude:  -73.985130)
        let regionRadius: CLLocationDistance = 400
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinates,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        addPin(location: coordinates)
    }
    
    private func addPin(location: CLLocationCoordinate2D) {
        let pin = MKPointAnnotation()
        pin.coordinate = location
        pin.title = "You"
        pin.subtitle = "Will simulate your location here"
        mapView.addAnnotation(pin)
        self.pin = pin
    }
    
    @objc private func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        
        if let pin = pin {
            pin.coordinate = coordinate
        } else {
            addPin(location: coordinate)
        }
        self.location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }

    class func show( _ locationSelected: ((_ location: CLLocation?)->())? = nil) {
        
        if let window = AppDelegate.appDelegate()?.window {
            
            let map = DebugMapView()
            map.translatesAutoresizingMaskIntoConstraints = false
            window.addSubview(map)
            map.translatesAutoresizingMaskIntoConstraints = false
            map.leadingAnchor.constraint(equalTo: window.leadingAnchor, constant: 32).isActive = true
            map.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            map.topAnchor.constraint(equalTo: window.topAnchor, constant: 60).isActive = true
            map.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            window.layoutIfNeeded()
            
            map.setup(location: LocationService.shared.currentLocation)
            map.cancelTap = { [weak map] in
                map?.removeFromSuperview()
            }
            map.okTap = { [weak map] (location) in
                locationSelected?(location)
                map?.removeFromSuperview()
            }
        }
    }
    
}


