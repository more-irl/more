//
//  MoreUI.swift
//  More
//
//  Created by Luko Gjenero on 12/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SCSafariPageController
import SDWebImage

class MoreUI {

    class func initialize() {
        SCSafariZoomedOutLayouter.moreCustom()
        SCPageViewController.moreCustom()
        SCSafariPageWrapperViewController.moreCustom()
        
        // not using for now
        // SDImageCodersManager.shared.addCoder(ProgressiveJPEGDecoder.shared)
    }

}
