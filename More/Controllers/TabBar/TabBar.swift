//
//  TabBar.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class TabBar: LoadableView {
    
    @IBOutlet weak var home: TabBarItem!
    @IBOutlet weak var times: TabBarItem!
    @IBOutlet weak var signal: TabBarItem!
    @IBOutlet weak var alerts: TabBarItem!
    @IBOutlet weak var profile: TabBarItem!
    
    private lazy var items: [TabBarItem] = [home, times, signal, alerts, profile]
    
    var tabSelected: ((_ item: TabBarItem.ButtonType)->())?
    
    override func setupNib() {
        super.setupNib()
        
        home.buttonTap = { [weak self] in
            if self?.home.isSelected == false {
                self?.selectItem(.home)
                self?.tabSelected?(.home)
            }
        }
        
        times.buttonTap = { [weak self] in
            if self?.times.isSelected == false {
                self?.selectItem(.times)
                self?.tabSelected?(.times)
            }
        }
        
        signal.buttonTap = { [weak self] in
            self?.tabSelected?(.signal)
        }
        
        alerts.buttonTap = { [weak self] in
            if self?.alerts.isSelected == false {
                self?.selectItem(.alerts)
                self?.tabSelected?(.alerts)
            }
        }
        
        profile.buttonTap = { [weak self] in
            if self?.profile.isSelected == false {
                self?.selectItem(.profile)
                self?.tabSelected?(.profile)
            }
        }
    }
    
    func selectItem(_ type: TabBarItem.ButtonType) {
        for item in items {
            item.isSelected = item.type == type.rawValue
        }
    }
    
    private func deselectAll() {
        
    }
    
    
    
}
