//
//  BranchService.swift
//  More
//
//  Created by Luko Gjenero on 29/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Branch

class BranchService {
    
    struct Notifications {
        static let ReferralIDLoaded = NSNotification.Name(rawValue: "com.more.branch.referralId")
    }
    
    static let shared = BranchService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        toForeground()
    }
    
    func initialize(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: { [weak self] params, error in
            
            if let params = params {
                
                //  check referral ID
                if let referralId = params["referralId"] as? String {
                    self?.referralId = referralId
                    NotificationCenter.default.post(name: Notifications.ReferralIDLoaded, object: self, userInfo: ["referralId": referralId])
                }
            }
            
            if error == nil {
                // DEBUG
                print("params: %@", params as? [String: AnyObject] ?? {})
            }
        })
    }
    
    func openApplicationURL(application: UIApplication, url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        return Branch.getInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func continueUserActivity(_ userActivity: NSUserActivity) {
        Branch.getInstance().continue(userActivity)
    }
    
    
    
    // MARK: - referral storage
    
    private let referralIdKey = "com.more.branch.referralId"
    
    private(set) var referralId: String?
    
    @objc private func toForeground() {
        referralId = UserDefaults.standard.string(forKey: referralIdKey)
    }
    
    @objc private func toBackground() {
        UserDefaults.standard.set(referralId, forKey: referralIdKey)
    }
    
    // MARK: - api
    
    func createInviteLink(complete: @escaping (_ url: String?)->()) {
        
        guard let id = ProfileService.shared.profile?.getId() else {
            complete(nil)
            return
        }
        
        let linkProperties = BranchLinkProperties()
        linkProperties.channel = "invite"
        let branchUniveralObject = BranchUniversalObject()
        branchUniveralObject.contentDescription = "Join me on More!"
        branchUniveralObject.imageUrl = "https://static1.squarespace.com/static/59e2d7b3a9db09df6bf184b3/t/5a706bcce4966b05b509fb2c/1532005882286/More-logo-with-upper.PNG?format=300w"
        branchUniveralObject.contentMetadata.customMetadata["action"] = "referral"
        branchUniveralObject.contentMetadata.customMetadata["referralId"] = "\(id)"
        branchUniveralObject.contentMetadata.customMetadata["source"] = "iOS"
        branchUniveralObject.getShortUrl(with: linkProperties, andCallback: { (url, error) in
            guard let url = url, url.count > 0 else {
                complete(nil)
                return
            }
            complete(url)
        })
    }

}
