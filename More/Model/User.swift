//
//  User.swift
//  More
//
//  Created by Luko Gjenero on 09/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import Firebase


struct User: MoreDataObject {
    
    let id: String
    let name: String
    let avatar: String
    let quote: String?
    let quoteAuthor: String?
    let age: Int?
    
    let location: Location?
    
    let avgTimeReview: Float?
    let avgUserReview: Float?
    let numReview: Int?
    let numGoings: Int?
    let lastReview: Review?
    var tags: [Review.Tag: Int]?
    
    // ....
    
    init(id: String,
         name: String,
         avatar:String,
         quote: String? = nil,
         quoteAuthor: String? = nil,
         age: Int? = nil,
         location: Location? = nil,
         avgTimeReview: Float? = nil,
         avgUserReview: Float? = nil,
         numReview: Int? = nil,
         numGoings: Int? = nil,
         lastReview: Review? = nil,
         tags: [Review.Tag: Int]? = nil) {
        self.id = id
        self.name = name
        self.avatar = avatar
        self.quote = quote
        self.quoteAuthor = quoteAuthor
        self.age = age
        self.location = location
        self.avgTimeReview = avgTimeReview
        self.avgUserReview = avgUserReview
        self.numReview = numReview
        self.numGoings = numGoings
        self.lastReview = lastReview
        self.tags = tags
    }
    
    init() {
        self.init(id: "", name: "", avatar: "")
    }
    
    func isMe() -> Bool {
        return id == ProfileService.shared.profile?.id
    }
    
    func short() -> ShortUser {
        return ShortUser(id: id, name: name, avatar: avatar, age: age)
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }
    
    // Data protocol
    
    var json: [String : Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String : Any]) -> User? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let user = try? JSONDecoder().decode(User.self, from: jsonData) {
            return user
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> User? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return User.fromJson(json)
        }
        return nil
    }
}
