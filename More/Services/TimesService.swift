//
//  TimesService.swift
//  More
//
//  Created by Luko Gjenero on 10/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

class TimesService {
    
    struct Notifications {
        static let TimeLoaded = NSNotification.Name(rawValue: "com.more.times.loaded")
        static let TimeLocation = NSNotification.Name(rawValue: "com.more.times.location")
        static let TimeMeetLocation = NSNotification.Name(rawValue: "com.more.times.meet")
        static let TimeMessage = NSNotification.Name(rawValue: "com.more.times.message")
        static let TimeStateChanged = NSNotification.Name(rawValue: "com.more.times.state")
        static let TimeReview = NSNotification.Name(rawValue: "com.more.times.review")
        static let TimeExpired = NSNotification.Name(rawValue: "com.more.times.expired")
    }
    
    static let shared = TimesService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdate), name: LocationService.Notifications.LocationUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(logout), name: ProfileService.Notifications.ProfileLogout, object: nil)
        load()
        let trackedIds = trackedTimesIds
        trackedTimesIds = [:]
        for (timeId, _) in trackedIds {
            track(timeId: timeId)
        }
    }
    
    @objc private func toForeground() {
        // TODO
    }
    
    @objc private func toBackground() {
        // TODO
    }
    
    @objc private func locationUpdate() {
        
        guard let coordinates = LocationService.shared.currentLocation?.coordinate else { return }
        
        let location = Location(coordinates: coordinates)
        for (_, time) in trackedTimes {
            guard time.shouldTrackLocation() else { continue }
            if time.isMine() {
                updateCreatorTimeLocation(timeId: time.id, location: location)
            } else {
                updateRequesterTimeLocation(timeId: time.id, location: location)
            }
        }
        if let time = currentActiveTime {
            guard time.shouldTrackLocation() else { return }
            if time.isMine() {
                updateCreatorTimeLocation(timeId: time.id, location: location)
            } else {
                updateRequesterTimeLocation(timeId: time.id, location: location)
            }
        }
    }
    
    @objc private func logout() {
        for timeId in trackedTimesIds.keys {
            removeTracking(timeId)
        }
    }
    
    // MARK: - data
    
    private let trackedTimesKey = "com.more.times.tracked"
    private var trackedTimesIds: [String: UInt] = [:]
    private var trackedTimes: [String: Time] = [:]
    
    private let currentActiveTimeKey = "com.more.times.active"
    private var currentActiveTime: Time? = nil

    private func load() {
        if let data = UserDefaults.standard.value(forKey:trackedTimesKey) as? Data,
            let trackedTimesIds = try? PropertyListDecoder().decode([String: UInt].self, from: data) {
            self.trackedTimesIds = trackedTimesIds
        }
    }
    
    private func loadActiveTime() {
        if let data = UserDefaults.standard.value(forKey:currentActiveTimeKey) as? Data,
            let currentActiveTime = try? PropertyListDecoder().decode(Time.self, from: data) {
            self.currentActiveTime = currentActiveTime
        }
    }
    
    private func save() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(trackedTimesIds), forKey:trackedTimesKey)
    }
    
    private func saveActiveTime() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(currentActiveTime), forKey:currentActiveTimeKey)
    }
    
    // MARK: - api
    
    func track(timeId: String) {
        
        guard trackedTimesIds[timeId] == nil else { return }
        
        trackedTimesIds[timeId] =
        Database.database().reference().child("times").child("active").child(timeId).observe(
            .value,
            with: { [weak self] (snapshot) in
                if snapshot.exists() {
                    let time = Time.fromSnapshot(snapshot)
                    self?.timeChanged(time)
                } else {
                    self?.timeExpired(timeId)
                }
            },
            withCancel: { [weak self] (_) in
                self?.removeTracking(timeId)
        })
        save()
    }
    
    func removeTracking(_ timeId: String) {
        if let handle = trackedTimesIds[timeId] {
            Database.database().reference().child("times").child("active").child(timeId).removeObserver(withHandle: handle)
        }
        trackedTimesIds.removeValue(forKey: timeId)
        trackedTimes.removeValue(forKey: timeId)
    }
    
    private func timeChanged(_ time: Time?) {
        guard let time = time else { return }
        
        checkForActiveTime(time)
        
        guard trackedTimesIds[time.id] != nil else {
            removeTracking(time.id)
            return
        }
        
        if let trackedTime = trackedTimes[time.id] {
            
            checkFinished(old: trackedTime, new: time)
            if time.state() == .met {
                checkReviews(old: trackedTime, new: time)
            } else {
                checkLocation(old: trackedTime, new: time)
                checkMessages(old: trackedTime, new: time)
            }
            
            trackedTimes[time.id] = time
        } else {
            if time.finalExpiration() < Date() {
                removeTracking(time.id)
            } else {
                trackedTimes[time.id] = time
                NotificationCenter.default.post(name: Notifications.TimeLoaded, object: self, userInfo: ["time": time])
                
                guard let coordinates = LocationService.shared.currentLocation?.coordinate else { return }
                let location = Location(coordinates: coordinates)
                if time.isMine() {
                    updateCreatorTimeLocation(timeId: time.id, location: location)
                } else {
                    updateRequesterTimeLocation(timeId: time.id, location: location)
                }
            }
        }
    }
    
    private func checkForActiveTime(_ time: Time) {
        if time.id == currentActiveTime?.id {
            if !time.shouldTrackLocation() {
                currentActiveTime = nil
            } else {
                currentActiveTime = time
            }
            saveActiveTime()
        } else if currentActiveTime == nil {
            if time.shouldTrackLocation() {
                currentActiveTime = time
                saveActiveTime()
            }
        }
    }
    
    private func timeExpired(_ timeId: String) {
        removeTracking(timeId)
        NotificationCenter.default.post(name: Notifications.TimeExpired, object: self, userInfo: ["timeId": timeId])
    }
    
    private func checkFinished(old: Time, new: Time) {
        if old.otherState() != new.otherState() || old.state() != new.state() {
            NotificationCenter.default.post(name: Notifications.TimeStateChanged, object: self, userInfo: ["time": new])
        }
    }
    
    private func checkReviews(old: Time, new: Time) {
        if old.otherHasReviewed() != new.otherHasReviewed(),
            let review = new.otherReview() {
            NotificationCenter.default.post(name: Notifications.TimeReview, object: self, userInfo: ["time": new, "review": review])
        }
        if old.haveReviewed() != new.haveReviewed(),
            let review = new.myReview() {
            NotificationCenter.default.post(name: Notifications.TimeReview, object: self, userInfo: ["time": new, "review": review])
        }
    }
    
    private func checkMessages(old: Time, new: Time) {
        
        for msg in new.messages ?? [] {
            let oldMsg = old.messages?.first(where: { $0.id == msg.id })
            if msg.isMine() {
                if let oldMsg = oldMsg, (oldMsg.readAt == nil && msg.readAt != nil) || (oldMsg.deliveredAt == nil && msg.deliveredAt != nil) {
                    NotificationCenter.default.post(name: Notifications.TimeMessage, object: self, userInfo: ["time": new, "message": msg])
                }
            } else {
                if oldMsg == nil || (oldMsg?.readAt == nil && msg.readAt != nil) || (oldMsg?.deliveredAt == nil && msg.deliveredAt != nil) {
                    NotificationCenter.default.post(name: Notifications.TimeMessage, object: self,  userInfo: ["time": new, "message": msg])
                }
                if msg.deliveredAt == nil && msg.readAt == nil {
                    setMesageDelivered(timeId: new.id, messageId: msg.id)
                }
            }
        }
    }
    
    private func checkLocation(old: Time, new: Time) {
        
        if old.isMine() {
            if let newLocation = new.requester.location {
                if old.requester.location != newLocation {
                    NotificationCenter.default.post(name: Notifications.TimeLocation, object: self, userInfo: ["time": new, "location": newLocation])
                }
            }
        } else {
            if let newLocation = new.signal.creator.location {
                if old.signal.creator.location != newLocation {
                    NotificationCenter.default.post(name: Notifications.TimeLocation, object: self, userInfo: ["time": new, "location": newLocation])
                }
            }
        }
        
        guard let newLocation = new.meeting else { return }
        if old.meeting != newLocation {
            NotificationCenter.default.post(name: Notifications.TimeMeetLocation, object: self, userInfo: ["time": new, "meeting": newLocation])
        }
    }
    
    func updatedTimeData(for timeId: String) -> Time? {
        return trackedTimes[timeId]
    }
    
    private func updateCreatorTimeLocation(timeId: String, location: Location) {
        Database.database().reference().child("times").child("active").child(timeId).child("signal").child("creator").child("location").setValue(location.json)
    }

    private func updateRequesterTimeLocation(timeId: String, location: Location) {
        Database.database().reference().child("times").child("active").child(timeId).child("requester").child("location").setValue(location.json)
    }
    
    func updateTimeMeeting(timeId: String, location: Location) {
        Database.database().reference().child("times").child("active").child(timeId).child("meeting").setValue(location.json)
    }
    
    func sendMessage(_ message: Message, for timeId: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        let messageValue = message.json
        
        Database.database().reference().child("times").child("active").child(timeId)
            .child("messageList").child(message.id).setValue(messageValue) { (error, reference) in
                if let error = error {
                    complete?(false, error.localizedDescription)
                    return
                }
                
                Analytics.logEvent("TimeMessage", parameters: ["timeId": timeId])
                
                complete?(true, nil)
        }
    }
    
    func setMesageDelivered(timeId: String, messageId: String) {
        Database.database().reference().child("times").child("active").child(timeId).child("messageList").child(messageId).child("deliveredAt").setValue(Date().timeIntervalSinceReferenceDate)
    }
    
    func setMesageRead(timeId: String, messageId: String) {
        Database.database().reference().child("times").child("active").child(timeId).child("messageList").child(messageId).child("readAt").setValue(Date().timeIntervalSinceReferenceDate)
    }
    
    func setCreatorIsTyping(timeId: String, isTyping: Bool) {
       Database.database().reference().child("times").child("active").child(timeId).child("signal").child("creator").child("isTyping").setValue(isTyping)
    }
    
    func setRequesterIsTyping(timeId: String, isTyping: Bool) {
        Database.database().reference().child("times").child("active").child(timeId).child("requester").child("isTyping").setValue(isTyping)
    }
    
    func getCreatorIsTypingReference(timeId: String) -> DatabaseReference {
        return Database.database().reference().child("times").child("active").child(timeId).child("signal").child("creator").child("isTyping")
    }
    
    func getRequesterIsTypingReference(timeId: String) -> DatabaseReference {
        return Database.database().reference().child("times").child("active").child(timeId).child("requester").child("isTyping")
    }
    
    func removeTypingObservers(timeId: String) {
         getCreatorIsTypingReference(timeId: timeId).removeAllObservers()
        getRequesterIsTypingReference(timeId: timeId).removeAllObservers()
    }
    
    func deleteTime(timeId: String) {
        removeTracking(timeId)
        Database.database().reference().child("times").child("active").child(timeId).removeValue()
        
        Analytics.logEvent("TimeDeleted", parameters: ["timeId": timeId])
    }
    
    func arrivedTime(timeId: String, creator: Bool) {
        let ref = Database.database().reference().child("times").child("active").child(timeId)
        if creator {
            ref.child("creatorState").setValue(TimeState.arrived.rawValue)
            Analytics.logEvent("TimeCreatorArrived", parameters: ["timeId": timeId])
        } else {
            ref.child("requesterState").setValue(TimeState.arrived.rawValue)
            Analytics.logEvent("TimeRequesterArrived", parameters: ["timeId": timeId])
        }
    }
    
    func finishTime(timeId: String, creator: Bool) {
        let ref = Database.database().reference().child("times").child("active").child(timeId)
        if creator {
            ref.child("creatorState").setValue(TimeState.met.rawValue)
            Analytics.logEvent("TimeCreatorMet", parameters: ["timeId": timeId])
        } else {
            ref.child("requesterState").setValue(TimeState.met.rawValue)
            Analytics.logEvent("TimeRequesterMet", parameters: ["timeId": timeId])
        }
    }
    
    func cancelTime(timeId: String, creator: Bool, flagged: Bool = false, message: String? = nil) {
        let ref = Database.database().reference().child("times").child("active").child(timeId)
        if flagged {
            ref.child("flagged").setValue(true)
            
            Analytics.logEvent("TimeFlagged", parameters: ["timeId": timeId])
        }
        if creator {
            if let message = message {
                ref.child("creatorCancelMessage").setValue(message)
            }
            ref.child("creatorState").setValue(TimeState.cancelled.rawValue)
            Analytics.logEvent("TimeCanceled", parameters: ["timeId": timeId, "creator": "yes"])
        } else {
            if let message = message {
                ref.child("requesterCancelMessage").setValue(message)
            }
            ref.child("requesterState").setValue(TimeState.cancelled.rawValue)
            Analytics.logEvent("TimeCanceled", parameters: ["timeId": timeId, "creator": "no"])
        }
    }
    
    func closeTime(timeId: String, creator: Bool) {
        let ref = Database.database().reference().child("times").child("active").child(timeId)
        if creator {
            ref.child("creatorState").setValue(TimeState.closed.rawValue)
            Analytics.logEvent("TimeClosed", parameters: ["timeId": timeId, "creator": "yes"])
        } else {
            ref.child("requesterState").setValue(TimeState.closed.rawValue)
            Analytics.logEvent("TimeClosed", parameters: ["timeId": timeId, "creator": "no"])
        }
    }
    
    func sendTimeReview(timeId: String, review: Review, complete: ((_ success: Bool, _ errorMsg: String?)->())?) {
        let reviewValue = review.json
        
        Database.database().reference().child("times").child("active").child(timeId)
            .child("reviewList").childByAutoId().setValue(reviewValue) { (error, reference) in
                if let error = error {
                    complete?(false, error.localizedDescription)
                    return
                }
                
                Analytics.logEvent("TimeReviewed", parameters: ["timeId": timeId])
                
                complete?(true, nil)
        }
    }
    
    func getTrackedTimes() -> [Time] {
        return Array(trackedTimes.values)
    }
    
    func getActiveTime() -> Time? {
        return currentActiveTime
    }
}
