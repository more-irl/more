//
//  Services.swift
//  More
//
//  Created by Luko Gjenero on 14/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//


class Services {

    class func initialize() {
        
        _ = LocationService.shared
        _ = ProfileService.shared
        _ = PushNotificationService.shared
        _ = SignalingService.shared
        _ = SignalTrackingService.shared
        _ = TimesService.shared
        _ = AlertService.shared
        _ = NetworkSpeedProvider.shared
        _ = WindowsService.shared
        _ = ConfigService.shared
    }
    
}
