//
//  DebugLocationManager.swift
//  More
//
//  Created by Luko Gjenero on 21/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import CoreLocation
import Mapbox
import MapboxCoreNavigation

class DebugLocationManager: NavigationLocationManager {
    
    private var test: Bool = false
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(locationTest), name: LocationService.Notifications.LocationTest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    @objc private func locationTest() {
        stopUpdatingLocation()
        test = LocationService.shared.testLocation
    }
    
    @objc private func locationUpdated() {
        guard test else { return }
        if let location = LocationService.shared.currentLocation {
            delegate?.locationManager?(self, didUpdateLocations: [location])
        }
    }
    
    @objc private func toForeground() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [unowned self] in
            if LocationService.shared.testLocation {
                self.stopUpdatingLocation()
                if let location = LocationService.shared.currentLocation {
                    self.delegate?.locationManager?(self, didUpdateLocations: [location])
                }
            }
        }
        
    }
    
    @objc private func toBackground() {
        // nothing
    }
}

/*

class DebugMapboxLocationManager: CLLocationManager, MGLLocationManager {
    var delegate: MGLLocationManagerDelegate?
    
    var authorizationStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    
}
 */
