//
//  AlertService.swift
//  More
//
//  Created by Luko Gjenero on 18/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class AlertService {
    
    enum AlertType: Int, Codable {
        case request, response, signalMessage, timeMessage, review, welcome
    }
    
    struct AlertItem: Codable {
        let type: AlertType
        let createdAt: Date
        let signal: Signal?
        let time: Time?
        let message: Message?
        let request: Request?
        let review: Review?
        let url: String?
        
        init(type: AlertType,
             createdAt: Date,
             signal: Signal? = nil,
             time: Time? = nil,
             message: Message? = nil,
             request: Request? = nil,
             review: Review? = nil,
             url: String? = nil) {
            
            self.type = type
            self.createdAt = createdAt
            self.signal = signal
            self.time = time
            self.message = message
            self.request = request
            self.review = review
            self.url = url
        }
    }
    
    struct Notifications {
        static let NewAlert = NSNotification.Name(rawValue: "com.more.alert.new")
        static let AlertsChanged = NSNotification.Name(rawValue: "com.more.alert.changed")
    }
    
    static let shared = AlertService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        // Hooks to monitor messages, requests, responses, reviews etc.
        
        NotificationCenter.default.addObserver(self, selector: #selector(signalRequest(_:)), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalResponse(_:)), name: SignalTrackingService.Notifications.SignalResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalMessage(_:)), name: SignalTrackingService.Notifications.SignalMessage, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(timeMessage(_:)), name: TimesService.Notifications.TimeMessage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(timeStateChanged(_:)), name: TimesService.Notifications.TimeStateChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(timeReview(_:)), name: TimesService.Notifications.TimeReview, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(timeExpired(_:)), name: TimesService.Notifications.TimeExpired, object: nil)
        
        load()
        addInitialAlertIfNecessary()
    }
    
    // MARK: - data
    
    private(set) var alerts: [AlertItem] = []
    private var lastRead: Date? = nil
    
    @objc private func toForeground() {
        // TODO: --
    }
    
    @objc private func toBackground() {
        save()
    }
    
    private let lastReadKey = "com.more.alerts.lastRead"
    
    private var filePath: URL? {
        let urls: [URL] = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentsUrl = urls.first {
            return documentsUrl.appendingPathComponent("alerts.data")
        }
        return nil
    }
    
    private func load() {
        guard let path = filePath?.path else { return }
        
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? Data else { return }
        alerts = (try? PropertyListDecoder().decode([AlertItem].self, from: data)) ?? []
        
        if let data = UserDefaults.standard.value(forKey:lastReadKey) as? Data,
            let date = try? PropertyListDecoder().decode(Date.self, from: data) {
            lastRead = date
        }
    }
    
    private func save() {
        guard let path = filePath?.path else { return }
        
        if let data = try? PropertyListEncoder().encode(alerts) {
            NSKeyedArchiver.archiveRootObject(data, toFile: path)
        }
        UserDefaults.standard.set(try? PropertyListEncoder().encode(lastRead), forKey:lastReadKey)
    }
    
    // last read
    
    var hasNewAlerts: Bool {
        if let lastRead = self.lastRead {
            return alerts.first(where: { $0.createdAt > lastRead }) != nil
        }
        return alerts.count > 0
    }
    
    func allRead() {
        lastRead = Date()
        save()
    }
    
    // queue
    
    private func insertAlert(_ alert: AlertItem) {
        let itemId = alert.signal?.id ?? alert.time?.id ?? ""
        alerts.removeAll(where: { $0.signal?.id == itemId || $0.time?.id == itemId })
        alerts.insert(alert, at: 0)
    }
    
    private func removeAlert(_ id: String) {
        alerts.removeAll(where: { $0.signal?.id == id || $0.time?.id == id })
    }
    
    // notifications
    
    @objc private func signalRequest(_ notice: Notification) {
        if let request = notice.userInfo?["request"] as? Request, !request.sender.isMe(),
            let signal = notice.userInfo?["signal"] as? Signal
        {
            let alert = AlertItem(type: .request, createdAt: request.createdAt, signal: signal, request: request)
            insertAlert(alert)
            NotificationCenter.default.post(name: Notifications.NewAlert, object: self, userInfo: ["alert": alert])
        }
    }
    
    @objc private func signalResponse(_ notice: Notification) {
        if let request = notice.userInfo?["request"] as? Request,
            let signal = notice.userInfo?["signal"] as? Signal
        {
            if signal.myRequest()?.accepted == true {
                let alert = AlertItem(type: .response, createdAt: Date(), signal: signal, request: request)
                insertAlert(alert)
                NotificationCenter.default.post(name: Notifications.NewAlert, object: self, userInfo: ["alert": alert])
            } else if signal.myRequest()?.accepted == false {
                removeAlert(signal.id)
                NotificationCenter.default.post(name: Notifications.AlertsChanged, object: self, userInfo: nil)
            }
        }
    }
    
    @objc private func signalMessage(_ notice: Notification) {
        if let request = notice.userInfo?["request"] as? Request,
            let signal = notice.userInfo?["signal"] as? Signal,
            let message = notice.userInfo?["message"] as? Message,
            !message.isMine(), message.readAt == nil, message.deliveredAt == nil
        {
            let alert = AlertItem(type: .signalMessage, createdAt: message.createdAt, signal: signal, message: message, request: request)
            insertAlert(alert)
            NotificationCenter.default.post(name: Notifications.NewAlert, object: self, userInfo: ["alert": alert])
        }
    }
    
    @objc private func timeMessage(_ notice: Notification) {
        if let time = notice.userInfo?["time"] as? Time,
            let message = notice.userInfo?["message"] as? Message,
            !message.isMine(), message.readAt == nil, message.deliveredAt == nil
        {
            let alert = AlertItem(type: .timeMessage, createdAt: message.createdAt, time: time, message: message)
            insertAlert(alert)
            NotificationCenter.default.post(name: Notifications.NewAlert, object: self, userInfo: ["alert": alert])
        }
    }
    
    @objc private func timeStateChanged(_ notice: Notification) {
        if let time = notice.userInfo?["time"] as? Time
        {
            if (time.state() == .met || time.state() == .closed) && (time.otherState() == .met || time.otherState() == .closed)  {
                let alert = AlertItem(type: .review, createdAt: Date(), time: time)
                insertAlert(alert)
                NotificationCenter.default.post(name: Notifications.NewAlert, object: self, userInfo: ["alert": alert])
            } else if time.state() == .cancelled || time.otherState() == .cancelled {
                removeAlert(time.id)
                NotificationCenter.default.post(name: Notifications.AlertsChanged, object: self, userInfo: nil)
            }
        }
    }
    
    @objc private func timeReview(_ notice: Notification) {
        if let time = notice.userInfo?["time"] as? Time,
            let review = notice.userInfo?["review"] as? Review
        {
            let alert = AlertItem(type: .review, createdAt: review.createdAt, time: time)
            insertAlert(alert)
            NotificationCenter.default.post(name: Notifications.NewAlert, object: self, userInfo: ["alert": alert])
        }
        
        // TODO
    }
    
    @objc private func timeExpired(_ notice: Notification) {
        // TODO
    }
    
    // MARK: - initial alert
    
    private func addInitialAlertIfNecessary() {
        guard alerts.first(where: { $0.type == .welcome }) == nil else { return }
        
        let alert = AlertItem(type: .welcome, createdAt: Date(), url: Urls.welcomeVideo)
        insertAlert(alert)
    }

}
