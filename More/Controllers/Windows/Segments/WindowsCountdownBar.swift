//
//  WindowsCountdownBar.swift
//  More
//
//  Created by Luko Gjenero on 18/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class WindowsCountdownBar: LoadableView {

    @IBOutlet private weak var days: WindowsCountdownBarItem!
    @IBOutlet private weak var hours: WindowsCountdownBarItem!
    @IBOutlet private weak var minutes: WindowsCountdownBarItem!
    @IBOutlet private weak var seconds: WindowsCountdownBarItem!
    
    override func setupNib() {
        super.setupNib()
        
        days.type = .days
        hours.type = .hours
        minutes.type = .minutes
        seconds.type = .seconds
    }
    
    func setup(for seconds: TimeInterval) {
        
        let secs = Int(seconds.truncatingRemainder(dividingBy: 60))
        let mins = Int((seconds / 60).truncatingRemainder(dividingBy: 60))
        let hours = Int((seconds / 3600).truncatingRemainder(dividingBy: 24))
        let days = Int(seconds / 86400)
        
        self.seconds.setup(for: "\(secs)")
        self.minutes.setup(for: "\(mins)")
        self.hours.setup(for: "\(hours)")
        self.days.setup(for: "\(days)")
    }

}
