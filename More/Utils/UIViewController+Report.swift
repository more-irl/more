//
//  UIViewController+Report.swift
//  More
//
//  Created by Luko Gjenero on 27/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import MessageUI

extension UIViewController: MFMailComposeViewControllerDelegate {

    func report(_ signal: SignalViewModel, complete: ((_ reported: Bool)->())? = nil) {
        let alert = UIAlertController(title: "Report issue", message: "Do you want to report content displayed in this signal?", preferredStyle: .actionSheet)
        
        let report = UIAlertAction(title: "Report", style: .destructive, handler: { [weak self] _ in
            self?.reportAction(signal, complete: complete)
        })
        
        alert.addAction(report)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func reportAction(_ signal: SignalViewModel, complete: ((_ reported: Bool)->())? = nil) {
        let alert = UIAlertController(title: "Report issue", message: "Please specify the issue with content displayed in this signal.", preferredStyle: .actionSheet)
        
        let inappropriateContent = UIAlertAction(title: "Inappropriate content", style: .destructive, handler: { [weak self] _ in
            self?.sendReport(signal, reason: "Inappropriate content")
            complete?(true)
        })
        
        let spam = UIAlertAction(title: "Spam", style: .default, handler: { [weak self] _ in
            self?.sendReport(signal, reason: "Spam")
            complete?(true)
        })
        
        let other = UIAlertAction(title: "Other", style: .default, handler: { [weak self] _ in
            self?.sendReport(signal, reason: "Other")
            complete?(true)
        })
        
        alert.addAction(inappropriateContent)
        alert.addAction(spam)
        alert.addAction(other)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func sendReport(_ signal: SignalViewModel, reason: String) {
        
        SignalTrackingService.shared.reportedSignal(signalId: signal.id)
        
        guard MFMailComposeViewController.canSendMail() else { return }
        
        let body = """
            I would like to report the following signal:
            Signal Id: \(signal.id)
            Creator: \(signal.creator.fullName) (\(signal.creator.id))
            Reason: \(reason)
            """
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setSubject("Report issue")
        mail.setMessageBody(body, isHTML: false)
        mail.setToRecipients([Emails.support])
        present(mail, animated: true, completion: { })
    }

    func report(_ user: UserViewModel, complete: ((_ reported: Bool)->())? = nil) {
        let alert = UIAlertController(title: "Report issue", message: "Do you want to report content displayed in this profile?", preferredStyle: .actionSheet)
        
        let report = UIAlertAction(title: "Report", style: .destructive, handler: { [weak self] _ in
            self?.reportAction(user, complete: complete)
        })
        
        alert.addAction(report)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func reportAction(_ user: UserViewModel, complete: ((_ reported: Bool)->())? = nil) {
        let alert = UIAlertController(title: "Report issue", message: "Please specify the issue with content displayed in this profile.", preferredStyle: .actionSheet)
        
        let inappropriateContent = UIAlertAction(title: "Inappropriate content", style: .destructive, handler: { [weak self] _ in
            self?.sendReport(user, reason: "Inappropriate content")
            complete?(true)
        })
        
        let spam = UIAlertAction(title: "Spam", style: .default, handler: { [weak self] _ in
            self?.sendReport(user, reason: "Spam")
            complete?(true)
        })
        
        let other = UIAlertAction(title: "Other", style: .default, handler: { [weak self] _ in
            self?.sendReport(user, reason: "Other")
            complete?(true)
        })
        
        alert.addAction(inappropriateContent)
        alert.addAction(spam)
        alert.addAction(other)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func sendReport(_ user: UserViewModel, reason: String, complete: ((_ reported: Bool)->())? = nil) {
        
        guard MFMailComposeViewController.canSendMail() else { return }
        
        let body = """
        I would like to report the following profile:
        Profile Id: \(user.id)
        Name: \(user.fullName)
        Reason: \(reason)
        """
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setSubject("Report issue")
        mail.setMessageBody(body, isHTML: false)
        mail.setToRecipients([Emails.support])
        present(mail, animated: true, completion: { })
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
