//
//  TimeRequestsView.swift
//  More
//
//  Created by Luko Gjenero on 11/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class TimeRequestsView: UICollectionView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    private static let cellIdentifier = "RequestCell"
    
    private var signalId: String = ""
    private var type: SignalType = .boozy
    private var requests: [RequestViewModel] = []
    
    var showBlanks: Bool = true
    
    var requestTap: ((_ request: RequestViewModel)->())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    private func setup() {
        register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Dummy")
        register(Cell.self, forCellWithReuseIdentifier: TimeRequestsView.cellIdentifier)
        dataSource = self
        delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSize = self.itemSize
            if itemSize.width != layout.itemSize.width || itemSize.height != itemSize.height {
                layout.itemSize = itemSize
                collectionViewLayout.invalidateLayout()
                reloadData()
            }
        }
    }
    
    private var itemSize: CGSize {
        return CGSize(width: 93, // floor((frame.width - contentInset.left - contentInset.right) / 3),
                      height: frame.height - contentInset.top - contentInset.bottom)
    }
    
    func setup(for model: SignalViewModel) {
        signalId = model.id
        type = model.type
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(reload(_:)), name: SignalTrackingService.Notifications.SignalRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload(_:)), name: SignalTrackingService.Notifications.SignalRequestExpired, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload(_:)), name: SignalTrackingService.Notifications.SignalMessage, object: nil)
        
        reload()
    }
    
    @objc private func reload(_ notice: Notification) {
        var id: String? = nil
        if let signal = notice.userInfo?["signal"] as? Signal {
            id = signal.id
        } else if let signalId = notice.userInfo?["signalId"] as? String {
            id = signalId
        }
        guard id != nil, id == signalId else { return }
        
        reload()
    }
    
    private func reload() {
        let signal = SignalTrackingService.shared.updatedSignalData(for: signalId)
        let now = Date()
        requests = signal?.requests?
            .filter { $0.expiresAt > now && $0.accepted == nil }
            .map { RequestViewModel(request: $0) } ?? []
        isScrollEnabled = requests.count > 0
        reloadData()
    }
    
    // MARK: - DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if showBlanks && requests.count == 0 {
            return 5
        }
        return requests.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TimeRequestsView.cellIdentifier, for: indexPath) as? Cell {
            if indexPath.item < requests.count {
                cell.setup(for: requests[indexPath.item], type: type)
            } else {
                cell.setupForEmpty()
            }
            return cell
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Dummy", for: indexPath)
    }
    
    // MARK: - delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item < requests.count {
            requestTap?(requests[indexPath.item])
        }
    }
    
    // MARK: - cell
    
    private class Cell: UICollectionViewCell {
        
        private let avatar: TimeAvatarView = {
            let avatar = TimeAvatarView()
            avatar.translatesAutoresizingMaskIntoConstraints = false
            avatar.clipsToBounds = true
            avatar.backgroundColor = .clear
            return avatar
        }()
        
        private let name: UILabel = {
            let label = SpecialLabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: "Avenir-Medium", size: 11)
            label.kern = 0.46
            label.textColor = UIColor(red: 67, green: 74, blue: 81)
            label.backgroundColor = .clear
            return label
        }()
        
        private let badge: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: "DIN-Bold", size: 13)
            label.textColor = .white
            label.backgroundColor = UIColor(red: 91, green: 200, blue: 250)
            label.textAlignment = .center
            label.layer.cornerRadius = 11.5
            label.layer.masksToBounds = true
            return label
        }()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            contentView.addSubview(avatar)
            contentView.addSubview(name)
            contentView.addSubview(badge)
            
            avatar.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            avatar.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -8).isActive = true
            avatar.widthAnchor.constraint(equalTo: avatar.heightAnchor).isActive = true
            avatar.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 24).isActive = true
            avatar.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 10).isActive = true
            let widthConstraint = NSLayoutConstraint(item: avatar, attribute: .width, relatedBy: .equal, toItem: contentView, attribute: .width, multiplier: 1, constant: 0)
            widthConstraint.priority = .defaultLow
            contentView.addConstraint(widthConstraint)
            
            name.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            name.topAnchor.constraint(equalTo: avatar.bottomAnchor, constant: 5).isActive = true
            name.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 4).isActive = true
            
            badge.topAnchor.constraint(equalTo: avatar.topAnchor, constant: -2).isActive = true
            badge.trailingAnchor.constraint(equalTo: avatar.trailingAnchor, constant: 2).isActive = true
            badge.heightAnchor.constraint(equalToConstant: 21).isActive = true
            badge.widthAnchor.constraint(equalToConstant: 21).isActive = true
            contentView.setNeedsLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(for model: RequestViewModel, type: SignalType) {
            avatar.imageUrl = model.sender.avatarUrl
            avatar.type = type
            avatar.setupRunningProgress(start: model.createdAt, end: model.expiresAt)
            name.text = model.sender.name
            badge.backgroundColor = type.color
            
            let unread = model.messages.filter { !$0.isMine() && $0.read == nil }
            if unread.count > 0 {
                badge.isHidden = false
                badge.text = "\(unread.count)"
            } else {
                badge.isHidden = true
            }
        }
        
        func setupForEmpty() {
            avatar.imageUrl = ""
            avatar.setupRunningProgress(start: Date(timeIntervalSince1970: 0), end: Date(timeIntervalSince1970: 1))
            name.text = ""
            badge.isHidden = true
        }
    }

}
