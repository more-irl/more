//
//  RequestViewModel.swift
//  More
//
//  Created by Luko Gjenero on 05/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class RequestViewModel: Codable, Hashable {

    let id: String
    let createdAt: Date
    let expiresAt: Date
    let sender: UserViewModel
    let accepted: Bool
    let messages: [MessageViewModel]
    
    init(request: Request) {
        id = request.id
        createdAt = request.createdAt
        expiresAt = request.expiresAt
        sender = UserViewModel(user: request.sender)
        accepted = request.accepted ?? false
        messages = request.messages?.map { MessageViewModel(message: $0) } ?? []
    }
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: RequestViewModel, rhs: RequestViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}
