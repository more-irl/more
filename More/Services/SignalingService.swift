//
//  SignalingService.swift
//  More
//
//  Created by Luko Gjenero on 31/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase
import MapKit
import GeoFire


struct Math {
    static let mileInMeters: Double = 1609.34
    static let kmInMeters: Double = 1000
}

class SignalingService {
    
    struct Notifications {
        static let SignalsLoaded = NSNotification.Name(rawValue: "com.more.signal.listLoaded")
        static let SignalEnteredRegion = NSNotification.Name(rawValue: "com.more.signal.entered")
        static let SignalExitedRegion = NSNotification.Name(rawValue: "com.more.signal.exited")
        static let ClaimedSignalsChanged = NSNotification.Name(rawValue: "com.more.signal.claimedChanged")
    }
    
    static let shared = SignalingService()
    
    private var geoFireRef: DatabaseReference
    private var geoFire: GeoFire
    private var query: GFCircleQuery?
        
    init() {
        geoFireRef = Database.database().reference().child("signalLocations")
        geoFire = GeoFire(firebaseRef: geoFireRef)
        
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configLoaded), name: ConfigService.Notifications.ConfigLoaded, object: nil)
        toForeground()
        startTrackingClaimables()
    }
    
    @objc private func toForeground() {
        // TODO
    }
    
    @objc private func toBackground() {
        // TODO
    }
    
    // MARK: - tracking around me
    
    func startTrackingAroundMe() {
        guard ProfileService.shared.profile?.getId() != nil else { return }
        
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
        updateTrackingAroundMe()
    }
    
    @objc private func locationUpdated() {
        updateTrackingAroundMe()
        updateClaimables(to: claimables)
    }
    
    @objc private func configLoaded() {
        updateTrackingAroundMe()
    }
    
    private func updateTrackingAroundMe() {
        guard let location = LocationService.shared.currentLocation else { return }
        
        if let query = query {
            if query.center.distance(from: location) > 100 {
                query.center = location
                query.searchCriteriaDidChange()
            }
            return
        }
        
        let radius = ProfileService.shared.profile?.isAdmin == true ? 1000 : ConfigService.shared.signalSearchRadius / 1000.0
        query = geoFire.query(at: location, withRadius: radius)
        query?.observe(.keyEntered, with: { [weak self] (signalId, _) in
            
            Database.database().reference().child("signals").child("active").child(signalId).observeSingleEvent(
                of: .value,
                with: { (snapshot) in
                    let now = Date()
                    if snapshot.exists(),
                        let signal = Signal.fromSnapshot(snapshot),
                        signal.createdAt <= now,
                        signal.expiresAt > now {
                        
                        Analytics.logEvent("SignalFound", parameters: ["signalId": signalId, "text": signal.text])
                        
                        NotificationCenter.default.post(name: Notifications.SignalEnteredRegion, object: self, userInfo: ["signal": signal])
                    }
                },
                withCancel: { _ in  /* nothing */ })
        })
        query?.observe(.keyExited, with: { [weak self] (signalId, _) in
            NotificationCenter.default.post(name: Notifications.SignalExitedRegion, object: self, userInfo: ["signalId": signalId])
        })
        query?.observeReady { [weak self] in
            NotificationCenter.default.post(name: Notifications.SignalsLoaded, object: self, userInfo: nil)
        }
    }
    
    func stopTrackingAroundMe() {
        query?.removeAllObservers()
        query = nil
        NotificationCenter.default.removeObserver(self, name: LocationService.Notifications.LocationUpdate, object: nil)
    }
    
    // MARK: - api
    
    func createSignal(from signal: Signal, log: Bool = true, complete:((_ id: String?, _ errorMsg: String?)->())?) {
        
        guard let _ = ProfileService.shared.profile?.getId(),
            let signalValue = signal.json else {
                complete?(nil, "Not logged in")
                return
        }
        
        var kindPath = "active"
        if signal.kind == .claimable {
            kindPath = "claimable"
        } else if signal.kind == .curated {
            kindPath = "curated"
        }
        
        Database.database().reference().child("signals").child(kindPath).childByAutoId().setValue(signalValue) { (error, reference) in
            if let error = error {
                complete?(nil, error.localizedDescription)
                return
            }
            
            guard let signalId = reference.key else {
                complete?(nil, "Unable to retreive the signal ID")
                return
            }
            
            Analytics.logEvent("SignalCreated", parameters: ["signalId": signalId, "text": signal.text])
            
            SignalingService.shared.geoFire.setLocation(
                CLLocation(latitude: signal.location.latitude ?? 0, longitude: signal.location.longitude ?? 0),
                forKey: signalId)
            
            complete?(signalId, nil)
        }
    }
    
    func updateSignalLocation(signalId: String, location: Location) {
    
       Database.database().reference().child("signals").child("active").child(signalId).child("location").setValue(location.json)
        Database.database().reference().child("signals").child("active").child(signalId).child("creator").child("location").setValue(location.json)
        
        SignalingService.shared.geoFire.setLocation(
            CLLocation(latitude: location.latitude ?? 0, longitude: location.longitude ?? 0),
            forKey: signalId)
    }
    
    func updateSignalRequestLocation(signalId: String, requestId: String, location: Location) {
        
        Database.database().reference().child("signals").child("active").child(signalId)
            .child("requestList").child(requestId).child("sender").child("location").setValue(location.json)
    }
    
    func requestTime(for signalId: String, complete:((_ id: String?, _ errorMsg: String?)->())?) {
        guard let profile = ProfileService.shared.profile, profile.getId() != nil else { return }
        
        let now = Date()
        let expiration = Date(timeIntervalSinceNow: ConfigService.shared.signalRequestExpiration)
        let request = Request(
            id: "---",
            createdAt: now,
            expiresAt: expiration,
            sender: profile.user,
            accepted: nil)
        
        let requestValue = request.json
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").childByAutoId().setValue(requestValue) { (error, reference) in
            if let error = error {
                complete?(nil, error.localizedDescription)
                return
            }
            
            guard let requestId = reference.key else {
                complete?(nil, "Unable to retreive the request ID")
                return
            }
            
            Analytics.logEvent("SignalRequestCreated", parameters: ["signalId": signalId, "requestId": requestId])
            
            complete?(requestId, nil)
        }
    }
    
    func acceptRequest(for signalId: String, request requestId: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        guard ProfileService.shared.profile?.getId() != nil else { return }
                
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("accepted").setValue(true) { (error, reference) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            Analytics.logEvent("SignalRequestAccepted", parameters: ["signalId": signalId, "requestId": requestId])
            
            complete?(true, nil)
        }
    }
    
    func deleteSignal(signalId: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        Database.database().reference().child("signals").child("active").child(signalId).removeValue { (error, _) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            Analytics.logEvent("SignalDeleted", parameters: ["signalId": signalId])
            
            complete?(true, nil)
        }
    }
    
    func cancelRequest(_ requestId: String, in signalId: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        guard let id = ProfileService.shared.profile?.getId() else { return }
        
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("accepted").setValue(false) { (error, reference) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            Analytics.logEvent("SignalRequestCancelled", parameters: ["signalId": signalId, "requestId": requestId])
            
            complete?(true, nil)
        }
        Database.database().reference().child("users").child(id).child("signals").child("active").child(signalId).removeValue()
    }
    
    func sendMessage(_ message: Message, for requestId: String, in signalId: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        let messageValue = message.json
        
        Database.database().reference().child("signals").child("active").child(signalId)
            .child("requestList").child(requestId).child("messageList").child(message.id).setValue(messageValue) { (error, reference) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            Analytics.logEvent("SignalMessageSent", parameters: ["signalId": signalId, "requestId": requestId])
                
            complete?(true, nil)
        }
        
    }
    
    func loadSignal(withId signalId: String, completion: ((_ signal: Signal?, _ errorMsg: String?)->())?) {
        Database.database().reference().child("signals").child("connected").child(signalId).observeSingleEvent(
            of: .value,
            with: { (snapshot) in
                if snapshot.exists(),
                    let signal = Signal.fromSnapshot(snapshot) {
                    completion?(signal, nil)
                } else {
                    completion?(nil, "Signal not found")
                }
            },
            withCancel: { error in
                completion?(nil, error.localizedDescription)
            })
    }
    
    // MARK: - claimable signals
    
    private var claimables: [Signal] = []
    private(set) var claimablesInRange: [Signal] = []
    private weak var claimableTimer: Timer?
    
    func claimSignal(from signal: Signal, completion: ((_ signalId: String?, _ errorMsg: String?)->())?) {
        
        guard let user = ProfileService.shared.profile?.user else { return }
        
        var location = signal.location
        if let myLocation = LocationService.shared.currentLocation {
            location = Location(coordinates: myLocation.coordinate)
        }
        
        let claimed = Signal(
            id: "",
            text: signal.text,
            imageUrls: signal.imageUrls,
            imagePaths: signal.imageUrls,
            type: signal.type,
            createdAt: Date(),
            expiresAt: Date(timeIntervalSinceNow: ConfigService.shared.signalExpiration),
            location: location,
            destination: signal.destination,
            destinationName: signal.destinationName,
            destinationAddress: signal.destinationAddress,
            creator: user)
        
        createSignal(from: claimed, log: false) { (signalId, errorMsg) in
            if let signalId = signalId {
                
                Analytics.logEvent("SignalClaimed", parameters: ["signalId": signal.id, "text": signal.text])
                
                Database.database().reference().child("signals").child("claimable").child(signal.id).child("createdAt").setValue(claimed.expiresAt.timeIntervalSinceReferenceDate)
                Database.database().reference().child("signals").child("claimable").child(signal.id).child("claimedBy").child(user.id).setValue(user.json)

                completion?(signalId, nil)
            } else {
                completion?(nil, errorMsg)
            }
        }
        
    }
    
    func startTrackingClaimables() {
        Database.database().reference().child("signals").child("claimable").observe(
            .value,
            with: { [weak self] (snapshot) in
                if snapshot.exists() {
                    var signals: [Signal] = []
                    for child in snapshot.children {
                        if let child = child as? DataSnapshot,
                            let signal = Signal.fromSnapshot(child) {
                            signals.append(signal)
                        }
                    }
                    self?.updateClaimables(to: signals)
                }
            },
            withCancel: { _ in  /* nothing */ })
        
        claimableTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: { [weak self] (_) in
            self?.updateClaimables(to: self?.claimables ?? [])
        })
    }
    
    func stopTrackingClaimables() {
        Database.database().reference().child("signals").child("claimable").removeAllObservers()
        claimableTimer?.invalidate()
    }
    
    private func updateClaimables(to signals: [Signal]) {
        
        let newInRange = signals.filter { $0.claimableInRange() && $0.claimableActive() }
        let exited = claimablesInRange.filter { !newInRange.contains($0) }
        let entered = newInRange.filter { !claimablesInRange.contains($0) }
        
        let reload = claimables.first(where: { !signals.contains($0) }) != nil ||
            signals.first(where: { !claimables.contains($0) }) != nil
        
        claimables = signals
        claimablesInRange = newInRange
        
        for signal in exited {
            NotificationCenter.default.post(name: Notifications.SignalExitedRegion, object: self, userInfo: ["signalId": signal.id])
        }
        
        for signal in entered {
            NotificationCenter.default.post(name: Notifications.SignalEnteredRegion, object: self, userInfo: ["signal": signal])
        }
        
        if reload {
            NotificationCenter.default.post(name: Notifications.ClaimedSignalsChanged, object: self, userInfo: nil)
        }
    }
    
    func getOwnClaimables() -> [Signal] {
        return claimables.filter { $0.isMine() }
    }
    
    func deleteClaimableSignal(signalId: String, complete:((_ success: Bool, _ errorMsg: String?)->())?) {
        
        Database.database().reference().child("signals").child("claimable").child(signalId).removeValue { (error, _) in
            if let error = error {
                complete?(false, error.localizedDescription)
                return
            }
            
            Analytics.logEvent("SignalClaimableDeleted", parameters: ["signalId": signalId])
            
            complete?(true, nil)
        }
    }
    
    func claimableSignalData(for signalId: String) -> Signal? {
        return claimables.first(where: { $0.id == signalId })
    }
    
}
