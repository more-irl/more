//
//  ProfileUserCell.swift
//  More
//
//  Created by Luko Gjenero on 18/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ProfileUserCell: ProfileBaseCell {

    static let height: CGFloat = 45
    
    @IBOutlet private weak var verified: UIImageView!
    @IBOutlet private weak var nameAndAge: UILabel!
    @IBOutlet private weak var message: UIButton!
    @IBOutlet private weak var rate: StarsView!
    @IBOutlet private weak var numberOfRatings: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        message.roundCorners(corners: .allCorners, radius: 18.5, background: .clear, shadow: .black)
    }
    
    
    // MARK: - base
    
    override func setup(for model: UserViewModel) {
        
        verified.image = UIImage(named: "verified")
        
        let text = NSMutableAttributedString()
        
        var part = NSAttributedString(
            string: "\(model.name)",
            attributes: [NSAttributedStringKey.foregroundColor : nameAndAge.textColor,
                         NSAttributedStringKey.font : nameAndAge.font])
        text.append(part)
        
        if model.age > 0 {
            part = NSAttributedString(
                string: ", ",
                attributes: [NSAttributedStringKey.foregroundColor : nameAndAge.textColor,
                             NSAttributedStringKey.font : nameAndAge.font])
            text.append(part)
            
            part = NSAttributedString(
                string: " \(model.age) ",
                attributes: [NSAttributedStringKey.foregroundColor : UIColor.white,
                             NSAttributedStringKey.font : nameAndAge.font])
            text.append(part)
        }
        
        nameAndAge.attributedText = text
        
        if model.numOfReviews > 0 {
            rate.noStar = UIImage(named: "no_star")
            rate.rate = CGFloat(model.avgReview)
        } else {
            rate.noStar = UIImage(named: "star_outline_2")
            rate.rate = 0
        }
        
        rate.rate = CGFloat(model.avgReview)
        
        numberOfRatings.text = "\(model.numOfReviews)"
    }
    
    override class func size(for model: UserViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: ProfileUserCell.height)
    }
    
    override class func isShowing(for model: UserViewModel) -> Bool {
        return true
    }
}
