//
//  SignalDetailsViewController.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

private let photosCell = String(describing: SignalDetailsPhotosCell.self)
private let quoteCell = String(describing: SignalDetailsQuoteCell.self)
private let userCell = String(describing: SignalDetailsUserCell.self)
private let tagsCell = String(describing: SignalDetailsTagsCell.self)
private let moreCell = String(describing: SignalDetailsMoreCell.self)
private let reviewsCell = String(describing: SignalDetailsReviewsCell.self)
private let reportCell = String(describing: SignalDetailsReportCell.self)
private let mapCell = String(describing: SignalDetailsMapCell.self)

class SignalDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var back: UIButton!
    @IBOutlet private weak var more: UIButton!
    @IBOutlet private weak var headerBackgorund: UIView!
    @IBOutlet private weak var topFade: FadeView!
    
    private(set) var model: SignalViewModel?
    private var rows: [String] = []
    private var heights: [String: CGFloat] = [:]
    
    var backTap: (()->())?
    
    var moreTap: (()->())?
    
    var scrolling: (()->())?
    
    var bottomPadding: CGFloat = 90 {
        didSet {
            updateInsets()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topFade.orientation = .up
        topFade.color = UIColor.black.withAlphaComponent(0.5)
        
        back.layer.transform = CATransform3DMakeRotation(-CGFloat.pi * 0.5, 0, 0, 1)

        tableView.contentInsetAdjustmentBehavior = .never
        tableView.insetsContentViewsToSafeArea = false
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Dummy")
        tableView.register(UINib(nibName: photosCell, bundle: nil), forCellReuseIdentifier: photosCell)
        tableView.register(UINib(nibName: quoteCell, bundle: nil), forCellReuseIdentifier: quoteCell)
        tableView.register(UINib(nibName: userCell, bundle: nil), forCellReuseIdentifier: userCell)
        tableView.register(UINib(nibName: moreCell, bundle: nil), forCellReuseIdentifier: moreCell)
        tableView.register(UINib(nibName: tagsCell, bundle: nil), forCellReuseIdentifier: tagsCell)
        tableView.register(UINib(nibName: reportCell, bundle: nil), forCellReuseIdentifier: reportCell)
        tableView.register(UINib(nibName: reviewsCell, bundle: nil), forCellReuseIdentifier: reviewsCell)
        tableView.register(UINib(nibName: mapCell, bundle: nil), forCellReuseIdentifier: mapCell)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        headerBackgorund.alpha = 0
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        updateInsets()
    }
    
    private func updateInsets() {
        tableView.contentInset = UIEdgeInsetsMake(0, 0, view.safeAreaInsets.bottom + bottomPadding, 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        clearHeightCache()
        tableView.reloadData()
    }

    @IBAction func backTouch(_ sender: Any) {
        backTap?()
    }
    
    @IBAction func moreTouch(_ sender: Any) {
        //guard let model = model else { return }
        //report(model)
        moreTap?()
    }
    
    func setup(for model: SignalViewModel) {
        self.model = model
        
        if model.creator.user.isMe() {
            more.isHidden = true
        }
        
        rows = []
        
        if SignalDetailsPhotosCell.isShowing(for: model) {
            rows.append(photosCell)
        }
        if SignalDetailsQuoteCell.isShowing(for: model) {
            rows.append(quoteCell)
        }
        if SignalDetailsUserCell.isShowing(for: model) {
            rows.append(userCell)
        }
        if SignalDetailsTagsCell.isShowing(for: model) {
            rows.append(tagsCell)
        }
        if SignalDetailsMoreCell.isShowing(for: model, type: .presence) {
            rows.append(moreCell)
        }
        if SignalDetailsReviewsCell.isShowing(for: model) {
            rows.append(reviewsCell)
        }
        if SignalDetailsMoreCell.isShowing(for: model, type: .reviews) {
            rows.append(moreCell)
        }
        if SignalDetailsReportCell.isShowing(for: model) {
            rows.append(reportCell)
        }
        if SignalDetailsMapCell.isShowing(for: model) {
            rows.append(mapCell)
        }
        
        tableView.reloadData()
    }
    
    func scrollToTop(animated: Bool) {
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: animated)
    }
    
    // MARK: - tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = rows[indexPath.row]
        if let model = model,
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SignalDetailsBaseCell {
            
            if let cell = cell as? SignalDetailsPhotosCell {
                cell.scrolling = { [weak self] in
                    self?.scrolling?()
                }
            }
            
            if let cell = cell as? SignalDetailsMoreCell, identifier == moreCell {
                if indexPath.row == rows.firstIndex(of: moreCell) {
                    cell.type = SignalDetailsMoreCell.MoreType.presence.rawValue
                } else {
                    cell.type = SignalDetailsMoreCell.MoreType.reviews.rawValue
                }
            }
            
            if let cell = cell as? SignalDetailsReviewsCell {
                cell.width = tableView.frame.width
                cell.readMore = { [weak self] in
                    self?.showReviews()
                }
            }
            
            cell.setup(for: model)
            return cell
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "Dummy", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return height(for: rows[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        let identifier = rows[indexPath.row]
        
        if identifier == userCell {
            showProfile()
        }
        
        if identifier == moreCell {
            if indexPath.row == rows.firstIndex(of: moreCell) {
                showProfile()
            } else {
                showReviews()
            }
        }
        
        if identifier == reviewsCell {
            showReviews()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrolling?()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerBreak = height(for: photosCell) + height(for: quoteCell) - 50 - view.safeAreaInsets.top
        headerBackgorund.alpha = min(1, max(0, (scrollView.contentOffset.y - headerBreak) / 50))
        
        trackDismissInteraction(offset: -scrollView.contentOffset.y)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        finishDismissInteraction(offset: -scrollView.contentOffset.y)
    }
    
    // MARK: - height cache
    
    private func clearHeightCache() {
        heights = [:]
    }
    
    private func height(for identifier: String) -> CGFloat {
        
        guard model != nil else { return 0 }
        
        if let height = heights[identifier] {
            return height
        }
        
        let height = calculateHeight(for: identifier)
        heights[identifier] = height
        return height
    }
    
    private func calculateHeight(for identifier: String) -> CGFloat {
        
        guard let model = model else { return 0 }
        
        switch identifier {
        case photosCell:
            return SignalDetailsPhotosCell.size(for: model, in: tableView.frame.size).height
        case quoteCell:
            return SignalDetailsQuoteCell.size(for: model, in: tableView.frame.size).height
        case userCell:
            return SignalDetailsUserCell.size(for: model, in: tableView.frame.size).height
        case tagsCell:
            return SignalDetailsTagsCell.size(for: model, in: tableView.frame.size).height
        case moreCell:
            return SignalDetailsMoreCell.size(for: model, in: tableView.frame.size).height
        case reviewsCell:
            return SignalDetailsReviewsCell.size(for: model, in: tableView.frame.size).height
        case reportCell:
            return SignalDetailsReportCell.size(for: model, in: tableView.frame.size).height
        case mapCell:
            return SignalDetailsMapCell.size(for: model, in: tableView.frame.size).height
        default:
            return 0
        }
    }
    
    // MARK: - details views
    
    private func showProfile() {
        guard let model = model else { return }
        
        navigationController?.showLoading()
        ProfileService.shared.loadProfile(withId: model.creator.id) { [weak self] (user, errorMsg) in
            self?.navigationController?.hideLoading()
            if let user = user {
                let vc = ProfileViewController()
                vc.backTap = {
                    self?.navigationController?.popViewController(animated: true)
                }
                vc.moreTap = { [weak self] in
                    self?.moreTap?()
                }
                _ = vc.view
                vc.setup(for: UserViewModel(profile: user))
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func showReviews() {
        guard let model = model else { return }
        
        navigationController?.showLoading()
        ProfileService.shared.loadProfile(withId: model.creator.id) { [weak self] (user, errorMsg) in
            self?.navigationController?.hideLoading()
            if let user = user {
                let vc = ReviewsViewController()
                vc.backTap = {
                self?.navigationController?.popViewController(animated: true)
                }
                _ = vc.view
                vc.setup(for: UserViewModel(profile: user))
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    // MARK: - pull down to dismiss
    
    private var finishingDismiss = false
    
    private var interactor: Interactor? {
        if let root = navigationController?.parent?.navigationController,
            let transitionDelegate = root.transitioningDelegate as? SignalDetailsTransitioningDelegate {
            return transitionDelegate.interactor
        }
        return nil
    }
    
    private func trackDismissInteraction(offset: CGFloat) {
        guard let interactor = interactor else { return }
        
        if finishingDismiss {
            if offset <= 0 {
                finishingDismiss = false
            }
            return
        }
        
        if offset > 0 {
            if interactor.hasStarted {
                interactor.update(min(offset, 150) / 151)
            } else {
                interactor.hasStarted = true
                dismiss(animated: true, completion: nil)
            }
        } else {
            if interactor.hasStarted {
                interactor.hasStarted = false
                interactor.cancel()
            }
        }
    }
    
    private func finishDismissInteraction(offset: CGFloat) {
        guard let interactor = interactor else { return }
        if interactor.hasStarted {
            interactor.hasStarted = false
            if offset >= 80 {
                interactor.finish()
            } else {
                interactor.cancel()
            }
            finishingDismiss = true
        }
    }
}

extension SignalDetailsViewController: SignalDetailsView {
    
    func photosFrame() -> CGRect {
        var rect = tableView.bounds
        if let model = model {
            rect = CGRect(origin: .zero, size: SignalDetailsPhotosCell.size(for: model, in: tableView.bounds.size))
        }
        rect.origin.y -= tableView.contentOffset.y
        
        return rect
    }
    
    func prepareToPresentSignalDetails(size: CGSize) -> CGRect {
        guard let model = model else { return CGRect(origin: .zero, size: size) }
        return CGRect(origin: .zero, size: SignalDetailsPhotosCell.size(for: model, in: size))
    }
    
    func prepareToDismissSignalDetails(duration: TimeInterval, complete: @escaping (_ frame: CGRect)->()) {
        
        var rect = tableView.bounds
        if let model = model {
            rect = CGRect(origin: .zero, size: SignalDetailsPhotosCell.size(for: model, in: tableView.bounds.size))
        }
        
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            complete(rect)
        }
        
    }
}



