//
//  SignalDetailsMoreCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class SignalDetailsMoreCell: SignalDetailsBaseCell {
    
    static let height: CGFloat = 45
    
    @objc enum MoreType: Int {
        case presence, reviews
    }
    
    @IBOutlet private weak var label: UILabel!
    
    @IBInspectable var type: Int = MoreType.presence.rawValue {
        didSet {
            guard let type = MoreType(rawValue: type) else { return }
            switch type {
            case .presence:
                setupForPresence()
            case .reviews:
                setupForReviews()
            }
        }
    }
    
    private func setupForPresence() {
        label.text = "Full Profile"
    }
    
    private func setupForReviews() {
        label.text = "All Reviews"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - base

    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: SignalDetailsMoreCell.height)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return true
    }
    
    class func isShowing(for model: SignalViewModel, type: MoreType) -> Bool {
        guard model.kind == .standard else { return false }
        switch type {
        case .presence:
            return true
        case .reviews:
            return model.creator.numOfReviews > 0
        }
    }
}
