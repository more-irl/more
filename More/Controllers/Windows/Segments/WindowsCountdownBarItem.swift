//
//  WindowsCountdownBarItem.swift
//  More
//
//  Created by Luko Gjenero on 18/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class WindowsCountdownBarItem: LoadableView {

    enum ItemType: String {
        case days, hours, minutes, seconds
    }
    
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var label: UILabel!
    
    var type: ItemType = .days {
        didSet {
            label.text = type.rawValue.uppercased()
        }
    }
    
    func setup(for value: String) {
        self.value.text = value
    }

}
