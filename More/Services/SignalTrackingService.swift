//
//  SignalTrackingService.swift
//  More
//
//  Created by Luko Gjenero on 01/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

class SignalTrackingService {

    struct Notifications {
        static let SignalLoaded = NSNotification.Name(rawValue: "com.more.signal.loaded")
        static let SignalExpirationChanged = NSNotification.Name(rawValue: "com.more.signal.expirationChanged")
        static let SignalExpired = NSNotification.Name(rawValue: "com.more.signal.expired")
        static let SignalRequest = NSNotification.Name(rawValue: "com.more.signal.request")
        static let SignalRequestExpired = NSNotification.Name(rawValue: "com.more.signal.requestExpired")
        static let SignalResponse = NSNotification.Name(rawValue: "com.more.signal.response")
        static let SignalMessage = NSNotification.Name(rawValue: "com.more.signal.message")
    }
    
    enum TrackingType: Int, Codable {
        case expiration, requests, response, messages
    }
    
    static let shared = SignalTrackingService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(toForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdate), name: LocationService.Notifications.LocationUpdate, object: nil)
        
        load()
        let trackedIds = trackedSignalIds
        trackedSignalIds = [:]
        for (signalId, _) in trackedIds {
            trackActive(signalId: signalId)
        }
    }
    
    // MARK: - data
    
    struct TrackedSignal {
        var signal: Signal
        var expiresAt: Date
        var timer: Timer
        var requestTimers: [String: Timer] = [:]
    }
    
    private let trackedSignalsKey = "com.more.signal.tracked"
    private let reportedSignalsKey = "com.more.signal.reported"
    
    private var trackedSignalIds: [String: UInt] = [:]
    private var trackedSignals: [String: TrackedSignal] = [:]
    private var reportedSignals: [String] = []
    
    @objc private func toForeground() {
        // TODO: --
    }
    
    @objc private func toBackground() {
        // TODO: --
    }
    
    @objc private func locationUpdate() {
        
        guard let coordinates = LocationService.shared.currentLocation?.coordinate else { return }
        
        let now = Date()
        let location = Location(coordinates: coordinates)
        for signal in getOwnActiveSignals() {
            guard signal.kind == .standard && signal.finalExpiration() > now else { continue }
            
            if signal.isMine() {
                SignalingService.shared.updateSignalLocation(signalId: signal.id, location: location)
            } else if let myRequest = signal.myRequest() {
                SignalingService.shared.updateSignalRequestLocation(signalId: signal.id, requestId: myRequest.id, location: location)
            }
        }
    }
    
    private func load() {
        if let data = UserDefaults.standard.value(forKey:trackedSignalsKey) as? Data,
            let trackedSignals = try? PropertyListDecoder().decode([String: UInt].self, from: data) {
            self.trackedSignalIds = trackedSignals
        }
        if let data = UserDefaults.standard.value(forKey:reportedSignalsKey) as? Data,
            let reportedSignals = try? PropertyListDecoder().decode([String].self, from: data) {
            self.reportedSignals = reportedSignals
        }
    }
    
    private func save() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(trackedSignalIds), forKey:trackedSignalsKey)
        UserDefaults.standard.set(try? PropertyListEncoder().encode(reportedSignals), forKey:reportedSignalsKey)
    }
    
    // MARK: - reporting
    
    func reportedSignal(signalId: String) {
        if !reportedSignals.contains(signalId) {
            reportedSignals.append(signalId)
        }
        signalExpired(signalId)
        
        guard let id = ProfileService.shared.profile?.getId() else { return }
        
        Database.database().reference().child("signals").child("active").child(signalId).child("reported").child(id).setValue(true)
    }
    
    // MARK: - api
    
    func trackActive(signalId: String) {
        
        if let trackedSignal = trackedSignals[signalId] {
            NotificationCenter.default.post(name: Notifications.SignalLoaded, object: self, userInfo: ["signal": trackedSignal.signal])
        }
        
        guard trackedSignalIds[signalId] == nil else { return }
        guard !reportedSignals.contains(signalId) else { return }
        
        trackedSignalIds[signalId] =
        Database.database().reference().child("signals").child("active").child(signalId).observe(
            .value,
            with: { [weak self] (snapshot) in
                if snapshot.exists() {
                    let signal = Signal.fromSnapshot(snapshot)
                    self?.signalChanged(signal)
                } else {
                    self?.signalExpired(signalId, cancelled: true)
                }
            },
            withCancel: { [weak self] (_) in
                self?.removeTracking(signalId)
            })
        save()
    }
    
    func removeTracking(_ signalId: String) {
        trackedSignals[signalId]?.timer.invalidate()
        if let handle = trackedSignalIds[signalId] {
            Database.database().reference().child("signals").child("active").child(signalId).removeObserver(withHandle: handle)
        }
        trackedSignalIds.removeValue(forKey: signalId)
        trackedSignals.removeValue(forKey: signalId)
        save()
    }

    private func signalChanged(_ signal: Signal?) {
        guard let signal = signal else { return }
        
        guard trackedSignalIds[signal.id] != nil else {
            removeTracking(signal.id)
            return
        }
        
        guard !signal.allExpired() else {
            signalExpired(signal.id)
            return
        }
        
        if var trackedSignal = trackedSignals[signal.id] {
            
            let cachedSignal = trackedSignal.signal
            
            trackedSignal.signal = signal
            trackedSignals[signal.id] = trackedSignal
            
            if signal.isMine() {
                checkRequests(cachedSignal)
            } else {
                checkResponses(cachedSignal)
            }
            
            if trackedSignal.expiresAt != signal.finalExpiration() {
                trackedSignal.timer.invalidate()
                trackedSignal.timer = trackSignalExpiration(signal)
                trackedSignal.expiresAt = signal.finalExpiration()
                NotificationCenter.default.post(name: Notifications.SignalExpirationChanged, object: self, userInfo: ["signal": signal])
            }
        } else {
            
            let now = Date()
            var timers: [String: Timer] = [:]
            if signal.isMine(), let requests = signal.requests {
                for request in requests {
                    if request.expiresAt > now {
                        let delay = request.expiresAt.timeIntervalSinceNow
                        timers[request.id] = Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { [weak self] (_) in
                            self?.signalRequestExpired(signalId: signal.id, requestId: request.id)
                        }
                    }
                }
            }
            
            trackedSignals[signal.id] = TrackedSignal(signal: signal, expiresAt: signal.finalExpiration(), timer: trackSignalExpiration(signal), requestTimers: timers)
            NotificationCenter.default.post(name: Notifications.SignalLoaded, object: self, userInfo: ["signal": signal])
        }
    }
    
    private func signalExpired(_ signalId: String, cancelled: Bool = false) {
        removeTracking(signalId)
        NotificationCenter.default.post(name: Notifications.SignalExpired, object: self, userInfo: ["signalId": signalId, "cancelled": cancelled])
    }
    
    private func signalRequestExpired(signalId: String, requestId: String, cancelled: Bool = false) {
        trackedSignals[signalId]?.requestTimers[requestId]?.invalidate()
        trackedSignals[signalId]?.requestTimers[requestId] = nil
        NotificationCenter.default.post(name: Notifications.SignalRequestExpired, object: self, userInfo: ["signalId": signalId, "requestId": requestId, "cancelled": cancelled])
    }
    
    private func checkRequests(_ cachedSignal: Signal) {
        guard let signal = trackedSignals[cachedSignal.id]?.signal else { return }
        
        let now = Date()
        let oldRequests = cachedSignal.requests ?? []
        var newRequests: [Request] = []
        var cancelledRequests: [Request] = []
        for new in signal.requests ?? [] {
            if let idx = oldRequests.firstIndex(of: new) {
                let old = oldRequests[idx]
                if old.accepted != new.accepted {
                    cancelledRequests.append(new)
                }
            } else {
                newRequests.append(new)
            }
        }
        
        for request in newRequests {
            guard request.expiresAt > now else { continue }
            NotificationCenter.default.post(name: Notifications.SignalRequest, object: self, userInfo: ["signal": signal, "request": request])
            let delay = request.expiresAt.timeIntervalSinceNow
            trackedSignals[signal.id]?.requestTimers[request.id] =
                Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { [weak self] (_) in
                    self?.signalRequestExpired(signalId: signal.id, requestId: request.id)
                }
        }
        
        for request in cancelledRequests {
            guard request.accepted == false else { continue }
            signalRequestExpired(signalId: signal.id, requestId: request.id, cancelled: true)
        }
        
        checkRequestMessages(signal: signal, old: cachedSignal.requests ?? [], new: signal.requests ?? [])
    }
    
    private func checkResponses(_ cachedSignal: Signal) {
        guard let signal = trackedSignals[cachedSignal.id]?.signal else { return }
        
        if let myOldRequest = cachedSignal.myRequest() {
            if let myNewRequest = signal.myRequest() {
                
                if myOldRequest.accepted == nil && myNewRequest.accepted == false {
                    NotificationCenter.default.post(name: Notifications.SignalResponse, object: self, userInfo: ["signal": signal, "request": myNewRequest])
                    removeTracking(signal.id)
                }
                checkResponseMessages(signal: signal, old: myOldRequest, new: myNewRequest)
            } else {
                let rejectedRequest = Request(id: myOldRequest.id, createdAt: myOldRequest.createdAt, expiresAt: myOldRequest.expiresAt, sender: myOldRequest.sender, accepted: false)
                NotificationCenter.default.post(name: Notifications.SignalResponse, object: self, userInfo: ["signal": signal, "request": rejectedRequest])
                removeTracking(signal.id)
            }
        } else if let myNewRequest = signal.myRequest() {
            NotificationCenter.default.post(name: Notifications.SignalRequest, object: self, userInfo: ["signal": signal, "request": myNewRequest])
        }
    }
    
    private func checkResponseMessages(signal: Signal, old: Request, new: Request) {
        
        for msg in new.messages ?? [] {
            let oldMsg = old.messages?.first(where: { $0.id == msg.id })
            if msg.isMine() {
                if let oldMsg = oldMsg, (oldMsg.readAt == nil && msg.readAt != nil) || (oldMsg.deliveredAt == nil && msg.deliveredAt != nil) {
                    NotificationCenter.default.post(name: Notifications.SignalMessage, object: self, userInfo: ["signal": signal, "request": new, "message": msg])
                }
            } else {
                if oldMsg == nil || (oldMsg?.readAt == nil && msg.readAt != nil) || (oldMsg?.deliveredAt == nil && msg.deliveredAt != nil) {
                    NotificationCenter.default.post(name: Notifications.SignalMessage, object: self,  userInfo: ["signal": signal, "request": new, "message": msg])
                }
                if msg.deliveredAt == nil && msg.readAt == nil {
                    setMesageDelivered(signalId: signal.id, requestId: new.id, messageId: msg.id)
                }
            }
        }
    }
    
    func setMesageDelivered(signalId: String, requestId: String, messageId: String) {
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("messageList").child(messageId).child("deliveredAt").setValue(Date().timeIntervalSinceReferenceDate)
    }
    
    func setMesageRead(signalId: String, requestId: String, messageId: String) {
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("messageList").child(messageId).child("readAt").setValue(Date().timeIntervalSinceReferenceDate)
    }
    
    private func checkRequestMessages(signal: Signal, old: [Request], new: [Request]) {
        
        for oldRequest in old {
            if let newRequest = new.first(where: { $0.id == oldRequest.id }) {
                checkResponseMessages(signal: signal, old: oldRequest, new: newRequest)
            }
        }
    }
    
    func setCreatorIsTyping(signalId: String, requestId: String, isTyping: Bool) {
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("creatorIsTyping").setValue(isTyping)
    }
    
    func setRequesterIsTyping(signalId: String, requestId: String, isTyping: Bool) {
        Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("requesterIsTyping").setValue(isTyping)
    }
    
    func getCreatorIsTypingReference(signalId: String, requestId: String) -> DatabaseReference {
        return Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("creatorIsTyping")
    }
    
    func getRequesterIsTypingReference(signalId: String, requestId: String) -> DatabaseReference {
        return Database.database().reference().child("signals").child("active").child(signalId).child("requestList").child(requestId).child("requesterIsTyping")
    }
    
    func removeTypingObservers(signalId: String, requestId: String) {
        getCreatorIsTypingReference(signalId: signalId, requestId: requestId).removeAllObservers()
        getRequesterIsTypingReference(signalId: signalId, requestId: requestId).removeAllObservers()
    }
    
    private func trackSignalExpiration(_ signal: Signal) -> Timer {
        let delay = signal.finalExpiration().timeIntervalSinceNow
        return Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { [weak self] (_) in
            self?.signalExpired(signal.id)
        }
    }
    
    func updatedSignalData(for signalId: String) -> Signal? {
        return trackedSignals[signalId]?.signal
    }
    
    func getOwnActiveSignals() -> [Signal] {
        var signals: [Signal] = []
        for trackedSignal in trackedSignals.values {
            if trackedSignal.signal.isMine() || (trackedSignal.signal.haveRequested() && !trackedSignal.signal.wasRejected()) {
                signals.append(trackedSignal.signal)
            }
        }
        return signals
    }
}
