//
//  UIViewController+Request.swift
//  More
//
//  Created by Luko Gjenero on 01/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func requested(for signal: SignalViewModel) {
        
        LocationService.shared.requestAlways()
        
        let vc = RequestSentOverlayViewController()
        _ = vc.view
        vc.doneTap = {
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }
        vc.setupRequest(for: signal)
        
        guard let root = self as? UITabBarController ?? tabBarController else { return }
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        root.view.addSubview(vc.view)
        vc.view.leadingAnchor.constraint(equalTo: root.view.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: root.view.trailingAnchor).isActive = true
        vc.view.topAnchor.constraint(equalTo: root.view.topAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: root.view.bottomAnchor).isActive = true
        vc.viewDidAppear(false)
    }
    
    func cannotRequest() {
        
        let vc = CannotRequestViewController()
        _ = vc.view
        vc.doneTap = {
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }
        
        let root = tabBarController ?? navigationController ?? self
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        root.view.addSubview(vc.view)
        vc.view.leadingAnchor.constraint(equalTo: root.view.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: root.view.trailingAnchor).isActive = true
        vc.view.topAnchor.constraint(equalTo: root.view.topAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: root.view.bottomAnchor).isActive = true
        vc.viewDidAppear(false)
        
    }
    
}
