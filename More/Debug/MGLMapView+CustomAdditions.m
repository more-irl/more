//
//  MGLMapView+CustomAdditions.c
//  More
//
//  Created by Luko Gjenero on 21/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

#import "MGLMapView+CustomAdditions.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"
// Supressing compiler warning until https://github.com/mapbox/mapbox-gl-native/issues/6867 is implemented
@implementation MGLMapView (CustomAdditions)
#pragma clang diagnostic pop

@dynamic locationManager;

@end
