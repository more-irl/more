//
//  QuickBloxService.swift
//  More
//
//  Created by Luko Gjenero on 08/01/2019.
//  Copyright © 2019 More Technologies. All rights reserved.
//

import Quickblox
import QuickbloxWebRTC

private let kQBRingThickness: CGFloat = 1
private let kQBAnswerTimeInterval: TimeInterval = 60
private let kQBDialingTimeInterval: TimeInterval = 5

class QuickBloxService {

    static private let applicationID: UInt = 75391
    static private let authKey = "DVkbWAv3EbCNWxm"
    static private let authSecret  = "UujEBOSsRqqbPq9"
    static private let accountKey = "pKbxYttsFGek36kVv_q3"
    
    static let shared = QuickBloxService()
    
    init() { }
    
    func initialize(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        
        QBSettings.applicationID = QuickBloxService.applicationID
        QBSettings.authKey = QuickBloxService.authKey
        QBSettings.authSecret  = QuickBloxService.authSecret
        QBSettings.accountKey = QuickBloxService.accountKey
        
        QBRTCConfig.setAnswerTimeInterval(kQBAnswerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(kQBDialingTimeInterval)
        QBRTCConfig.setStatsReportTimeInterval(1)
        
        QBRTCClient.initializeRTC()

    }
    
}
