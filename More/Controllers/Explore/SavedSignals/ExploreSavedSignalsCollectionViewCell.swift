//
//  ExploreSavedSignalsCollectionViewCell.swift
//  More
//
//  Created by Luko Gjenero on 11/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class ExploreSavedSignalsCollectionViewCell: UICollectionViewCell {

    private let contentViewController = SignalDetailsNavigationController()
    
    var tapped: (()->())? {
        get {
            return contentViewController.tapped
        }
        set {
            contentViewController.tapped = newValue
        }
    }
    
    var back: (()->())? {
        get {
            return contentViewController.back
        }
        set {
            contentViewController.back = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(contentViewController.view)
        contentViewController.view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentViewController.view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentViewController.view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentViewController.view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    func setup(for model: SignalViewModel, parent: UIViewController) {
        contentViewController.setup(for: model)
        contentViewController.willMove(toParentViewController: parent)
        parent.addChildViewController(contentViewController)
        contentViewController.didMove(toParentViewController: parent)
    }
    
    override func prepareForReuse() {
        contentViewController.willMove(toParentViewController: nil)
        contentViewController.removeFromParentViewController()
        contentViewController.didMove(toParentViewController: nil)
    }
    
    /*
    func setCornerRadius(_ radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    */
}
