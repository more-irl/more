//
//  ShortSignal.swift
//  More
//
//  Created by Luko Gjenero on 10/11/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import Firebase

struct ShortSignal: MoreDataObject {
    
    let id: String
    let text: String?
    let type: SignalType
    let expiresAt: Date
    let creator: ShortUser
    let imageUrls: [String]?
    
    init(id: String,
         text: String? = nil,
         type: SignalType,
         expiresAt: Date,
         creator: ShortUser,
         imageUrls: [String]? = nil) {
        self.id = id
        self.text = text
        self.type = type
        self.expiresAt = expiresAt
        self.creator = creator
        self.imageUrls = imageUrls
    }
    
    init(signal: Signal) {
        self.id = signal.id
        self.text = signal.text
        self.type = signal.type
        self.expiresAt = signal.expiresAt
        self.creator = signal.creator.short()
        self.imageUrls = signal.imageUrls
    }
    
    init() {
        self.init(id: "", text: nil, type: .adventurous, expiresAt: Date(), creator: ShortUser())
    }
    
    func isMine() -> Bool {
        return creator.isMe()
    }
    
    // Hashable
    
    var hashValue: Int {
        return id.hashValue + id.hashValue
    }
    
    static func == (lhs: ShortSignal, rhs: ShortSignal) -> Bool {
        return lhs.id == rhs.id
    }
    
    // Data protocol
    
    var json: [String: Any]? {
        get {
            if let jsonData = try? JSONEncoder().encode(self),
                var json = try? JSONSerialization.jsonObject(with: jsonData) as? [String: Any] {
                json?.removeValue(forKey: "id")
                return json
            }
            return nil
        }
    }
    
    static func fromJson(_ json: [String : Any]) -> ShortSignal? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []),
            let shortSignal = try? JSONDecoder().decode(ShortSignal.self, from: jsonData) {
            return shortSignal
        }
        return nil
    }
    
    static func fromSnapshot(_ snapshot: DataSnapshot) -> ShortSignal? {
        if var json = snapshot.value as? [String: Any] {
            json["id"] = snapshot.key
            return ShortSignal.fromJson(json)
        }
        return nil
    }
}
