//
//  ExploreSectionController.swift
//  More
//
//  Created by Luko Gjenero on 31/12/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import IGListKit

private let cellIdentifier = "ExploreCollectionViewCell"

class ExploreSectionController: ListSectionController {

    
    private let topBarScale: CGFloat
    private let size: CGSize
    private var model: SignalViewModel?
    private var loading: Bool = false
    
    var selected: ((_ model: SignalViewModel)->())?
    
    init(topBarScale: CGFloat, size: CGSize) {
        self.topBarScale = topBarScale
        self.size = size
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return size
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
        if let cell = collectionContext?.dequeueReusableCell(withNibName: "ExploreCollectionViewCell", bundle: nil, for: self, at: index) as? ExploreCollectionViewCell {
            if loading {
                cell.setupEmpty()
            }
            if let model = model {
                cell.setup(for: model, scale: topBarScale)
            }
            return cell
        }
        
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, withReuseIdentifier: "Dummy", for: self, at: index)
    }
    
    override func didUpdate(to object: Any) {
        model = object as? SignalViewModel
        loading = object as? String == "loading"
    }
    
    override func didSelectItem(at index: Int) {
        guard let model = model else { return }
        selected?(model)
    }
    
    // MARK: - manage signals
    
    
    
}

