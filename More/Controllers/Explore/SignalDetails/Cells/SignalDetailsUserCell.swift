//
//  SignalDetailsUserCell.swift
//  More
//
//  Created by Luko Gjenero on 15/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

class SignalDetailsUserCell: SignalDetailsBaseCell {
    
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var name: UIButton!
    @IBOutlet private weak var tagline: UILabel!
    @IBOutlet private weak var quote: UILabel!
    @IBOutlet private weak var author: UILabel!
    
    // MARK: - base
    
    override func setup(for model: SignalViewModel) {
        
        if let url = URL(string: model.creator.avatarUrl) {
            avatar.sd_progressive_setImage(with: url, placeholderImage: UIImage.profileThumbPlaceholder(), options: .highPriority)
        } else {
            avatar.sd_cancelCurrentImageLoad_progressive()
            avatar.image = nil
        }
        
        name.setTitle("Meet \(model.creator.name)", for: .normal)
        
        
        quote.isHidden = model.quote.count == 0
        tagline.isHidden = model.quote.count == 0
        tagline.text = model.creator.quote
        author.isHidden = true
        
        /*
        if model.quote.count > 0 {
            quote.isHidden = false
            tagline.isHidden = false
            tagline.text = model.creator.quote
            author.isHidden = model.creator.quoteAuthor.count == 0
            author.text = model.creator.quoteAuthor
        } else {
            quote.isHidden = true
            tagline.isHidden = true
            author.isHidden = true
        }
        */
    }
    
    override class func size(for model: SignalViewModel, in size: CGSize) -> CGSize {
        
        if model.creator.quote.count > 0 {
            let font = UIFont(name: "Avenir-Heavy", size: 13) ?? UIFont.systemFont(ofSize: 13)
            let quoteHeight = model.creator.quote.height(withConstrainedWidth: size.width - 85, font: font)
            
            var authorHeight: CGFloat = 0
            
            /*
            if model.creator.quoteAuthor.count > 0 {
                let font = UIFont(name: "Gotham-Medium", size: 9) ?? UIFont.systemFont(ofSize: 9)
                authorHeight = 8 + model.creator.quoteAuthor.height(withConstrainedWidth: size.width - 85, font: font)
            }
            */
            
            return CGSize(width: size.width, height: 185 + quoteHeight + authorHeight)
        }
        
        return CGSize(width: size.width, height: 160)
    }
    
    override class func isShowing(for model: SignalViewModel) -> Bool {
        return model.kind == .standard
    }
    
}
