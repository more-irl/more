//
//  ExploreCollectionViewCellContent.swift
//  More
//
//  Created by Luko Gjenero on 10/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit
import SDWebImage

@IBDesignable
class ExploreCollectionViewCellContent: LoadableView {

    @IBOutlet private weak var topBar: SignalTopBar!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet private weak var typeLabel: UILabel!
    @IBOutlet private weak var quoteLabel: UILabel!
    @IBOutlet private weak var timerBackground: UIView!
    @IBOutlet private weak var countdown: CountdownLabel!
    @IBOutlet private weak var claimRibbon: ExploreClaimRibbon!
    
    override func setupNib() {
        super.setupNib()
        
        self.layer.cornerRadius = 16
        self.layer.masksToBounds = true
        
        countdown.setDateFormat("mm:ss")
        timerBackground.roundCorners(corners: .allCorners, radius: 45)
    }
    
    func setup(for model: SignalViewModel, scale: CGFloat) {
        
        if model.kind == .claimable {
            topBar.setupForMore(title: "Claim This Signal")
            claimRibbon.isHidden = false
            claimRibbon.type = .longClaim
            countdown.reset()
        } else if model.kind == .curated {
            topBar.setupForMore(title: "Claim This Signal")
            claimRibbon.isHidden = false
            claimRibbon.type = .longCurated
            countdown.reset()
        } else {
            topBar.setup(for: model)
            claimRibbon.isHidden = true
            countdown.countdown(to: model.expiresAt)
        }
        
        image.sd_progressive_setImage(with: URL(string: model.imageUrls.first ?? ""), placeholderImage: UIImage.signalPlaceholder(), options: SDWebImageOptions.highPriority)
        typeLabel.textColor = model.type.color
        typeLabel.text = model.type.rawValue.uppercased()
        quoteLabel.text = model.quote
        
        
        topBar.layer.transform = CATransform3DMakeScale(scale, scale, 0)
    }
    
    func setupEmpty() {
        topBar.setupEmpty()
        image.sd_cancelCurrentImageLoad_progressive()
        image.image = nil
        typeLabel.text = ""
        quoteLabel.text = ""
        countdown.reset()
    }
    
}
