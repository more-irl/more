//
//  MessagingOverlayViewController.swift
//  More
//
//  Created by Luko Gjenero on 24/10/2018.
//  Copyright © 2018 More Technologies. All rights reserved.
//

import UIKit

class RequestMessagingOverlayViewController: UIViewController {

    @IBOutlet private weak var requestBar: SignalRequestBar!
    @IBOutlet private weak var topBar: SignalRequestTopBar!
    @IBOutlet private weak var messagingView: MessagingView!
    @IBOutlet private weak var bottomPadding: NSLayoutConstraint!
    @IBOutlet weak var messagingViewHeight: NSLayoutConstraint!
    
    private var emptyView = RequestEmptyView()
    
    private var presenting: Bool = false
    private var model: SignalViewModel? = nil
    
    private var messagingShown = false
    private var message: Message? = nil
    
    var requested: (()->())?
    var removed: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        requestBar.isHidden = false
        topBar.isHidden = true
        messagingView.isHidden = true
        
        requestBar.requestTap = { [weak self] in
            if self?.model?.requested == true {
                self?.removeRequest()
            } else {
                self?.requestTimeIfNeeded()
            }
        }
        
        topBar.downTap = { [weak self] in
            self?.collapseMessageView()
        }
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(dragTop(sender:)))
        topBar.addGestureRecognizer(pan)
        
        messagingView.insets = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        messagingView.send = { [weak self] (text) in
            self?.sendMessage(text)
        }
        messagingView.edit = { [weak self] (text) in
            self?.expandMessageView()
        }
        messagingView.timeFormat = "Confirm to deliver"
        messagingView.isEditable = true
        
        emptyView.isExpanded = true
        emptyView.skipTap = { [weak self] in
            self?.collapseMessageView()
        }
        emptyView.editTap = { [weak self] in
            self?.expandMessageView()
        }
        messagingView.emptyView = emptyView
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: SignalTrackingService.Notifications.SignalExpired, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: SignalTrackingService.Notifications.SignalResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: SignalTrackingService.Notifications.SignalMessage, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardUp(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDown(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if let signal = model,
            let request = signal.requests.first {
            SignalTrackingService.shared.setRequesterIsTyping(signalId: signal.id, requestId: request.id, isTyping: false)
        }
        stopTrackingTyping()
        
        NotificationCenter.default.post(
            name: MoreTabBarController.Notifications.MuteSignalAlerts,
            object: nil,
            userInfo: ["signalId": model?.id ?? "", "mute": true ])
    }
    
    private var isFirst: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard isFirst else { return }
        if model?.requested == true {
            topBar.isHidden = false
            messagingView.isHidden = false
            bottomPadding.constant = -74
            messagingView.emptyView?.removeFromSuperview()
            messagingView.emptyView = nil
            topBar.safeAreaHeight = 0
            topBar.downHidden = true
            topBar.counterOnTop = true
            requestBar.countdownHidden = true
            messagingView.insets = .zero
        }
        bottomContraint = bottomPadding
        trackKeyboard(onlyFor: [messagingView.input.textView])
        trackKeyboardAndPushUp()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard isFirst else { return }
        isFirst = false
        
        if messagingViewHeight.constant > 73 {
            if let signal = model,
                let request = signal.requests.first {
                
                let unreadList = request.messages.filter({ !$0.isMine() && $0.read == nil })
                guard unreadList.count > 0 else { return }
                
                DispatchQueue.global(qos: .default).async {
                    for message in unreadList {
                        SignalTrackingService.shared.setMesageRead(signalId: signal.id, requestId: request.id, messageId: message.id)
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard isFirst else { return }
        
        if model?.requested == true {
            messagingView.layoutIfNeeded()
            var messageViewHeight = messagingView.height - messagingView.insets.top
            messageViewHeight = min(collapsedMessagingViewMaxHeight, messageViewHeight)
            messagingViewHeight.constant = messageViewHeight
            view.layoutIfNeeded()
        }
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        if topBar.counterOnTop == false {
            topBar.safeAreaHeight = view.safeAreaInsets.top
        }
    }
    
    private var collapsedMessagingViewMaxHeight: CGFloat {
        let viewHeight = view.bounds.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom
        let maxHeight = viewHeight - requestBar.frame.height + messagingView.insets.top - topBar.frame.height - 120
        return maxHeight
    }
    
    private var expandedMessagingViewMaxHeight: CGFloat {
        return view.bounds.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom - topBar.frame.height + 37
    }

    private func presentMessagingView() {
        
        guard !presenting else { return }
        presenting = true
        
        messagingView.reload()
        let height = expandedMessagingViewMaxHeight
        messagingViewHeight.constant = height
        
        let start = view.frame.height - requestBar.frame.height
        
        topBar.layer.transform = CATransform3DMakeTranslation(0, start, 0)
        messagingView.layer.transform = CATransform3DMakeTranslation(0, start, 0)
        
        topBar.isHidden = false
        messagingView.isHidden = false
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.topBar.layer.transform = CATransform3DIdentity
                self.messagingView.layer.transform = CATransform3DIdentity
                self.requestBar.alpha = 0
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            },
            completion: { (_) in
                self.messagingView.showKeyboard()
                self.messagingView.scrollToBottom()
            })
    }
    
    private func collapseMessageView(fully: Bool = false, duration: Double = 0.3, curve: UIView.AnimationOptions = .curveEaseInOut, force: Bool = false) {
        
        NotificationCenter.default.post(
            name: MoreTabBarController.Notifications.MuteSignalAlerts,
            object: nil,
            userInfo: ["signalId": model?.id ?? "", "mute": true ])

        messagingView.tableView.showTyping = false
        guard messagingShown else { return }
        
        if model?.requested == false {
            messagingView.resetInput()
            if let message = message {
                messagingView.setup(for: [MessageViewModel(message: message)])
            }
        }
        
        var messageViewHeight = messagingView.height - messagingView.insets.top
        if model?.requested == false {
            messageViewHeight = max(140, messageViewHeight)
        }
        let height = fully ? 73 : min(collapsedMessagingViewMaxHeight, messageViewHeight)
    
        guard self.messagingViewHeight.constant > height || force else { return }
        
        messagingView.hideKeyboard()
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: [.beginFromCurrentState, curve],
            animations: {
                self.messagingViewHeight.constant = height
                self.topBar.safeAreaHeight = 0
                self.topBar.downHidden = true
                self.topBar.counterOnTop = true
                self.requestBar.alpha = 1
                self.requestBar.countdownHidden = true
                self.emptyView.isExpanded = false
                self.messagingView.insets = .zero
                self.view.backgroundColor = fully ? .clear :  UIColor.black.withAlphaComponent(0.3)
                
                self.view.layoutIfNeeded()
            },
            completion: { (_) in
                self.messagingView.scrollToBottom()
            })
    }
    
    private func expandMessageView(duration: Double = 0.3, curve: UIView.AnimationOptions = .curveEaseInOut, force: Bool = false) {
        
        NotificationCenter.default.post(
            name: MoreTabBarController.Notifications.MuteSignalAlerts,
            object: nil,
            userInfo: ["signalId": model?.id ?? "", "mute": false ])
        
        let height = expandedMessagingViewMaxHeight
        
        guard self.messagingViewHeight.constant < height || force else { return }
        
        messagingView.tableView.showTyping = otherIsTyping
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: [.beginFromCurrentState, curve],
            animations: {
                self.messagingViewHeight.constant = height
                self.messagingView.insets = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
                self.topBar.safeAreaHeight = self.view.safeAreaInsets.top
                self.topBar.downHidden = false
                self.topBar.counterOnTop = false
                self.requestBar.alpha = 0
                self.emptyView.isExpanded = true
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                
                if self.model?.requested == false {
                    self.messagingView.setup(for: [])
                }
                
                self.view.layoutIfNeeded()
            },
            completion: { (_) in
                if self.model?.requested == false {
                    if let message = self.message {
                        self.messagingView.setInput(message.text)
                    }
                }
                self.messagingView.showKeyboard()
            })
        
        if let signal = model,
            let request = signal.requests.first {
            
            let unreadList = request.messages.filter({ !$0.isMine() && $0.read == nil })
            guard unreadList.count > 0 else { return }
            
            DispatchQueue.global(qos: .default).async {
                for message in unreadList {
                    SignalTrackingService.shared.setMesageRead(signalId: signal.id, requestId: request.id, messageId: message.id)
                }
            }
        }
    }
    
    @objc private func keyboardUp(_ notification: Notification) {
        
        if let userInfo = notification.userInfo,
            let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber,
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
            
            expandMessageView(duration: animationDuration.doubleValue, curve: UIView.AnimationOptions(rawValue: animationCurve.uintValue))
            return
        }
        
        expandMessageView()
    }
    
    @objc private func keyboardDown(_ notification: Notification) {
        
        if let userInfo = notification.userInfo,
            let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber,
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
            
            collapseMessageView(duration: animationDuration.doubleValue, curve: UIView.AnimationOptions(rawValue: animationCurve.uintValue))
            return
        }
        
        collapseMessageView()
    }
    
    func setup(for model: SignalViewModel) {
        
        self.model = model
        
        if model.requested {
            messagingShown = true
            messagingView.placeholder = "Your message"
            messagingView.timeFormat = "h:mm a"
            messagingView.isEditable = false
        } else {
            messagingView.placeholder = "Say why this sounds fun"
        }
        
        topBar.setup(for: model)
        if let user = ProfileService.shared.profile?.user {
            messagingView.setup(for: UserViewModel(user: user))
        }
        messagingView.setup(for: model.messages)
        requestBar.setup(for: model, confirm: messagingShown)
        emptyView.setup(for: model.creator)
        
        stopTrackingTyping()
        trackTyping()
    }
    
    func fullyCollapse() {
        collapseMessageView(fully: true)
    }
    
    // MARK: - drag
    
    private var dragStart: CGPoint = .zero
    private var dragStartHeight: CGFloat = 0
    
    @objc private func dragTop(sender: UIPanGestureRecognizer) {
        
        guard messagingView.layer.animationKeys() == nil else { return }
        
        let point = sender.location(in: view)
        let offset = dragStart.y - point.y
        switch sender.state {
        case .began:
            dragStart = point
            dragStartHeight = messagingViewHeight.constant
        case .changed:
            moveTop(to: offset + dragStartHeight)
        case .cancelled, .ended, .failed:
            settleTop(to: offset + dragStartHeight)
        default: ()
        }
    }
    
    private func moveTop(to height: CGFloat) {
        var keyboardHeight = abs(bottomPadding.constant)
        keyboardHeight = keyboardHeight > 100 ? keyboardHeight : 0
        self.messagingViewHeight.constant = height - keyboardHeight
        self.view.layoutIfNeeded()
    }
    
    private func settleTop(to height: CGFloat) {
        var keyboardHeight = abs(bottomPadding.constant)
        keyboardHeight = keyboardHeight > 100 ? keyboardHeight : 0
        let correctedHeight = height - keyboardHeight
        
        let maxHeight = expandedMessagingViewMaxHeight
        var messageViewHeight = messagingView.height - messagingView.insets.top
        if model?.requested == false {
            messageViewHeight = max(140, messageViewHeight)
        }
        let midHeight = min(collapsedMessagingViewMaxHeight, messageViewHeight)
        let minHeight = CGFloat(73)
        
        let minDist = abs(minHeight - correctedHeight)
        let midDist = abs(midHeight - correctedHeight)
        let maxDist = abs(maxHeight - height)
        
        if minDist < midDist && model?.requested == true {
            collapseMessageView(fully: true, force: true)
        } else if midDist < maxDist {
            collapseMessageView(fully: false, force: true)
        } else {
            expandMessageView(force: true)
        }
    }
    
    // MARK: - interaction

    @objc private func updateData(_ notice: Notification) {
        DispatchQueue.main.async { [weak self] in
            guard let signal = notice.userInfo?["signal"] as? Signal, signal.id == self?.model?.id else { return }
            
            let model = SignalViewModel(signal: signal)
            self?.model = model
            self?.setup(for: model)
            
            guard let request = notice.userInfo?["request"] as? Request else { return }
            guard let msg = notice.userInfo?["message"] as? Message, !msg.isMine() else { return }
            
            if let height = self?.messagingViewHeight.constant, height > 73 {
                if msg.readAt == nil {
                    DispatchQueue.global(qos: .default).async {
                        SignalTrackingService.shared.setMesageRead(signalId: signal.id, requestId: request.id, messageId: msg.id)
                    }
                }
            }
        }
    }
    
    private func requestTimeIfNeeded() {
        guard let model = model else { return }
        guard ProfileService.shared.profile != nil else { return }
        
        if !messagingShown {
            if !PushNotificationService.shared.permissionsRequested {
                PushNotificationService.shared.requestPersmissions { [weak self] in
                    self?.presentMessagingView()
                    self?.messagingShown = true
                    self?.requestBar.setup(for: model, confirm: true)
                }
            } else {
                presentMessagingView()
                messagingShown = true
                requestBar.setup(for: model, confirm: true)
            }
        } else if model.requested {
            expandMessageView()
        } else {
            
            let signals = SignalTrackingService.shared.getOwnActiveSignals().filter { $0.kind == .standard }
            guard signals.count < 3 else {
                cannotRequest()
                return
            }
            
            showLoading()
            
            if LocationService.shared.currentLocation != nil {
                internalRequestTime()
            } else {
                NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: LocationService.Notifications.LocationUpdate, object: nil)
            }
        }
    }
    
    @objc private func locationUpdated() {
        NotificationCenter.default.removeObserver(self, name: LocationService.Notifications.LocationUpdate, object: nil)
        internalRequestTime()
    }
    
    private func internalRequestTime() {
        guard let model = model else { return }
        
        SignalingService.shared.requestTime(for: model.id) { [weak self] (requestId, erroMsg) in
            
            self?.hideLoading()
            
            if let requestId = requestId {
                
                if let msg = self?.message {
                    self?.sendMessageToServer(msg, requestId: requestId)
                }
                
                self?.requested?()
                
            } else {
                self?.errorAlert(text: erroMsg ?? "Unknown error")
            }
        }
    }
    
    private func sendMessage(_ text: String) {
        
        guard let profile = ProfileService.shared.profile else { return }
        guard let model = model else { return }
        
        let messageId = "\(profile.id.hashValue)-\(Date().hashValue)"
        let message = Message(id: messageId, createdAt: Date(), sender: profile.user, text: text, deliveredAt: nil, readAt: nil)
        let msgModel = MessageViewModel(message: message)
        
        if model.requested == true {
            messagingView.newMessage(msgModel)
            sendMessageToServer(message)
        } else {
            if self.message != nil {
                model.messages = [msgModel]
                messagingView.setup(for: model.messages)
            } else {
                messagingView.newMessage(MessageViewModel(message: message))
            }
            self.message = message
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                self?.collapseMessageView()
            }
        }
    }
    
    private func sendMessageToServer(_ message: Message, requestId: String? = nil) {
        
        guard ProfileService.shared.profile?.getId() != nil,
            let signalId = model?.id
            else { return }
        
        guard let requestId = requestId ?? model?.requests.first?.id else { return }
        
        SignalingService.shared.sendMessage(message, for: requestId, in: signalId) { (success, errorMsg) in
            if !success {
                // mark message as not sent
            }
        }
    }
    
    private func removeRequest() {
        guard ProfileService.shared.profile?.getId() != nil,
            let signalId = model?.id
            else { return }
        
        guard let requestId = model?.requests.first?.id else { return }
        
        showLoading()
        SignalingService.shared.cancelRequest(requestId, in: signalId) { [weak self] (success, errorMsg) in
            self?.hideLoading()
            if success {
                self?.removed?()
            } else {
                self?.errorAlert(text: errorMsg ?? "Unknown error")
            }
        }
    }
    
    // MARK: - typing
    
    private var otherIsTyping = false
    
    private func trackTyping() {
        
        // me typing
        messagingView.input.isTypingChanged = { [weak self] in
            guard let isTyping = self?.messagingView.input.isTyping else { return }
            guard let signal = self?.model else { return }
            guard let request = signal.requests.first else { return }
            
            SignalTrackingService.shared.setRequesterIsTyping(signalId: signal.id, requestId: request.id, isTyping: isTyping)
        }
        
        // other side typing
        guard let signal = model else { return }
        guard let request = signal.requests.first else { return }

        SignalTrackingService.shared.getCreatorIsTypingReference(signalId: signal.id, requestId: request.id)
            .observe(.value) { [weak self] (snapshot) in
                if snapshot.exists(), let isTyping = snapshot.value as? Bool {
                    if let height = self?.messagingViewHeight.constant,
                        let max = self?.expandedMessagingViewMaxHeight,
                        height >= max {
                        self?.messagingView.tableView.showTyping = isTyping
                    }
                    self?.otherIsTyping = isTyping
                }
            }
    }
    
    private func stopTrackingTyping() {
        messagingView.input.isTypingChanged = nil
        if let signal = model,
            let request = signal.requests.first {
            SignalTrackingService.shared.removeTypingObservers(signalId: signal.id, requestId: request.id)
        }
    }
}

@IBDesignable
class MessagingOverlayViewControllerView : UIView {
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hit = super.hitTest(point, with: event)
        if hit == self {
            return nil
        }
        return hit
    }
    
}
